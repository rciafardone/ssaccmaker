/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package luz.ve.ssacc.engine.parsers;

import luz.ve.ssacc.engine.components.Operand;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import luz.ve.ssacc.engine.components.State;
import luz.ve.ssacc.engine.functions.*;
import luz.ve.ssacc.utilities.SacUtility;
/**
 *
 * @author "Renzo Ciafardone Sciannelli"
 */
public class OperandParser
{
    //**************************************************************************
    //*****************************VARIABLES************************************
    //**************************************************************************
    
    //Example: (Count:001:2) is OK, (count:1:9) is OK, (count::) is NOT OK, (count:1:12) is NOT OK, (count:0001:2) is NOT OK
    //@?OP1(FUN(Count):(SC(0001)=(txt(alive))(val(0.0))(col(255:255:255))):FIELD(0))OPX(<)OP2(FUN(Constant):(SC(0002)=(txt(DEFAULT))(val(2.0))(col(255:255:255))):FIELD(2));
    public static final String S_REGEX_FUNCTION = "FUN\\((.*)\\)"; //group 1 = function name.
    public static final String S_REGEX_FIELD = "FIELD\\((\\d)\\)"; //group 1 = field number.
    public static final String S_REGEX_OPERAND = S_REGEX_FUNCTION + ":(.*):" + S_REGEX_FIELD; //group 1 = function name, group 2 = state, group 3 field.
    public static final Pattern P_REGEX_FUNCTION = Pattern.compile(OperandParser.S_REGEX_FUNCTION, Pattern.CASE_INSENSITIVE);
    public static final Pattern P_REGEX_OPERAND = Pattern.compile(OperandParser.S_REGEX_OPERAND, Pattern.CASE_INSENSITIVE);

    //**************************************************************************
    //*****************************CONSTRUCTORS*********************************
    //**************************************************************************
    //**************************************************************************
    //*****************************METHODS**************************************
    //**************************************************************************
    /**
     * 
     * @param oLine
     * @return 
     */
    public static Operand parse(String oLine)
    {
        Matcher pMatcher = OperandParser.P_REGEX_OPERAND.matcher(oLine);

        if (pMatcher.find())
        {
            Function aFunction = FunctionParser.parse(pMatcher.group(1));

            State aState = StateParser.parse(pMatcher.group(2));

            int aField = Integer.parseInt(pMatcher.group(3));

            Operand nOperand = new Operand(aFunction, aState, aField);

            return nOperand;
        }
        else
        {
            SacUtility.showParsingError("OperandParser - Configuración operando: Formato incorrecto.");
            return null;
        }
    }
    
    //@?OP1(FUN(Count):(SC(0001)=(txt(alive))(val(0.0))(col(255:255:255))):FIELD(0))
   
    public static String convertToString(Operand aOperand)
    {
        StringBuilder sOperand = new StringBuilder();

        sOperand.append("OPN(");

        sOperand.append("FUN(").append(aOperand.getFunction().toString()).append("):");

        sOperand.append(aOperand.getVariable()).append(":FIELD(");

        sOperand.append(aOperand.getField());

        sOperand.append("))");

        return sOperand.toString();
    }
    //**************************************************************************
    //****************************END OF CLASS**********************************
    //**************************************************************************
}
