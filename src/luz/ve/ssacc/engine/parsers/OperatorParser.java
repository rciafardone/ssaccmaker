/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package luz.ve.ssacc.engine.parsers;

import java.util.regex.Matcher;
import java.util.regex.Pattern;
import luz.ve.ssacc.engine.components.Operator;
import luz.ve.ssacc.utilities.SacUtility;

/**
 *
 * @author Renzo Ciafardone Sciannelli
 */
public class OperatorParser
{
// FULL EXAMPLE:     @?OP1(FUN(Count):(SC(0001)=(txt(alive))(val(0.0))(col(255:255:255))):FIELD(0))OPX(>)OP2(FUN(Constant):(SC(0003)=(txt(DEFAULT))(val(3.0))(col(255:255:255))):FIELD(2));
// OPERAND EXAMPLE: OP1(FUN(Count):(SC(0001)=(txt(alive))(val(0.0))(col(255:255:255))):FIELD(0))    
    public static final String S_REGEX_OPERATOR = OperatorParser.generateOperatorRegex();
    
    public static final Pattern P_REGEX_OPERATOR = Pattern.compile(OperatorParser.S_REGEX_OPERATOR, Pattern.CASE_INSENSITIVE);

    public static Operator parse(String oLine)
    {
        Matcher pMatcher = OperatorParser.P_REGEX_OPERATOR.matcher(oLine);

        if (pMatcher.find())
        {
            return new Operator(pMatcher.group(0));
        }
        else
        {
            SacUtility.showParsingError("Configuración operador: Formato incorrecto.");
            return null;
        }
    }

public static String getFormatedOperator(Operator cOperator)
    {
        StringBuilder sOperator = new StringBuilder();
        sOperator.append("OPX(");
        sOperator.append(cOperator.getSymbol());
        sOperator.append(")");
        return sOperator.toString();
    }

    private static String generateOperatorRegex()
    {
        StringBuilder regex = new StringBuilder();
        regex.append("(");
        for (int index = 0; index < Operator.OPERATORS_LIST.length; index++)
        {
            regex.append(Operator.OPERATORS_LIST[index]);
            if (index < Operator.OPERATORS_LIST.length - 1)
            {
                regex.append("|");
            }
        }
        regex.append(")");
        return regex.toString();
    }

}
