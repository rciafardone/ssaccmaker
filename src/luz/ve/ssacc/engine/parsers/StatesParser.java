/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package luz.ve.ssacc.engine.parsers;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import luz.ve.ssacc.engine.components.State;
import luz.ve.ssacc.engine.components.States;
import luz.ve.ssacc.utilities.SacUtility;



/**
 *
 * @author "Renzo Ciafardone Sciannelli"
 */
public final class StatesParser {

    //**************************************************************************
    //*****************************VARIABLES************************************
    //**************************************************************************
   
   
    public static final String DEFAULT_NAME = "";
    public static final String DEFAULT_DESCRIPTION = "";
    public static final int FIRST_STATE = 0;

     // Expresiones regulares
    private static final String S_REGEX_HEADER = "@(StatesBlockIni|SBI)=\\(([^@]+)\\);"; // marca el inicio del conjunto de estados y contiene la propiedad name_, no puede estar vacia.
    private static final String S_REGEX_DESC = "@(StatesDescription|SDC)=\\(([^@]*)\\);"; // la descripcion, puede estar vacia.
 
    
    private static final String S_REGEX_FOOTER = "@(StatesBlockEnd|SBE);"; // cierra el bloque del conjunto de estados
    private static final String S_REGEX_FULL_SACS = S_REGEX_HEADER + "([\\s\\S])*" + S_REGEX_FOOTER; // Toda la informacion del conjunto de estados

    private static final Pattern P_REGEX_HEADER = Pattern.compile(StatesParser.S_REGEX_HEADER, Pattern.CASE_INSENSITIVE);
    private static final Pattern P_REGEX_DESC = Pattern.compile(StatesParser.S_REGEX_DESC, Pattern.CASE_INSENSITIVE);
    private static final Pattern P_REGEX_FOOTER = Pattern.compile(StatesParser.S_REGEX_FOOTER, Pattern.CASE_INSENSITIVE);
    private static final Pattern P_REGEX_FULL_SACS = Pattern.compile(StatesParser.S_REGEX_FULL_SACS, Pattern.CASE_INSENSITIVE);

    
    /*
     * @StateConfigurationIni(GameOfLifeStates);
     * @StateConfigurationDesc(Estos son los 2 estados que se definen en el juego de la vida);
     * @SC(0000)=(texto(muerto))(val(0));
     * @SC(0001)=(texto(vivo))(val(0));
     * @StateConfigurationEnd;
     */

    //**************************************************************************
    //*****************************CONSTRUCTORS*********************************
    //**************************************************************************

   
   

    //**************************************************************************
    //*****************************METHODS**************************************
    //**************************************************************************

    
    

    /*
     * Formats the States Set to be stored on the SACS file
     */
   
    public static String ConvertToString(States oStates)
    {
        StringBuilder sStates = new StringBuilder();

        sStates.append(getFormatedHeader(oStates.getName()));
        sStates.append(SacUtility.EOL);
        sStates.append(SacUtility.EOL);
        sStates.append(getFormatedDesc(oStates.getDescription()));
        sStates.append(SacUtility.EOL);
        sStates.append(SacUtility.EOL);
        sStates.append(getFormatedList(oStates.getList()));
        sStates.append(SacUtility.EOL);
        sStates.append(getFormatedFooter());
        sStates.append(SacUtility.EOL);
        
        return sStates.toString();
    }


    /*
     * @StateConfigurationIni(0001);
     */
    private static String getFormatedHeader(String name)
    {
        StringBuilder header = new StringBuilder();

        header.append("@StatesBlockIni=(");
        header.append(name);
        header.append(");");
            
        return header.toString();
    }

    
     /*
     * @StatesConfigurationDesc = (Aqui va alguna descripcion de los estados);
     */
    private static String getFormatedDesc(String descripcion)
    {
        StringBuilder desc = new StringBuilder();

        desc.append("@StatesDescription=(" );
        desc.append(descripcion);
        desc.append(");");

        return desc.toString();
    }

    /*
     * Returns a String with all the States with the SACS format
     */
    private static String getFormatedList(List<State> list)
    {
        StringBuilder aList = new StringBuilder();
        for (State sState : list)
        {
            aList.append("@");
            aList.append(sState.toString());
            aList.append(";").append(SacUtility.EOL);
        }
        return aList.toString();
    }


    /*
     * @StateConfigurationEnd;
     */
    private static String getFormatedFooter()
    {
        String footer = "@StatesBlockEnd;";
        return footer;
    }


    /*
     * If the String passed as parameter has the correct format the name property is setted and
     * true returned, else nothing is done and false is returned.
     * Ex: @StatesConfigurationIni(Game of Life);
     */
    public static String parseHeader(String oLine)
    {
        Matcher pMatcher = StatesParser.P_REGEX_HEADER.matcher(oLine);

        if (pMatcher.find())
        {
            return(pMatcher.group(2));
        } 

        SacUtility.showParsingError("Bloque de estados - Cabecera: Formato incorrecto.");
        return null;

    }

  
    

    /*
     * If the String passed as parameter has the correct format the description_ property is setted and
     * true returned else nothing is done and false is returned.
     */
    public static String parseDescription(String oLine)
    {
        Matcher pMatcher = StatesParser.P_REGEX_DESC.matcher(oLine);

        if (pMatcher.find())
        {
            return (pMatcher.group(2));
        }

        SacUtility.showParsingError("Bloque de estados - Descripción: Formato incorrecto.");
        return null;
    }
   

    /*
     * Reads the string and extracts all the states to add them to the statesList_
     */
    public static List<State> parseStatesList(String oLine)
    {
        ArrayList<State> stateList = new ArrayList<>();
        
        Matcher pMatcher = StateParser.P_REGEX_STATE.matcher(oLine);
        
        while (pMatcher.find())
        {
            State nState = StateParser.parse(pMatcher.group(0));
            
            if (nState!=null)
            {
                stateList.add(nState);
            }
        }

        if (stateList.isEmpty()) // If no states are added this is not a valid string
        {
            SacUtility.showParsingError("Bloque de estados - Lista de Estados: Formato incorrecto.");
            return null;
        }

        return stateList;
    }


    /*
     * If the String passed as parameter has the correct format true is returned to indicate
     * that the StateSet configuration is over
     * true returned, else nothing is done and false is returned.
     */
    public static boolean parseFooter(String oLine)
    {
        Matcher pMatcher = StatesParser.P_REGEX_FOOTER.matcher(oLine);

        if (pMatcher.find())
        {
            return true;
        }

        SacUtility.showParsingError("Bloque de estados - Final bloque de estados: Formato incorrecto.");
        return false;
    }

    
    /**
     * Toma el contenido del string y lo convierte en un Conjunto de estados que
     * se devuelve para ser cargado en el automata
     * El String puede ser un archivo ADAC completo o solo un SACS.
     * Si el String de entrada no contiene un conjunto de estados completo se 
     * retorna NULL, cualquier linea que no tenga el formato de un comando de
     * vecindario sera ignorada
     * @param fileContent
     * @return 
     */
    public static States parse(String fileContent)
    {
        Matcher pMatcher = StatesParser.P_REGEX_FULL_SACS.matcher(fileContent);
        States nStates = new States();
        boolean validFooter;
        
        if (pMatcher.find()) // Primero nos aseguramos que tenga pies y cabeza.
        {
            String sbSac = pMatcher.group(0); // Filtra el string extrayendo solo lo que pertenezca al bloque del conjunto de estados
                        
            nStates.setName(parseHeader(sbSac)); //Asignamos cada elemento del conjunto de estados
            nStates.setDescription(parseDescription(sbSac));
            nStates.setList(parseStatesList(sbSac));
            validFooter = parseFooter(sbSac);
        }
        else
        {
            SacUtility.showParsingError("El archivo de estados no tiene un formato valido." + SacUtility.EOL);
            return null;
        }

        //Si algun elemento es invalido ya se genero el mensaje de error y se devuelve null
        if (nStates.getName() == null
                || nStates.getDescription() == null
                || nStates.getList() == null
                || !validFooter)
        {
            return null;
        }


        return nStates; //devolvemos el conjunto de estados para ser asignado
    }
    /**
     * *****************************END OF CLASS***********************************/
/*******************************END OF CLASS***********************************/
/*******************************END OF CLASS**************************R.C.S.***/
}
