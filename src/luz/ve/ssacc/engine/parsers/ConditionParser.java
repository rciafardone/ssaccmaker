/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package luz.ve.ssacc.engine.parsers;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import luz.ve.ssacc.engine.components.Condition;
import luz.ve.ssacc.engine.components.Operand;
import luz.ve.ssacc.engine.components.Operator;
import luz.ve.ssacc.utilities.SacUtility;


/**
 *
 * @author "Renzo Ciafardone Sciannelli"
 */
public final class ConditionParser
{
    //**************************************************************************
    //*****************************VARIABLES************************************
    //**************************************************************************
    // public static final String S_REGEX_CONDITION = "@\\?" + "([^;]*)" + ";";
    // public static final String S_REGEX_CONDITION = "@\\?" + "((([^:]*):)(([^:]*):)(([^:]*)))"+ OperatorParser.sRegexOperator + "((([^:]*):)(([^:]*):)(([^:]*)))" + ";";

    //"@?((Count:(SC(0001)=(txt(DEFAULT))(val(0.0))):0)<(Constant:(SC(0002)=(txt(DEFAULT))(val(0.0))):0));"
    
    // @?(
    
    // EXAMPLE: @?OP1(FUN(Count):(SC(0001)=(txt(alive))(val(0.0))(col(255:255:255))):FIELD(0))OPX(>)OP2(FUN(Constant):(SC(0003)=(txt(DEFAULT))(val(3.0))(col(255:255:255))):FIELD(2));
       
    public static final String S_REGEX_CONDITION = "@\\?(OPN\\((.*)\\))(OPX\\((.*)\\))(OPN\\((.*)\\));";
    public static final Pattern P_REGEX_CONDITION = Pattern.compile(ConditionParser.S_REGEX_CONDITION, Pattern.CASE_INSENSITIVE);
    
    public static final int NUM_FUNCTION_A = 0;
    public static final int NUM_FUNCTION_B = 1;


    
    //**************************************************************************
    //*****************************CONSTRUCTORS*********************************
    //**************************************************************************
   

    //**************************************************************************
    //*****************************METHODS**************************************
    //**************************************************************************
    

    public static Condition parse(String oLine)
    {
        Matcher pMatcher = ConditionParser.P_REGEX_CONDITION.matcher(oLine);
        Operand operandA;
        Operator operatorX;
        Operand operandB;

        if (pMatcher.find())
        {
            operandA = OperandParser.parse(pMatcher.group(2));
            operatorX = OperatorParser.parse(pMatcher.group(4));
            operandB = OperandParser.parse(pMatcher.group(6));

            if (operandA != null && operatorX != null && operandB != null)
            {
                return new Condition(operandA, operatorX, operandB);
            }
        }

        SacUtility.showParsingError("Bloque de Reglas - ConditionParser - Condición Invalida.");
        return null;
    }

    
    // EXAMPLE: @?OP1(FUN(Count):(SC(0001)=(txt(alive))(val(0.0))(col(255:255:255))):FIELD(0))OPX(>)OP2(FUN(Constant):(SC(0003)=(txt(DEFAULT))(val(3.0))(col(255:255:255))):FIELD(2));
    
    /**
     * Returns the condition formated as it will appear on the SACR File.
     * @param oCondition
     * @return 
     */
    public static String convertToString(Condition oCondition)
    {
        StringBuilder sCondition = new StringBuilder();
        
        sCondition.append("@?");
        sCondition.append(oCondition.getOperandA().toString());
        sCondition.append(oCondition.getOperator());
        sCondition.append(oCondition.getOperandB().toString());
        sCondition.append(";");

        return sCondition.toString();
    }


    //**************************************************************************
    //****************************END OF CLASS**********************************
    //**************************************************************************
}
