/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package luz.ve.ssacc.engine.parsers;

import java.awt.Dimension;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import luz.ve.ssacc.engine.components.Cell;
import luz.ve.ssacc.engine.components.Grid;
import luz.ve.ssacc.engine.components.State;
import luz.ve.ssacc.utilities.SacUtility;

/**
 *
 * @author Renzo Ciafardone Sciannelli <rciafardone@gmail.com>
 */
public class GridParser
{
    

    //**************************************************************************
    //*****************************VARIABLES************************************
    //**************************************************************************
  
    private static final String N_LINE = System.lineSeparator(); 
    
    private static final String S_REGEX_HEADER = "@(GridOriState|GOS)"; // marca el inicio del conjunto de grid.
    private static final String S_REGEX_DIMENSIONS = "(\\(([-|+]?[0-9]+)(:)([-|+]?[0-9]+)*\\))";
    private static final String S_REGEX_HEADER_FULL = S_REGEX_HEADER + S_REGEX_DIMENSIONS + "=";
    private static final String S_REGEX_CONTENT = "(" + CellParser.S_REGEX_CELL + ")+;";
    private static final String S_REGEX_FOOTER = "@(GridOriState|GIS);"; // marca el final del conjunto de grid.
    private static final String S_REGEX_GRID_FULL = S_REGEX_HEADER_FULL + "([^@]+);";
   
    private static final Pattern P_REGEX_HEADER_FULL = Pattern.compile(GridParser.S_REGEX_HEADER_FULL, Pattern.CASE_INSENSITIVE);
    private static final Pattern P_REGEX_GRID_FULL = Pattern.compile(GridParser.S_REGEX_GRID_FULL, Pattern.CASE_INSENSITIVE);
    private static final String GRID_GOS = "@GridOriState";
    
    /*
     * @GridOriState(40:60)=
     * (0:0:1)(0:1:0)(1:0:0)(1:1:0)(0:2:0)
     * ;
     * @GOS;
     */
    
    
    //**************************************************************************
    //*******************************METHODS************************************
    //**************************************************************************
    public static Grid parse(String sGrid)
    {
        Grid nGrid;
        Dimension dimension;
        int rows;
        int cols;
        List<Cell> cells;

        dimension = GridParser.parseHeader(sGrid);
        rows = dimension.height;
        cols = dimension.width;

        cells = GridParser.parseCells(sGrid);
     
        nGrid = GridParser.fillGrid(new Grid(rows, cols, new State()), cells);

        return nGrid;
    }

    /**
     * Obitiene las dimensiones del grid del encabezado.
     *
     * @param oLine
     * @return
     */
    private static Dimension parseHeader(String oLine)
    {
        int rows = 0;
        int cols = 0;
        Matcher pMatcher = GridParser.P_REGEX_HEADER_FULL.matcher(oLine);

        if (pMatcher.find())
        {
            rows = Integer.parseInt(pMatcher.group(3));
            cols = Integer.parseInt(pMatcher.group(5));
        }

        return new Dimension(cols, rows);
    }

    /**
     * Lee el String para producir la lista de celdas del patrón.
     *
     * @param oLine
     * @return
     */
    public static List<Cell> parseCells(String oLine)
    {
        String sCells = "";

        List<Cell> basicPattern = new ArrayList<>();

        Matcher pMatcher = GridParser.P_REGEX_GRID_FULL.matcher(oLine);

        if (pMatcher.find())
        {
            sCells = pMatcher.group(6);
        }

        pMatcher = CellParser.P_REGEX_CELL.matcher(sCells);

        while (pMatcher.find()) // Busca las coordenadas individuales dentro del patrón
        {
            Cell nCell = CellParser.parse(pMatcher.group(0));
            basicPattern.add(nCell);
        }
        
        return basicPattern;
    }

    /**
     * Copia los IDs de la lista de celdas en el grid.
     *
     * @param grid
     * @param cells
     * @return
     */
    private static Grid fillGrid(Grid grid, List<Cell> cells)
    {
        cells.forEach((Cell cell) ->
        {
            grid.getArray()[cell.getRow()][cell.getColumn()] = cell.getStateId();
        });
        return grid;
    }
 
    
    
    
    /**
     * 
     * Retorna un String con el formato de archivo para el grid.
     *
     * @param grid
     * @return
     */
    public static String convertToString(Grid grid)
    {
        StringBuilder formattedGrid = new StringBuilder();

        formattedGrid.append(GridParser.getFormattedHeader(grid));
        formattedGrid.append(GridParser.getFormattedGrid(grid));
        formattedGrid.append(GridParser.getFormattedFooter());

        return formattedGrid.toString();
    }

    /**
     * Retorna un String con el formato de archivo del encabezado del bloque
     * Grid. Ej.: @GridInitialState(40:60)=
     *
     * @param grid
     * @return
     */
    private static String getFormattedHeader(Grid grid)
    {
        StringBuilder formattedHeader = new StringBuilder();

        formattedHeader.append(GridParser.GRID_GOS).append("(");
        formattedHeader.append(grid.getRows());
        formattedHeader.append(":");
        formattedHeader.append(grid.getColumns());
        formattedHeader.append(")=");
        return formattedHeader.toString();
    }

    /**
     * Retorna el grid con el formato de archivo.
     *
     * @param grid
     * @return
     */
    public static String getFormattedGrid(Grid grid)
    {
        final String EOL = System.lineSeparator();
        int gridRows = grid.getRows();
        int gridCols = grid.getColumns();
        StringBuilder fLine = new StringBuilder();

        for (Short r = 0; r < gridRows; r++)
        {
            fLine.append(EOL);
            for (Short c = 0; c < gridCols; c++)
            {
                fLine.append("(")
                    .append(r.toString()).append(":")
                    .append(c.toString()).append(":")
                    .append(grid.getArray()[r][c])
                    .append(")");
            }
        }

        fLine.append(";");
        return fLine.toString();
    }

    /**
     * Retorna un String con el formato de archivo del pie del bloque Grid. Ej.:
     *
     * @GridOriState;
     *
     * @param grid
     * @return
     */
    private static String getFormattedFooter()
    {
        StringBuilder formattedFooter = new StringBuilder();

        formattedFooter.append(N_LINE);
        formattedFooter.append(GridParser.GRID_GOS).append(";");

        return formattedFooter.toString();
    }

    public static String convertToFileFormat(Grid grid)
    {
        StringBuilder formattedGrid = new StringBuilder();
        StringBuilder cell = new StringBuilder();

        for (int r = 0; r < grid.getRows(); r++)
        {
            for (int c = 0; c < grid.getColumns(); c++)
            {
                cell.setLength(0);
                cell.append("(");
                cell.append(Integer.toString(r));
                cell.append(":");
                cell.append(Integer.toString(c));
                cell.append(":");
                cell.append(Short.toString(grid.getArray()[r][c]));
                cell.append(")");
                formattedGrid.append(cell.toString());
            }
            formattedGrid.append(SacUtility.EOL);
        }
        return formattedGrid.toString();
    }

    //**************************************************************************
    //**************************************************************************
    //**************************************************************************
}
