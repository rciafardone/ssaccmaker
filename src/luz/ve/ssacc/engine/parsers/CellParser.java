/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package luz.ve.ssacc.engine.parsers;

import java.util.regex.Matcher;
import java.util.regex.Pattern;
import luz.ve.ssacc.engine.components.Cell;

/**
 *
 * @author Renzo Ciafardone Sciannelli
 */
public class CellParser
{
   
    private static final String S_REGEX_COORDINATE = "(\\(([-|+]?[0-9]+)(:)([-|+]?[0-9]+)*\\))";
    public static final String S_REGEX_CELL = "\\((\\d{1,4})(:)(\\d{1,4})(:)(\\d{1,4})\\)";
    
    public static final Pattern P_REGEX_COORDINATE = Pattern.compile(CellParser.S_REGEX_COORDINATE, Pattern.CASE_INSENSITIVE);
    public static final Pattern P_REGEX_CELL = Pattern.compile(CellParser.S_REGEX_CELL, Pattern.CASE_INSENSITIVE);

    public static Cell parse(String oLine)
    {
        int nRow;
        int nCol;
        Short stateId;
        
        Matcher pMatcher;
        pMatcher = CellParser.P_REGEX_CELL.matcher(oLine);
       
        if (pMatcher.matches())
        {
            nRow = Integer.parseInt(pMatcher.group(1));
            nCol = Integer.parseInt(pMatcher.group(3));
            stateId = Short.parseShort(pMatcher.group(5));
            return (new Cell(nRow, nCol, stateId));
        }
        return null;
    }
   
    /**
     * Devuelve las coordenadas de la celda en un formato de archivo
     * @param cell
     * @return 
     */
    public static String getFormatedCoordinates(Cell cell)
    {
        StringBuilder sCell = new StringBuilder();
        sCell.append("(");
        sCell.append(Integer.toString(cell.getRow())).append(":");
        sCell.append(Integer.toString(cell.getColumn())).append(")");
        return sCell.toString();
    }

    /**
     * Convierte los datos recibidos en un string formateado para guardar en
     * archivo.
     *
     * @param row
     * @param col
     * @param stateId
     * @return
     */
    public static String convertToString(int row, int col, Short stateId)
    {
        StringBuilder sCell = new StringBuilder();

        sCell.append("(");
        sCell.append(Integer.toString(row)).append(":");
        sCell.append(Integer.toString(col)).append(":");
        sCell.append(stateId.toString());
        sCell.append(")");
     
        return sCell.toString();
    }
    
    /**
     * Convierte la celda en un string formateado para guardar en archivo.
     *
     * @param cell
     * @return
     */
    public static String convertToString(Cell cell)
    {
        StringBuilder sCell = new StringBuilder();

        sCell.append("(");
        sCell.append(Integer.toString(cell.getRow())).append(":");
        sCell.append(Integer.toString(cell.getColumn())).append(":");
        sCell.append(cell.getStateId().toString());
        sCell.append(")");
     
        return sCell.toString();
    }
    
    
    
//******************************************************************************
//******************************************************************************
//******************************************************************************
}
