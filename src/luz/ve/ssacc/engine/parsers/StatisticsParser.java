/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package luz.ve.ssacc.engine.parsers;

import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import luz.ve.ssacc.engine.components.Automaton;
import luz.ve.ssacc.engine.components.State;
import luz.ve.ssacc.engine.components.States;
import luz.ve.ssacc.engine.components.Statistics;
import luz.ve.ssacc.utilities.SacUtility;

/**
 *
 * @author Renzo Ciafardone Sciannelli <rciafardone@gmail.com>
 */
public class StatisticsParser
{
    
    //**************************************************************************
    //*****************************VARIABLES************************************
    //**************************************************************************
    /*
    step-states;s(0);s(1);s(2);s(3);
    p0;7630;2370;0;0;
    p1;7771;2229;0;0;
    p2;7836;2164;0;0;
    
    (step-states;)((s\\((\\d{1,4})\\);)+)
    "p(\\d{1,9});((\\d{1,4};)+)"
     */
    public static final String S_HEADER = "step-states;"; // encabezado titulos
    private static final String S_STATE_ID = "s\\((\\d{1,4})\\);"; // identificadores de estados
    private static final String S_STATES_ID = "(" + S_STATE_ID + ")+"; // identificadores de estados
    private static final String S_HEADER_FULL = "(" + S_HEADER + ")" + "(" + S_STATES_ID + ")";

    private static final String S_STEP_NUMBER = "p(\\d{1,9});"; // número del paso
    private static final String S_STAT = "(\\d{1,9});"; // cantidad por estado
    private static final String S_STATS = "(" + S_STAT + ")+"; // grupo de estadisticas de estado
    private static final String S_STATS_FULL = "(" + S_STEP_NUMBER + ")" + "(" + S_STATS + ")";

    private static final Pattern P_STAT = Pattern.compile(StatisticsParser.S_STAT, Pattern.CASE_INSENSITIVE);
    private static final Pattern P_STATS = Pattern.compile(StatisticsParser.S_STATS, Pattern.CASE_INSENSITIVE);
    private static final Pattern P_HEADER_FULL = Pattern.compile(StatisticsParser.S_HEADER_FULL, Pattern.CASE_INSENSITIVE);
    private static final Pattern P_STATE_ID = Pattern.compile(StatisticsParser.S_STATE_ID, Pattern.CASE_INSENSITIVE);
    private static final Pattern P_STATES_ID = Pattern.compile(StatisticsParser.S_STATES_ID, Pattern.CASE_INSENSITIVE);

    private static final Pattern P_STATS_FULL = Pattern.compile(StatisticsParser.S_STATS_FULL, Pattern.CASE_INSENSITIVE);


     //**************************************************************************
    //*****************************CONSTRUCTORS*********************************
    //**************************************************************************
   

    //**************************************************************************
    //*****************************METHODS**************************************
    //**************************************************************************
    
    /**
     * Convierte el archivo de estadisticas en un objeto para graficar.
     * @param fileContent
     * @param cA
     * @return
     */
    public static Statistics parse(String fileContent, Automaton cA)
    {
        List<State> states;
        List<Integer[]> data;

        states = parseHeader(fileContent, cA);
        data = parseSteps(fileContent, states);

        if (states != null && data != null)
        {
            return new Statistics(states, data);
        }
        else
        {
            return null;
        }
    }

    /**
     * Extrae los identificadores de los estados y los asocia con los estados
     * del automata activo.
     *
     * step|states;s(0);s(1);s(2);s(3);
     *
     * @param oLine
     */
    private static List<State> parseHeader(String oLine, Automaton cA)
    {
        List<State> states = new ArrayList<>();
        String stateId;
        StringBuilder sStates = new StringBuilder();
        States tempStates = cA.getStates();
        boolean statesEmpty = false;
        
        Matcher pMatcher = StatisticsParser.P_HEADER_FULL.matcher(oLine);
        
        if (tempStates.getList().size() == 1 && tempStates.getName().equals(States.DEFAULT_NAME))
        {
            statesEmpty = true;
        }

        if (pMatcher.find())
        {
            sStates.append(pMatcher.group(2)); // grupo de id de estados

            pMatcher = StatisticsParser.P_STATE_ID.matcher(sStates.toString()); // extraer cada id individual

            while (pMatcher.find())
            {
                stateId = pMatcher.group(1);
                
                if(statesEmpty)
                {
                    states.add(SacUtility.createDummyState(Short.parseShort(stateId)));
                }
                else
                {
                    states.add(tempStates.getState(Short.parseShort(stateId)));
                }
            }
        }
        else
        {
            SacUtility.showParsingError("Archivo estadisticas - Cabecera: Formato incorrecto.");
            return null;
        }

        return states;
    }
    
    
    /**
     * Extrae los datos de cada paso de las estadisticas.
     * p0;7630;2370;0;0;
     * @param oLine 
     */
    private static List<Integer[]> parseSteps(String oLine, List<State> states)
    {
        List<Integer[]> steps = new ArrayList<>();
        Matcher stepMatcher;
        Matcher statMatcher;
        int col;
     
        stepMatcher = StatisticsParser.P_STATS_FULL.matcher(oLine);
        while (stepMatcher.find())
        {
            Integer[] tempData = new Integer[states.size()]; // se crea el array para los datos del paso actual.
            col=0; // reseteamos variable a cero para empezar a guardar los datos.
            
            statMatcher = StatisticsParser.P_STAT.matcher(stepMatcher.group(3));
            while (statMatcher.find())
            {
                tempData[col] = Integer.parseInt(statMatcher.group(1));
                col++;
            }
        
            steps.add(tempData);
        }
        
        return steps;
    }
    
    
    /*
     * If the String passed as parameter has the correct format the name property is setted and
     * true returned, else nothing is done and false is returned.
     * Ex: @StatesConfigurationIni(Game of Life);
     */
    public static boolean parseHeader(String oLine)
    {
        Matcher pMatcher = StatisticsParser.P_HEADER_FULL.matcher(oLine);

        if (pMatcher.find())
        {
            return true;
        } 

        SacUtility.showParsingError("Archivo estadisticas - Cabecera: Formato incorrecto.");
        return false;

    }

    
    
    
    
    
}
