/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package luz.ve.ssacc.engine.parsers;

import java.awt.Color;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import luz.ve.ssacc.engine.components.Grid;
import luz.ve.ssacc.engine.components.Grids;
import luz.ve.ssacc.engine.components.State;
import luz.ve.ssacc.utilities.SacUtility;

/**
 *
 * @author Renzo Ciafardone Sciannelli <rciafardone@gmail.com>
 */
/*
 Aquí se establece la configuración de la retícula.
 @GridConfigurationIni=(Some Grid);
 @GridDescription=(Some Grid Description);
 @GridBackgroundState=(@SC(0000)=(txt(dead))(val(0.0)));
 @GridFrontierKind=(1);
 @GridArrayWidth=(100);
 @GridArrayHeight=(100);
 @GridCellSize=(5);
 @GridDrawBorder=(true);
 @GridOriState(100:100)=
 (0:0:0)(0:1:0)(0:2:0)(0:3:0)(1:0:0)(1:1:0)(1:2:0)(1:3:0)(2:0:0)(2:1:0)(2:2:0);
 @GridOriState;
 @GridConfigurationEnd;
 */
public class GridsParser
{

    private static final String S_REGEX_NAME = "@(GridConfigurationIni|GCI)=\\(([^@]+)\\);"; // marca el inicio del conjunto de grids y contiene la propiedad _name.
    private static final String S_REGEX_DESC = "@(GridDescription|GCD)=\\(([^@]*)\\);"; // la descripcion, puede estar vacia.
    private static final String S_REGEX_BACK = "@(GridBackgroundState|GBS)=\\(([^\\s]+)\\);"; // el estado de fondo
    private static final String S_REGEX_FRON = "@(GridFrontierKind|GFK)=\\(([^@]*)\\);"; // el tipo de frontera
    private static final String S_REGEX_STEPS = "@(GridNumberSteps|GNS)=\\((\\d+)\\);"; // el numero de pasos
    private static final String S_REGEX_WIDTH = "@(GridArrayWidth|GAW)=\\((\\d+)\\);"; // el ancho del grid
    private static final String S_REGEX_HEIGHT = "@(GridArrayHeight|GAH)=\\((\\d+)\\);"; // el alto del grid
    private static final String S_REGEX_CELL_SIZE = "@(GridCellSize|GCS)=\\((\\d+)\\);"; // el tamaño de la celda    
    private static final String S_REGEX_DRAW_BORDER = "@(GridDrawBorder|GDB)=\\((true|false)\\);"; // Si se dibujara el borde
    private static final String S_REGEX_BORDER_COLOR = "@(GridBorderColor|GBC)=\\((\\d{3}):(\\d{3}):(\\d{3})\\);"; // color para el borde
    private static final String S_REGEX_ANIMATION_DELAY = "@(GridAnimationDelay|GAD)=\\((\\d+)\\);"; // el retardo en la animación    
    private static final String S_REGEX_FOOTER = "@(GridConfigurationEnd|GCE);"; // indica el final del bloque del Grid

    private static final Pattern P_REGEX_NAME = Pattern.compile(GridsParser.S_REGEX_NAME, Pattern.CASE_INSENSITIVE);
    private static final Pattern P_REGEX_DESC = Pattern.compile(GridsParser.S_REGEX_DESC, Pattern.CASE_INSENSITIVE);
    private static final Pattern P_REGEX_BACK = Pattern.compile(GridsParser.S_REGEX_BACK, Pattern.CASE_INSENSITIVE);
    private static final Pattern P_REGEX_FRON = Pattern.compile(GridsParser.S_REGEX_FRON, Pattern.CASE_INSENSITIVE);
    private static final Pattern P_REGEX_STEPS = Pattern.compile(GridsParser.S_REGEX_STEPS, Pattern.CASE_INSENSITIVE);
    private static final Pattern P_REGEX_WIDTH = Pattern.compile(GridsParser.S_REGEX_WIDTH, Pattern.CASE_INSENSITIVE);
    private static final Pattern P_REGEX_HEIGHT = Pattern.compile(GridsParser.S_REGEX_HEIGHT, Pattern.CASE_INSENSITIVE);
    private static final Pattern P_REGEX_CELL_SIZE = Pattern.compile(GridsParser.S_REGEX_CELL_SIZE, Pattern.CASE_INSENSITIVE);
    private static final Pattern P_REGEX_DRAW_BORDER = Pattern.compile(GridsParser.S_REGEX_DRAW_BORDER, Pattern.CASE_INSENSITIVE);
    private static final Pattern P_REGEX_BORDER_COLOR = Pattern.compile(GridsParser.S_REGEX_BORDER_COLOR, Pattern.CASE_INSENSITIVE);
    private static final Pattern P_REGEX_ANIMATION_DELAY = Pattern.compile(GridsParser.S_REGEX_ANIMATION_DELAY, Pattern.CASE_INSENSITIVE);
    private static final Pattern P_REGEX_FOOTER = Pattern.compile(GridsParser.S_REGEX_FOOTER, Pattern.CASE_INSENSITIVE);

   

    /**
     * Cadenas para el formateo del archivo
     */
    private static final String GRID_NAME = "@GridConfigurationIni";
    private static final String GRID_DESC = "@GridDescription";
    private static final String GRID_BACK = "@GridBackgroundState";
    private static final String GRID_FRON = "@GridFrontierKind";
    private static final String GRID_STEPS = "@GridNumberSteps";
    private static final String GRID_WIDT = "@GridArrayWidth";
    private static final String GRID_HEIG = "@GridArrayHeight";
    private static final String GRID_CELL = "@GridCellSize";
    private static final String GRID_DRAW = "@GridDrawBorder";
    private static final String GRID_BCOL = "@GridBorderColor";
    private static final String GRID_ANIM = "@GridAnimationDelay";
    
    /**
     * Extrae de la cadena la información referente al grid
     *
     * @param sGrids
     * @return
     */
    public static Grids parse(String sGrids)
    {
        String name = GridsParser.parseName(sGrids);
        String description = GridsParser.parseDescription(sGrids);
        int frontierType = GridsParser.parseFrontierType(sGrids);

        int rows = GridsParser.parseHeight(sGrids);
        int cols = GridsParser.parseWidth(sGrids);
        int cellSize = GridsParser.parseCellSize(sGrids);

        boolean drawBorder = GridsParser.parseDrawBorder(sGrids);
        Color borderColor = GridsParser.parseBorderColor(sGrids);

        int steps = GridsParser.parseNumSteps(sGrids);
        int animationDelay = GridsParser.parseAnimationDelay(sGrids);

        State background = GridsParser.parseBackground(sGrids);
        
        Grid originalGrid = GridParser.parse(sGrids);

        return new Grids(
            name,
            description,
            frontierType,
            rows,
            cols,
            cellSize,
            drawBorder,
            borderColor,
            steps,
            animationDelay,
            background,
            originalGrid);
    }

    /**
     * Extrae de la cadena el nombre del grid
     *
     * @param oLine
     * @return
     */
    private static String parseName(String oLine)
    {
        Matcher pMatcher = GridsParser.P_REGEX_NAME.matcher(oLine);
        String sName;

        if (pMatcher.find())
        {
            sName = pMatcher.group(2);
            return sName;
        }

        SacUtility.showParsingError("Bloque de Grid - Nombre: Formato incorrecto.");
        return null;
    }

    /**
     * Lee del archivo la descrición del grid
     *
     * @param oLine
     * @return
     */
    private static String parseDescription(String oLine)
    {
        Matcher pMatcher = GridsParser.P_REGEX_DESC.matcher(oLine);
        String sDesc;

        if (pMatcher.find())
        {
            sDesc = pMatcher.group(2);

            return sDesc;

        }

        SacUtility.showParsingError("Bloque de Grid - Descripción: Formato incorrecto.");
        return null;
    }

    /**
     * Lee del archivo el estado de fondo del grid
     *
     * @param oLine
     * @return
     */
    public static State parseBackground(String oLine)
    {
        Matcher pMatcher = GridsParser.P_REGEX_BACK.matcher(oLine);

        if (pMatcher.find())
        {
            State nState = StateParser.parse(pMatcher.group(2));
            if (nState != null)
            {
                return nState;
            }
        }

        SacUtility.showParsingError("Bloque de Grid - GBS - Estado de Fondo: Formato incorrecto.");
        return null;
    }

    /**
     * Lee del archivo el tipo de frontera
     *
     * @param sGrid
     * @return
     */
    private static int parseFrontierType(String sGrid)
    {
        Matcher pMatcher = GridsParser.P_REGEX_FRON.matcher(sGrid);
        int frontierKind = -1;

        if (pMatcher.find())
        {
            frontierKind = Integer.parseInt(pMatcher.group(2));
            return frontierKind;
        }

        SacUtility.showParsingError("Bloque de Grid - GFK: Formato incorrecto.");
        return frontierKind;
    }

    /**
     * Lee del archivo el número de pasos de la simulación
     *
     * @param sGrid
     * @return
     */
    private static int parseNumSteps(String sGrid)
    {
        Matcher pMatcher = GridsParser.P_REGEX_STEPS.matcher(sGrid);
        int numSteps = -1;

        if (pMatcher.find())
        {
            numSteps = Integer.parseInt(pMatcher.group(2));
            return numSteps;
        }

        SacUtility.showParsingError("Bloque de Grid - GNS: Formato incorrecto.");
        return numSteps;
    }

    /**
     * Lee del archivo el número de columnas del grid (anchura)
     *
     * @param sGrid
     * @return
     */
    private static int parseWidth(String sGrid)
    {
        Matcher pMatcher = GridsParser.P_REGEX_WIDTH.matcher(sGrid);
        int width = -1;

        if (pMatcher.find())
        {
            width = Integer.parseInt(pMatcher.group(2));
            return width;
        }

        SacUtility.showParsingError("Bloque de Grid - GAW: Formato incorrecto.");
        return width;
    }

    /**
     * Lee del archivo el número de filas del grid (altura)
     *
     * @param sGrid
     * @return
     */
    private static int parseHeight(String sGrid)
    {
        Matcher pMatcher = GridsParser.P_REGEX_HEIGHT.matcher(sGrid);
        int height = -1;

        if (pMatcher.find())
        {
            height = Integer.parseInt(pMatcher.group(2));
            return height;
        }

        SacUtility.showParsingError("Bloque de Grid - GAH: Formato incorrecto.");
        return height;
    }

    /**
     * Lee del archivo el tamaño de la celda del grid en pixels
     *
     * @param sGrid
     * @return
     */
    private static int parseCellSize(String sGrid)
    {
        Matcher pMatcher = GridsParser.P_REGEX_CELL_SIZE.matcher(sGrid);
        int cellSize = -1;

        if (pMatcher.find())
        {
            cellSize = Integer.parseInt(pMatcher.group(2));
            return cellSize;
        }

        SacUtility.showParsingError("Bloque de Grid - GCS: Formato incorrecto.");
        return cellSize;
    }

    /**
     * Lee del archivo la opción de dibujar borde
     *
     * @param sGrid
     * @return
     */
    private static boolean parseDrawBorder(String sGrid)
    {
        Matcher pMatcher = GridsParser.P_REGEX_DRAW_BORDER.matcher(sGrid);

        if (pMatcher.find())
        {
            return Boolean.parseBoolean(pMatcher.group(2));
        }

        SacUtility.showParsingError("Bloque de Grid - GDB: Formato incorrecto.");
        return false;

    }

    /**
     * Lee del archivo el color del borde del grid
     *
     * @param sGrid
     * @return
     */
    private static Color parseBorderColor(String sGrid)
    {
        Matcher pMatcher = GridsParser.P_REGEX_BORDER_COLOR.matcher(sGrid);
        int red;
        int green;
        int blue;
        if (pMatcher.find())
        {
            red = Integer.parseInt(pMatcher.group(2));
            green = Integer.parseInt(pMatcher.group(3));
            blue = Integer.parseInt(pMatcher.group(4));
            return new Color(red, green, blue);
        }

        SacUtility.showParsingError("Bloque de Grid - GBC: Formato incorrecto.");
        return null;
    }

    /**
     * Lee del archivo el valor del retardo de la animación
     *
     * @param sGrid
     * @return
     */
    private static int parseAnimationDelay(String sGrid)
    {
        Matcher pMatcher = GridsParser.P_REGEX_ANIMATION_DELAY.matcher(sGrid);
        int delay = -1;

        if (pMatcher.find())
        {
            delay = Integer.parseInt(pMatcher.group(2));
            return delay;
        }

        SacUtility.showParsingError("Bloque de Grid - GAD: Formato incorrecto.");
        return delay;
    }

   

    /**
     * Retorna en forma de String con formato de archivo el objeto oGrids
     *
     * @param oGrids
     * @return
     */
    public static String convertToString(Grids oGrids)
    {
        StringBuilder sGrids = new StringBuilder();

        String nLine = System.lineSeparator();

        sGrids.append(GridsParser.getFormattedName(oGrids));
        sGrids.append(nLine);
        sGrids.append(GridsParser.getFormattedDescription(oGrids));
        sGrids.append(nLine);
        sGrids.append(GridsParser.getFormattedBackground(oGrids));
        sGrids.append(nLine);
        sGrids.append(GridsParser.getFormattedFrontier(oGrids));
        sGrids.append(nLine);
        sGrids.append(GridsParser.getFormattedNumSteps(oGrids));
        sGrids.append(nLine);
        sGrids.append(GridsParser.getFormattedAnimationDelay(oGrids));
        sGrids.append(nLine);
        sGrids.append(GridsParser.getFormattedWidth(oGrids));
        sGrids.append(nLine);
        sGrids.append(GridsParser.getFormattedHeight(oGrids));
        sGrids.append(nLine);
        sGrids.append(GridsParser.getFormatedCellSize(oGrids));
        sGrids.append(nLine);
        sGrids.append(GridsParser.getFormattedDrawBorder(oGrids));
        sGrids.append(nLine);
        sGrids.append(GridsParser.getFormattedBorderColor(oGrids));
        sGrids.append(nLine);
        sGrids.append(oGrids.getGridOri().toString());
        sGrids.append(nLine);
        sGrids.append("@GridConfigurationEnd;");

        return sGrids.toString();
    }

    /**
     * Retorna en forma de String con formato de archivo el objeto grids sin
     * incluir el contenido de las celdas.
     *
     * @param grids
     * @return
     */
    public static String getGridsWithoutCells(Grids grids)
    {
        StringBuilder sGrids = new StringBuilder();

        String nLine = System.lineSeparator();

        sGrids.append(GridsParser.getFormattedName(grids));
        sGrids.append(nLine);
        sGrids.append(GridsParser.getFormattedDescription(grids));
        sGrids.append(nLine);
        sGrids.append(GridsParser.getFormattedBackground(grids));
        sGrids.append(nLine);
        sGrids.append(GridsParser.getFormattedFrontier(grids));
        sGrids.append(nLine);
        sGrids.append(GridsParser.getFormattedNumSteps(grids));
        sGrids.append(nLine);
        sGrids.append(GridsParser.getFormattedAnimationDelay(grids));
        sGrids.append(nLine);
        sGrids.append(GridsParser.getFormattedWidth(grids));
        sGrids.append(nLine);
        sGrids.append(GridsParser.getFormattedHeight(grids));
        sGrids.append(nLine);
        sGrids.append(GridsParser.getFormatedCellSize(grids));
        sGrids.append(nLine);
        sGrids.append(GridsParser.getFormattedDrawBorder(grids));
        sGrids.append(nLine);
        sGrids.append(GridsParser.getFormattedBorderColor(grids));
        sGrids.append(nLine);
        sGrids.append("@GridConfigurationEnd;");

        return sGrids.toString();
    }


    
    /**
     * Retorna el nombre del grid con el formato de archivo
     *
     * @param aGrids
     * @return
     */
    private static String getFormattedName(Grids aGrids)
    {
        StringBuilder fLine = new StringBuilder();

        fLine.append(GridsParser.GRID_NAME).append("=(");
        fLine.append(aGrids.getName()).append(");");

        return fLine.toString();
    }

    /**
     * Retorna la descripción del grid con el formato de archivo
     *
     * @param aGrids
     * @return
     */
    private static String getFormattedDescription(Grids aGrids)
    {
        StringBuilder fLine = new StringBuilder();

        fLine.append(GridsParser.GRID_DESC).append("=(");
        fLine.append(aGrids.getDescription()).append(");");

        return fLine.toString();
    }

    /**
     * Retorna el tipo de frontera del grid con el formato de archivo
     *
     * @param aGrids
     * @return
     */
    private static String getFormattedFrontier(Grids aGrids)
    {
        StringBuilder fLine = new StringBuilder();

        fLine.append(GridsParser.GRID_FRON).append("=(");
        fLine.append(aGrids.getFrontierType());
        fLine.append(");");

        return fLine.toString();
    }

    /**
     * Retorna el numero de pasos de la simulación con el formato de archivo
     *
     * @param aGrids
     * @return
     */
    private static String getFormattedNumSteps(Grids aGrids)
    {
        StringBuilder fLine = new StringBuilder();

        fLine.append(GridsParser.GRID_STEPS).append("=(");
        fLine.append(aGrids.getNumSteps()).append(");");

        return fLine.toString();
    }

    /**
     * Retorna el retardo de la animación con el formato de archivo
     *
     * @param aGrids
     * @return
     */
    private static String getFormattedAnimationDelay(Grids aGrids)
    {
        StringBuilder fLine = new StringBuilder();

        fLine.append(GridsParser.GRID_ANIM).append("=(");
        fLine.append(aGrids.getAnimationDelay()).append(");");

        return fLine.toString();
    }

    /**
     * Retorna el numero de columnas del grid (ancho) con el formato de archivo
     *
     * @param aGrids
     * @return
     */
    private static String getFormattedWidth(Grids aGrids)
    {
        StringBuilder fLine = new StringBuilder();

        fLine.append(GridsParser.GRID_WIDT).append("=(");
        fLine.append(aGrids.getGridIni().getColumns()).append(");");

        return fLine.toString();
    }

    /**
     * Retorna el numero de filas del grid (altura) con el formato de archivo
     *
     * @param aGrids
     * @return
     */
    private static String getFormattedHeight(Grids aGrids)
    {
        StringBuilder fLine = new StringBuilder();

        fLine.append(GridsParser.GRID_HEIG).append("=(");
        fLine.append(aGrids.getGridIni().getRows()).append(");");

        return fLine.toString();
    }

    /**
     * Retorna el tamaño de la celda con el formato de archivo
     *
     * @param aGrids
     * @return
     */
    private static String getFormatedCellSize(Grids aGrids)
    {
        StringBuilder fLine = new StringBuilder();

        fLine.append(GridsParser.GRID_CELL).append("=(");
        fLine.append(aGrids.getCellSize()).append(");");

        return fLine.toString();
    }

    /**
     * Retorna el color del borde con formato de archivo
     *
     * @param currentGrids
     * @return
     */
    private static String getFormattedBorderColor(Grids currentGrids)
    {
        StringBuilder fLine = new StringBuilder();

        fLine.append(GridsParser.GRID_BCOL).append("=(");

        fLine.append(String.format("%03d", currentGrids.getBorderColor().getRed()));
        fLine.append(":");
        fLine.append(String.format("%03d", currentGrids.getBorderColor().getGreen()));
        fLine.append(":");
        fLine.append(String.format("%03d", currentGrids.getBorderColor().getBlue()));

        fLine.append(");");

        return fLine.toString();
    }

    /**
     * Retorna la opcion de dibujar borde con formato de archivo
     *
     * @param aGrids
     * @return
     */
    private static String getFormattedDrawBorder(Grids aGrids)
    {
        StringBuilder fLine = new StringBuilder();

        fLine.append(GridsParser.GRID_DRAW).append("=(");
        fLine.append(aGrids.isBorderOn()).append(");");

        return fLine.toString();
    }

    /**
     * Retorna el estado de fondo con el formato de archivo
     *
     * @param aGrids
     * @return
     */
    private static String getFormattedBackground(Grids aGrids)
    {
        StringBuilder fLine = new StringBuilder();

        fLine.append(GridsParser.GRID_BACK).append("=(");
        fLine.append(aGrids.getBackground().toString()).append(");");

        return fLine.toString();
    }

   
}
