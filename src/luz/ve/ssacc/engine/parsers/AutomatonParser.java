/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package luz.ve.ssacc.engine.parsers;

import java.util.regex.Matcher;
import java.util.regex.Pattern;
import luz.ve.ssacc.engine.components.Automaton;
import luz.ve.ssacc.engine.components.Grids;
import luz.ve.ssacc.engine.components.Neighborhood;
import luz.ve.ssacc.engine.components.Rules;
import luz.ve.ssacc.engine.components.States;

/**
 * Esta clase convierte el Automata celular en un string para imprimir o
 * guardar en archivo
 *
 * @author Renzo Ciafardone Sciannelli
 */



public class AutomatonParser
{

    /*
     Ejemplo:
     identificacion: @CAI=(0001000120100623094810);
     nombre:         @CAN=(GameOfLifeSample001);
     descripcion:    @CAD(Este autómata está basado en el juego de la vida 
     creado por el matemático británico John Horton Conway. Está
     definido por 4 reglas (en lenguaje natural, que se pueden 
     expresar en solo 3 dentro del sistema) y 2 estados con 
     vecindario de Moore);
     */

    public static final String DEFAULT_NAME = "ADAC sin nombre";
    public static final String DEFAULT_DESCRIPTION = "";
    
    /**
     * Expresiones regulares para el parsing del archivo
     */
    private static final String S_REGEX_NAME = "@(CellularAutomataName|CAN)=\\(([^@]+)\\);"; // marca el inicio del conjunto de estados y contiene la propiedad name_, no puede estar vacia.
    private static final String S_REGEX_ID = "@(CellularAutomataId|CAI)=\\(([^@]+)\\);"; // marca el inicio del conjunto de estados y contiene el numero de indetificacion, no puede estar vacia.
    private static final String S_REGEX_DESC = "@(CellularAutomataDescription|CAD)=\\(([^@]*)\\);"; // la descripcion, puede estar vacia.
    private static final String S_REGEX_FOOTER = "@(CellularAutomataEnd|CAE);"; // indica el final del bloque del automata
            
    
   private static final Pattern P_REGEX_NAME = Pattern.compile(AutomatonParser.S_REGEX_NAME, Pattern.CASE_INSENSITIVE);
   private static final Pattern P_REGEX_ID = Pattern.compile(AutomatonParser.S_REGEX_ID, Pattern.CASE_INSENSITIVE);
   private static final Pattern P_REGEX_DESC = Pattern.compile(AutomatonParser.S_REGEX_DESC, Pattern.CASE_INSENSITIVE);
   private static final Pattern P_REGEX_FOOTER = Pattern.compile(AutomatonParser.S_REGEX_FOOTER, Pattern.CASE_INSENSITIVE);
    
    
    /**
     * Cadenas para el formateo del archivo
     */
    private static final String AUTOMATA_ID = "@CellularAutomataId"; 
    private static final String AUTOMATA_NAME = "@CellularAutomataName"; // marca el inicio del conjunto de estados y contiene la propiedad name_, no puede estar vacia.
    private static final String AUTOMATA_DESC = "@CellularAutomataDescription"; // la descripcion, puede estar vacia.
    private static final String AUTOMATA_FOOTER = "@CellularAutomataEnd;"; // cierra el bloque del automata
    
    public static Automaton parse(String sAdac)
    {
        String name = AutomatonParser.parseName(sAdac);
        String id = AutomatonParser.parseId(sAdac);
        String description = AutomatonParser.parseDescription(sAdac);
        States states = StatesParser.parse(sAdac);
        Neighborhood neigh = NeighborhoodParser.parse(sAdac);
        Rules rules = RulesParser.parse(sAdac);
        Grids grids = GridsParser.parse(sAdac);

        return new Automaton(name, id, description, states, rules, neigh, grids);
    }


    /**
     * Analiza el string para extraer el nombre
     * @param oLine
     * @return 
     */
    public static String parseName(String oLine)
    {
        Matcher pMatcher = AutomatonParser.P_REGEX_NAME.matcher(oLine);

        if (pMatcher.find())
        {
            return (pMatcher.group(2));
        }

        return null;
    }

    /**
     * Analiza el string para extraer el ID
     * @param oLine
     * @return
     */
    public static String parseId(String oLine)
    {
        Matcher pMatcher = AutomatonParser.P_REGEX_ID.matcher(oLine);

        if (pMatcher.find())
        {
            return (pMatcher.group(2));
        }
        return null;
    }

    /**
     * Analiza el string para extraer la descripción
     * @param oLine
     * @return
     */
    public static String parseDescription(String oLine)
    {
        Matcher pMatcher = AutomatonParser.P_REGEX_DESC.matcher(oLine);

        if (pMatcher.find())
        {
            return (pMatcher.group(2));
        }
        return null;
    }

    /**
     * Convierte el objeto automata en una cadena
     * @param automata
     * @return 
     */
    public static String convertToString(Automaton automata)
    {
        String nLine = System.lineSeparator();
        StringBuilder sAutomata = new StringBuilder();

        sAutomata.append(AutomatonParser.getFormatedHeader(automata));
        sAutomata.append(nLine);
        
        sAutomata.append(automata.getStates().toString());
        sAutomata.append(nLine);

        sAutomata.append((automata.getRules().toString()));
        sAutomata.append(nLine);
        sAutomata.append(nLine);
        
        sAutomata.append(automata.getNeighborhood().toString());
        sAutomata.append(nLine);
        
        sAutomata.append(automata.getGrids().toString());
        sAutomata.append(nLine);
        
        sAutomata.append(AutomatonParser.AUTOMATA_FOOTER);
        
        return sAutomata.toString();
    }

    /**
     * Convierte el objeto automata en una cadena - No incluye el contenido del
     * grid inicial.
     *
     * @param automata
     * @return
     */
    public static String convertToStringX(Automaton automata)
    {
        String nLine = System.lineSeparator();
        StringBuilder sAutomata = new StringBuilder();

        sAutomata.append(AutomatonParser.getFormatedHeader(automata));
        sAutomata.append(nLine);
        
        sAutomata.append(automata.getStates().toString());
        sAutomata.append(nLine);

        sAutomata.append((automata.getRules().toString()));
        sAutomata.append(nLine);
        sAutomata.append(nLine);
        
        sAutomata.append(automata.getNeighborhood().toString());
        sAutomata.append(nLine);
        
        sAutomata.append(GridsParser.getGridsWithoutCells(automata.getGrids()));
        sAutomata.append(nLine);
        
        sAutomata.append(AutomatonParser.AUTOMATA_FOOTER);
        
        return sAutomata.toString();
    }

    public static String getFormatedHeader(Automaton automata)
    {
        String nLine = System.lineSeparator();
        StringBuilder sHeader = new StringBuilder();
        
        sHeader.append(AutomatonParser.getFormatedName(automata.getName()));
        sHeader.append(nLine);
        sHeader.append(AutomatonParser.getFormatedId(automata.getId()));
        sHeader.append(nLine);
        sHeader.append(AutomatonParser.getFormatedDescription(automata.getDescription()));
        sHeader.append(nLine);
        return sHeader.toString();
    }

    
    private static String getFormatedName(String sName)
    {
        StringBuilder fName = new StringBuilder();
        fName.append(AutomatonParser.AUTOMATA_NAME).append("=(").append(sName).append(");");
        return fName.toString();
    }

    private static String getFormatedId(String sId)
    {
        StringBuilder fId = new StringBuilder();
        fId.append(AutomatonParser.AUTOMATA_ID).append("=(").append(sId).append(");");
        return fId.toString();
    }

    private static String getFormatedDescription(String sDescription)
    {
        StringBuilder fDesc = new StringBuilder();
        fDesc.append(AutomatonParser.AUTOMATA_DESC).append("=(").append(sDescription).append(");");
        return fDesc.toString();
    }

}
