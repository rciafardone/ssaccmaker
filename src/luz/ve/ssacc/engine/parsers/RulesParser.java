/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package luz.ve.ssacc.engine.parsers;

import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import javax.swing.JOptionPane;
import luz.ve.ssacc.engine.components.Rule;
import luz.ve.ssacc.engine.components.Rules;
import luz.ve.ssacc.utilities.SacUtility;
/*
 * Esta clase maneja la creacion de las reglas @author "Renzo Ciafardone
 * Sciannelli"
 */
public final class RulesParser
{

    //**************************************************************************
    //*****************************VARIABLES************************************
    //**************************************************************************
    
    public static final int AS_A_STRING_ARRAY = 0;
    public static final String DEFAULT_NAME = "N/A";
    public static final String DEFAULT_DESCRIPTION = "N/A";
    
    // Expresiones regulares
    private static final String S_REGEX_HEADER = "@(RulesBlockIni|RBI)=\\(([^@]+)\\);"; // marca el inicio del conjunto de reglas y contiene la propiedad name_, no puede estar vacia.
    private static final String S_REGEX_DESC = "@(RulesDesc|RulesDescription|RDS)=\\(([^@]*)\\);"; // la descripcion, puede estar vacia.
    private static final String S_REGEX_FOOTER = "@(RulesBlockEnd|RBE);"; // cierra el bloque del conjunto de reglas
    private static final String S_REGEX_FULL_SACR = S_REGEX_HEADER + "([\\s\\S]*)" + S_REGEX_FOOTER; // Toda la informacion del conjunto de reglas
    private static final Pattern P_REGEX_HEADER = Pattern.compile(RulesParser.S_REGEX_HEADER, Pattern.CASE_INSENSITIVE);
    private static final Pattern P_REGEX_DESC = Pattern.compile(RulesParser.S_REGEX_DESC, Pattern.CASE_INSENSITIVE);
    private static final Pattern P_REGEX_FOOTER = Pattern.compile(RulesParser.S_REGEX_FOOTER, Pattern.CASE_INSENSITIVE);
    private static final Pattern P_REGEX_FULL_SACR = Pattern.compile(RulesParser.S_REGEX_FULL_SACR, Pattern.CASE_INSENSITIVE);

    //**************************************************************************
    //*****************************CONSTRUCTORS*********************************
    //**************************************************************************
   
    

    //**************************************************************************
    //*****************************METHODS**************************************
    //**************************************************************************
    
    /**
     * @RBS(Reglas del Juego de la vida);
     * @param nRules
     * @return
     */
    private static String getFormattedName(Rules nRules)
    {
        StringBuilder sName = new StringBuilder();

        sName.append("@RulesBlockIni=(").append(nRules.getName()).append(");");

        return sName.toString();
    }

    /**
     * Return a String of the description with the format to store in the SACR
     * file
     *
     * @param nRules
     * @return
     */
    private static String getFormattedDescription(Rules nRules)
    {
        StringBuilder sDesc = new StringBuilder();

        sDesc.append("@RulesDescription=(").append(nRules.getDescription()).append(");");

        return sDesc.toString();
    }

    /*
     * Creates the SACR file header
     */
    private static String getFormattedHeader(Rules nRules)
    {
        StringBuilder sHeader = new StringBuilder();
        sHeader.append(getFormattedName(nRules)).append(SacUtility.EOL);
        sHeader.append(getFormattedDescription(nRules)).append(SacUtility.EOL);
        return sHeader.toString();
    }


    /*
     * Creates the SACR file header
     */
    private static String getFormattedRules(Rules nRules)
    {
        StringBuilder aList = new StringBuilder();

        for (Rule aRule : nRules.getList())
        {
            aList.append(SacUtility.EOL);
            aList.append(aRule.toString());
        }

        return aList.toString();
    }

    /*
     * Creates the SACR file footer. Note: Just a wrapper, I added this just for
     * uniformity's sake
     */
    private static String getFormattedFooter()
    {
        return "@RulesBlockEnd;";
    }

    /*
     * Returns the Rules formated into a String be stored on s SACR Sample:
     *
     * @RBS(Reglas del Juego de la vida); @RulesDescription = (Estas reglas
     * emulan el comportamiento de…);
     *
     * @Rule(001)=(0001:0000); @?(CON(s(0001):vcn)<(3)); @Rule(001);
     *
     * @RBE;
     */
    public static String ConvertToString(Rules oRules)
    {
        StringBuilder sRules = new StringBuilder();

        sRules.append(RulesParser.getFormattedHeader(oRules));
        sRules.append(RulesParser.getFormattedRules(oRules)).append(SacUtility.EOL);
        sRules.append(RulesParser.getFormattedFooter());

        return sRules.toString();
    }

    /**
     * Regresa el id mas alto de la lista (Se debio crear esta funcion debido a que
     * el ID permanece aunque se agreguen o eliminen reglas)
     * @param oRules
     * @return 
     */
    public static String getMaxId(Rules oRules)
    {
        int curId;
        int maxId = 0;
        for (Rule aRule : oRules.getList())
        {
            curId = aRule.getId();
            if (curId > maxId)
            {
                maxId = curId;
            }
        }
        return String.format("{0:000}", maxId);
    }

    
    
    /**
     * Retorna el id mas bajo que no este en la lista. La lista puede no estar 
     * ordenada
     * @param oRules
     * @return 
     */
    public static int getNewRuleId(Rules oRules)
    {
        int usedId;
        int newId = 0;
        
        for (int i = 0; i < oRules.getList().size(); i++)
        {
            usedId = oRules.getList().get(i).getId();
            if (usedId == newId)
            {
                i = 0;
                newId++;
            }
        }
        
        return newId;
    }


    /*
     * If the String passed as parameter has the correct format the name
     * property is setted and true returned, else nothing is done and false is
     * returned. Ex: @StatesConfigurationIni(Game of Life);
     */
    public static String parseHeader(String oLine)
    {
        Matcher pMatcher = RulesParser.P_REGEX_HEADER.matcher(oLine);

        if (pMatcher.find())
        {
            return (pMatcher.group(2));
        }

        SacUtility.showParsingError("Bloque de Reglas - Cabecera: Formato incorrecto.");
        return null;
    }

    /**
     * If the String passed as parameter has the correct format the description_
     * property is setted and true returned else nothing is done and false is
     * returned.
     * @param oLine
     * @return 
     */
    public static String parseDescription(String oLine)
    {
        Matcher pMatcher = RulesParser.P_REGEX_DESC.matcher(oLine);

        if (pMatcher.find())
        {
            return (pMatcher.group(2));
        }

        SacUtility.showParsingError("Bloque de Reglas - Descripción: Formato incorrecto.");
        return null;
    }

    /**
     * Reads the string and extracts all the states to add them to the
     * statesList_
     *
     * @param oLine
     * @return
     */
    public static List<Rule> parseRulesList(String oLine)
    {
        List<Rule> rulesList = new ArrayList<>();
  
        Matcher pMatcher = RuleParser.P_REGEX_RULE.matcher(oLine);
        while (pMatcher.find())
        {
            Rule aNewRule;
            aNewRule = RuleParser.parse(pMatcher.group(0));
            
            if ( aNewRule!=null)
            {
                rulesList.add(aNewRule);
            }
        }

        if (rulesList.isEmpty()) // Si no se agrega ninguna regla no es un string valido
        {
            SacUtility.showParsingError("Bloque de Reglas - Lista de Reglas: Formato incorrecto.");
            return null;
        }

        return rulesList;
    }

    /**
     * If the String passed as parameter has the correct format true is returned
     * to indicate that the StateSet configuration is over true returned, else
     * nothing is done and false is returned.
     * @param oLine
     * @return 
     */
    public static boolean parseFooter(String oLine)
    {
        Matcher pMatcher = RulesParser.P_REGEX_FOOTER.matcher(oLine);

        if (pMatcher.find())
        {
            return true;
        }

        SacUtility.showParsingError("Bloque de Reglas - Final bloque de Reglas: Formato incorrecto.");
        return false;
    }

    /**
     * Transforma el contenido del String en un objeto SacRulesSet si cumple con
     * el formato correcto. El String puede ser un archivo ADAC completo o solo
     * un SACR. Si el String de entrada no contiene un conjunto de estados
     * completo se retorna falso.
     * @param fileContent
     * @return 
     */
    public static Rules parse(String fileContent)
    {
        Matcher pMatcher = RulesParser.P_REGEX_FULL_SACR.matcher(fileContent);
        
        if (pMatcher.find()) // Primero nos aseguramos que tenga pies y cabeza.
        {
            String sbSac = pMatcher.group(0); // Filtra el string extrayendo solo lo que pertenezca al bloque del conjunto de reglas

            String head = parseHeader(sbSac);
            String desc = parseDescription(sbSac);
            List<Rule> rulesList;
            
            rulesList = parseRulesList(sbSac);

            if (head != null && desc != null && rulesList != null)
            {
                return (new Rules(head, desc, rulesList));
            }
        }

        SacUtility.showParsingError("El bloque de reglas no tiene un formato valido." + SacUtility.EOL);
        return null;
    }

    /**
     * Se asegura de que las reglas tengan las condiciones mínimas necesarias
     * para ser usadas, Las reglas no se salvaran si se cumple una de las
     * siguientes condiciones: 
     * - No se le coloco identificacion. 
     * - No se define ninguna regla. 
     * - Se define una regla pero no se le colocan condiciones. 
     * - Incongruencias internas. 
     * Nota: La descripcion es opcional y por tanto se puede dejar en blanco; 
     * además los estados especificados en las reglas son independientes del 
     * conjunto de estados actualmente activos en el ADAC,
     * incongruencias entre ambos son responsabilidad del usuario y se
     * manifestaran al tratar de correr el AC.
     * @param nRules
     * @return 
     */
    public static boolean isValid(Rules nRules)
    {

        if ((nRules.getName().equals("")) || (nRules.getName().equals(RulesParser.DEFAULT_NAME)))
        {
            StringBuilder sTitle = new StringBuilder();
            StringBuilder sInfo = new StringBuilder();

            sTitle.append("Advertencia");
            sInfo.append("El nombre de las reglas esta vacio o no es valido.");
            sInfo.append(SacUtility.EOL);
            sInfo.append(SacUtility.EOL);
            sInfo.append("Sugerencia:");
            sInfo.append(SacUtility.EOL);
            sInfo.append("Llene el campo nombre de Reglas");
            sInfo.append(SacUtility.EOL);
            sInfo.append("Ejemplo: >>Juego de la vida<<");
            sInfo.append(SacUtility.EOL);
            sInfo.append("Ejemplo: >>SACR001<<");
            sInfo.append(SacUtility.EOL);

            JOptionPane errorDialog = new JOptionPane();
            JOptionPane.showMessageDialog(errorDialog, sInfo.toString(), sTitle.toString(), JOptionPane.WARNING_MESSAGE);
            return false;
        }

        if (nRules.getList().isEmpty())
        {
            StringBuilder sTitle = new StringBuilder();
            StringBuilder sInfo = new StringBuilder();

            sTitle.append("Advertencia");
            sInfo.append("No se a creado ninguna regla para este conjunto.");
            sInfo.append(SacUtility.EOL).append(SacUtility.EOL);
            sInfo.append("Sugerencia:");
            sInfo.append(SacUtility.EOL);
            sInfo.append("Presione el boton + para agregar una regla");
            sInfo.append(SacUtility.EOL);

            JOptionPane errorDialog = new JOptionPane();
            JOptionPane.showMessageDialog(errorDialog, sInfo.toString(), sTitle.toString(), JOptionPane.WARNING_MESSAGE);
            return false;
        }

        for (Rule aRule : nRules.getList())
        {
                StringBuilder sTitle = new StringBuilder();
                StringBuilder sInfo = new StringBuilder();

                sTitle.append("Advertencia");
                sInfo.append("La siguiente regla no tiene condiciones validas: ");
                sInfo.append(RuleParser.getFormattedId(aRule));
                sInfo.append(SacUtility.EOL);
                sInfo.append("Sugerencia:");
                sInfo.append(SacUtility.EOL);
                sInfo.append("Vaya al dialogo de creación de condiciones para la regla indicada.");
                sInfo.append(SacUtility.EOL);

                JOptionPane errorDialog = new JOptionPane();
                JOptionPane.showMessageDialog(errorDialog, sInfo.toString(), sTitle.toString(), JOptionPane.WARNING_MESSAGE);
                return false;
        }
        return true;
    }

    /**
     * 
     * Devuelve solo los ids de las reglas en un String[]. Es importante
     * mantener el orden del ArrayList para poder utilizar esa informacion.
     * @param nRules
     * @return 
     */
    public static String[] idAsAStringArray(Rules nRules)
    {
        String[] list = new String[nRules.getList().size()];
        int i = 0;

        for (Rule oCurrent : nRules.getList())
        {
            list[i] = RuleParser.getFormattedTitle(oCurrent);
            i++;
        }

        return list;
    }

}
