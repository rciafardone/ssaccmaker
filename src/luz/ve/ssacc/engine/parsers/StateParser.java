/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package luz.ve.ssacc.engine.parsers;

import java.awt.Color;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import luz.ve.ssacc.engine.components.State;

/**
 *
 * @author "Renzo Ciafardone Sciannelli"
 */
public class StateParser
{
    //**************************************************************************
    //*****************************VARIABLES************************************
    //**************************************************************************

    // Expresiones regulares
    // @SC(0001)=(txt(Vivo))(val(1.0))(col(255:051:051)); Asi se debe ver un estado en forma de texto

    private static final String S_REGEX_ID = "((stateconfiguration|sc)\\((\\d{1,4})\\))";
    private static final String S_REGEX_TEXT = "(\\((texto|text|txt)\\(([^@()]*)\\)\\))";
    private static final String S_REGEX_VALUE = "(\\((valor|value|val)\\((\\d*((.|,)\\d+)?)\\)\\))";
    private static final String S_REGEX_COLOR = "(\\((color|col)\\((\\d{3}):(\\d{3}):(\\d{3})\\)\\))";
    public static final String S_REGEX_STATE = S_REGEX_ID + "=" + S_REGEX_TEXT + S_REGEX_VALUE + S_REGEX_COLOR;
    
    public static Pattern P_REGEX_STATE = Pattern.compile(StateParser.S_REGEX_STATE, Pattern.CASE_INSENSITIVE);
  //  public static Pattern P_REGEX_ID = Pattern.compile(StateParser.S_REGEX_ID, Pattern.CASE_INSENSITIVE);
  //  private static Pattern pRegexText = Pattern.compile(StateParser.S_REGEX_TEXT, Pattern.CASE_INSENSITIVE);
  //  private static Pattern pRegexValue = Pattern.compile(StateParser.S_REGEX_VALUE, Pattern.CASE_INSENSITIVE);
    private static final Pattern P_REGEX_COLOR = Pattern.compile(StateParser.S_REGEX_COLOR, Pattern.CASE_INSENSITIVE);
      
    //Constants for behavioral control
    public static final int ADD = 0;
    public static final int EDIT = 1;
    public static final int DEL = 2;
    public static final int OK = 0;
    public static final int VALUE_OUT_OF_RANGE = 1;
    public static final int UNEXPECTED_ERROR = -1;
    public static final int ID_MIN = 0;
    public static final int ID_MAX = 9999;
    public static final int DEFAULT_ID = 0;
    public static final String DEFAULT_ID_STRING = "0000";
    public static final String DEFAULT_TXT = "";
    public static final double DEFAULT_VAL = 0d;
   
    //**************************************************************************
    //*****************************CONSTRUCTORS*********************************
    //**************************************************************************
   

    //**************************************************************************
    //*****************************METHODS**************************************
    //**************************************************************************

    /**
     * Chequea si el string contiene el formato correcto para estado y llena los
     * campos correspondientes
     *
     * @param oLine
     * @return 
     */
    public static State parse(String oLine)
    {
        Matcher pMatcher = StateParser.P_REGEX_STATE.matcher(oLine);
        
        if (pMatcher.find())
        {
            Short id = Short.parseShort(pMatcher.group(3)); //group 3: state id
            String label = pMatcher.group(6); //group 6: text
            double value = Double.parseDouble(pMatcher.group(9)); //group 9: value
            Color color = parseColor(pMatcher.group(12));

            if (id != -1)
            {
                return new State(id, label, value, color);
            }
        }

        return null;
    }

    
    
    public static Color parseColor(String oLine)
    {
     
        Matcher pMatcher = StateParser.P_REGEX_COLOR.matcher(oLine);
        if (pMatcher.find())
        {
            int red = Integer.parseInt(pMatcher.group(3));
            int green = Integer.parseInt(pMatcher.group(4));
            int blue = Integer.parseInt(pMatcher.group(5));
            
            return new Color(red,green,blue);
        }

        return null;
    }
    
    
    /**
     * Formats the State's Id property into the string that will be saved in the
     * SACS file.
     *
     * @param aId
     * @return
     */
    private static String getFormattedId(Short aId)
    {
        StringBuilder id = new StringBuilder();
        id.append("SC(");
        id.append(formatId(aId));
        id.append(")");

        return id.toString();
    }

    /**
     * Formats the State's Text property into the string that will be saved in
     * the SACS file.
     *
     * @param aLabel
     * @return
     */
    private static String getFormattedText(String aLabel)
    {
        StringBuilder txt = new StringBuilder();

        txt.append("(txt(");
        txt.append(aLabel);
        txt.append("))");

        return txt.toString();
    }

    
    /**
     * Formats the State's value property into the string that will be saved in
     * the SACS file.
     *
     * @param aDouble
     * @return
     */
    private static String getFormattedValue(Double aDouble)
    {
        StringBuilder val = new StringBuilder();

        val.append("(val(");
        val.append(aDouble);
        val.append("))");

        return val.toString();
    }

   /**
     * Formats the State's value property into the string that will be saved in
     * the SACS file.
     *
     * @param aDouble
     * @return
     */
    private static String getFormattedColor(Color color)
    {
        StringBuilder col = new StringBuilder();

        col.append("(col(");
        col.append(String.format("%03d", color.getRed()));
        col.append(":");
        col.append(String.format("%03d", color.getGreen()));
        col.append(":");
        col.append(String.format("%03d", color.getBlue()));
        col.append("))");

        return col.toString();
    }


    /**
     * Recibe un entero y lo convierte en un String con el formato del id del
     * estado
     *
     * @param iId
     * @return
     */
    public static String formatId(Short iId)
    {
        if (!(iId < StateParser.ID_MIN || iId > StateParser.ID_MAX))
        {
            String format = "%0" + Integer.toString(StateParser.DEFAULT_ID_STRING.length()) + "d";

            String idFormated = String.format(format, iId);

            return (idFormated);
        }

        return StateParser.DEFAULT_ID_STRING;
    }

    /**
     * Se asegura de que el string recibido tenga el formato correcto = 0000
     *
     * @param oLine
     * @return
     */
    public static boolean checkFormatId(String oLine)
    {
        String sRegex = "([0-9][0-9][0-9][0-9])";
        Pattern pId = Pattern.compile(sRegex, Pattern.CASE_INSENSITIVE);
        Matcher pMatcher = pId.matcher(oLine);

        return(pMatcher.matches());
    }


        /**
     *
     * @param oState
     * @return
     */
    public static String ConvertToString(State oState)
    {
        StringBuilder sState = new StringBuilder();

        sState.append(getFormattedId(oState.getId()));
        sState.append("=");
        sState.append(getFormattedText(oState.getText()));
        sState.append(getFormattedValue(oState.getValue()));
        sState.append(getFormattedColor(oState.getColor()));
        
        return sState.toString();
    }
    
    
    

    /******************************************************************************/
    /*******************************END OF CLASS***********************************/
    /******************************************************************************/
}
