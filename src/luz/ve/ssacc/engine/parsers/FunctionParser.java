/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package luz.ve.ssacc.engine.parsers;

import luz.ve.ssacc.engine.functions.Add;
import luz.ve.ssacc.engine.functions.Average;
import luz.ve.ssacc.engine.functions.Constant;
import luz.ve.ssacc.engine.functions.Count;
import luz.ve.ssacc.engine.functions.Function;
import luz.ve.ssacc.engine.functions.Max;
import luz.ve.ssacc.engine.functions.Min;

/**
 *
 * @author renzo
 */
public class FunctionParser
{
    /**
     * Retorna la function segun el caso, la cual queda almacenada en la
     * condición
     *
     * @param sFunction
     * @return
     */
    public static Function parse(String sFunction)
    {
        if (sFunction.equalsIgnoreCase("add"))
        {
            return new Add();
        }

        if (sFunction.equalsIgnoreCase("average"))
        {
            return new Average();
        }

        if (sFunction.equalsIgnoreCase("constant"))
        {
            return new Constant();
        }

        if (sFunction.equalsIgnoreCase("count"))
        {
            return new Count();
        }

        if (sFunction.equalsIgnoreCase("max"))
        {
            return new Max();
        }

        if (sFunction.equalsIgnoreCase("min"))
        {
            return new Min();
        }
      
        return null;
    }

}
