
/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package luz.ve.ssacc.engine.parsers;

import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import luz.ve.ssacc.engine.components.Cell;
import luz.ve.ssacc.engine.components.Neighborhood;


/*
 * Esta clase define los vecindarios para los automatas finitos
 *
 * @author "Renzo Ciafardone Sciannelli"
 */
public class NeighborhoodParser
{
    //**************************************************************************
    //*****************************VARIABLES************************************
    //**************************************************************************
   
    // Expresiones regulares
    private static final String S_REGEX_HEADER = "@(NeighborhoodBlockIni|NBI)=\\(([^@]+)\\);"; // marca el inicio del vecindario y contiene la propiedad name_, no puede estar vacia.
    private static final String S_REGEX_DESC = "@(NeighborhoodDescription|nbd)=\\(([^@]*)\\);"; // la descripcion, puede estar vacia.
    private static final String S_REGEX_RADIX = "@(NeighborhoodPatternRadix|npr)=([^@]+);";  // raiz del patron del vecindario.
    private static final String S_REGEX_INCLUDE_RADIX = "@(NeighborhoodIncludesRadix|nir)=\\((true|false)\\);";  // ¿la raiz se incluira en el vecindario?
    private static final String S_REGEX_PATTERN = "@(NeighborhoodBasePattern|nbp)=((\\(-?[0-9]+(:-?[0-9]+)*\\))+);";  // el patron del vecindario.
    private static final String S_REGEX_RADIUS = "@(NeighborhoodRadius|nrd)=\\(([1-9]|[1-9][0-9])\\);"; // el radio. rango 1-99
    private static final String S_REGEX_FOOTER = "@(NeighborhoodBlockEnd|NBE);"; // cierra el vecindario
    private static final String S_REGEX_FULL_SACN = S_REGEX_HEADER + "([\\s\\S])*" + S_REGEX_FOOTER; // Toda la informacion del vecindario
  //  private static final String sRegexCoordinate = "(\\((-?[0-9]+)(:-?[0-9]+)*\\))";
    private static final Pattern P_REGEX_HEADER = Pattern.compile(NeighborhoodParser.S_REGEX_HEADER, Pattern.CASE_INSENSITIVE);
    private static final Pattern P_REGEX_DESC = Pattern.compile(NeighborhoodParser.S_REGEX_DESC, Pattern.CASE_INSENSITIVE);
    private static final Pattern P_REGEX_RADIX = Pattern.compile(NeighborhoodParser.S_REGEX_RADIX, Pattern.CASE_INSENSITIVE);
    private static final Pattern P_REGEX_INCLUDE_RADIX = Pattern.compile(NeighborhoodParser.S_REGEX_INCLUDE_RADIX, Pattern.CASE_INSENSITIVE);
    private static final Pattern P_REGEX_PATTERN = Pattern.compile(NeighborhoodParser.S_REGEX_PATTERN, Pattern.CASE_INSENSITIVE);
    private static final Pattern P_REGEX_RADIUS = Pattern.compile(NeighborhoodParser.S_REGEX_RADIUS, Pattern.CASE_INSENSITIVE);
    private static final Pattern P_REGEX_FOOTER = Pattern.compile(NeighborhoodParser.S_REGEX_FOOTER, Pattern.CASE_INSENSITIVE);
    private static final Pattern P_REGEX_FULL_SACN = Pattern.compile(NeighborhoodParser.S_REGEX_FULL_SACN, Pattern.CASE_INSENSITIVE);
 //   private static final Pattern P_REGEX_COORDINATE = Pattern.compile(NeighborhoodParser.sRegexCoordinate, Pattern.CASE_INSENSITIVE);
    
        private static final String N_LINE = System.lineSeparator();


    //**************************************************************************
    //*****************************CONSTRUCTORS*********************************
    //**************************************************************************
   
    
    //**************************************************************************
    //*****************************METHODS**************************************
    //**************************************************************************
    /**
     * Toma el contenido del string y lo convierte en un vecindario que se carga
     * en el objeto actual. Si el String de entrada no contiene un vecindario
     * completo se retorna null. Cualquier linea que no tenga el formato de un
     * comando de vecindario será ignorada.
     * @param sNeighborhood //String donde debe estar el bloque de veindario
     * @return
     */
    public static Neighborhood parse(String sNeighborhood)
    {
        Matcher sacnMatcher = NeighborhoodParser.P_REGEX_FULL_SACN.matcher(sNeighborhood);

        if (sacnMatcher.find())
        {
            String sacn = sacnMatcher.group(0);

            String nHead = parseHeader(sacn);
            String nDesc = parseDescription(sacn);
            Cell nRadix = parseRadix(sacn);
            boolean nIncRadix = parseIncludesRadix(sacn);
            int nRadius = parseRadius(sacn);
            List<Cell> nPattern = parsePattern(sacn);
            
            return new Neighborhood(nHead, nDesc, nRadix, nIncRadix, nRadius, nPattern);  
        }
        return null;
    }
    
    
    /*
     * Returns Neighborhood as a String Sample:
     * @NeighborhoodBlockIni(Moore r1); 
     * @NeighborhoodDesc=(Aqui va alguna descripcion del vecindario); 
     * @NeighborhoodRadius=(1);
     * @NeighborhoodBasePattern=(-1:0)(0:-1)(1:0)(0:1)(-1:-1)(-1:1)(1:-1)(1:1);
     * @NeighborhoodBlockEnd;
     */
    public static String convertToString(Neighborhood aNeighborhood)
    {
        StringBuilder neigh = new StringBuilder();
      
        neigh.append(NeighborhoodParser.getFormattedHeader(aNeighborhood));
        neigh.append(N_LINE);
        neigh.append(NeighborhoodParser.getFormattedDesc(aNeighborhood));
        neigh.append(N_LINE);
        neigh.append(NeighborhoodParser.getFormattedRadix(aNeighborhood));
        neigh.append(N_LINE);
        neigh.append(NeighborhoodParser.getFormattedIncludesRadix(aNeighborhood));
        neigh.append(N_LINE);
        neigh.append(NeighborhoodParser.getFormattedRadius(aNeighborhood));
        neigh.append(N_LINE);
        neigh.append(NeighborhoodParser.getFormattedPattern(aNeighborhood, Neighborhood.BOTH_PATTERNS));
        neigh.append(N_LINE);
        neigh.append(NeighborhoodParser.getFormattedFooter());
        neigh.append(N_LINE);
        return neigh.toString();
    }

    /*
     * @NeighborhoodConfigurationIni(Moore r1);
     */
    public static String getFormattedHeader(Neighborhood aNeighborhood)
    {
        StringBuilder header = new StringBuilder();

        header.append("@NeighborhoodBlockIni=(");
        header.append(aNeighborhood.getName());
        header.append(");");

        return header.toString();
    }

    /*
     * @NeighborhoodDesc =(Aqui va alguna descripcion del vecindario);
     */
    private static String getFormattedDesc(Neighborhood aNeighborhood)
    {
        StringBuilder desc = new StringBuilder();

        desc.append("@NeighborhoodDescription=(");
        desc.append(aNeighborhood.getDescription());
        desc.append(");");

        return desc.toString();
    }


    /*
     * @NeighborhoodPatternRadix = (3:3);
     */
    private static String getFormattedRadix(Neighborhood aNeighborhood)
    {
        StringBuilder radix = new StringBuilder();

        radix.append("@NeighborhoodPatternRadix=");
        radix.append(aNeighborhood.getRadix().toString());
        radix.append(";");

        return radix.toString();
    }

    private static String getFormattedIncludesRadix(Neighborhood aNeighborhood)
    {
        StringBuilder includesRadix = new StringBuilder();

        includesRadix.append("@NeighborhoodIncludesRadix=(");
        includesRadix.append(String.valueOf(aNeighborhood.getIncludesRadix()));
        includesRadix.append(");");

        return includesRadix.toString();
    }
    
    
    /*
     * @NeighborhoodRadius = (1);
     */
    private static String getFormattedRadius(Neighborhood aNeighborhood)
    {
        StringBuilder radius = new StringBuilder();

        radius.append("@NeighborhoodRadius=(");
        radius.append(aNeighborhood.getRadius());
        radius.append(");");

        return radius.toString();
    }

    /*
     * @NeighborhoodBasePattern =
     * (-1:0)(0:-1)(1:0)(0:1)(-1:-1)(-1:1)(1:-1)(1:1);
     */
    private static String getFormattedPattern(Neighborhood aNeighborhood, int selectorFlag)
    {
        StringBuilder formattedPattern = new StringBuilder();
        
        switch (selectorFlag)
        {
            case (Neighborhood.BASE_PATTERN): //Patrón del vecindario sin tomar el cuenta el radio
            {
                formattedPattern.append("@NeighborhoodBasePattern=");
                formattedPattern.append(NeighborhoodParser.getFormattedCellList(aNeighborhood.getCellsBaseList()));
                formattedPattern.append(";");
                break;
            }

            case (Neighborhood.FULL_PATTERN): //Patrón del vecindario ampliado según el radio
            {
                formattedPattern.append("@NeighborhoodFullPattern=");
                formattedPattern.append(NeighborhoodParser.getFormattedCellList(aNeighborhood.getCellsList()));
                formattedPattern.append(";");
                break;
            }

            case (Neighborhood.BOTH_PATTERNS): // Ambas versiones del patrón
            {
                formattedPattern.append("@NeighborhoodBasePattern=");
                formattedPattern.append(NeighborhoodParser.getFormattedCellList(aNeighborhood.getCellsBaseList()));
                formattedPattern.append(";");
                formattedPattern.append(N_LINE);
                formattedPattern.append("@NeighborhoodFullPattern=");
                formattedPattern.append(NeighborhoodParser.getFormattedCellList(aNeighborhood.getCellsList()));
                formattedPattern.append(";");
                break;
            }

            default:
            {
                return null;
            }
        }

        return formattedPattern.toString();
    }

    /**
     * Formatea el bloque de celdas del patrón del vecindario.
     * @param cellList
     * @return 
     */
    public static String getFormattedCellList(List<Cell> cellList)
    {
        StringBuilder formatedCoordinates = new StringBuilder();

        for (Cell aCell : cellList)
        {
            formatedCoordinates.append(aCell.getCoordinatesAsString());
        }

        return formatedCoordinates.toString();
    }

    /**
     * @NeighborhoodConfigurationEnd;
     * @return 
     */
    private static String getFormattedFooter()
    {
        String footer = "@NeighborhoodBlockEnd;";
        return footer;
    }


    /*
     * If the String passed as parameter has the correct format the name
     * property is extracted else null returned. 
     * Ex: @NeighborhoodConfigurationIni(Moore r1);
     * RETURNS : "Moore r1"
     */
    public static String parseHeader(String oLine)
    {
        Matcher pMatcher = NeighborhoodParser.P_REGEX_HEADER.matcher(oLine);

        if (pMatcher.find())
        {
            return (pMatcher.group(2));
        }

        return null;
    }

    /**
     *
     * @param oLine
     * @return
     */
    public static String parseDescription(String oLine)
    {
        Matcher pMatcher = NeighborhoodParser.P_REGEX_DESC.matcher(oLine);

        if (pMatcher.find())
        {
            return (pMatcher.group(2));
        }
        return null;
    }

    /**
     *
     * @param oLine
     * @return
     */
    public static int parseRadius(String oLine)
    {
        Matcher pMatcher = NeighborhoodParser.P_REGEX_RADIUS.matcher(oLine);

        if (pMatcher.find())
        {
            return (Integer.parseInt(pMatcher.group(2)));
        }

        return -1;
    }

    /**
     *
     * @param oLine
     * @return
     */
    public static Cell parseRadix(String oLine)
    {
        Matcher pMatcher = NeighborhoodParser.P_REGEX_RADIX.matcher(oLine);
        
        if (pMatcher.find())
        {
            return CellParser.parse(pMatcher.group(2));
        }
        return null;
    }

    public static boolean parseIncludesRadix(String oLine)
    {
        Matcher pMatcher = NeighborhoodParser.P_REGEX_INCLUDE_RADIX.matcher(oLine);
        String includesRadix = "";

        if (pMatcher.find())
        {
            includesRadix = pMatcher.group(2);
        }
        else
        {
            throw new IllegalArgumentException("function: parseIncludesRadix - Valor desconocido: " + includesRadix);
        }
        return (Boolean.valueOf(includesRadix));
    }
    
    /**
     * @param oLine
     * @return 
     */
    public static List<Cell> parsePattern(String oLine)
    {
        int row;
        int col;
        Short stateId = 0;
        Matcher pMatcher = NeighborhoodParser.P_REGEX_PATTERN.matcher(oLine); // Busca el patron en la linea

        if (pMatcher.find())
        {
            List<Cell> basicPattern = new ArrayList<>();
            pMatcher = CellParser.P_REGEX_COORDINATE.matcher(pMatcher.group(2));
            while (pMatcher.find())  // Busca las coordenadas individuales dentro del patron
            {
                row = Integer.parseInt(pMatcher.group(2));
                col = Integer.parseInt(pMatcher.group(4));
                basicPattern.add(new Cell(row, col, stateId));
            }
            return basicPattern;
        }
        return null;
    }

    /**
     *
     * @param oLine La cadena a ser analizada.
     * @return Verdadero si el formato es correcto.
     */
    public static boolean parseFooter(String oLine)
    {
        Matcher pMatcher = NeighborhoodParser.P_REGEX_FOOTER.matcher(oLine);
        return (pMatcher.find());
    }

    //*****************************END OF CLASS*********************************
    //*****************************END OF CLASS*********************************
    //*****************************END OF CLASS*********************************
    
}
