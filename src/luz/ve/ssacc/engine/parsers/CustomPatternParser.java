/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package luz.ve.ssacc.engine.parsers;

import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import luz.ve.ssacc.engine.components.Cell;
import luz.ve.ssacc.engine.components.CustomPattern;


/**
 *
 * @author Renzo Ciafardone Sciannelli
 */
public class CustomPatternParser
{
     //**************************************************************************
    //*****************************VARIABLES************************************
    //**************************************************************************
    /*
     * @CustomPatternIni=();
     * @CustomPattern =
     * (-1:0)(0:-1)(1:0)(0:1)(-1:-1)(-1:1)(1:-1)(1:1);
     * @NeighborhoodConfigurationEnd;
     */

    // Expresiones regulares
    private static final String S_REGEX_HEADER = "@(PatternBlockIni|PBI)\\(([^@]+)\\);"; // marca el inicio del patron y contiene la propiedad _name, no puede estar vacia.
    private static final String S_REGEX_DESC = "@(PatternDescription|PDC)=\\(([^@]*)\\);"; // la descripcion, puede estar vacia.
    //private static final String S_REGEX_PATTERN = "@(Pattern|PTR)=((\\(-?[0-9]+(:-?[0-9]+)*\\))+);";  // el patron.
    private static final String S_REGEX_PATTERN = "@(Pattern|PTR)=([^@]+);";  // el patron.
    
    private static final String S_REGEX_FOOTER = "@(PatternBlockEnd|PBE);"; // cierra el patron
    
    private static final String S_REGEX_FULL_SACP = S_REGEX_HEADER + "([\\s\\S])*" + S_REGEX_FOOTER; // Toda la informacion del patron
 
    private static final Pattern P_REGEX_HEADER = Pattern.compile(CustomPatternParser.S_REGEX_HEADER, Pattern.CASE_INSENSITIVE);
    private static final Pattern P_REGEX_DESC = Pattern.compile(CustomPatternParser.S_REGEX_DESC, Pattern.CASE_INSENSITIVE);
    private static final Pattern P_REGEX_PATTERN = Pattern.compile(CustomPatternParser.S_REGEX_PATTERN, Pattern.CASE_INSENSITIVE);
    private static final Pattern P_REGEX_FOOTER = Pattern.compile(CustomPatternParser.S_REGEX_FOOTER, Pattern.CASE_INSENSITIVE);
    
    private static final Pattern P_REGEX_FULL_SACP = Pattern.compile(CustomPatternParser.S_REGEX_FULL_SACP, Pattern.CASE_INSENSITIVE);
  
    //**************************************************************************
    //*****************************CONSTRUCTORS*********************************
    //**************************************************************************
    
    
    //**************************************************************************
    //*****************************METHODS**************************************
    //**************************************************************************
  
    
    /**
     * Lee el String y devuelve la instancia del objeto patrón corrrespondiente. 
     * @param sSac
     * @return 
     */
    public static CustomPattern parse(String sSac)
    {
        Matcher sacpMatcher = CustomPatternParser.P_REGEX_FULL_SACP.matcher(sSac);

        if (sacpMatcher.find())
        {
            String sacp = sacpMatcher.group(0);
            return new CustomPattern(CustomPatternParser.parseHeader(sacp),
                                     CustomPatternParser.parseDescription(sacp),
                                     CustomPatternParser.parseCells(sacp));
        }

        return null;
    }

    /**
     * Lee el String para obtener el encabezado del patrón.
     * @param oLine
     * @return 
     */
    public static String parseHeader(String oLine)
    {
        Matcher pMatcher = CustomPatternParser.P_REGEX_HEADER.matcher(oLine);

        if (pMatcher.find())
        {
            return (pMatcher.group(2));
        }

        return null;
    }


   /**
    * Lee el String para obtener la descripción del patrón.
    * @param oLine
    * @return 
    */
    public static String parseDescription(String oLine)
    {
        Matcher pMatcher = CustomPatternParser.P_REGEX_DESC.matcher(oLine);

        if (pMatcher.find())
        {
            return (pMatcher.group(2));
        }
        return null;
    }

    /**
     * Lee el String para producir la lista de celdas del patrón.
     * @param oLine
     * @return 
     */
    public static List<Cell> parseCells(String oLine)
    {
        Matcher pMatcher = CustomPatternParser.P_REGEX_PATTERN.matcher(oLine); // Busca el patrón en la linea

        if (pMatcher.find())
        {
            List<Cell> basicPattern = new ArrayList<>();

            pMatcher = CellParser.P_REGEX_CELL.matcher(pMatcher.group(2));

            while (pMatcher.find())  // Busca las coordenadas individuales dentro del patrón
            {
                Cell nCell = CellParser.parse(pMatcher.group(0));
                basicPattern.add(nCell);
            }

            return basicPattern;
        }
        return null;
    }

    /**
     * Detecta el final del bloque de patrón dentro del String.
     * @param oLine
     * @return 
     */
    public static boolean parseFooter(String oLine)
    {
        Matcher pMatcher = CustomPatternParser.P_REGEX_FOOTER.matcher(oLine);
        return(pMatcher.find());
    }

    
    /**
     * Convierte el patrón ingresado en un String con el formato de archivo.
     * @param aPattern
     * @return 
     */
    public static String convertToString(CustomPattern aPattern)
    {
        StringBuilder neigh = new StringBuilder();
      
        neigh.append(CustomPatternParser.getFormattedHeader(aPattern));
        neigh.append(System.lineSeparator());
        neigh.append(CustomPatternParser.getFormattedDesc(aPattern));
        neigh.append(System.lineSeparator());
        neigh.append(CustomPatternParser.getFormattedPattern(aPattern));
        neigh.append(System.lineSeparator());
        neigh.append(CustomPatternParser.getFormattedFooter(aPattern));
        neigh.append(System.lineSeparator());
        
        return neigh.toString();
    }

    /**
     * Devuelve el encabezado del patrón con el formato de archivo.
     * @param aPattern
     * @return 
     */
    public static String getFormattedHeader(CustomPattern aPattern)
    {
        StringBuilder header = new StringBuilder();

        header.append("@PatternBlockIni(");
        header.append(aPattern.getName());
        header.append(");");

        return header.toString();
    }

   /**
    * Devuelve la descripción del patrón con el formato de archivo.
    * @param aPattern
    * @return 
    */
    private static String getFormattedDesc(CustomPattern aPattern)
    {
        StringBuilder desc = new StringBuilder();

        desc.append("@PatternDescription=(");
        desc.append(aPattern.getDescription());
        desc.append(");");

        return desc.toString();
    }

    /**
     * Formatea el patrón como debe aparecer en el archivo.
     * @param pattern
     * @return
     */
    private static String getFormattedPattern(CustomPattern pattern)
    {
        StringBuilder formattedPattern = new StringBuilder();

        formattedPattern.append("@Pattern=");
        formattedPattern.append(CustomPatternParser.getFormattedCellList(pattern.getCells(), true));
        formattedPattern.append(";");

        return formattedPattern.toString();
    }

    /**
     * Devuelve la lista de celdas con el formato de archivo.
     *
     * @param cellList //lista de celdas del patrón
     * @param asList //Formatea el texto como una lista para facilitar la
     * lectura
     * @return
     */
    private static String getFormattedCellList(List<Cell> cellList, boolean asList)
    {
        StringBuilder formatedCoordinates = new StringBuilder();

       
        for (Cell aCell : cellList)
        {
         /*   if (asList)
            {
                formatedCoordinates.append(System.lineSeparator());
            }
          */  
            formatedCoordinates.append(aCell.toString());
        }

        formatedCoordinates.append(System.lineSeparator());
        return formatedCoordinates.toString();
    }

    private static String getFormattedFooter(CustomPattern aPattern)
    {
        String footer = "@PatternBlockEnd;";
        return footer;
    }


  
    //*****************************END OF CLASS*********************************
    //*****************************END OF CLASS*********************************
    //*****************************END OF CLASS*********************************
    

}
