/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package luz.ve.ssacc.engine.parsers;

import java.util.regex.Matcher;
import java.util.regex.Pattern;
import luz.ve.ssacc.engine.components.Condition;
import luz.ve.ssacc.engine.components.Rule;
import luz.ve.ssacc.utilities.SacUtility;


/**
 *
 * @author "Renzo Ciafardone Sciannelli"
 */
public final class RuleParser
{
    //**************************************************************************
    //*****************************VARIABLES************************************
    //**************************************************************************

    public static final int INVALID_RULE_ID = -1;
    public static final String DEFAULT_ID_STRING = "000";
    
    private static final String S_REGEX_RULE_INI = "@Rule\\(([0-9]{3})\\)=\\(([0-9]{4})>>([0-9]{4})\\);";
    private static final String S_REGEX_RULE_END = "@Rule\\(([0-9]{3})\\);";
    private static final String S_REGEX_RULE = S_REGEX_RULE_INI +  "([\\s\\S]*?)" + S_REGEX_RULE_END;
    
    private static final Pattern P_REGEX_RULE_INI = Pattern.compile(RuleParser.S_REGEX_RULE_INI, Pattern.CASE_INSENSITIVE);
    public static Pattern P_REGEX_RULE = Pattern.compile(RuleParser.S_REGEX_RULE, Pattern.CASE_INSENSITIVE);

    //**************************************************************************
    //*****************************CONSTRUCTORS*********************************
    //**************************************************************************
    
    
    //**************************************************************************
    //*****************************METHODS**************************************
    //**************************************************************************
    /**
     * Cheque si el string contiene el formato correcto para la regla y llena
     * los campos correspondientes. Sample: @Rule(001)=(0001:0000);
     *
     * @param oLine
     * @return
     */
    public static Rule parse(String oLine)
    {
        Rule nRule;
        Matcher pMatcher = RuleParser.P_REGEX_RULE_INI.matcher(oLine);
        if (pMatcher.find())
        {
            Short sId = Short.parseShort(pMatcher.group(1));
            Short stateIni = Short.parseShort(pMatcher.group(2));
            Short stateEnd = Short.parseShort(pMatcher.group(3));

            nRule = new Rule(sId, stateIni, stateEnd);
        }
        else
        {
            SacUtility.showParsingError("Bloque Regla - Inicio: Formato incorrecto." + "\n\nContenido:\n" + oLine);
            return null;
        }

        nRule = addConditionsToRule(nRule, oLine);
        
        if (nRule.getConditions().isEmpty())
        {
            SacUtility.showParsingError("Bloque de Reglas - RuleParser - Ninguna condicion fue ingresada.");
            return null;
        }

        return nRule;
    }

    private static Rule addConditionsToRule(Rule nRule, String oLine)
    {
        Matcher pMatcher = ConditionParser.P_REGEX_CONDITION.matcher(oLine);

        while (pMatcher.find())
        {
            Condition nCond;
            nCond = ConditionParser.parse(pMatcher.group(0));

            if (nCond == null)
            {
                SacUtility.showParsingError("Bloque de Reglas - RuleParser - Condicion: Formato incorrecto.");
                return null;
            }

            nRule.addCondition(nCond);
        }

        return nRule;
    }

    
    /*
     * Return the rule with the format to be saved into the SACR file
     */
    public static String convertToString(Rule oRule)
    {
        StringBuilder sRule = new StringBuilder();

        sRule.append(getFormattedHeader(oRule)).append("\n");
        sRule.append(getFormattedConditions(oRule)).append("\n");
        sRule.append(getFormattedFooter(oRule)).append("\n");

        return sRule.toString();
    }

    /*
     * Retorna un string con la cabecera de la regla. @Rule(001)=(0001:0000);
     */
    public static String getFormattedHeader(Rule aRule)
    {
        StringBuilder sHeader = new StringBuilder();

        sHeader.append("@Rule(").append(getFormattedId(aRule)).append(")=(");
        sHeader.append(StateParser.formatId(aRule.getStateIniId())).append(">>");
        sHeader.append(StateParser.formatId(aRule.getStateEndId())).append(");");

        return sHeader.toString();
    }

    public static String getFormattedTitle(Rule aRule)
    {
        StringBuilder sHeader = new StringBuilder();

        sHeader.append("(").append(getFormattedId(aRule)).append(")=(");
        sHeader.append(StateParser.formatId(aRule.getStateIniId())).append(">>");
        sHeader.append(StateParser.formatId(aRule.getStateEndId())).append(")");

        return sHeader.toString();
    }

    public static String getFormattedId(Rule rule)
    {
        if (!(rule.getId() < StateParser.ID_MIN || rule.getId() > StateParser.ID_MAX))
        {
            String format = "%0" + Integer.toString(RuleParser.DEFAULT_ID_STRING.length()) + "d";

            String idFormated = String.format(format, rule.getId());

            return (idFormated);
        }
        
        return "NoID";
    }
    

    /*
     * Retorna un string con la cabecera de la regla. @Rule(001);
     */
    public static String getFormattedFooter(Rule aRule)
    {
        StringBuilder sFooter = new StringBuilder();

        sFooter.append("@Rule(").append(getFormattedId(aRule)).append(");");

        return sFooter.toString();
    }

    public static String getFormattedConditions(Rule aRule)
    {
        String eol = System.getProperty("line.separator");
        StringBuilder sConditions = new StringBuilder();

        int numCond = 0;
        int listSize = aRule.getConditions().size();

        for (Condition aCond : aRule.getConditions())
        {
            sConditions.append(aCond.toString());
            if (numCond < listSize - 1)
            {
                sConditions.append(eol);
            }
            numCond++;
        }

        return sConditions.toString();
    }

    
//******************************************************************************
//******************************************************************************
//******************************************************************************

}
