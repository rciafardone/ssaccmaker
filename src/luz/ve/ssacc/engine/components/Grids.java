/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package luz.ve.ssacc.engine.components;

import java.awt.Color;
import luz.ve.ssacc.engine.parsers.GridsParser;
import luz.ve.ssacc.utilities.SacUtility;

/**
 *
 * @author Renzo Ciafardone Sciannelli
 */
public final class Grids
{
    public static final int FRONTERA_ABIERTA = 0;
    public static final int FRONTERA_CERRADA = 1;

    public static final String DEFAULT_NAME = "Nombre Grid";
    public static final String DEFAULT_DESCRIPTION = "Descripción aqui";

    public static final Color DEFAULT_BORDER_COLOR = Color.WHITE;

    public static final int DEFAULT_ROW_NUMBER = 40;
    public static final int DEFAULT_COL_NUMBER = 90;
    public static final int DEFAULT_CELL_SIZE = 10;
    public static final int DEFAULT_NUM_STEPS = 100;
    public static final int DEFAULT_DELAY = 100;
    
    
    public static final int ROW_NUMBER_MIN = 1;
    public static final int ROW_NUMBER_MAX = 1000;
    public static final int COLUMN_NUMBER_MIN = 1;
    public static final int COLUMN_NUMBER_MAX = 1000;
    public static final int CELL_SIZE_MIN = 1;
    public static final int CELL_SIZE_MAX = 100;
        
    private String name;
    private String description;
    private int frontierType;
    private int rows;
    private int columns;
    private int cellSize;
    private boolean drawBorderFlag;
    private Color borderColor;
    private int numSteps;
    private int animationDelay;
    private State background;
    
    private Grid gridOri; //preserves initial state
    private Grid gridIni;
    private Grid gridEnd;

    public Grids()
    {
        this.name = Grids.DEFAULT_NAME;
        this.description = Grids.DEFAULT_DESCRIPTION;
        this.frontierType = Grids.FRONTERA_CERRADA;
        this.rows = Grids.DEFAULT_ROW_NUMBER;
        this.columns = Grids.DEFAULT_COL_NUMBER;
        this.cellSize = Grids.DEFAULT_CELL_SIZE;
        this.drawBorderFlag = true;
        this.borderColor = Grids.DEFAULT_BORDER_COLOR;
        this.numSteps = Grids.DEFAULT_NUM_STEPS;
        this.animationDelay = Grids.DEFAULT_DELAY;
        this.background = new State();
        this.gridOri = new Grid(this.rows, this.columns, this.background);
        this.gridIni = new Grid(this.rows, this.columns, this.background);
        this.gridEnd = new Grid(this.rows, this.columns, this.background);
    }

    public Grids(
        String name,
        String description,
        int frontierType,
        int rows,
        int columns,
        int cellSize,
        boolean drawBorder,
        Color borderColor,
        int numSteps,
        int animationDelay,
        State background)
    {
        this.name = name;
        this.description = description;
        this.frontierType = frontierType;
        this.rows = rows;
        this.columns = columns;
        this.cellSize = cellSize;
        this.drawBorderFlag = drawBorder;
        this.borderColor = borderColor;
        this.numSteps = numSteps;
        this.animationDelay = animationDelay;
        this.background = background;

        this.gridOri = new Grid(rows, columns, background);
        this.gridIni = new Grid(rows, columns, background);
        this.gridEnd = new Grid(rows, columns, background);
    }

    public Grids(
        String name,
        String description,
        int frontierType,
        int rows,
        int columns,
        int cellSize,
        boolean drawBorder,
        Color borderColor,
        int numSteps,
        int animationDelay,
        State background,
        Grid initialGrid
    )
    {
        this.name = name;
        this.description = description;
        this.frontierType = frontierType;
        
        this.rows = rows;
        this.columns = columns;
        this.cellSize = cellSize;
        
        this.drawBorderFlag = drawBorder;
        this.borderColor = borderColor;
        
        this.numSteps = numSteps;
        this.animationDelay = animationDelay;
        
        this.background = background;
        
        this.gridOri = new Grid(rows, columns, background);
        this.gridIni = new Grid(rows, columns, background);
        this.gridEnd = new Grid(rows, columns, background);
        
        SacUtility.gridCopy(initialGrid, this.gridOri);
        SacUtility.gridCopy(initialGrid, this.gridIni); 
        SacUtility.gridCopy(initialGrid, this.gridEnd);
    }
    
    public void setName(String name)
    {
        this.name = name;
    }

    public void setDescription(String description)
    {
        this.description = description;
    }
    
    public String getName()
    {
        return this.name;
    }

    public String getDescription()
    {
        return this.description;
    }

    public void setFrontierType(int frontierType)
    {
        this.frontierType = frontierType;
    }

    public int getFrontierType()
    {
        return this.frontierType;
    }

    public void setRows(int rows)
    {
        this.rows = rows;
    }

    public int getRows()
    {
        return this.rows;
    }

    public void setColumns(int cols)
    {
        this.columns = cols;
    }
    
    public int getColumns()
    {
        return this.columns;
    }

    public void setCellSize(int size)
    {
        this.cellSize = size;
    }

    public int getCellSize()
    {
        return this.cellSize;
    }

    public void setBorderFlag(boolean drawBorderFlag)
    {
        this.drawBorderFlag = drawBorderFlag;
    }

    public boolean isBorderOn()
    {
        return this.drawBorderFlag;
    }

    public void setBorderColor(Color borderColor)
    {
        this.borderColor = borderColor;
    }

    public Color getBorderColor()
    {
        return this.borderColor;
    }
    
    public void setNumSteps(int steps)
    {
        this.numSteps = steps;
    }

    public int getNumSteps()
    {
        return this.numSteps;
    }

    public void setAnimationDelay(int delay)
    {
        this.animationDelay = delay;
    }

    public int getAnimationDelay()
    {
        return this.animationDelay;
    }

    public void setBackground(State newBackground)
    {
        this.background = newBackground;
    }

    public State getBackground()
    {
        return this.background;
    }
    
    public Grid getGridOri()
    {
        return this.gridOri;
    }

    public void setGridOri(Grid gridOri)
    {
        this.gridOri = gridOri;
    }
    
    public void setGridIni(Grid gridIni)
    {
        this.gridIni = gridIni;
    }

    public Grid getGridIni()
    {
        return this.gridIni;
    }

    public Grid getGridEnd()
    {
        return this.gridEnd;
    }

    public void setGridEnd(Grid newGrid)
    {
        this.gridEnd = newGrid;
    }

    
    
    
     /**
     * Esta funcion agrega el patron en el punto especificado.
     * @param origin Coordenadas donde se coloca el patrón
     * @param pattern 
     * @param overrideOption
     */
    public void addPattern(Cell origin, CustomPattern pattern, int overrideOption)
    {
        for (Cell patternCell : pattern.getCells(origin))
        {
            this.setArrayState(patternCell);
        }
    }
    
    /**
     * Coloca el estado de la celda en la pocision del grid.
     * @param currentCell 
     */
    public void setArrayState(Cell currentCell)
    {
        switch (this.getFrontierType())
        {
            case Grids.FRONTERA_CERRADA:
            {
                this.setStateWithClosedFrontier(currentCell);
            }
            case Grids.FRONTERA_ABIERTA:
            {
                this.setStateWithOpenFrontier(currentCell);
            }
        }
    }
      
     /**
     * Recibe una celda. normaliza las coordenadas dependiendo del tamaño del grid
     * y coloca el estado de la celda en dichas coordianas.
     * @param aCell
     * @return 
     */
    private void setStateWithClosedFrontier(Cell aCell)
    {
        int newRow = Grids.relocateCoordinate(aCell.getRow(),this.getRows());
        int newCol = Grids.relocateCoordinate(aCell.getColumn(),this.getColumns());
        
        this.getGridEnd().getArray()[newRow][newCol] = aCell.getStateId();
    }
    
    /**
     * Al tener frontera abierta simplemente ignora los estados que esten fuera de los limites del grid
     * @param aCell 
     */
    private void setStateWithOpenFrontier(Cell aCell)
    {
        if ((aCell.getRow() >= 0) && (aCell.getRow() < this.getRows()))
        {
            if ((aCell.getColumn() >= 0) && (aCell.getColumn() < this.getColumns()))
            {
                this.getGridEnd().getArray()[aCell.getRow()][aCell.getColumn()] = aCell.getStateId();
            }
        }
    }
    
    private static int relocateCoordinate(int coordinate, int gridLenght)
    {
        int relocatedCoordinate;
        if ((coordinate >= 0) && (coordinate < gridLenght))
        {
            relocatedCoordinate = coordinate; // Puesto que la coordenada esta dentro de la dimension del grid no hacemos nada
        }
        else
        {
            relocatedCoordinate = coordinate % gridLenght; // Ajustamos
            if (relocatedCoordinate < 0)
            {
                relocatedCoordinate = relocatedCoordinate + gridLenght;
            }
        }
        return relocatedCoordinate;
    }
   
    public Short getStateId(Cell currentCell)
    {
        switch (this.getFrontierType())
        {
            case Grids.FRONTERA_ABIERTA:
                return this.getStateIdWithOpenFrontier(currentCell);

            case Grids.FRONTERA_CERRADA:
                return this.getStateIdWithClosedFrontier(currentCell);

        }
        return null;
    }
    
    
    public Short getStateIdWithClosedFrontier(Cell cell)
    {
        int currentRow;
        int currentColumn;

        if (cell.getRow() >= 0 && cell.getRow() < this.getRows())
        {
            currentRow = cell.getRow();  //Si esta dentro de los limites del grid lo dejo asi
        }
        else
        {
            if (cell.getRow() < 0)
            {
                currentRow = this.getRows() + cell.getRow();
            }
            else
            {
                currentRow = cell.getRow() - this.getRows();
            }
        }

        if (cell.getColumn() >= 0 && cell.getColumn() < this.getColumns())
        {
            currentColumn = cell.getColumn();  //Si esta dentro de los limites del grid lo dejo asi
        }
        else
        {
            if (cell.getColumn() < 0)
            {
                currentColumn = this.getColumns() + cell.getColumn();
            }
            else
            {
                currentColumn = cell.getColumn() - this.getColumns();
            }
        }

        return this.getGridIni().getArray()[currentRow][currentColumn];
    }
   
/**
 * Devuelve el estado de la celda del Grid de origen
 * @param cell
 * @return 
 */    
    public Short getStateIdWithOpenFrontier(Cell cell)
    {
        if ((cell.getRow() >= 0) && (cell.getRow() < this.getRows()) && 
            (cell.getColumn() >= 0) && (cell.getColumn() < this.getColumns()))
        {
            return this.getGridIni().getArray()[cell.getRow()][cell.getColumn()];
        }
        else
        {
            return this.getBackground().getId();
        }
    }

    /**
     * Actualiza el Grid Inicial (gridIni) con los datos del Grid Final (grid)
     */
    public void update()
    {
        for (int rowIndex = 0; rowIndex < this.getRows(); rowIndex++)
        {
            System.arraycopy(
                this.gridEnd.getArray()[rowIndex], 0, 
                this.gridIni.getArray()[rowIndex], 0, 
                this.getColumns());
        }
    }

    /**
     * Redimensiona los grids al tamaño especificado concervado los datos
     * originales cuando es posible. Si el nuevo tamaño es menor al original
     * toda la información fuera de los limites es truncada.
     *
     * @param rows
     * @param columns
     */
    public void resize(int rows, int columns)
    {
        Grid newGridOri = new Grid(rows, columns, this.getBackground());
        Grid newGridIni = new Grid(rows, columns, this.getBackground());
        Grid newGridEnd = new Grid(rows, columns, this.getBackground());

        for (int r = 0; r < rows; r++)
        {
            for (int c = 0; c < columns; c++)
            {
                if (r < this.getGridIni().getRows() && c < this.getGridIni().getColumns())
                {
                    newGridOri.getArray()[r][c] = this.getGridOri().getStateId(r, c);
                    newGridIni.getArray()[r][c] = this.getGridOri().getStateId(r, c);
                    newGridEnd.getArray()[r][c] = this.getGridOri().getStateId(r, c);
                }
            }
        }

        this.setGridOri(newGridOri);
        this.setGridIni(newGridIni);
        this.setGridEnd(newGridEnd);
    }
 
    @Override
    public String toString()
    {
        return GridsParser.convertToString(this);
    }
}
