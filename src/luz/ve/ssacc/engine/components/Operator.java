/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package luz.ve.ssacc.engine.components;

import luz.ve.ssacc.engine.parsers.OperatorParser;

/**
 *
 * @author renzo
 */



public final class Operator
{

    private String symbol;
    public static final String[] OPERATORS_LIST =
    {
        "==", "!=", ">=", "<=", "<", ">" 
    };
    
    public Operator()
    {
        this.setSymbol(OPERATORS_LIST[0]);
    }
    
    public Operator(Operator cOperator)
    {
        this.setSymbol(cOperator.getSymbol());
    }

    public Operator(String sOperator)
    {
        this.setSymbol(sOperator);
    }
    

    public String getSymbol()
    {
        return this.symbol;
    }

    public void setSymbol(String sSymbol)
    {
        this.symbol = sSymbol;
    }

    @Override
    public String toString()
    {
        return OperatorParser.getFormatedOperator(this);
    }

}
