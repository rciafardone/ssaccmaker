/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package luz.ve.ssacc.engine.components;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Iterator;
import luz.ve.ssacc.engine.parsers.AutomatonParser;
import luz.ve.ssacc.engine.parsers.StatisticsParser;
import luz.ve.ssacc.utilities.SacUtility;

/**
 *
 * @author Renzo Ciafardone Sciannelli
 */
public class Automaton
{

    private String name;
    private String id;
    private String description;
    private States states;
    private Neighborhood neighborhood;
    private Rules rules;
    private Grids grids;
    private HashMap<Short, Integer> statistics;
    
    public static final String DEFAULT_NAME = "Nombre Automata Celular";
    public static final String DEFAULT_DESCRIPTION = "Descripción aqui";
    /**
     *
     * @param name
     * @param id
     * @param description
     * @param states
     * @param rules
     * @param neighborhood
     * @param grids
     */
    public Automaton(String name,
                     String id,
                     String description,
                     States states,
                     Rules rules,
                     Neighborhood neighborhood,
                     Grids grids)
    {
        this.name = name;
        this.id = id;
        this.description = description;
        this.states = states;
        this.rules = rules;
        this.neighborhood = neighborhood;
        this.grids = grids;
    }

    public Automaton(Automaton newCA)
    {
        this(
            newCA.name,
            newCA.id,
            newCA.description,
            newCA.states,
            newCA.rules,
            newCA.neighborhood,
            newCA.grids
        );
    }

    
    public Automaton()
    {
        this.name = Automaton.DEFAULT_NAME;
        this.id = Automaton.generateNewId();
        this.description = Automaton.DEFAULT_DESCRIPTION;
        this.states = new States();
        this.rules = new Rules();
        this.neighborhood = new Neighborhood();
        this.grids = new Grids();
    }

    
    
    /**
     * Ejecuta las reglas para cada una de las celdas del gridIni y almacena los
     * resultados en el gridEnd. Luego actualiza gridIni con gridEnd
     * @return 
     */
    public Grid step()
    {
        this.setStatistics(); //coloca las estadisticas en 0;
        for (int rowIndex = 0; rowIndex < this.getGrids().getRows(); rowIndex++)
        {
            for (int colIndex = 0; colIndex < this.getGrids().getColumns(); colIndex++)
            {
                this.getRules().apply(this.getGrids().getGridIni().getCell(rowIndex, colIndex), this);
                this.addStatistic(this.getGrids().getGridEnd().getStateId(rowIndex, colIndex));
            }
        }
        this.getGrids().update();
        return this.getGrids().getGridEnd();
    }

    /**
     *
     * @return un String con el automata formateado como debe aparecer en
     * el archivo
     */
    @Override
    public String toString()
    {
        return AutomatonParser.convertToString(this);
    }

    public String toStringWithoutCells()
    {
        return AutomatonParser.convertToStringX(this);
    }

    /**
     * Inicia el mapa de las estadisticas para todos los estados en 0.
     */
    public void setStatistics()
    {
        this.statistics = new HashMap<>();

        for (State state : this.states.getList())
        {
            this.statistics.put(state.getId(), 0);
        }
    }

    /**
     * Aumenta en 1 el estado indicado en el hashmap.
     * @param stateId 
     */
    public void addStatistic(Short stateId)
    {
        this.statistics.put(stateId, this.statistics.get(stateId) + 1);
    }

    /**
     * Devuelve cada estado con su cantidad en cada paso.
     *
     * @return
     */
    public String getFormattedStepStatistics()
    {
        Iterator<Short> keySetIterator = this.statistics.keySet().iterator();

        StringBuilder stats = new StringBuilder();
        while (keySetIterator.hasNext())
        {
            Short key = keySetIterator.next();
            stats.append("s(").append(key).append(")=");
            stats.append(this.statistics.get(key)).append(";");
            stats.append(SacUtility.EOL);
        }
        return stats.toString();
    }
   
    
    /**
     * Crea el encabezado del archivo de estadisticas.
     * @return 
     */
    public String getFormattedStatsHeader()
    {
     Iterator<Short> keySetIterator = this.statistics.keySet().iterator();

        StringBuilder header = new StringBuilder();
        header.append(StatisticsParser.S_HEADER);
        while (keySetIterator.hasNext())
        {
            Short key = keySetIterator.next();
            header.append("s(").append(key).append(")").append(";");
        }
        header.append(SacUtility.EOL);
        return header.toString();
    }
    
    
    /**
     * Devuelve un string con las cantidades de los estados en cada paso.
     * num_paso;stat_state_1;stat_state_2;stat_state_3;...;stat_state_n;
     * p0;2;3;3;1;
     *
     * @param cStep
     * @return
     */
    public String getFormattedStatesStatsByStep(Integer cStep)
    {
        Iterator<Short> keySetIterator = this.statistics.keySet().iterator();

        StringBuilder stats = new StringBuilder();
        stats.append("p").append(cStep.toString()).append(";");
        while (keySetIterator.hasNext())
        {
            Short key = keySetIterator.next();
            stats.append(this.statistics.get(key)).append(";");
        }
        stats.append(SacUtility.EOL);
        return stats.toString();
    }

    /**
     * @return the name
     */
    public String getName()
    {
        return name;
    }

    /**
     * @param name the name to set
     */
    public void setName(String name)
    {
        this.name = name;
    }

    /**
     * @return the id
     */
    public String getId()
    {
        return id;
    }

    /**
     * @param id the id to set
     */
    public void setId(String id)
    {
        this.id = id;
    }

    /**
     * @return the description
     */
    public String getDescription()
    {
        return description;
    }

    /**
     * @param description the description to set
     */
    public void setDescription(String description)
    {
        this.description = description;
    }

    /**
     * @return the states
     */
    public States getStates()
    {
        return states;
    }

    /**
     * @param states the states to set
     */
    public void setStates(States states)
    {
        this.states = states;
    }

    /**
     * @return the neighborhood
     */
    public Neighborhood getNeighborhood()
    {
        return neighborhood;
    }

    /**
     * @param neighborhood the neighborhood to set
     */
    public void setNeighborhood(Neighborhood neighborhood)
    {
        this.neighborhood = neighborhood;
    }

    /**
     * @return the rules
     */
    public Rules getRules()
    {
        return rules;
    }

    /**
     * @param rules the rules to set
     */
    public void setRules(Rules rules)
    {
        this.rules = rules;
    }

    /**
     * @return the grids
     */
    public Grids getGrids()
    {
        return grids;
    }

    /**
     * @param grids the grids to set
     */
    public void setGrids(Grids grids)
    {
        this.grids = grids;
    }

    /**
     * Hace un chequeo general de los elementos básicos del autómata, si todos
     * cumplen con los requerimientos mínimos se devuelve verdadero
     *
     * @param currentCA
     * @return
     */
    public static boolean ready(Automaton currentCA)
    {
        if (currentCA == null)
        {
            SacUtility.showWarning("El Automata celular debe ser definido antes de poder correr una animación");
            return false;
        }

        if (currentCA.getStates() == null)
        {
            SacUtility.showWarning("El Automata celular debe poseer un conjunto de estados válidos.");
            return false;
        }

        if (currentCA.getNeighborhood() == null)
        {
            SacUtility.showWarning("El Automata celular debe poseer un vecindario válido.");
            return false;

        }
        
        if (currentCA.getRules() != null)
        {
            if (currentCA.getRules().getList().size() < 1)
            {
                SacUtility.showWarning("El conjunto de reglas debe poseer al menor una regla válida.");
                return false;
            }
        }
        else
        {
            SacUtility.showWarning("El Automata celular debe poseer un conjunto de reglas válidas.");
            return false;
        }

        if (currentCA.getGrids() == null)
        {
            SacUtility.showWarning("El Automata celular debe poseer una retícula válida");
            return false;
        }

        if (currentCA.getStates().getList().size() < 1)
        {
            SacUtility.showWarning("El Automata celular debe poseer al menos un estado.");
            return false;
        }

        if (currentCA.getRules().getList().size() < 1)
        {
            SacUtility.showWarning("El Automata celular debe poseer al menos una regla.");
            return false;
        }

        if (currentCA.getNeighborhood().getCellsList().size() < 1)
        {
            SacUtility.showWarning("El vecindario debe tener al menos una celda");
            return false;
        }

        return true;
    }

   
    /**
     * Creates the ADAC ID based on the date and time Sample: 20100824095311
     * @return
     */

    public static String generateNewId()
    {
        StringBuilder adacId = new StringBuilder();

        Calendar cal = Calendar.getInstance();

        SimpleDateFormat sdf1 = new SimpleDateFormat("yyyyMMdd");
        SimpleDateFormat sdf2 = new SimpleDateFormat("HHmmss");

        adacId.append(sdf1.format(cal.getTime())).append(sdf2.format(cal.getTime()));

        return adacId.toString();
    }

}
//******************************************************************************