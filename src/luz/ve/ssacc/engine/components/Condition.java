/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package luz.ve.ssacc.engine.components;
import luz.ve.ssacc.engine.parsers.ConditionParser;
import luz.ve.ssacc.utilities.SacUtility;
import net.sourceforge.jeval.EvaluationException;
import net.sourceforge.jeval.Evaluator;
/**
 *
 * @author Renzo Ciafardone Sciannelli
 */
public class Condition 
{
    
    public static final int OPERAND_A_ID = 0;
    public static final int OPERAND_B_ID = 1;
    
    private Operand operandA;
    private Operand operandB;
    private Operator operator;
    private Evaluator evaluator;
    private String expression;

    
    
    public Condition()
    {
        this.operandA = new Operand();
        this.operandB = new Operand();
        this.operator = new Operator();
        this.evaluator = new Evaluator();
        
        try
        {
            this.expression = "#{a}" + this.operator.getSymbol() + "#{b}";

            this.evaluator.parse(this.expression);

        } catch (EvaluationException ee)
        {
            System.out.println(ee);
        }

    }


    
    /**
     * Constructor operandA es evaluado contra operandB segun el operator
     *
     * @param operandA
     * @param operator
     * @param operandB
     */
    public Condition(Operand operandA,
                     Operator operator,
                     Operand operandB)
    {
        this.operandA = operandA;
        this.operandB = operandB;
        this.operator = operator;
        this.evaluator = new Evaluator();
                
        try
        {
            this.expression = "#{a}" + this.operator.getSymbol() + "#{b}";
            
            this.evaluator.parse(this.expression);
 
        } catch (EvaluationException ee)
        {
            System.out.println(ee);
        }
        
    }
    
   
    /**
     * Retorna el operand indicado por el operandId.
     *
     * @param operandId
     * @return
     */
    public Operand getOperand(int operandId)
    {
        switch (operandId)
        {
            case (Condition.OPERAND_A_ID):
            {
                return this.operandA;
            }
            case (Condition.OPERAND_B_ID):
            {
                return this.operandB;
            }

        }
        return null;
    }

    public void setOperandA(Operand function)
    {
        this.operandA = function;
    }

    public Operand getOperandA()
    {
        return this.operandA;
    }

    public void setOperandB(Operand function)
    {
        this.operandB = function;
    }

    public Operand getOperandB()
    {
        return this.operandB;
    }

    public void setOperator(Operator operator)
    {
        this.operator = operator;
    }

    public Operator getOperator()
    {
        return this.operator;
    }

    public boolean evaluate(Cell cell, Automaton cA)
    {
        boolean result = false;
        try
        {
            String a = this.operandA.executeFunction(cell, cA).toString();
            this.evaluator.putVariable("a", a);
            String b = this.operandB.executeFunction(cell, cA).toString();
            this.evaluator.putVariable("b", b);
            result = this.evaluator.getBooleanResult(this.expression);
            
        } catch (EvaluationException ee)
        {
            StringBuilder mess = new StringBuilder();
            mess.append(ee);
            mess.append(SacUtility.EOL);
            mess.append(cell.toString());
            mess.append(SacUtility.EOL);
            mess.append(this.toString());
            
            System.out.println(mess.toString());
        }

        return result;
    }
 
    
    
    @Override
    public String toString()
    {
        return ConditionParser.convertToString(this);
    }
//******************************************************************************
}
