/*
 * Esta clase define el objeto Cell para guardar coordenadas 
 */

package luz.ve.ssacc.engine.components;
import java.util.List;
import luz.ve.ssacc.engine.parsers.CellParser;

/**
 *
 * @author Renzo Ciafardone Sciannelli
 */
public class Cell
{

    private int row;
    private int column;
    private Short stateId;

    /**
     * 
     * @param row
     * @param column 
     * @param stateId 
     */
    public Cell(int row, int column, Short stateId)
    {
        this.row = row;
        this.column = column;
        this.stateId = stateId;
    }

   
    public Cell()
    {
        this.row = 0;
        this.column = 0;
        this.stateId = 0;
    }

    public int getRow()
    {
        return row;
    }

    public void setRow(int row)
    {
        this.row = row;
    }

    public int getColumn()
    {
        return column;
    }

    public void setColumn(int column)
    {
        this.column = column;
    }

    public Short getStateId()
    {
        return stateId;
    }

    public void setStateId(Short stateId)
    {
        this.stateId = stateId;
    }


    /**
     * Makes a deep copy of originalCell
     *
     * @return
     */
    public Cell copy()
    {
        Cell copiedCell = new Cell(this.getRow(), this.getColumn(), this.getStateId());
        return copiedCell;
    }

    /**
     * Actualiza las coordenadas de la celda actual con la suma las coordenadas de la celda suministrada.
     * @param aCell
     * @return 
     */
    public Cell addCoordinates(Cell aCell)
    {
        int newCellRow = this.getRow() + aCell.getRow(); //for clarity's sake
        int newCellCol = this.getColumn() + aCell.getColumn(); //for clarity's sake also

        return new Cell(newCellRow, newCellCol, this.getStateId());
    }

    /**
     * Devuelve verdadero si las coordenadas son iguales
     * @param aCell
     * @return 
     */
    public boolean hasSameCoordinatesAs(Cell aCell)
    {
        return((this.getRow() == aCell.getRow()) && (this.getColumn() == aCell.getColumn()));
    }

    /**
     * Devuelve verdadero si una celda con las mismas coordenadas ya existe en la lista
     * @param cellList
     * @return 
     */
    public boolean isContainedBy(List<Cell> cellList)
    {
        for (Cell currentCell : cellList)
        {
            if (this.hasSameCoordinatesAs(currentCell))
            {
                return true;
            }
        }
        return false;
    }

    /**
     * Devuelve las coordenadas de la celda como un string
     * @return 
     */
    public String getCoordinatesAsString()
    {
        return CellParser.getFormatedCoordinates(this);
    }
    
    
    @Override
    public String toString()
    {
        return CellParser.convertToString(this);
    }

}
