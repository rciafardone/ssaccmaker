/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package luz.ve.ssacc.engine.components;
import java.awt.Color;
import luz.ve.ssacc.engine.parsers.StateParser;

/**
 *
 * @author Renzo Ciafardone Sciannelli
 */
public class State
{
    private Short id; 
    private String text;
    private Double value;  
    private Color color; 
    
    
    public final static int FIELD_NUM_ID = 0;
    public final static int FIELD_NUM_TEXT = 1;
    public final static int FIELD_NUM_VALUE = 2;
    public final static int FIELD_NUM_COLOR = 3;
    
   
    public final static Short DEFAULT_ID = 0; 
    public final static String DEFAULT_TEXT = "DEFAULT";
    public final static Double DEFAULT_VALUE = 0.0;
    public final static Color DEFAULT_COLOR = Color.GRAY;

    
    /**
     *
     * @param id Identifica de manera unica el estado
     * @param label Etiqueta asociada al estado
     * @param value Valor variable que puede aplicar en ciertos automatas
     * @param color Color del estado en el grid
     */
    public State(Short id, String label, Double value, Color color)
    {
        this.id = id;
        this.text = label;
        this.value = value;
        this.color = color;
    }
    
    /**
     * Constructor por defecto
     */
    public State()
    {
        this(
            State.DEFAULT_ID,
            State.DEFAULT_TEXT,
            State.DEFAULT_VALUE,
            State.DEFAULT_COLOR
            );
    }

    /**
     *
     * @param id
     */
    public State(Short id)
    {
        this(
            id,
            State.DEFAULT_TEXT,
            State.DEFAULT_VALUE,
            State.DEFAULT_COLOR);
    }

    /**
     *
     * @param state
     */
    public State(State state)
    {
        this(state.getId(), state.text, state.getValue(), state.getColor());
    }

    public void setId(Short id)
    {
        this.id = id;
    }

    public Short getId()
    {
        return this.id;
    }

    public void setText(String label)
    {
        this.text = label;
    }
    
    public String getText()
    {
        return this.text;
    }

    public void setValue(Double value)
    {
        this.value = value;
    }

    public Double getValue()
    {
        return this.value;
    }
    
    public void setColor(Color color)
    {
        this.color = color;
    }

    public Color getColor()
    {
        return this.color;
    }


 /*
     * Copia el contenido del estado que se pasa por parametro
     */
    public State copy(State aState)
    {
        this.setId(aState.getId());
        this.setText(aState.getText());
        this.setValue(aState.getValue());
        this.setColor(aState.getColor());
        return this;
    }

/**
 * Devuelve el ID formateado de la siguiente manera: 
 * @return 
 */    
    public String getFormatedId()
    {
        return StateParser.formatId(this.getId());
    }

    @Override
    public String toString()
    {
        return StateParser.ConvertToString(this);
    }
}
