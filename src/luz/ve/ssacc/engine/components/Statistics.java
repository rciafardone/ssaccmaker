/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package luz.ve.ssacc.engine.components;

import java.util.List;
import luz.ve.ssacc.engine.parsers.StatisticsParser;

/**
 *
 * @author Renzo Ciafardone Sciannelli <rciafardone@gmail.com>
 */
public final class Statistics
{
     private List<State> states;
     private List<Integer[]> data;

    public Statistics(List<State> states, List<Integer[]> data)
    {
        this.setData(data);
        this.setStates(states);
    }
   
    public Statistics(Statistics stats)
    {
        this(stats.getStates(), stats.getData());
    }

    
    
    public Statistics(String sFileContent, Automaton automaton)
    {
        this(StatisticsParser.parse(sFileContent, automaton));
    }
    
    
    public List<State> getStates()
    {
        return states;
    }

    public void setStates(List<State> states)
    {
        this.states = states;
    }

    public List<Integer[]> getData()
    {
        return data;
    }

    public void setData(List<Integer[]> data)
    {
        this.data = data;
    }

}
