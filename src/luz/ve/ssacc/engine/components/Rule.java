/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package luz.ve.ssacc.engine.components;
import java.util.ArrayList;
import java.util.List;
import luz.ve.ssacc.engine.parsers.RuleParser;


/**
 *
 * @author Renzo Ciafardone Sciannelli
 */


public class Rule implements Comparable<Rule>
{
    
    private Short id;
    private Short stateIdIni;
    private Short stateIdEnd;
    private List<Condition> conditions;

    public static final int DEFAULT_ID = 0;

    /**
     *
     */
    public Rule()
    {
        super();
        this.id = Rule.DEFAULT_ID;
        this.stateIdIni = 0;
        this.stateIdEnd = 0;
        this.conditions = new ArrayList<>();
        this.conditions.add(new Condition());
    }

    /**
     *
     * @param id
     * @param stateIniId
     * @param stateEndId
     */
    public Rule(Short id, Short stateIniId, Short stateEndId)
    {
        super();
        this.id = id;
        this.stateIdIni = stateIniId;
        this.stateIdEnd = stateEndId;
        this.conditions = new ArrayList<>();
    }

    /**
     *
     * @param id
     * @param stateIniId
     * @param stateEndId
     * @param conditions
     */
    public Rule(Short id, Short stateIniId, Short stateEndId,
                List<Condition> conditions)
    {
        super();
        this.id = id;
        this.stateIdIni = stateIniId;
        this.stateIdEnd = stateEndId;
        this.conditions = conditions;
    }

    public void setId(Short id)
    {
        this.id = id;
    }

    public Short getId()
    {
        return this.id;
    }

    /**
     * Esta funcion aplica la regla al probar todas las condiciones, si todas se
     * cumplen retorna verdadero (true), indicando que la regla aplica.
     *
     * @param currentCell para tener las coordenadas de la celda a evaluar
     * @param automata
     * @return
     */
    public boolean apply(Cell currentCell, Automaton automata)
    {
        for (Condition condition : this.getConditions())
        {
            if (!condition.evaluate(currentCell, automata))
            {
                return false;
            }
        }
        return true;
    }

    public void setStateIniId(Short stateIni)
    {
        this.stateIdIni = stateIni;
    }

    public Short getStateIniId()
    {
        return this.stateIdIni;
    }

    public void setStateEndId(Short stateEnd)
    {
        this.stateIdEnd = stateEnd;
    }

    public Short getStateEndId()
    {
        return this.stateIdEnd;
    }

    public List<Condition> getConditions()
    {
        return this.conditions;
    }

    /**
     * Agrega la condicion a la regla, sila lista de condiciones no ha sido 
     * iniciada, se crea y luego se agrega.
     * @param newCondition 
     */
    public void addCondition(Condition newCondition)
    {
        if(this.conditions==null)
        {
            this.conditions = new ArrayList<>();
        }
        
        this.conditions.add(newCondition);
    }

    public String[] getConditionsAsAStringArray()
    {
        String[] tempConditions = new String[this.getConditions().size()];

        for (int i = 0; i < this.getConditions().size(); i++)
        {
            tempConditions[i] = this.getConditions().get(i).toString();
        }

        return tempConditions;
    }

    @Override
    public int compareTo(Rule compareRule)
    {
        int compareId = ((Rule) compareRule).getId();

        return this.id - compareId;
    }

    @Override
    public String toString()
    {
        return RuleParser.convertToString(this);
    }
    
//******************************************************************************
}
