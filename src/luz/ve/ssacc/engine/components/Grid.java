/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package luz.ve.ssacc.engine.components;

import java.util.Arrays;
import luz.ve.ssacc.engine.parsers.GridParser;
import luz.ve.ssacc.utilities.SacUtility;


/**
 * Clase que define la retícula
 * @author Renzo Ciafardone Sciannelli
 */
public final class Grid
{
    //private final String name;
    private final Short[][] array;
   // private final int _frontierType;
   // private State _backgroundState;

   
    /**
     *
     * @param rows define cuantas filas tendra la rejilla
     * @param columns define cuantas columnas tendra la rejilla
     * @param background
     */
    public Grid(int rows, int columns, State background)
    {
        this.array = new Short[rows][columns];
        this.fill(background);
    }
    
    
    /**
     *
     * @param rows define cuantas filas tendra la rejilla
     * @param columns define cuantas columnas tendra la rejilla
     */
    public Grid(int rows, int columns)
    {
        this.array = new Short[rows][columns];
    }
    
    
    
    public Grid(Short[][] stateArray, State background)
    {
        this.array = stateArray;
        this.fill(background);
    }
   
    
   /**
    * 
    * @return 
    */
    public Short[][] getArray()
    {
        return this.array;
    }

    /**
     * Regresa el ID del estado del grid en las coordenadas especificadas
     * @param row
     * @param col
     * @return 
     */
    public Short getStateId(int row, int col)
    {
        return this.getArray()[row][col];
    }

    public void setStateId(Cell gridCell)
    {
        this.getArray()[gridCell.getRow()][gridCell.getColumn()] = gridCell.getStateId();
    }

    /**
     * Actualiza el estado en las coordenadas de la celda indicada.
     * @param cell
     * @param newState
     */
    public void updateCellState(Cell cell, State newState)
    {
        this.getArray()[cell.getRow()][cell.getColumn()] = newState.getId();
    }
    
    /**
     * Retorna la celda que corresponde a las coordenadas dadas.
     *
     * @param row
     * @param column
     * @return
     */
    public Cell getCell(int row, int column)
    {
        return new Cell(row, column, this.getStateId(row, column));
    }
    
    /**
     * Regresa el estado del grid en las coordenadas especificadas
     * @param gridCell
     * @return 
     */
    public Short getStateId(Cell gridCell)
    {
        return this.getArray()[gridCell.getRow()][gridCell.getColumn()];
    }
    
    /**
     *
     * @return int numero de filas
     */
    public int getRows()
    {
        return this.array.length;
    }

    /**
     *
     * @return int numero de columnas.
     */
    public int getColumns()
    {
        return this.array[0].length;

    }

    /**
     * Retorna un String con el printGrid formateado segun sus dimensiones
     * currentColumn con caracteres adicionales para facilitar su lectura. 
     * Nota: Solo para usar durante debugging.
     *
     * @return
     */
    public String toStringTest()
    {
        String eol = System.getProperty("line.separator");
        StringBuilder temp = new StringBuilder();
        StringBuilder printGrid = new StringBuilder();

        // temp.append(this.getName()).append(eol);
        for (int row = 0; row < this.getRows(); row++)
        {
            temp.append("(:");
            for (int col = 0; col < this.getColumns(); col++)
            {
                temp.append(this.getArray()[row][col].toString()).append(":");
            }
            temp.append(")");
            temp.append(eol);
            printGrid.append(temp);
            temp.setLength(0);
        }
                
        return printGrid.toString();
    }

    /**
     * Llena todas las celdas del grid con el mismo estado de fondo
     *
     * @param stateBackground Es el estado de fondo del grid
     * @return State[][] El arreglo de estados
     */
    public Short[][] fill(State stateBackground)
    {
        for (Short[] row : this.array)
        {
            Arrays.fill(row, stateBackground.getId());
        }

        return this.array;
    }

    @Override
    public String toString()
    {
        return GridParser.convertToString(this);
    }

    public String toFile(int step, String stats)
    {
        StringBuilder data = new StringBuilder();
        data.append("step: ").append(Integer.toString(step)).append(SacUtility.EOL);
        data.append(stats).append(SacUtility.EOL);
        data.append(GridParser.convertToFileFormat(this));
        
        return data.toString();
    }

    /**
     * Actualiza el grid agregando el patrón indicado.
     *
     * @param pattern
     */
    public void add(CustomPattern pattern)
    {
        for (Cell cell : pattern.getCells())
        {
            if(cell.getRow()<this.getRows() && cell.getColumn()<this.getColumns())
            {
                this.setStateId(cell);
            }
        }
   }
        
        
        
//******************************************************************************
//******************************************************************************
//******************************************************************************
}
