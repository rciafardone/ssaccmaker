/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package luz.ve.ssacc.engine.components;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Objects;
import luz.ve.ssacc.engine.parsers.RulesParser;
/**
 *
 *
 * @author Renzo Ciafardone Sciannelli
 *
 */
public class Rules
{

    private String name;
    private String description;
    private List<Rule> ruleList;
    
    public static final String DEFAULT_NAME = "Nombre Reglas";
    public static final String DEFAULT_DESCRIPTION = "Descripción aqui";
    

    public Rules()
    {
        this.name = Rules.DEFAULT_NAME;
        this.description = Rules.DEFAULT_DESCRIPTION;
        this.ruleList = new ArrayList<>();
        this.add(new Rule());
    }

    public Rules(String name)
    {
        this.name = name;
        this.ruleList = new ArrayList<>();
    }

    public Rules(String name, String description, List<Rule> ruleList)
    {
        this.name = name;
        this.description = description;
        this.ruleList = ruleList;
    }

    public void setName(String name)
    {
    this.name = name;
    }
    
    public String getName()
    {
        return this.name;
    }

    public void setDescription(String description)
    {
    this.description = description;
    }
    
    public String getDescription()
    {
        return this.description;
    }
    
    public void setList(List<Rule> ruleList)
    {
    this.ruleList = ruleList;
    
    }
    
    public List<Rule> getList()
    {
        return this.ruleList;
    }

    public final void add(Rule rule)
    {
        this.ruleList.add(rule);
    }

    /**
     *
     * Retorna una regla especificada por id.
     * @param id
     * @return 
     */
    public Rule getRule(int id)
    {
        for (Rule aRule : this.getList())
        {
            if (aRule.getId() == id)
            {
                return aRule;
            }
        }
        return null;
    }

    /**
     * Retorno verdadero si el id existe
     *
     * @param id
     * @return
     */
    public boolean conteins(int id)
    {
        for (Rule aRule : this.getList())
        {
            if (aRule.getId() == id)
            {
                return true;
            }
        }
        return false;
    }

    
    
    public boolean delete(Rule nRule)
    {
        return this.ruleList.remove(nRule);
    }

    public boolean delete(int id)
    {
        for (int i = 0; i < this.getList().size(); i++)
        {
            if (this.getList().get(i).getId() == id)
            {
                this.getList().remove(i);
                return true;
            }
        }
        return false;
    }
    
    
    /**
     * Cada una de las reglas es aplicada a la celda, si varias reglas se
     * cumplen prevalece la ultima que se aplique.
     * En esta parte se discrimina que regla puede aplicarse a la celda según el 
     * estado inicial.
     *
     * @param cell Celda del grid a la que se aplican las reglas
     * @param cA Objeto que contiene toda la informacion del Autómata Celular
     */
    public void apply(Cell cell, Automaton cA)
    {
        for (Rule rule : this.getList())
        {
            if (Objects.equals(rule.getStateIniId(), cell.getStateId())) //Verifica estado inicial
            {
                if (rule.apply(cell, cA))
                {
                    cA.getGrids().getGridEnd().updateCellState(cell, cA.getStates().getState(rule.getStateEndId()));
                }
            }
        }
    }

    
    /**
     * Ordena las reglas por su id.
     */
    public void sort()
    {
        Collections.sort(this.ruleList);
    }    
        
    /**
     * Retorna el valor mas bajo posible para un ID de regla que no este en uso
     *
     * @return
     */
    public Short getNewRuleId()
    {
        Short newId = 0;
        
        while(this.conteins(newId))
        {
            newId++;
        }
        
        return newId;
    }

    /**
     * Retorna todas las reglas formateadas como un StringArray
     * @return 
     */
    public String[] AsAStringArray()
    {
        String[] rules = new String[this.getList().size()];

        for (int i = 0; i < this.getList().size(); i++)
        {
            rules[i] = this.getList().get(i).toString();
        }

        return rules;
    }
    
    
    @Override
    public String toString()
    {
        return RulesParser.ConvertToString(this);
    }

    
//******************************************************************************
//******************************************************************************
//******************************************************************************
}
