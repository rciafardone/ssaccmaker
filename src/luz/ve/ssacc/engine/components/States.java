/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package luz.ve.ssacc.engine.components;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import luz.ve.ssacc.engine.parsers.StatesParser;

/**
 *
 * @author Renzo Ciafardone Sciannelli
 */
public class States
{

    public static final String DEFAULT_NAME = "Nombre Estados";
    public static final String DEFAULT_DESCRIPTION = "Descripción aqui";

    private String name;
    private String description;
    private List<State> stateList;
    

    public States()
    {
        this.name = DEFAULT_NAME;
        this.description = DEFAULT_DESCRIPTION;
        this.stateList = new ArrayList<>();
        this.add(new State());
    }

    public States(String name)
    {
        this.name = name;
        this.stateList = new ArrayList<>();
    }

    public States(String name, String description, List<State> stateList)
    {
        this.name = name;
        this.description = description;
        this.stateList = stateList;
    }

    public void setName(String name)
    {
        this.name = name;
    }

    public String getName()
    {
        return this.name;
    }

    public void setDescription(String description)
    {
        this.description = description;
    }

    public String getDescription()
    {
        return this.description;
    }

    public void setList(List<State> list)
    {
        this.stateList = list;
    }

    public List<State> getList()
    {
        return this.stateList;
    }

    public final void add(State state)
    {
        this.stateList.add(state);
    }

   
        /**
     * Retorna el estado que tenga el mismo id que se esta proporcionando, de no
     * encontrarlo retorna NULL.
     *
     * @param id
     * @return
     */
    public State getState(Short id)
    {
        for (State state : this.stateList)
        {
            if (state.getId().equals(id))
            {
                return state;
            }
        }
        return null;
    }

    /**
     * Retorna verdadero si el Id de estado proporcionado se encuentra en el
     * conjunto de estados
     *
     * @param stateId
     * @return
     */
    public boolean conteinsId(int stateId)
    {
        for (State state : this.stateList)
        {
            if (state.getId() == stateId)
            {
                return true;
            }
        }
        return false;
    }

    /**
     * Retorna el estado que tenga el mismo id que se esta proporcionando, de no encontrarlo retorna NULL
     *
     * @param state
     * @return
     */
    public State getState(State state)
    {
        for (State s : this.stateList)
        {
            if (s.getId().equals(state.getId()))
            {
                return s;
            }
        }
        return null;
    }

    
    
    
  
    /**
     * Devuelve un estado al azar.
     * @return 
     */
    public State GetRandomState()
    {
        Random randomGenerator = new Random();
        
        int stateIndex = randomGenerator.nextInt(this.getList().size());
        
        return this.getState((short)stateIndex);
    }

    /**
     * Devuelve un array de String que contiene lo ID de los estados
     * @return 
     */
    public String[] getIdAsAStringArray()
    {
        String[] list = new String[this.getList().size()];
        int i = 0;

        for (State oCurrent : this.getList())
        {
            list[i] = oCurrent.getId().toString();
            i++;
        }

        return list;
    }
    
    /**
     * Revisa los id de los estados y devuelve el mas alto mas uno (1)
     * @return 
     */
    public int getNextHighestId()
    {
        int highestId;
        int currentId;
        Integer numStates = this.getList().size();

        if (numStates != 0)
        {
            highestId = 0;
            for (State aState : this.getList()) //consigue el id mas alto
            {
                currentId = aState.getId();
                if (currentId > highestId)
                {
                    highestId = currentId;
                }
            }
            highestId++;

        } else
        {

            highestId = 0;
        }

        return highestId;
    }
    
    
    /**
     * retorna un string con la lista de estados formateada
     *
     * @return
     */
    @Override
    public String toString()
    {
        return StatesParser.ConvertToString(this);
    }
   
}
