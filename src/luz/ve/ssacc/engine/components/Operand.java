/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package luz.ve.ssacc.engine.components;

import luz.ve.ssacc.engine.functions.Constant;
import luz.ve.ssacc.engine.functions.Function;
import luz.ve.ssacc.engine.parsers.OperandParser;

/**
 * Esta es la Clase context para el strategy pattern de las funciones
 *
 * @author Renzo Ciafardone Sciannelli
 */
public class Operand
{

    private Function function;
    private State variable;
    private int field;

    public static final int OPERAND_A_ID = 0;
    public static final int OPERAND_B_ID = 1;
    public static final Function DEFAULT_FUNCTION = new Constant();
    public static final int DEFAULT_FIELD = State.FIELD_NUM_ID;
    
   

    public Operand()
    {
        this(
            Operand.DEFAULT_FUNCTION,
            new State(),
            Operand.DEFAULT_FIELD);
    }

    /**
     *
     * @param function la funcion que hara el calculo
     * @param variable el estado que se utilizará en el calculo
     * @param field el campo del estado que se utilizará
     */
    public Operand(Function function, State variable, int field)
    {
        this.function = function;
        this.variable = variable;
        this.field = field;
    }

    public String executeFunction(Cell currentCell, Automaton automata)
    {
        return(this.function.apply(this.variable, this.field, currentCell, automata));
    }

    public void setFunction(Function function)
    {
        this.function = function;
    }

    public void setVariable(State variable)
    {
        this.variable = variable;
    }

    public void setField(int field)
    {
        this.field = field;
    }
    
    public Function getFunction()
    {
        return this.function;
    }

    public State getVariable()
    {
        return this.variable;
    }

    public int getField()
    {
        return this.field;
    }
/*
    public String getStringToEvaluate()
    {

    }
*/
    @Override
    public String toString()
    {
        return OperandParser.convertToString(this);
    }

//******************************************************************************    
}
