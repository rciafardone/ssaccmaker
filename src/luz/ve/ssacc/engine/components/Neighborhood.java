/*
 * Esta clase almacena la lista de coordenadas del vecindario y procee una 
 * funcion para obtener los vecinos de una celda 
 */

package luz.ve.ssacc.engine.components;
import java.util.ArrayList;
import java.util.List;
import luz.ve.ssacc.engine.parsers.NeighborhoodParser;
import luz.ve.ssacc.utilities.MessageBox;
import luz.ve.ssacc.utilities.SacUtility;

/**
 *
 * @author Renzo Ciafardone Sciannelli
 */
public class Neighborhood
{
    private String name;
    private String description;
    private List<Cell> cellsBaseList; //La forma basica del vecindario. La celda raíz no esta incluida
    private List<Cell> cellsFullList; //El vecindario ampliado según el radio.
    private Cell radix; // es la celda en base a la cual se establecen las coordenadas del vecindario.
    private boolean includesRadix; //Indica si en el vecindario se incluirá la celda raíz en los calculos de las funciones
    private int radius;
    
    public static final String DEFAULT_NAME = "Nombre Vecindario";
    public static final String DEFAULT_DESCRIPTION = "Descripción vecindario";
    public static final Cell DEFAULT_RADIX = new Cell(0, 0, State.DEFAULT_ID);
    public static final int DEFAULT_RADIUS = 1;
    public static final int BASE_PATTERN = 0;
    public static final int FULL_PATTERN = 1;
    public static final int BOTH_PATTERNS = 2;
    
    public Neighborhood(String name,
                        String description,
                        Cell radix, 
                        boolean includeRadix,
                        int radius,
                        List<Cell> cells)
    {
        this.name = name;
        this.description = description;
        this.radix = radix;
        this.includesRadix = includeRadix;
        this.radius = radius;
        this.cellsBaseList = cells;
        this.setCellFullList();
    }

    public Neighborhood(String name,
                        Cell radix,
                        boolean includeRadix)
    {
        this(
            name,
            DEFAULT_DESCRIPTION,
            radix,
            includeRadix,
            DEFAULT_RADIUS,
            new ArrayList<Cell>());
    }

    public Neighborhood()
    {
        this(
            DEFAULT_NAME,
            DEFAULT_DESCRIPTION,
            DEFAULT_RADIX,
            true,
            DEFAULT_RADIUS,
            new ArrayList<Cell>());
        
        this.addCell(DEFAULT_RADIX);
    }

    public void setName(String name)
    {
        this.name = name;
    }

    public String getName()
    {
        return this.name;
    }

     public void setDescription(String description)
    {
        this.description = description;
    }

    public String getDescription()
    {
        return this.description;
    }
    
    /**
     * Agrega una celda al patrón básico del vecindario.
     * @param newCell 
     */
    public final void addCell(Cell newCell)
    {
        this.cellsBaseList.add(newCell);
        this.setCellFullList();
    }

    /**
     * Agrega una celda al patrón básico del vecindario.
     * @param row
     * @param column
     * @param stateId 
     */
    public void addCell(int row, int column, Short stateId)
    {
        this.cellsBaseList.add(new Cell(row, column, stateId));
        this.setCellFullList();
    }
  
    /**
     * Establece el valor de la raíz del vecindario.
     * @param radix 
     */
    public void setRadix(Cell radix)
    {
        this.radix = radix;
        if (this.getIncludesRadix())
        {
            this.addCell(this.radix);
        }
        this.setCellFullList();
    }

    /**
     * Devuelve el valor de la raíz del vecindario.
     * @return 
     */
    public Cell getRadix()
    {
        return this.radix;
    }

    /**
     * Establece el valor que indica si la raíz se incluye o no en el cálculo de
     * las funciones.
     *
     * @param includeRadix
     */
    public void setIncludesRadix(boolean includeRadix)
    {
        this.includesRadix = includeRadix;
        this.includeRadixChangeControl();
    }

    /**
     * Indica si se incluye la raíz o no en los cálculos del vecindario.
     *
     * @return
     */
    public boolean getIncludesRadix()
    {
        return this.includesRadix;
    }

    /**
     * Establece el valor del radio del vecindario.
     * @param radius 
     */
    public void setRadius(int radius)
    {
        this.radius = radius;
        this.setCellFullList();
    }

    /**
     * Devuelve el radio del vecindario
     * @return 
     */
    public int getRadius()
    {
        return this.radius;
    }
    
    /**
     * Asigna un nuevo valor a la celda de las coordenadas indicadas.
     * @param newCell
     * @param row
     * @param col
     * @return 
     */
    public boolean setCell(Cell newCell, int row, int col)
    {
        for (Cell cell : this.cellsBaseList)
        {
            if (cell.getRow() == row)
            {
                if (cell.getColumn() == col) {
                    cell.setStateId(newCell.getStateId());
                    return true;
                }
            }
        }
        return false;
    }

    /**
     * Devuelve la celda que con las coordenadas indicadas.
     * @param row
     * @param col
     * @return 
     */
    public Cell getCell(int row, int col)
    {
        for (Cell currentCell : this.cellsBaseList)
        {
            if (currentCell.getRow() == row)
            {
                if (currentCell.getColumn() == col) {
                    return currentCell;
                }
            }
        }
        return null;
    }
    
    public Cell getCell(Cell cellToSearch)
    {
       return this.getCell(cellToSearch.getRow(), cellToSearch.getColumn());
    }
    
    public void setCellsBaseList(List<Cell> cells)
    {
        this.cellsBaseList = cells;
        this.setCellFullList();
    }

        /**
     * Controla el estado de la variable include radix y la lista de celdas.
     *
     * @param includeRadix
     */
    private void includeRadixChangeControl()
    {
        if (this.includesRadix)
        {
            if (!Neighborhood.contains(this.radix, cellsBaseList))
            {
                this.addCell(this.radix);
            }
        }
        else
        {
            Neighborhood.removeCell(this.radix, cellsBaseList);
        }
        this.setCellFullList();
    }

    /**
     *
     * La funcion devuelve la lista de celdas con las coordenadas relativas a la
     * celda raiz (radix) sin tomar en cuenta el radio.
     *
     * @return
     */
    public List<Cell> getCellsBaseList()
    {
        return this.cellsBaseList;
    }

    /**
     * La funcion devuelve la lista de celdas con las coordenadas relativas a la
     * celda raiz (radix).
     *
     * @return
     */
    public List<Cell> getCellsList()
    {
       return this.cellsFullList;
    }

     /**
     * La funcion devuelve la lista de celdas con las coordenadas relativas a la
     * celda actual.
     *
     * @param currentCell
     * @return
     */
    public List<Cell> getCellsList(Cell currentCell)
    {
        return Neighborhood.normalize(currentCell, this.getCellsList());
    }
    /**
     *
     * Toma el patrón del vecindario y lo expande según el valor del radio. Se
     * utiliza en al momento de graficar.
     */
    private void setCellFullList()
    {
        this.cellsFullList = new ArrayList<>();

        if (this.cellsBaseList != null)
        {
            this.cellsFullList.addAll(this.cellsBaseList);

            List<Cell> tmpCellList = new ArrayList<>();
            tmpCellList.addAll(this.cellsBaseList);

            Cell tmpCell;

            for (int counter = 0; counter < this.getRadius() - 1; counter++)
            {
                for (Cell radixCell : this.cellsFullList)
                {
                    for (Cell patternCell : this.cellsBaseList)
                    {
                        tmpCell = radixCell.addCoordinates(patternCell);

                        if (!tmpCell.isContainedBy(tmpCellList))
                        {
                            tmpCellList.add(tmpCell);
                        }
                    }
                }
                if (!this.getIncludesRadix())
                {
                    Neighborhood.removeCell(this.getRadix(), tmpCellList);
                }

                this.cellsFullList.clear();
                this.cellsFullList.addAll(tmpCellList);
                tmpCellList.clear();
            }
        }
        else
        {
            SacUtility.showParsingError("Bloque de Vecindario - GNB: Lista de coordenadas básicas vacia.");
        }
    }

    
   

    /**
     *  
     * La funcion devuelve la lista de celdas con las coordenadas absolutas en el grid
     * que forman el vecindario de la celda que esta siendo analizada
     * NOTA: No toma el cuenta las dimensiones del Grid
     * 
     * @param radix Celda en base a la cual se hace la normalización.
     * @param cellList Lista de celdas del vecindario
     * @return 
     */
    public static List<Cell> normalize(Cell radix, List<Cell> cellList)
    {
        int newRow;
        int newCol;

        List<Cell> normalizedCellList = new ArrayList<>();

        for (Cell currentCell : cellList)
        {
            newCol = radix.getColumn() + currentCell.getColumn();
            newRow = radix.getRow() + currentCell.getRow();

            Cell newCell = new Cell(newRow, newCol, currentCell.getStateId());
            normalizedCellList.add(newCell);
        }

        return normalizedCellList;
    }

    
    /**
     * Makes a deep copied List of a pattern
     *
     * @param originalList
     * @return
     */
    private static List<Cell> copyPattern(List<Cell> originalList)
    {
        List<Cell> copiedList = new ArrayList<>();

        for (int i = 0; i < originalList.size(); i++)
        {
            copiedList.add(originalList.get(i).copy());
        }

        return copiedList;
    }

    /**
     * Devuelve verdadero si existe una celda con las mismas coordenadas en el
     * patrón del vecindario.
     *
     * @param aCell
     * @return
     */
    public boolean contains(Cell aCell)
    {
        for (Cell currentCell : this.cellsFullList)
        {
            if (aCell.hasSameCoordinatesAs(currentCell))
            {
                return true;
            }
        }
        return false;
    }
    
    public static boolean contains(Cell aCell, List<Cell> list)
    {
        for (Cell currentCell : list)
        {
            if (aCell.hasSameCoordinatesAs(currentCell))
            {
                return true;
            }
        }
        return false;
    }

    /**
     * Devuelve verdadero si existe una celda con las mismas coordenadas en el
     * patrón del vecindario y la borra.
     *
     * @param aCell
     * @param list
     * @return
     */
    public static boolean removeCell(Cell aCell, List<Cell> list)
    {
        for (Cell currentCell : list)
        {
            if (aCell.hasSameCoordinatesAs(currentCell))
            {
                list.remove(currentCell);
                return true;
            }
        }
        return false;
    }
    
    
    
    
    @Override
    public String toString()
    {
        return NeighborhoodParser.convertToString(this);
    }

    public boolean ready()
    {

        if (this.radix == null)
        {
            StringBuilder sMensaje = new StringBuilder();
            sMensaje.append("Problema con el vecindario:\n\n");
            sMensaje.append("La raíz del veindario es null!!!\n\n");
            MessageBox.show("Aviso Importante", sMensaje.toString());
            return false;
        }

        if (this.cellsBaseList.size() < 1)
        {
            StringBuilder sMensaje = new StringBuilder();
            sMensaje.append("Problema con el vecindario:\n\n");
            sMensaje.append("El vecindario debe contener al menos una celda valida!!!\n\n");
            MessageBox.show("Aviso Importante", sMensaje.toString());
            return false;
        }



        return true;
    }

}
