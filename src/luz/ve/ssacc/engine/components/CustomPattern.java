/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package luz.ve.ssacc.engine.components;

import java.util.ArrayList;
import java.util.List;
import java.awt.Dimension;
import java.awt.Point;
import luz.ve.ssacc.engine.parsers.CustomPatternParser;
/**
 *
 * @author Renzo Ciafardone Sciannelli
 */
public class CustomPattern
{

    private String name;
    private String description;
    private List<Cell> cells;

    public static final String DEFAULT_NAME = "Nombre Patrón";
    public static final String DEFAULT_DESCRIPTION = "Descripción patrón";

    /**
     *
     * @param name
     * @param description
     * @param cells
     */
    public CustomPattern(String name, String description, List<Cell> cells)
    {
        this.name = name;
        this.description = description;
        this.cells = cells;
    }

    public CustomPattern()
    {
        this.name = CustomPattern.DEFAULT_NAME;
        this.description = CustomPattern.DEFAULT_DESCRIPTION;
        this.cells = new ArrayList<>();
    }
    
    
    public void setName(String name)
    {
        this.name = name;
    }

    public String getName()
    {
        return this.name;
    }

    public void setDescription(String desc)
    {
        this.description = desc;
    }

    public String getDescription()
    {
        return this.description;
    }

    public List<Cell> getCells()
    {
        return cells;
    }

    
    /**
     * Retorna la lista de celdas normalizadas segun la celda suministrada
     * @param position
     * @return 
     */
    public List<Cell> getCells(Cell position)
    {
        return this.normalize(position);
    }
    
    
    
    public void setCells(List<Cell> cells)
    {
        this.cells = cells;
    }

    public final boolean addCell(Cell cell)
    {
        if (this.Contains(cell)) //Si la celda ya existe no la agrega
        {
            return false;
        }

        this.cells.add(cell);
        return true;
    }

    public Cell getCell(int row, int col)
    {
        for (Cell currentCell : this.cells)
        {
            if (currentCell.getRow() == row && currentCell.getColumn() == col)
            {
                return currentCell;
            }
        }
        return null;
    }

    /**
     * 
     * @param cellToSearch
     * @return 
     */
    public Cell getCell(Cell cellToSearch)
    {
        return this.getCell(cellToSearch.getRow(), cellToSearch.getColumn());
    }

    /**
     *
     * @param cellToRemove
     */
    public void delCell(Cell cellToRemove)
    {
        this.getCells().remove(this.getCell(cellToRemove));
    }

    /**
     *
     * La función devuelve la lista de celdas con las coordenadas absolutas en
     * el grid que forman el patrón. 
     * NOTA: No toma el cuenta las dimensiones del Grid.
     * 
     * @param originPoint
     * @return 
     */
    public List<Cell> normalize(Cell originPoint)
    {
        int newRow;
        int newCol;
        List<Cell> normalizedPattern = new ArrayList<>();

        for (Cell currentCell : this.getCells())
        {
            newRow = currentCell.getRow() + originPoint.getRow();
            newCol = currentCell.getColumn() + originPoint.getColumn();

            Cell newCell = new Cell(newRow, newCol, currentCell.getStateId());
            normalizedPattern.add(newCell);
        }

        return normalizedPattern;
    }

    
    
    /**
     * Empaqueta las coordenadas del patrón para que ocupen el espacio mas
     * cercano posible a la esquina superior izquierda del grid.
     *
     * @return
     */
    public List<Cell> packCells()
    {
        Double tempRow;
        Double tempCol;
      
        Point upperLeft = CustomPattern.getBoundingBoxUpperLeftCorner(this.getCells());

        for (Cell cell : this.getCells())
        {
            tempRow = cell.getRow() - upperLeft.getX();
            tempCol = cell.getColumn() - upperLeft.getY();

            cell.setRow(tempRow.intValue());
            cell.setColumn(tempCol.intValue());
        }

        return this.getCells();
    }

    
    /**
     * Retorna el ancho y el alto del espacio máximo que ocupa el patron
     *
     * @param cellList
     * @return
     */
    public static Dimension getBoundingBox(List<Cell> cellList)
    {
        Dimension size = new Dimension();
        
        if (!cellList.isEmpty())
        {
            Point upperLeft = CustomPattern.getBoundingBoxUpperLeftCorner(cellList);
            Point lowerRight = CustomPattern.getBoundingBoxLowerRightCorner(cellList);

            Double height = (lowerRight.getX() - upperLeft.getX());
            Double width = (lowerRight.getY() - upperLeft.getY());

            size.setSize(width, height);
        }

        return size;
    }

    public Dimension getBoundingBox()
    {
        return CustomPattern.getBoundingBox(this.getCells());
    }

    
    /**
     * Retorna la coordena superior izquierda de la caja que contiene el patrón
     * @param cellList
     * @return 
     */
    public static Point getBoundingBoxUpperLeftCorner(List<Cell> cellList)
    {
        Point upperLeft;

        int minRow = cellList.get(0).getRow();
        int minCol = cellList.get(0).getColumn();

        for (Cell aCell : cellList)
        {
            if (aCell.getRow() < minRow)
            {
                minRow = aCell.getRow();
            }

            if (aCell.getColumn() < minCol)
            {
                minCol = aCell.getColumn();
            }
        }

        upperLeft = new Point(minRow, minCol);

        return upperLeft;
    }
    
    /**
     * Retorna la coordena inferior derecha de la caja que contiene al patrón
     * @param cellList
     * @return
     */
    public static Point getBoundingBoxLowerRightCorner(List<Cell> cellList)
    {
        Point lowerRight;

        int maxRow = cellList.get(0).getRow();
        int maxCol = cellList.get(0).getColumn();

        for (Cell aCell : cellList)
        {
            if (aCell.getRow() > maxRow)
            {
                maxRow = aCell.getRow();
            }

            if (aCell.getColumn() > maxCol)
            {
                maxCol = aCell.getColumn();
            }
        }

        lowerRight = new Point(maxRow, maxCol);

        return lowerRight;
    }


    /**
     * Devuelve verdadero si la celda esta contenida en el patrón.
     *
     * @param aCell
     * @return
     */
    public boolean Contains(Cell aCell)
    {
        for (Cell currentCell : this.cells)
        {
            if (aCell.hasSameCoordinatesAs(currentCell))
            {
                return true;
            }
        }
        return false;
    }

    /**
     * Si la celda con la coordenadas x,y especificadas existe entonces retorna
     * su indice.
     *
     * @param row
     * @param column
     * @return
     */
    public int getCellIndex(int row, int column)
    {
        return (this.getCells().indexOf(this.getCell(row, column)));
    }

    /**
     * Busca si existe una celda con las mismas coordenadas en el patrón y la
     * reemplaza por el valor de cell.
     *
     * @param cell
     */
    public void updateCell(Cell cell)
    {
        if (this.getCell(cell.getRow(), cell.getColumn()) != null)
        {
            this.getCell(cell.getRow(), cell.getColumn()).setStateId(cell.getStateId());

        }
        else
        {
            this.addCell(cell);
        }
        /*
        Collections.replaceAll(
            this.getCells(),
            this.getCell(cell.getRow(), cell.getColumn()),
            cell);
         */
    }

    /**
     * Convierte la instancia en un string con el formato para guardar en
     * archivo.
     *
     * @return
     */
    @Override
    public String toString()
    {
        return CustomPatternParser.convertToString(this);
    }

    //**************************************************************************
}
