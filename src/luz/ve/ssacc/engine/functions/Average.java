/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package luz.ve.ssacc.engine.functions;

import java.awt.Color;
import luz.ve.ssacc.engine.components.Automaton;
import luz.ve.ssacc.engine.components.Cell;
import luz.ve.ssacc.engine.components.State;
import luz.ve.ssacc.utilities.SacUtility;

/**
 *
 * @author Renzo Ciafardone Sciannelli
 */
public class Average implements Function
{
    private static final String NAME="average";
    
    @Override
    /**
     * 
     */
    public String getName()
    {
        return NAME;
    }

    @Override
    public String apply(State state, int field, Cell currentCell, Automaton automata)
    {
        if (automata.getNeighborhood().getCellsList().isEmpty())
        {
            throw new UnsupportedOperationException("Lista Vacia");
        }
       
        switch (field)
        {
            case (State.FIELD_NUM_ID):
            {
                return averageId(state, currentCell, automata).toString();
            }

            case (State.FIELD_NUM_TEXT):
            {
                return averageText(state, currentCell, automata);
            }
            
            case (State.FIELD_NUM_VALUE):
            {
                return averageValue(state, currentCell, automata).toString();
            }
           
            case (State.FIELD_NUM_COLOR):
            {
               return averageColor(state, currentCell, automata).toString();
            }
        }


        return null;
    }

    private Integer averageId(State state, Cell cell, Automaton automata)
    {
        Integer id = 0;
        int count = 0;

        for (Cell nCell : automata.getNeighborhood().getCellsList(cell))
        {
            if (state.getId().equals(automata.getGrids().getStateId(nCell)))
            {
                id = id + automata.getGrids().getStateId(nCell);
                count++;
            }
        }
        id = id / count;

        return id;
    }

    private String averageText(State state, Cell cell, Automaton automata)
    {
        StringBuilder text = new StringBuilder();
        //TODO: determinar que significa average para un string
        return text.toString();
    }
    
    private Double averageValue(State state, Cell cell, Automaton automata)
    {
        Double value = 0.0;
        int count = 0;

        for (Cell nCell : automata.getNeighborhood().getCellsList(cell))
        {
            if (state.getId().equals(automata.getGrids().getStateId(nCell)))
            {
                value = value + automata.getStates().getState(automata.getGrids().getStateId(nCell)).getValue();
                count++;
            }
        }
        value = value / count;
        return value;
    }

    private Color averageColor(State state, Cell cell, Automaton automata)
    {
        boolean firstColorFlag = true;
        Color color = Color.BLACK;
        for (Cell nCell : automata.getNeighborhood().getCellsList(cell))
        {
            if (state.getId().equals(automata.getGrids().getStateId(nCell)))
            {
                if (firstColorFlag)
                {
                    color = automata.getStates().getState(automata.getGrids().getStateId(nCell)).getColor();
                    firstColorFlag = false;
                } else
                {
                    color = SacUtility.blend(color, automata.getStates().getState(automata.getGrids().getStateId(nCell)).getColor());
                }
            }
        }
        return color;
    }
    
    @Override
    public String toString()
    {
        return this.getName();
    }

//******************************************************************************
}
