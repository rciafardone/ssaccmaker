/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package luz.ve.ssacc.engine.functions;

import java.awt.Color;
import luz.ve.ssacc.engine.components.Automaton;
import luz.ve.ssacc.engine.components.Cell;
import luz.ve.ssacc.engine.components.State;
import luz.ve.ssacc.utilities.SacUtility;

/**
 *
 * @author Renzo Ciafardone Sciannelli
 */
public class Min implements Function
{

    private static final String NAME = "min";

    @Override
    /**
     *
     */
    public String getName()
    {
        return NAME;
    }

    @Override
    public String apply(State state, int field, Cell currentCell, Automaton automata)
    {
        if (automata.getNeighborhood().getCellsList().isEmpty())
        {
            throw new UnsupportedOperationException("Lista Vacia");
        }

         switch (field)
        {
            case (State.FIELD_NUM_ID):
            {
                return minId(currentCell, automata).toString();
            }
            case (State.FIELD_NUM_TEXT):
            {
                return minText(currentCell, automata);
            }
            case (State.FIELD_NUM_VALUE):
            {
                return minValue(currentCell, automata).toString();
            }
            case (State.FIELD_NUM_COLOR):
            {
                return minColor(currentCell, automata).toString();
            }
        }

        return null; //Nunca debe llegar aqui
    }

    private Short minId(Cell cell, Automaton automata)
    {
        Short id = 0;
        for (Cell nCell : automata.getNeighborhood().getCellsList(cell))
        {
            if (id > automata.getGrids().getStateId(nCell))
            {
                id = automata.getGrids().getStateId(nCell);
            }
        }

        return id;
    }
    
    private String minText(Cell cell, Automaton automata)
    {
        String text = "";
        for (Cell nCell : automata.getNeighborhood().getCellsList(cell))
        {
            if (text.compareTo(automata.getStates().getState(automata.getGrids().getStateId(nCell)).getText()) > 0)
            {
                text = automata.getStates().getState(automata.getGrids().getStateId(nCell)).getText();
            }
        }
        return text;

    }

    private Double minValue(Cell cell, Automaton automata)
    {
        Double value = 0.0;
        for (Cell nCell : automata.getNeighborhood().getCellsList(cell))
        {
            if (value > automata.getStates().getState(automata.getGrids().getStateId(nCell)).getValue())
            {
                value = automata.getStates().getState(automata.getGrids().getStateId(nCell)).getValue();
            }
        }
        return value;
    }

    private Color minColor(Cell cell, Automaton automata)
    {
        Color color=Color.BLACK;
        for (Cell nCell : automata.getNeighborhood().getCellsList(cell))
        {
            if (SacUtility.compareColors(color, automata.getStates().getState(automata.getGrids().getStateId(nCell)).getColor()) > 0)
            {
                color = automata.getStates().getState(automata.getGrids().getStateId(nCell)).getColor();
            }
        }

        return color;
    }
    
    @Override
    public String toString()
    {
        return this.getName();
    }

//******************************************************************************
}
