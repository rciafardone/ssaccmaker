/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package luz.ve.ssacc.engine.functions;

import luz.ve.ssacc.engine.components.Automaton;
import luz.ve.ssacc.engine.components.State;
import luz.ve.ssacc.engine.components.Cell;


/**Esta es la interfaz para el strategy pattern de las funciones, el context es
 * la clase Operand.
 *
 * @author renzo
 */
public interface Function
{
    
    /**
     *This is the current list of possible functions.
     */
    public static final String[] FUNCTION_LIST ={"add", "average", "constant", "count", "max", "min"};    
    
    /**
    * Esta funcion aplica la condicion
    * @param variable
    * @param field
    * @param currentCell
    * @param automata
    * @return 
    */
    public String apply(State variable, int field, Cell currentCell, Automaton automata);
    
 
    public String getName();
    
    
    
//******************************************************************************    
}
