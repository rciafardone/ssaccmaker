/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package luz.ve.ssacc.engine.functions;

import luz.ve.ssacc.engine.components.Automaton;
import luz.ve.ssacc.engine.components.Cell;
import luz.ve.ssacc.engine.components.State;

/**
 *
 * @author Renzo Ciafardone Sciannelli
 */
public class Constant implements Function
{

    private static final String NAME = "constant";

    @Override
    public String getName()
    {
        return NAME;
    }

    @Override
    public String apply(State state, int field, Cell currentCell, Automaton automata)
    {
         switch (field)
        {
            case (State.FIELD_NUM_ID):
            {
                return Integer.toString(state.getId());
            }

            case (State.FIELD_NUM_TEXT):
            {
                return state.getText();
            }

            case (State.FIELD_NUM_VALUE):
            {
                return Double.toString(state.getValue());
            }

            case (State.FIELD_NUM_COLOR):
            {
                return state.getColor().toString();
            }
        }
        return null; //no debe nunca llegar aqui
    }

    @Override
    public String toString()
    {
        return this.getName();
    }

//******************************************************************************
}
