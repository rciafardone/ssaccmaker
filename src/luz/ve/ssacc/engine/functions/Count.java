/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package luz.ve.ssacc.engine.functions;
import java.util.List;
import luz.ve.ssacc.engine.components.Automaton;
import luz.ve.ssacc.engine.components.Cell;
import luz.ve.ssacc.engine.components.State;
/**
 * 
 * comway
 * @author Renzo Ciafardone Sciannelli
 */
public class Count implements Function
{
    private static final String NAME="count";
    
    @Override
    /**
     * 
     */
    public String getName()
    {
        return NAME;
    }

   
    @Override
    /**
     * Count cuenta el campo indicado en field pero devuelve el resultado en VALUE
     */
    public String apply(State state, int field, Cell currentCell, Automaton automata)
    {
        Double total = 0.0;
       
        
        
        if (automata.getNeighborhood().getCellsList().isEmpty())
        {
            throw new UnsupportedOperationException(">>Lista Vacia - Count<<");
        }

        List<Cell> neighborhood = automata.getNeighborhood().getCellsList(currentCell);
        
        switch (field)
        {
            case (State.FIELD_NUM_ID):
            {
                for (Cell nCell : neighborhood)
                {
                    if (state.getId().equals(automata.getGrids().getStateId(nCell)))
                    {
                        total++;
                    }
                }
                break;
            }
            case (State.FIELD_NUM_TEXT):
            {
                for (Cell nCell : neighborhood)
                {
                    if (state.getText().equals(automata.getStates().getState(automata.getGrids().getStateId(nCell)).getText()))
                    {
                        total++;
                    }
                }
                break;
            }
            case (State.FIELD_NUM_VALUE):
            {
               for (Cell nCell : neighborhood)
                {
                    if (state.getValue().equals(automata.getStates().getState(automata.getGrids().getStateId(nCell)).getValue()))
                    {
                        total++;
                    }
                }
                break;
            }
            
            case (State.FIELD_NUM_COLOR):
            {
               for (Cell nCell : neighborhood)
                {
                    if (state.getColor().equals(automata.getStates().getState(automata.getGrids().getStateId(nCell)).getColor()))
                    {
                        total++;
                    }
                }
                break;
            }
        }

        return total.toString();
    }

    
   @Override
    public String toString()
    {
        return this.getName();
    }

//******************************************************************************
}
