/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package luz.ve.ssacc.engine.functions;

import java.awt.Color;
import luz.ve.ssacc.engine.components.Automaton;
import luz.ve.ssacc.engine.components.Cell;
import luz.ve.ssacc.engine.components.State;
import luz.ve.ssacc.utilities.SacUtility;

/**
 *
 * @author Renzo Ciafardone Sciannelli
 */
public class Max implements Function
{
   private static final String NAME="max";
    
    @Override
    /**
     * 
     */
    public String getName()
    {
        return NAME;
    }

    @Override

    public String apply(State state, int field, Cell currentCell, Automaton automata)
    {
        if (automata.getNeighborhood().getCellsList().isEmpty())
        {
            throw new UnsupportedOperationException("Lista Vacia");
        }

        switch (field)
        {
            case (State.FIELD_NUM_ID):
            {
                return maxId(currentCell, automata).toString();
            }

            case (State.FIELD_NUM_TEXT):
            {
                return maxText(currentCell, automata);
            }

            case (State.FIELD_NUM_VALUE):
            {
                return maxValue(currentCell, automata).toString();
            }

            case (State.FIELD_NUM_COLOR):
            {
                return maxColor(currentCell, automata).toString();
            }
        }

        return null; //Nunca debe llegar aqui
    }

    /**
     * Devuelve el id de mayor valor.
     * @param cell
     * @param automata
     * @return 
     */
    private Short maxId(Cell cell, Automaton automata)
    {
        Short id = 0;
        for (Cell nCell : automata.getNeighborhood().getCellsList(cell))
        {
            if (id < automata.getGrids().getStateId(nCell))
            {
                id = automata.getGrids().getStateId(nCell);
            }
        }

        return id;
    }

    /**
     * Devuelve el string mas alto alfabeticamente (z>a).
     * @param cell
     * @param automata
     * @return 
     */
    private String maxText(Cell cell, Automaton automata)
    {
        String text = "";
        for (Cell nCell : automata.getNeighborhood().getCellsList(cell))
        {
            if (text.compareTo(automata.getStates().getState(automata.getGrids().getStateId(nCell)).getText()) < 0)
            {
                text = automata.getStates().getState(automata.getGrids().getStateId(nCell)).getText();
            }
        }
        return text;
    }

    /**
     * Devuelve el valor mas alto.
     * @param cell
     * @param automata
     * @return 
     */
    private Double maxValue(Cell cell, Automaton automata)
    {
        Double value = 0.0;
        for (Cell nCell : automata.getNeighborhood().getCellsList(cell))
        {
            if (value < automata.getStates().getState(automata.getGrids().getStateId(nCell)).getValue())
            {
                value = automata.getStates().getState(automata.getGrids().getStateId(nCell)).getValue();
            }
        }
        return value;

    }

    
    private Color maxColor(Cell cell, Automaton automata)
    {
        Color color=Color.BLACK;
        for (Cell nCell : automata.getNeighborhood().getCellsList(cell))
        {
            if (SacUtility.compareColors(color, automata.getStates().getState(automata.getGrids().getStateId(nCell)).getColor()) < 0)
            {
                color = automata.getStates().getState(automata.getGrids().getStateId(nCell)).getColor();
            }
        }

        return color;
    }
    
    
    @Override
    public String toString()
    {
        return this.getName();
    }

//******************************************************************************    
}
