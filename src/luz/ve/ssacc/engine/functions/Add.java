/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package luz.ve.ssacc.engine.functions;

import java.awt.Color;
import luz.ve.ssacc.engine.components.Automaton;
import luz.ve.ssacc.engine.components.Cell;
import luz.ve.ssacc.engine.components.State;
import luz.ve.ssacc.utilities.SacUtility;

/**
 *
 * Suma todos los elementos del conjunto de valores
 *
 * @author Renzo Ciafardone Sciannelli
 */
public class Add implements Function
{
    private static final String NAME="add";
    
    @Override
    /**
     * 
     */
    public String getName()
    {
        return NAME;
    }

    @Override
    public String apply(State state, int field, Cell currentCell, Automaton automata)
    {

        int id = 0;
        StringBuilder text = new StringBuilder();
        Double value = 0.0;
        Color color = Color.BLACK;

        if (automata.getNeighborhood().getCellsList().isEmpty())
        {
            throw new UnsupportedOperationException("Add - apply - Lista Vencindario Vacia");
        }

        switch (field)
        {
            case (State.FIELD_NUM_ID):
            {
                for (Cell nCell : automata.getNeighborhood().getCellsList(currentCell))
                {
                    if (state.getId().equals(automata.getGrids().getStateId(nCell)))
                    {
                        id = id + automata.getGrids().getStateId(nCell);
                    }
                }
                return Integer.toString(id);
            }

            case (State.FIELD_NUM_TEXT):
            {
                for (Cell nCell : automata.getNeighborhood().getCellsList(currentCell))
                {
                    if (state.getId().equals(automata.getGrids().getStateId(nCell)))
                    {
                        text.append(automata.getGrids().getStateId(nCell));
                    }
                }
                return text.toString();
            }

            case (State.FIELD_NUM_VALUE):
            {

                for (Cell nCell : automata.getNeighborhood().getCellsList(currentCell))
                {
                    if (state.getId().equals(automata.getGrids().getStateId(nCell)))
                    {
                        value = value + automata.getGrids().getStateId(nCell);
                    }
                }
                return Double.toString(value);
            }

            case (State.FIELD_NUM_COLOR):
            {
                boolean firstColorFlag = true;
                for (Cell nCell : automata.getNeighborhood().getCellsList(currentCell))
                {
                    if (state.getId().equals(automata.getGrids().getStateId(nCell)))
                    {
                        if (firstColorFlag)
                        {
                            color = automata.getStates().getState(automata.getGrids().getStateId(nCell)).getColor();
                            firstColorFlag = false;

                        } else
                        {
                            color = SacUtility.blend(color, automata.getStates().getState(automata.getGrids().getStateId(nCell)).getColor());
                        }
                    }
                }
                return color.toString();
            }
        }
        return null; //no debe nunca llegar aqui
    }
        
    @Override
    public String toString()
    {
        return this.getName();
    }

}
//******************************************************************************   

