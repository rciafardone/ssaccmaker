/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package luz.ve.ssacc.utilities;

import java.awt.Color;
import luz.ve.ssacc.engine.components.Automaton;
import java.io.File;
import javax.swing.*;
import java.util.*;
import luz.ve.ssacc.engine.components.Condition;
import luz.ve.ssacc.engine.components.Grid;
import luz.ve.ssacc.engine.components.Rule;
import luz.ve.ssacc.engine.components.State;


/**
 *
 * @author "Renzo Ciafardone Sciannelli"
 */
public class SacUtility
{

    //**************************************************************************
    //*****************************VARIABLES************************************
    //**************************************************************************
    //File extentions
    /*
    public final static String adac = "adac";
    public final static String sacs = "sacs";
    public final static String sacn = "sacn";
    public final static String sacr = "sacr";
    public final static String sacg = "sacg";
    public final static String sacp = "sacp";
    public final static String saci = "saci";
    public final static String sace = "sace";
    public final static String sact = "sact";
     */
    // Edit Options: a Window can be opened in 2 ways: Edit or New.
    public final static int CANCEL = -1;  
    public final static int ADD_NEW = 0; // Add: Opens the window with no data loaded
    public final static int EDIT_CURRENT = 1; // Edit: loads the data from a SACO file and allows the edition.
    public final static int DEL = 2; // DEL: Current Loaded file will be discarted.
    public final static int LOAD_FILE = 3; // LOAD_FILE: Loads a file.
    public final static int OK = 4; 
    
    public final static String EOL = System.getProperty("line.separator");
    //**************************************************************************
    //*****************************CONSTRUCTORS*********************************
    //**************************************************************************

    //**************************************************************************
    //*****************************METHODS**************************************
    //**************************************************************************

    
    /**
     * Copia el contenido de source en el grid destiny.
     * @param source Grid de origen
     * @param destiny Grid de destino
     * @return Verdadero si la copia fue exitosa.
     */
    public static Grid gridCopy(Grid source, Grid destiny)
    {
        
       int rowMin; //smaller value for rows
       int colMin; //smaller value for columns
      
        if (destiny.getRows() <= source.getRows())
        {
            rowMin = destiny.getRows();
        }
        else
        {
            rowMin = source.getRows();
        }

        if (destiny.getColumns() <= source.getColumns())
        {
            colMin = destiny.getColumns();
        }
        else
        {
            colMin = source.getColumns();
        }

        
        for (int rowIndex = 0; rowIndex < rowMin; rowIndex++)
        {
            System.arraycopy(source.getArray()[rowIndex], 0, destiny.getArray()[rowIndex], 0, colMin);
        }


        return destiny;
    }
    
    /**
     * Crea un estado no default.
     *
     * @param id
     * @return
     */
    public static State createDummyState(Short id)
    {
        State randomState = new State(id);

        randomState.setText("text" + id);
        randomState.setValue((double) id);
        randomState.setColor(SacUtility.getRandomColor());
        
        return randomState;
    }
    
    public static Integer convertStringDoubleToInteger(String doubleValue)
    {
        Integer i = new Double(doubleValue).intValue();
        return i;
    }
    
    public static void test(Automaton adac, JTextArea area)
    {
        // writeBunchOfNumbers(area);
  
            printNeighborhood(adac, area);
            
    }
    
    public static void test(Integer a)
    {
        SacUtility.showWarning(a.toString());
    }

    /**
     * Retorna un color aleatorio.
     * @return 
     */
    public static Color getRandomColor()
    {
        int r;
        int g;
        int b;
        Random randomGenerator = new Random();
        r = randomGenerator.nextInt(256);
        g = randomGenerator.nextInt(256);
        b = randomGenerator.nextInt(256);

        return new Color(r, g, b);
    }

    
    public static String printNeighborhood(Automaton adac, JTextArea area)
    {
        StringBuilder neigh = new StringBuilder();
        
        double start = System.currentTimeMillis();

        neigh.append(adac.getNeighborhood().getCellsList().toString());

        double end = System.currentTimeMillis();

        neigh.append("\n\nExecution time was ").append(end-start).append(" ms.");
        area.setText(neigh.toString());
        
        return neigh.toString();
    }
    
    
    
    
    /*
     * Get the extension of a file.
     */
    public static String getExtension(File f)
    {
        String s = f.getName();
        int i = s.lastIndexOf('.');

        if (i > 0 && i < s.length() - 1)
        {
            return s.substring(i + 1).toLowerCase();
        }
        return null;
    }


    /*
     * Returns an ImageIcon, or null if the path was invalid.
     */
    protected static ImageIcon createImageIcon(String path)
    {
        java.net.URL imgURL = SacUtility.class.getResource(path);
        if (imgURL != null)
        {
            return new ImageIcon(imgURL);
        } else
        {
            return null;
        }
    }


    
    public static ComboBoxModel<String> convertFileListToComboBoxModel(ArrayList<File> aList)
    {
        String[] stringList = new String[aList.size()];

        for (int i = 0; i < aList.size(); i++)
        {
            stringList[i] = aList.get(i).getName();
        }

        return (new JComboBox(stringList).getModel());
    }

    public static void updateComboBoxList(JComboBox[] cbList, int fileType)
    {
        cbList[fileType].setModel(convertFileListToComboBoxModel(SacFileManager.getListOfFiles(fileType)));
    }


   
  
    
   
    public static String convertArrayToString(String[] sArray)
    {
        StringBuilder sResult = new StringBuilder();

        if (sArray == null)
        {
            return null;
        }

        if (sArray.length > 0)
        {
            sResult.append(sArray[0]);
            for (int i = 1; i < sArray.length; i++)
            {
                sResult.append("\n").append(sArray[i]);
            }
        }
        return sResult.toString();
    }

    
 
    /**
     * 
     * Muestra los mensajes de error causados por los metodos de parsing
     * @param sError El error que se desea mostrar
     */
    public static void showParsingError(String sError)
    {
        JOptionPane errorDialog = new JOptionPane();
        JOptionPane.showMessageDialog(errorDialog, sError, "Error Grave", JOptionPane.WARNING_MESSAGE);
    }
    
    /**
     * Muestra los mensajes de advertencia causados por los metodos de parsing
     * @param sWarning La advertencia que se desea mostrar
     */
    public static void showWarning(String sWarning)
    {
        JOptionPane errorDialog = new JOptionPane();
        JOptionPane.showMessageDialog(errorDialog, sWarning, "¡ATENCIÓN!", JOptionPane.WARNING_MESSAGE);
    }

    /**
     *
     * @author Cameron Behar 
     * Fuente: http://www.java2s.com/Code/Java/2D-Graphics-GUI/Blendtwocolors.htm
     * @param c0
     * @param c1
     * @return
     */
    public static Color blend(Color c0, Color c1)
    {
        double totalAlpha = c0.getAlpha() + c1.getAlpha();
        double weight0 = c0.getAlpha() / totalAlpha;
        double weight1 = c1.getAlpha() / totalAlpha;

        double r = weight0 * c0.getRed() + weight1 * c1.getRed();
        double g = weight0 * c0.getGreen() + weight1 * c1.getGreen();
        double b = weight0 * c0.getBlue() + weight1 * c1.getBlue();
        double a = Math.max(c0.getAlpha(), c1.getAlpha());

        return new Color((int) r, (int) g, (int) b, (int) a);
    }
    
    /**
     * Cambia un color en base al valor que se le pasa
     * @param c0
     * @param modifier
     * @return 
     */
    public static Color shiftColor(Color c0, Short modifier)
    {
        double m = (modifier * 35);
        double r = (c0.getRed() + m)%255;
        double g = (c0.getGreen() + m)%255;
        double b = (c0.getBlue() + m)%255;
        double a = (c0.getAlpha());

        return new Color((int) r, (int) g, (int) b, (int) a);
    }


/*
    public int getPopularElement(List<Cell> lista)
    {
        Cell[] array = new Cell[lista.size()];
        lista.toArray(array); // fill the array
        
        int count = 1;
        int tempCount;
        int popular = array[0];
        int temp = 0;
        
        for (int i = 0; i < (a.length - 1); i++)
        {
            temp = a[i];
            tempCount = 0;
            for (int j = 1; j < a.length; j++)
            {
                if (temp == a[j])
                {
                    tempCount++;
                }
            }
            if (tempCount > count)
            {
                popular = temp;
                count = tempCount;
            }
        }
        
        return popular;
    }
   */
    
    public static int compareColors(Color c1, Color c2)
    {
        return Integer.compare(c1.getRed() + c1.getGreen() + c1.getBlue(), c2.getRed() + c2.getGreen() + c2.getBlue());
    }

    /**
     * Devuelve verdadero si todos los estados Iniciales y Finales de las reglas
     * estan representados en el conjunto de estados. Solo verifica los IDs de
     * los estados.
     *
     * @param automaton
     * @return
     */
    public static boolean checkStatesRulesMatch(Automaton automaton)
    {
        StringBuilder stateNotFound = new StringBuilder();
        boolean match = true;
        
        for (Rule rule : automaton.getRules().getList()) // Estados de inicio y final
        {
            if (!automaton.getStates().conteinsId(rule.getStateIniId())) //Verifica existencia de estado inicial en conjunto de reglas
            {
                stateNotFound.setLength(0);
                stateNotFound.append("El estado INICIAL de la regla:").append(EOL).append(EOL);
                stateNotFound.append(rule.toString());
                stateNotFound.append(EOL);
                stateNotFound.append("No se encuentra en el conjunto de estados en uso:").append(EOL);
                stateNotFound.append(automaton.getStates().getName()).append(EOL);
                stateNotFound.append("Se recomienda cargar el conjunto de estados que fue utilizado para crear las reglas, o editar el existente para agregar los faltantes.");
                stateNotFound.append(EOL).append("De lo contrario el sistema no se comportará de manera coherente durante la edición y simulación");
                SacUtility.showWarning(stateNotFound.toString());
                match = false;
            }

            if (!automaton.getStates().conteinsId(rule.getStateEndId())) //Verifica existencia de estado final en conjunto de reglas
            {
                stateNotFound.setLength(0);
                stateNotFound.append("El estado FINAL de la regla:").append(EOL).append(EOL);
                stateNotFound.append(rule.toString());
                stateNotFound.append(EOL);
                stateNotFound.append("No se encuentra en el conjunto de estados en uso:").append(EOL).append(EOL);
                stateNotFound.append(automaton.getStates().getName()).append(EOL).append(EOL);
                stateNotFound.append("Se recomienda cargar el conjunto de estados que fue utilizado para crear las reglas, o editar el existente para agregar los faltantes.");
                stateNotFound.append(EOL).append("De lo contrario el sistema no se comportará de manera coherente durante la edición y simulación");
                SacUtility.showWarning(stateNotFound.toString());
                match = false;
            }
            
            for (Condition condition : rule.getConditions()) //Estados en operandos
            {
                if (!automaton.getStates().conteinsId(condition.getOperandA().getVariable().getId())) //Verifica existencia de estado en el operando A de las reglas
                {
                    stateNotFound.setLength(0);
                    stateNotFound.append("No existe en el conjunto de estados en uso el estado variable con ID = ");
                    stateNotFound.append(condition.getOperandA().getVariable().getId());
                    stateNotFound.append(" del operando A de la condición: ").append(EOL).append(condition.toString()).append(EOL);
                    stateNotFound.append("Regla: ").append(EOL);
                    stateNotFound.append(rule.toString()).append(EOL);
                    stateNotFound.append("Conjunto de estados en uso:").append(EOL).append(EOL);
                    stateNotFound.append(automaton.getStates().getName()).append(EOL).append(EOL);
                    stateNotFound.append("Se recomienda cargar el conjunto de estados que fue utilizado para crear las reglas, o editar el existente para agregar los faltantes.");
                    stateNotFound.append(EOL).append("De lo contrario el sistema no se comportará de manera coherente durante la edición y simulación");
                    SacUtility.showWarning(stateNotFound.toString());
                    match = false;
                }

                if (!automaton.getStates().conteinsId(condition.getOperandB().getVariable().getId())) //Verifica existencia de estado en el operando B de las reglas
                {
                    stateNotFound.setLength(0);
                    stateNotFound.append("No existe en el conjunto de estados en uso el estado variable con ID = ");
                    stateNotFound.append(condition.getOperandB().getVariable().getId());
                    stateNotFound.append(" del operando B de la condición: ").append(EOL).append(condition.toString()).append(EOL);
                    stateNotFound.append("Regla: ").append(EOL);
                    stateNotFound.append(rule.toString()).append(EOL);
                    stateNotFound.append("Conjunto de estados en uso:").append(EOL).append(EOL);
                    stateNotFound.append(automaton.getStates().getName()).append(EOL).append(EOL);
                    stateNotFound.append("Se recomienda cargar el conjunto de estados que fue utilizado para crear las reglas, o editar el existente para agregar los faltantes.");
                    stateNotFound.append(EOL).append("De lo contrario el sistema no se comportará de manera coherente durante la edición y simulación");
                    SacUtility.showWarning(stateNotFound.toString());
                    match = false;
                }
            }
        }
        return match;
    }

    /*
     * ****************************END OF CLASS********************************
     * ****************************END OF CLASS********************************
     * ****************************END OF CLASS************************R.C.S.**
     */
}
