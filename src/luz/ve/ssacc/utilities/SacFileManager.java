/*
 * This class will deal with all the action related to the input and output
 * from SSACC MOD 1. It will also create the default directories necesaries to
 * store the ADAC and SACO files.
 */

package luz.ve.ssacc.utilities;


import java.io.*;
import java.net.URISyntaxException;
import java.util.*;
import javax.swing.*;
import luz.ve.ssacc.engine.components.Automaton;

/**
 *
 * @author "Renzo Ciafardone Sciannelli"
 */
public class SacFileManager {
    //**************************************************************************
    //*****************************VARIABLES************************************
    //**************************************************************************
    public static final int SAC_FILE_NUM = 8; //Number of Types or Files that SSACC uses to store AC.
    private static final String[] DESCRIPTION_LIST = new String[SAC_FILE_NUM];
    private static final String[] FILE_EXT = new String[SAC_FILE_NUM];
    public static final String[] SAC_PATHS = new String[SAC_FILE_NUM];
   
    //types of files
    public final static int SAC_ADAC = 0;
    public final static int SAC_STATES = 1;
    public final static int SAC_RULES = 2;
    public final static int SAC_NEIGHBORHOOD = 3;
    public final static int SAC_GRID = 4;
    public final static int SAC_ANIMATION = 5;
    public final static int SAC_PATTERN = 6;
    public final static int SAC_STATISTICS = 7;
    // public final static int SAC_TEST = 8;

    //fileToSave extentions
    public final static String EXT_ADAC = "adac";
    public final static String EXT_STATES = "sacs";
    public final static String EXT_RULES = "sacr";
    public final static String EXT_NEIGHBORHOOD = "sacn";
    public final static String EXT_GRID = "sacg";
    public final static String EXT_ANIMATION = "saca";
    public final static String EXT_PATTERN = "sacp";
    public final static String EXT_STATISTICS = "sace";

    // fileToSave descriptions
    private final static String DESC_ADAC = ".adac - archivo adac";
    private final static String DESC_STATES = ".sacs - archivo de estados";
    private final static String DESC_RULES = ".sacr - archivo de reglas";
    private final static String DESC_NEIGHBORHOOD = ".sacn - archivo de vecindario";
    private final static String DESC_GRID = ".sacg - archivo de retícula";
    private final static String DESC_ANIMATION = ".saca - archivo de animación";
    private final static String DESC_PATTERN = ".sacp - archivo de patrones";
    private final static String DESC_STATISTICS = ".sace - archivo de estadisticas";

    // fileToSave paths
    public final static String APP_PATH = SacFileManager.getJarPath();
    public final static String FILE_SEPARATOR = java.io.File.separator;

    private final static String DATA_PATH = APP_PATH + FILE_SEPARATOR + "data";

    private final static String FILE_PATH_ADAC = DATA_PATH + FILE_SEPARATOR + "adac"; //automatas
    private final static String FILE_PATH_SACA = DATA_PATH + FILE_SEPARATOR + "saca"; //animaciones
    private final static String FILE_PATH_SACG = DATA_PATH + FILE_SEPARATOR + "sacg"; //reticulas
    private final static String FILE_PATH_SACN = DATA_PATH + FILE_SEPARATOR + "sacn"; //vecindarios
    private final static String FILE_PATH_SACP = DATA_PATH + FILE_SEPARATOR + "sacp"; //patrones
    private final static String FILE_PATH_SACR = DATA_PATH + FILE_SEPARATOR + "sacr"; //reglas
    private final static String FILE_PATH_SACS = DATA_PATH + FILE_SEPARATOR + "sacs"; //estados
    private final static String FILE_PATH_SACE = FILE_PATH_SACA; //DATA_PATH + FILE_SEPARATOR + "sace"; //estadisticas
    
//  private final static String FILE_PATH_TEST = DATA_PATH + FILE_SEPARATOR + "test"; //pruebas

    //  public static String functionListFilePathFull;
    //  public static String variableListFilePathFull;


    //**************************************************************************
    //*****************************CONSTRUCTORS*********************************
    //**************************************************************************
    public SacFileManager()
    {
        setConstants();
    }

    //**************************************************************************
    //*****************************METHODS**************************************
    //**************************************************************************

    private static void setConstants()
    {
        FILE_EXT[SAC_ADAC] = EXT_ADAC;
        FILE_EXT[SAC_STATES] = EXT_STATES;
        FILE_EXT[SAC_RULES] = EXT_RULES;
        FILE_EXT[SAC_NEIGHBORHOOD] = EXT_NEIGHBORHOOD;
        FILE_EXT[SAC_GRID] = EXT_GRID;
        FILE_EXT[SAC_PATTERN] = EXT_PATTERN;
        FILE_EXT[SAC_ANIMATION] = EXT_ANIMATION;
        FILE_EXT[SAC_STATISTICS] = EXT_STATISTICS;

        DESCRIPTION_LIST[SAC_ADAC] = DESC_ADAC;
        DESCRIPTION_LIST[SAC_STATES] = DESC_STATES;
        DESCRIPTION_LIST[SAC_RULES] = DESC_RULES;
        DESCRIPTION_LIST[SAC_NEIGHBORHOOD] = DESC_NEIGHBORHOOD;
        DESCRIPTION_LIST[SAC_GRID] = DESC_GRID;
        DESCRIPTION_LIST[SAC_PATTERN] = DESC_PATTERN;
        DESCRIPTION_LIST[SAC_ANIMATION] = DESC_ANIMATION;
        DESCRIPTION_LIST[SAC_STATISTICS] = DESC_STATISTICS;

    //    SAC_PATHS[SAC_ENGLISH] = filePathEng_;
        
        SAC_PATHS[SAC_ADAC] = FILE_PATH_ADAC;
        SAC_PATHS[SAC_STATES] = FILE_PATH_SACS;
        SAC_PATHS[SAC_RULES] = FILE_PATH_SACR;
        SAC_PATHS[SAC_NEIGHBORHOOD] = FILE_PATH_SACN;
        SAC_PATHS[SAC_GRID] = FILE_PATH_SACG;
        SAC_PATHS[SAC_PATTERN] = FILE_PATH_SACP;
        SAC_PATHS[SAC_ANIMATION] = FILE_PATH_SACA;
        SAC_PATHS[SAC_STATISTICS] = FILE_PATH_SACE;
        // SAC_PATHS[SAC_TEST] = FILE_PATH_TEST;
        
    }

    

    /**
     * Devuelve un String con la ruta donde esta el .jar
     *
     * @return
     */
    public static String getJarPath()
    {
        try
        {
            File a = new File(SacFileManager.class.getProtectionDomain().getCodeSource().getLocation().toURI().getPath());
            StringBuilder jarPath = new StringBuilder();
            jarPath.append(a.getCanonicalPath());
            return jarPath.substring(0, jarPath.lastIndexOf(File.separator));
        } catch (URISyntaxException | IOException e)
        {
            return (e.getMessage());
        }
    }

    /**
     * Devuelve el filtro según el tipo de archivo
     * @param fileType
     * @return
     */
    public static SacFileFilter getFileFilter(int fileType)
   {
       SacFileFilter aFilter = new SacFileFilter(DESCRIPTION_LIST[fileType], FILE_EXT[fileType]);
       return aFilter;
   }

    /*
     * returns the current application directory path
     */
    public static File getAppDirectory()
    {
        File appDir = new File(APP_PATH);
        return appDir;
    }

    
    /*
     * Returns a File variable with a default name for fileToSave to save
     * which is the same as the name field unless user chooses otherwise.
     */
    private static File getDefaultName(int fileType, String fileName)
    {
        StringBuilder aFilePath = new StringBuilder();
        aFilePath.append(SAC_PATHS[fileType]).append(FILE_SEPARATOR);

        aFilePath.append(fileName);
        aFilePath.append(".");
        aFilePath.append(FILE_EXT[fileType]);
        
        File fDir = new File(aFilePath.toString());
        return fDir;
    }

    /**
     * Activa el dialogo para abrir los archivos SACS
     * @param wFrame
     * @param fileType
     * @return 
     */
    public static String getSacFileName(
        JFrame wFrame,
        int fileType)
    {
        SacFileChooser cFileChooser = new SacFileChooser();

        cFileChooser.setCurrentDirectory(new File(SAC_PATHS[fileType]));
        cFileChooser.setFileFilter(getFileFilter(fileType));
        int returnVal = cFileChooser.showOpenDialog(wFrame);

        if (returnVal == JFileChooser.APPROVE_OPTION)
        {
            return cFileChooser.getSelectedFile().getAbsolutePath();
        }

        return null;
    }

        
    /**
     * Activa el dialogo para salvar los archivos SAC.
     * @param currentAc
     * @param editOption
     * @param frame
     * @param fileType
     * @return 
     */
    public static int saveSacFile(
        Automaton currentAc,
        int editOption,
        JFrame frame,
        int fileType)
    {
        SacFileChooser cFileChooser = new SacFileChooser();
        // cFileChooser.setCurrentDirectory(getSacoDirectory(fileType));
        cFileChooser.setFileFilter(getFileFilter(fileType));
        int returnVal = cFileChooser.showSaveDialog(frame);

        return returnVal;

    }

    /**
     * Activa el fileChoocer para salvar archivos. Version mejorada para ser
     * mas general
     *
     * @param fileName
     * @param cComponent La ventana en uso
     * @param fileType El tipo de archivo
     * @param fileContent El contenido del archivo a salvar
     */
    public static void saveFile(String fileName, String fileContent, int fileType, JFrame cComponent)
    {
        int returnVal;
        SacFileChooser cFileChooser;
        
        cFileChooser = new SacFileChooser();
        cFileChooser.setCurrentDirectory(new File(SAC_PATHS[fileType]));
        cFileChooser.setFileFilter(getFileFilter(fileType));
        cFileChooser.setSelectedFile(SacFileManager.getDefaultName(fileType, fileName));

        returnVal = cFileChooser.showSaveDialog(cComponent);

        if (returnVal == JFileChooser.APPROVE_OPTION)
        {
            File fileToSave = cFileChooser.getSelectedFile();

            try
            {
                try (BufferedWriter bw = new BufferedWriter(new FileWriter(fileToSave)))
                {
                    bw.write(fileContent);
                }

            } catch (IOException ex)
            {
                StringBuilder sMensaje = new StringBuilder();
                sMensaje.append("Problema salvando el siguiente archivo:\n\n");
                sMensaje.append(fileToSave.getAbsolutePath()).append("\n\n");
                sMensaje.append("El sistema reportó la siguiente excepcion:\n\n");
                sMensaje.append(ex.getMessage()).append("\n\n");
                sMensaje.append("Contenido del archivo:\n").append(fileContent);
                MessageBox.show("Aviso Importante", sMensaje.toString());
            }
        }
    }

    /**
     * Crea el directorio especificado, si falla devuelve falso
     * @param dirPath
     * @return 
     */
    public static boolean createFolder(String dirPath)
    {
        try
        {
            File aFile = new File(dirPath);

            if (aFile.exists() && aFile.isDirectory())
            {
                return true;
            } else
            {
                MessageBox.show(
                    "¡Atención!",
                    "No se encontro el directorio: "
                    + dirPath + "\n\n" + "Directorio Creado.");

                return aFile.mkdirs();
            }

        } catch (Exception e)
        {
            MessageBox.show(
                "Error Grave - createDirectory",
                "Creación de directorios fallida.\n"
                + e.getMessage());
        }
        
        return false;
    }
  
    /**
     * Crea el directorio especificado, si falla devuelve falso. 
     * No advierte de la creación.
     *
     * @param dirPath
     * @return
     */
    public static boolean createFolderSilently(String dirPath)
    {
        try
        {
            File aFile = new File(dirPath);

            if (aFile.exists() && aFile.isDirectory())
            {
                return true;
            }
            else
            {
                return aFile.mkdirs();
            }

        } catch (Exception e)
        {
            MessageBox.show(
                "Error Grave - createDirectory",
                "Creación de directorio fallida.\n" + e.getMessage());
        }

        return false;
    }

    /**
     * Crea los directorios que no existan para el almacenamiento de los datos
     * al arrancar la aplicación
     */
    public static void setSacFolders()
    {
        try
        {
            StringBuilder failedDirectories = new StringBuilder();
            int nFailedDirs = 0;

            for (String subFolder : SacFileManager.SAC_PATHS)
            {
                if (!SacFileManager.createFolder(subFolder))
                {
                    failedDirectories.append(subFolder).append("\n");
                    nFailedDirs++;
                }
            }

            if (nFailedDirs > 0)
            {
                MessageBox.show("Atención",
                    "Los siguientes directorios no pudieron ser creados: \n"
                    + failedDirectories.toString());
            }
        } catch (Exception e)
        {
            MessageBox.show(
                "Error Grave - setSacDirectories",
                "Creación de directorios fallida!!!\n"
                + e.getMessage());
        }
    }

    /*
 * Eliminates any special characters, if any and reduces white spaces inside the
 * Strings to only 1.
 * If other cleaning is needed i will do it here to keep the readFile method unchanged.
     */
    public static String processLine(String aLine)
    {
        String pRegex = "[\\t\\n\\f\\r]";
        String pOnly1Space = "[ ]{2,}";

        aLine = aLine.trim(); // eliminates white spaces from begining and end
        aLine = aLine.replaceAll(pRegex, ""); // eliminates special white spaces inside the string.
        aLine = aLine.replaceAll(pOnly1Space, " "); // reduces white spaces to 1.

        return aLine;
    }


    /*
     * Reads the fileToRead and returns the whole content dIS a string
     */
    public static String readFile(String name) throws IOException
    {
        BufferedReader br;
        String line;

        br = new BufferedReader(new FileReader(name));

        StringBuilder fullText = new StringBuilder();

        while ((line = br.readLine()) != null)
        {
            if (line.length() > 0)
            {
                fullText.append(processLine(line)).append("\n");
            }
        }
        br.close();
        return fullText.toString();

    }

    /**
     *
     * Lee el fileToSave linea por linea y retorna su contenido en un arrayList.
     * @param nFileName Nombre del archivo
     * @return 
     */
    public static ArrayList<String> readFileInLine(String nFileName)
    {
        ArrayList<String> fList = new ArrayList<>();
        String strLine;

        try
        {
            // Open the fileToSave that is the first
            // command line parameter
            FileInputStream fstream = new FileInputStream(nFileName);
            // Get the object of DataInputStream
            DataInputStream in;
            in = new DataInputStream(fstream);
            BufferedReader br = new BufferedReader(new InputStreamReader(in));

            //Read File Line By Line
            while ((strLine = br.readLine()) != null)
            {
                fList.add(strLine);
            }
            //Close the input stream
            in.close();
        } catch (IOException e)
        {//Catch exception if any
            MessageBox.show("ERROR GRAVE - readFileLine", e.getMessage());
        }
        return fList;
    }


    /*
     * Returns an Array list with all the files that contains the apropiate directory
     */
    public static ArrayList<File> getListOfFiles(int fileType)
    {
        ArrayList<File> listOfFiles = new ArrayList<>();

        StringBuilder aFilePath = new StringBuilder();

        aFilePath.append(APP_PATH).append(SAC_PATHS[fileType]);

        File fDir = new File(aFilePath.toString());

        File[] arrayOfFiles = fDir.listFiles();

        for (File arrayOfFile : arrayOfFiles)
        {
            if (arrayOfFile.getName().endsWith(FILE_EXT[fileType]))
            {
                listOfFiles.add(arrayOfFile);
            }
        }

        return listOfFiles;
    }


    /*
     * Lee las listas desde los archivos correspondientes y las devuelve como un String array
     */
    public static String[] loadListFromFile(String fileName)
    {
        String[] aList; //will hold the final list of functions that will be returned
        ArrayList<String> listFromFile;
        int aSize;

        try
        {
            listFromFile = new ArrayList<>(readFileInLine(fileName));
            aSize = listFromFile.size();
            aList = new String[aSize];
            for (int i = 0; i < aSize; i++) // Transforms an Arraylist into a String[];
            {
                aList[i] = listFromFile.get(i).replace(";", "");
            }

            return aList;

        } catch (Exception e)
        {
            MessageBox.show("ERROR GRAVE", e.getMessage());
        }
        return null;
    }

  


   /**
    * Lee archivos de tipo SACS y los transforma en objetos StatesSet.
    * @param fileName
    * @return 
    */
    public static String readSacFile(String fileName)
    {
        try
        {
            StringBuilder fileContent = new StringBuilder();
            fileContent.append(SacFileManager.readFile(fileName));
            return fileContent.toString();

        } catch (IOException ex)
        {
            StringBuilder sMensaje = new StringBuilder();

            sMensaje.append("El archivo SAC presenta problemas.\n\n");
            sMensaje.append("El sistema reportó la siguiente excepcion:\n\n");
            sMensaje.append(ex.getMessage());

            MessageBox.show("Aviso Importante", sMensaje.toString());
        }

        return null;
    }

    /**
     * Lee archivos de tipo ADAC y los transforma en objetos Automaton
     *
     * @param fileName Nombre del archivo
     * @param fileType Tipo del archivo
     * @return La cadena con el contenido del archivo.
     */
    public static String readAdacFile(String fileName, int fileType)
    {
        try
        {
            //   StringBuilder filePath = new StringBuilder();
            StringBuilder fileContent = new StringBuilder();

            //   filePath.append(SacFileManager.getSacoDirectory(fileType).toString());
            //   filePath.append(FILE_SEPARATOR).append(fileName);
            fileContent.append(SacFileManager.readFile(fileName));

            return fileContent.toString();

        } catch (IOException ex)
        {
            StringBuilder sMensaje = new StringBuilder();
            sMensaje.append("El archivo SAC presenta problemas.\n\n");
            sMensaje.append("El sistema reportó la siguiente excepcion:\n\n");
            sMensaje.append(ex.getMessage());
            MessageBox.show("Aviso Importante", sMensaje.toString());
        }

        return null;
    }

    /*
     * Lee archivos de configuracion
     */
    public static String readConfigFile(String fileName)
    {
        try
        {
            StringBuilder filePath = new StringBuilder();
            StringBuilder fileContent = new StringBuilder();

            filePath.append(fileName);
            fileContent.append(SacFileManager.readFile(filePath.toString()));

            return fileContent.toString();

        } catch (IOException ex)
        {
            StringBuilder sMensaje = new StringBuilder();
            sMensaje.append("El archivo de configuración presenta problemas.\n\n");
            sMensaje.append(fileName).append("\n\n");
            sMensaje.append("El sistema reportó la siguiente excepcion:\n\n");
            sMensaje.append(ex.getMessage());
            MessageBox.show("Aviso Importante", sMensaje.toString());
        }

        return null;
    }

    

}
