/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package luz.ve.ssacc.utilities;

import java.awt.Dimension;
import java.awt.Toolkit;
import javax.swing.JFrame;
import javax.swing.JTextArea;

/**
 *
 * @author "Renzo Ciafardone Sciannelli"
 */
public class MessageBox
{
    //**************************************************************************
    //*****************************VARIABLES************************************
    //**************************************************************************

    

    //**************************************************************************
    //*****************************CONSTRUCTORS*********************************
    //**************************************************************************
    private MessageBox()
    {
    }

    //**************************************************************************
    //*****************************METHODS**************************************
    //**************************************************************************

     public static void show(String aTitle, String aMessage)
    {
        Toolkit toolkit = Toolkit.getDefaultToolkit();
        Dimension screenSize = toolkit.getScreenSize();

        int x = screenSize.width / 2;
        int y = screenSize.height / 2;

        JTextArea txMessage = new JTextArea();
        txMessage.setText(aMessage);
        txMessage.setEditable(false);
        txMessage.setLineWrap(true);
        txMessage.setWrapStyleWord(true);
        txMessage.setSize(400,150);

        JFrame messageFrame = new JFrame();
        messageFrame.add(txMessage);
        messageFrame.setTitle(aTitle);
        messageFrame.setLocation(x,y);
        messageFrame.setSize(400, 200);
        messageFrame.setVisible(true);
     }

}
