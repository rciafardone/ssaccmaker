/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package luz.ve.ssacc.utilities;

import luz.ve.ssacc.engine.components.Automaton;
import javax.swing.*;
/**
 *
 * @author "Renzo Ciafardone Sciannelli"
 */
public class DrawText
{

    //**************************************************************************
    //*****************************VARIABLES************************************
    //**************************************************************************

    private Automaton adac_;
    private JTextArea canvas_;
   

    //**************************************************************************
    //*****************************CONSTRUCTORS*********************************
    //**************************************************************************
DrawText()
{

}

DrawText(Automaton adac, JTextArea canvas)
{
    this.setAdac(adac);
    this.setCanvas(canvas);
}

    //**************************************************************************
    //*****************************METHODS**************************************
    //**************************************************************************
    public final void setAdac(Automaton adac)
    {
        this.adac_ = adac;
    }

    public final void setCanvas(JTextArea canvas)
    {
        this.canvas_ = canvas;
    }

    private JTextArea getCanvas()
    {
        return this.canvas_;
    }
    
    private Automaton getAdac()
    {
        return this.adac_;
    }
    
    //**************************************************************************
    //****************************END OF CLASS**********************************
    //**************************************************************************

}
