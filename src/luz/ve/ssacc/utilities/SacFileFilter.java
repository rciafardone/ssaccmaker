/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package luz.ve.ssacc.utilities;
import java.io.File;
import javax.swing.JFileChooser;
import javax.swing.filechooser.FileFilter;

/**
 *
 * @author "Renzo Ciafardone Sciannelli"
 */

public class SacFileFilter extends FileFilter{
    //**************************************************************************
    //*****************************VARIABLES************************************
    //**************************************************************************
    String description;
    String extensions[];


    //**************************************************************************
    //*****************************CONSTRUCTORS*********************************
    //**************************************************************************
    public SacFileFilter(String description, String extension)
    {
        this(description, new String[]
                {
                    extension
                });
    }

    public SacFileFilter(String description, String extensions[])
    {
        if (description == null)
        {
            this.description = extensions[0];
        } else
        {
            this.description = description;
        }
        this.extensions = extensions.clone();
        toLower(this.extensions);
    }

    //**************************************************************************
    //*****************************METHODS**************************************
    //**************************************************************************


  private void toLower(String array[]) {
    for (int i = 0, n = array.length; i < n; i++) {
      array[i] = array[i].toLowerCase();
    }
  }

  public String getDescription() {
    return description;
  }

    public boolean accept(File file)
    {
        if (file.isDirectory())
        {
            return true;
        } else
        {
            String path = file.getAbsolutePath().toLowerCase();
            for (String extension : extensions)
            {
                if ((path.endsWith("." + extension)))
                {
                    return true;
                }
            }
        }
        return false;
    }


}
