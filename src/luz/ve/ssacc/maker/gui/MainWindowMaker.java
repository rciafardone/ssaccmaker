/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package luz.ve.ssacc.maker.gui;

import luz.ve.ssacc.maker.gui.extras.SacGuiManager;
import java.awt.EventQueue;
import java.io.File;
import javax.swing.JFileChooser;
import javax.swing.JFrame;

import luz.ve.ssacc.engine.components.Automaton;
import luz.ve.ssacc.engine.components.States;
import luz.ve.ssacc.engine.parsers.AutomatonParser;
import luz.ve.ssacc.utilities.SacFileManager;
import luz.ve.ssacc.utilities.SacUtility;
import luz.ve.ssacc.utilities.SacFileChooser;
import static luz.ve.ssacc.utilities.SacFileManager.SAC_PATHS;

/**
 *
 * @author Renzo Ciafardone Sciannelli <rciafardone@gmail.com>
 */
public class MainWindowMaker extends javax.swing.JFrame
{

    /**
     * Aqui se almacena lo referente al Autómata Celular en uso.
     */
    public static Automaton currentCA = new Automaton();
    
    public SacFileManager cFileM = new SacFileManager(); //NO BORRAR: Establece direcciónes de directorios
    
    public WindowStates editorStates = new WindowStates(this);
    public WindowNeighborhood editorNeighborhood = new WindowNeighborhood(this);
    public WindowGrid editorGrid = new WindowGrid(this);
    public WindowStatistics showStats = new WindowStatistics(this);
    public WindowGridDraw editorGridDraw;
    public WindowPattern editorPattern = new WindowPattern(this);
    public WindowRules editorRules = new WindowRules(this);
   
    public static Double WINDOW_POSITION_X;
    public static Double WINDOW_POSITION_Y;
    
    /**
     * Creates new form SSACCMakerUI
     */
    public MainWindowMaker()
    {
        initComponents();
        this.initialGuiConfig();

    }

    private void initialGuiConfig()
    {
        SacGuiManager.centralizeWindows( 
            this,
            editorStates,
            editorNeighborhood,
            editorRules,
            editorGrid,
            editorPattern,
            showStats);
    }

    /**
     * Configura el estado inicial del programa
     */
    private static void startUpMaker()
    {
        MainWindowMaker makerFrame = new MainWindowMaker();
        makerFrame.setVisible(true);
        makerFrame.clearGUI();
        SacFileManager.setSacFolders();
    }

    /**
     * Abre el archivo ADAC con la opción correspondiente
     *
     * @param editOption La acción que se tomará con el autómata
     * @param fileType El tipo de archivo
     */
    private void openAdacFile(int editOption, int fileType)
    {
        SacFileChooser cFileChooser = new SacFileChooser();
        cFileChooser.setCurrentDirectory(new File(SAC_PATHS[fileType]));
        cFileChooser.setFileFilter(SacFileManager.getFileFilter(fileType));
        int returnVal = cFileChooser.showOpenDialog(this);

        if (returnVal == JFileChooser.APPROVE_OPTION)
        {
            this.load(editOption, cFileChooser.getSelectedFile().getAbsolutePath());
        }
        this.pack();
    }

    /**
     * Llena los campos del dialogo del Automata según la opción.
     *
     * @param editOption
     * @param selectedFile
     * @return
     */
    private void load(int editOption, String selectedFile)
    {
        String fileContent;
        switch (editOption)
        {
            case SacUtility.ADD_NEW:
            {
                currentCA = new Automaton();
                break;
            }
            case SacUtility.LOAD_FILE:
            {
                if (selectedFile.isEmpty())
                {
                    currentCA = new Automaton();
                }
                else
                {
                    fileContent = SacFileManager.readAdacFile(selectedFile, SacFileManager.SAC_ADAC);
                    currentCA = AutomatonParser.parse(fileContent);
                }
                break;
            }
        }

        updateGUI();
    }

    /**
     * Prepara la ventana y el objeto para la creación de un nuevo automata
     */
    private void resetAutomaton()
    {
        clearGUI();
        MainWindowMaker.currentCA = new Automaton();
        updateGUI();
    }

    /**
     * Abre la ventana de dialogo para crear/editar estados.
     *
     * @param editOption
     */
    public void openWindowStates(int editOption)
    {
        if (editorStates.load(editOption, this.lStatesName.getText()) != null)
        {
            MainWindowMaker.currentCA.setName(this.tfAutomatonName.getText());
            this.lStatesName.setText(MainWindowMaker.currentCA.getStates().getName());
            SacGuiManager.switchWindowStatus(this, false, true);
            SacGuiManager.switchWindowStatus(editorStates, true, true);
            this.pack();
        }
    }

    /**
     * Abre la ventana de dialogo para crear/editar vecindarios.
     *
     * @param editOption
     */
    private void openWindowNeighborhood(int editOption)
    {
        if (editorNeighborhood.load(editOption, this.lNeighborhoodName.getText()) != null)
        {
            MainWindowMaker.currentCA.setName(this.tfAutomatonName.getText());
            this.lNeighborhoodName.setText(MainWindowMaker.currentCA.getNeighborhood().getName());
            SacGuiManager.switchWindowStatus(this, false, true);
            SacGuiManager.switchWindowStatus(editorNeighborhood, true, true);
            this.pack();
        }
    }

    
    
    
    /**
     * Abre la ventana de dialogo para crear/editar Reglas.
     *
     * @param editOption
     */
    private void openWindowRules(int editOption)
    {
        if (editorRules.load(editOption, this.lRulesName.getText()) != null)
        {
            MainWindowMaker.currentCA.setName(this.tfAutomatonName.getText());
            editorRules.updateGui();
            this.lRulesName.setText(MainWindowMaker.currentCA.getRules().getName());
            SacUtility.checkStatesRulesMatch(MainWindowMaker.currentCA);

            SacGuiManager.switchWindowStatus(this, false, true);
            SacGuiManager.switchWindowStatus(editorRules, true, true);
            this.pack();
        }
    }

    /**
     * Abre la ventana de dialogo para crear/editar reticulas.G
     *
     * @param editOption
     */
    public void openWindowGrid(int editOption)
    {
        if (editorGrid.load(editOption, this.lGridName.getText()) != null)
        {
            MainWindowMaker.currentCA.setName(this.tfAutomatonName.getText());
            this.lGridName.setText(MainWindowMaker.currentCA.getGrids().getName());
            SacGuiManager.switchWindowStatus(this, false, true);
            SacGuiManager.switchWindowStatus(editorGrid, true, true);
            this.pack();
        }
    }

    /**
     * Abre la ventana de los patrones
     *
     * @param editOption
     */
    private void openWindowPattern()
    {
        MainWindowMaker.currentCA.setName(this.tfAutomatonName.getText());
        SacGuiManager.switchWindowStatus(this, false, true);
        SacGuiManager.switchWindowStatus(editorPattern, true, true);
        this.editorPattern.load(SacUtility.ADD_NEW, "");
    }

    
    
    /**
     * Abre la ventana de las estadisticas.
     */
    private void openWindowStatistics()
    {
        SacGuiManager.switchWindowStatus(this, false, true);
        SacGuiManager.switchWindowStatus(showStats, true, true);
        int size =MainWindowMaker.currentCA.getStates().getList().size();
        String name =MainWindowMaker.currentCA.getStates().getName();
        
        if(size==1 && name.equals(States.DEFAULT_NAME))
        {
            StringBuilder message = new StringBuilder();
            
            message.append("Nombre de estados: ").append(name).append(SacUtility.EOL);
            message.append("Número de estados: ").append(size).append(SacUtility.EOL);
            message.append("Estas caracteristicas indican que no se tiene seleccionado un conjunto de estados real.").append(SacUtility.EOL);
            message.append("Si se desea mostrar las estadisticas con los datos reales correspondientes a los estados por favor ").append(SacUtility.EOL);
            message.append("cargue el autómata o el conjunto de estados correspondiente a la simulación que generó el archivo.").append(SacUtility.EOL);
            message.append(SacUtility.EOL).append("Puede continuar pero se mostrarán los datos con etiquetas y colores aleatorios.").append(SacUtility.EOL);
            
            SacUtility.showWarning(message.toString());
        
        }
        
        this.showStats.loadName();
    }

    /**
     * Actualiza la GUI principal basado en el contenido actual del automata,
     * además recarga el AC a nivel local en cada uno de los modulos
     */
    private void updateGUI()
    {
        this.labelAutomatonId.setText(MainWindowMaker.currentCA.getId());
        this.tfAutomatonName.setText(MainWindowMaker.currentCA.getName());
        this.taAutomatonDescription.setText(MainWindowMaker.currentCA.getDescription());
        this.taAutomatonContent.setText(MainWindowMaker.currentCA.toStringWithoutCells());
        this.lStatesName.setText(MainWindowMaker.currentCA.getStates().getName());
        this.lNeighborhoodName.setText(MainWindowMaker.currentCA.getNeighborhood().getName());
        this.lRulesName.setText(MainWindowMaker.currentCA.getRules().getName());
        this.lGridName.setText(MainWindowMaker.currentCA.getGrids().getName());
    }

    /**
     * Asegura que todos los campos de la ventana principal esten en blanco y
     * asigna un nuevo id unico al inicio del nuevo automata.
     */
    private void clearGUI()
    {
        this.labelAutomatonId.setText(Automaton.generateNewId());
        this.tfAutomatonName.setText("");
        this.lGridName.setText("");
        this.lNeighborhoodName.setText("");
        this.lRulesName.setText("");
        this.lStatesName.setText("");
        this.taAutomatonContent.setText("");
        this.taAutomatonDescription.setText("");
    }

    /**
     * Actualiza los datos del Automata
     */
    public void updateAdac()
    {
        MainWindowMaker.currentCA.setName(this.tfAutomatonName.getText());
        MainWindowMaker.currentCA.setId(this.labelAutomatonId.getText());
        MainWindowMaker.currentCA.setDescription(this.taAutomatonDescription.getText());
    }

    public void saveAdac()
    {
        updateAdac();
        SacFileManager.saveFile(
            MainWindowMaker.currentCA.getName(),
            MainWindowMaker.currentCA.toString(),
            SacFileManager.SAC_ADAC,
            this);
    }

    private void OpenWindowAnimation()
    {
        if (Automaton.ready(MainWindowMaker.currentCA))
        {
            JFrame simulationFrame = new WindowAnimation(this);
            simulationFrame.setVisible(true);
            //  simulationFrame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        }
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents()
    {

        jPanelBackground = new javax.swing.JPanel();
        panelAutomatonName = new javax.swing.JPanel();
        tfAutomatonName = new javax.swing.JTextField();
        automatonNameLabel = new javax.swing.JLabel();
        automatonIdLabel = new javax.swing.JLabel();
        automatonAdd = new javax.swing.JButton();
        automatonOpen = new javax.swing.JButton();
        labelAutomatonId = new javax.swing.JLabel();
        automatonSave = new javax.swing.JButton();
        jPanelDescription = new javax.swing.JPanel();
        jScrollPane1 = new javax.swing.JScrollPane();
        taAutomatonDescription = new javax.swing.JTextArea();
        jScrollPane2 = new javax.swing.JScrollPane();
        taAutomatonContent = new javax.swing.JTextArea();
        pTools = new javax.swing.JPanel();
        pSimulation = new javax.swing.JPanel();
        simulationStart = new javax.swing.JButton();
        pPatterns = new javax.swing.JPanel();
        bPatternOpen = new javax.swing.JButton();
        pStatistics = new javax.swing.JPanel();
        bStatsOpen = new javax.swing.JButton();
        panelComponents = new javax.swing.JPanel();
        panelStates = new javax.swing.JPanel();
        statesLoad = new javax.swing.JButton();
        statesAdd = new javax.swing.JButton();
        statesEdit = new javax.swing.JButton();
        lStatesName = new javax.swing.JLabel();
        panelNeighbor = new javax.swing.JPanel();
        neighborhoodLoad = new javax.swing.JButton();
        neighborhoodAdd = new javax.swing.JButton();
        neighborhoodEdit = new javax.swing.JButton();
        lNeighborhoodName = new javax.swing.JLabel();
        panelRules = new javax.swing.JPanel();
        rulesLoad = new javax.swing.JButton();
        rulesAdd = new javax.swing.JButton();
        rulesEdit = new javax.swing.JButton();
        lRulesName = new javax.swing.JLabel();
        panelGrid = new javax.swing.JPanel();
        gridOpen = new javax.swing.JButton();
        gridAdd = new javax.swing.JButton();
        gridEdit = new javax.swing.JButton();
        lGridName = new javax.swing.JLabel();
        jMenuSSACC = new javax.swing.JMenuBar();
        jMenuADAC = new javax.swing.JMenu();
        jMenuItemAbrirAdac = new javax.swing.JMenuItem();
        jMenuItemGuardarAdac = new javax.swing.JMenuItem();
        jMenuItemBorrarAdac = new javax.swing.JMenuItem();
        jMenuStates = new javax.swing.JMenu();
        jMenuItemAbrirStates = new javax.swing.JMenuItem();
        jMenuItemEditarStates = new javax.swing.JMenuItem();
        jMenuItemNuevoStates = new javax.swing.JMenuItem();
        jMenuRules = new javax.swing.JMenu();
        jMenuItemAbrirRules = new javax.swing.JMenuItem();
        jMenuItemEditarRules = new javax.swing.JMenuItem();
        jMenuItemNuevoRules = new javax.swing.JMenuItem();
        jMenuNeighborhood = new javax.swing.JMenu();
        jMenuItemAbrirNeighborhood = new javax.swing.JMenuItem();
        jMenuItemEditarNeighborhood = new javax.swing.JMenuItem();
        jMenuItemNuevoNeighborhood = new javax.swing.JMenuItem();
        jMenuGrid = new javax.swing.JMenu();
        jMenuItemAbrirGrid = new javax.swing.JMenuItem();
        jMenuItemEditarGrid = new javax.swing.JMenuItem();
        jMenuItemNuevoGrid = new javax.swing.JMenuItem();
        jMenuPattern = new javax.swing.JMenu();
        jMenuItemAbrirPattern = new javax.swing.JMenuItem();
        jMenuHelp = new javax.swing.JMenu();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setTitle("SSACC");
        setResizable(false);
        addWindowListener(new java.awt.event.WindowAdapter()
        {
            public void windowActivated(java.awt.event.WindowEvent evt)
            {
                formWindowActivated(evt);
            }
        });

        jPanelBackground.setBorder(javax.swing.BorderFactory.createEtchedBorder());

        panelAutomatonName.setBorder(javax.swing.BorderFactory.createEmptyBorder(1, 1, 1, 1));

        automatonNameLabel.setText("Nombre:");

        automatonIdLabel.setText("ID:");

        automatonAdd.setIcon(new javax.swing.ImageIcon(getClass().getResource("/luz/ve/ssacc/maker/gui/images/SSACC - redo 20x20.png"))); // NOI18N
        automatonAdd.addActionListener(new java.awt.event.ActionListener()
        {
            public void actionPerformed(java.awt.event.ActionEvent evt)
            {
                automatonAddActionPerformed(evt);
            }
        });

        automatonOpen.setIcon(new javax.swing.ImageIcon(getClass().getResource("/luz/ve/ssacc/maker/gui/images/SSACC Open 20x20.png"))); // NOI18N
        automatonOpen.addActionListener(new java.awt.event.ActionListener()
        {
            public void actionPerformed(java.awt.event.ActionEvent evt)
            {
                automatonOpenActionPerformed(evt);
            }
        });

        labelAutomatonId.setFont(new java.awt.Font("Ubuntu", 1, 15)); // NOI18N
        labelAutomatonId.setText("00000");

        automatonSave.setIcon(new javax.swing.ImageIcon(getClass().getResource("/luz/ve/ssacc/maker/gui/images/SSACC Ok 20x20.png"))); // NOI18N
        automatonSave.addActionListener(new java.awt.event.ActionListener()
        {
            public void actionPerformed(java.awt.event.ActionEvent evt)
            {
                automatonSaveActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout panelAutomatonNameLayout = new javax.swing.GroupLayout(panelAutomatonName);
        panelAutomatonName.setLayout(panelAutomatonNameLayout);
        panelAutomatonNameLayout.setHorizontalGroup(
            panelAutomatonNameLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(panelAutomatonNameLayout.createSequentialGroup()
                .addGap(6, 6, 6)
                .addComponent(automatonNameLabel)
                .addGap(18, 18, 18)
                .addComponent(tfAutomatonName, javax.swing.GroupLayout.PREFERRED_SIZE, 289, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(automatonIdLabel)
                .addGap(18, 18, 18)
                .addComponent(labelAutomatonId, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGap(94, 94, 94)
                .addComponent(automatonSave)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(automatonAdd, javax.swing.GroupLayout.PREFERRED_SIZE, 55, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(automatonOpen))
        );

        panelAutomatonNameLayout.linkSize(javax.swing.SwingConstants.HORIZONTAL, new java.awt.Component[] {automatonAdd, automatonOpen, automatonSave});

        panelAutomatonNameLayout.setVerticalGroup(
            panelAutomatonNameLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(panelAutomatonNameLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(panelAutomatonNameLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(automatonSave, javax.swing.GroupLayout.PREFERRED_SIZE, 28, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(automatonAdd, javax.swing.GroupLayout.PREFERRED_SIZE, 28, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(automatonOpen, javax.swing.GroupLayout.PREFERRED_SIZE, 28, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGroup(panelAutomatonNameLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(automatonNameLabel)
                        .addComponent(tfAutomatonName, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(labelAutomatonId)
                        .addComponent(automatonIdLabel)))
                .addContainerGap())
        );

        panelAutomatonNameLayout.linkSize(javax.swing.SwingConstants.VERTICAL, new java.awt.Component[] {automatonAdd, automatonOpen, automatonSave});

        jPanelDescription.setBorder(javax.swing.BorderFactory.createTitledBorder("Descripción"));

        taAutomatonDescription.setColumns(20);
        taAutomatonDescription.setRows(5);
        jScrollPane1.setViewportView(taAutomatonDescription);

        taAutomatonContent.setEditable(false);
        taAutomatonContent.setColumns(20);
        taAutomatonContent.setRows(5);
        jScrollPane2.setViewportView(taAutomatonContent);

        javax.swing.GroupLayout jPanelDescriptionLayout = new javax.swing.GroupLayout(jPanelDescription);
        jPanelDescription.setLayout(jPanelDescriptionLayout);
        jPanelDescriptionLayout.setHorizontalGroup(
            jPanelDescriptionLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanelDescriptionLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanelDescriptionLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 558, Short.MAX_VALUE)
                    .addComponent(jScrollPane2))
                .addContainerGap())
        );
        jPanelDescriptionLayout.setVerticalGroup(
            jPanelDescriptionLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanelDescriptionLayout.createSequentialGroup()
                .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 65, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jScrollPane2)
                .addContainerGap())
        );

        pTools.setBorder(javax.swing.BorderFactory.createTitledBorder("Herramientas"));

        pSimulation.setBorder(javax.swing.BorderFactory.createTitledBorder("Simulación"));

        simulationStart.setIcon(new javax.swing.ImageIcon(getClass().getResource("/luz/ve/ssacc/maker/gui/images/SSACCIcon64x64.gif"))); // NOI18N
        simulationStart.setToolTipText("Iniciar modulo de simulación");
        simulationStart.addActionListener(new java.awt.event.ActionListener()
        {
            public void actionPerformed(java.awt.event.ActionEvent evt)
            {
                simulationStartActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout pSimulationLayout = new javax.swing.GroupLayout(pSimulation);
        pSimulation.setLayout(pSimulationLayout);
        pSimulationLayout.setHorizontalGroup(
            pSimulationLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(pSimulationLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(simulationStart, javax.swing.GroupLayout.PREFERRED_SIZE, 118, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        pSimulationLayout.setVerticalGroup(
            pSimulationLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(pSimulationLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(simulationStart, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addContainerGap())
        );

        pPatterns.setBorder(javax.swing.BorderFactory.createTitledBorder("Patrones"));

        bPatternOpen.setIcon(new javax.swing.ImageIcon(getClass().getResource("/luz/ve/ssacc/maker/gui/images/SSACC - glider icon 45x45.jpg"))); // NOI18N
        bPatternOpen.setToolTipText("");
        bPatternOpen.setBorder(javax.swing.BorderFactory.createEtchedBorder());
        bPatternOpen.addActionListener(new java.awt.event.ActionListener()
        {
            public void actionPerformed(java.awt.event.ActionEvent evt)
            {
                bPatternOpenActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout pPatternsLayout = new javax.swing.GroupLayout(pPatterns);
        pPatterns.setLayout(pPatternsLayout);
        pPatternsLayout.setHorizontalGroup(
            pPatternsLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(pPatternsLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(bPatternOpen, javax.swing.GroupLayout.DEFAULT_SIZE, 118, Short.MAX_VALUE)
                .addContainerGap())
        );
        pPatternsLayout.setVerticalGroup(
            pPatternsLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(bPatternOpen, javax.swing.GroupLayout.DEFAULT_SIZE, 54, Short.MAX_VALUE)
        );

        pStatistics.setBorder(javax.swing.BorderFactory.createTitledBorder("Estadísticas"));

        bStatsOpen.setIcon(new javax.swing.ImageIcon(getClass().getResource("/luz/ve/ssacc/maker/gui/images/SSACC - statistics icon 45x45.png"))); // NOI18N
        bStatsOpen.setToolTipText("");
        bStatsOpen.setBorder(javax.swing.BorderFactory.createEtchedBorder());
        bStatsOpen.addActionListener(new java.awt.event.ActionListener()
        {
            public void actionPerformed(java.awt.event.ActionEvent evt)
            {
                bStatsOpenActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout pStatisticsLayout = new javax.swing.GroupLayout(pStatistics);
        pStatistics.setLayout(pStatisticsLayout);
        pStatisticsLayout.setHorizontalGroup(
            pStatisticsLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(pStatisticsLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(bStatsOpen, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addContainerGap())
        );
        pStatisticsLayout.setVerticalGroup(
            pStatisticsLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(bStatsOpen, javax.swing.GroupLayout.DEFAULT_SIZE, 54, Short.MAX_VALUE)
        );

        javax.swing.GroupLayout pToolsLayout = new javax.swing.GroupLayout(pTools);
        pTools.setLayout(pToolsLayout);
        pToolsLayout.setHorizontalGroup(
            pToolsLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(pToolsLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(pToolsLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(pStatistics, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addGroup(pToolsLayout.createSequentialGroup()
                        .addGroup(pToolsLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(pSimulation, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(pPatterns, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(0, 0, Short.MAX_VALUE)))
                .addContainerGap())
        );
        pToolsLayout.setVerticalGroup(
            pToolsLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(pToolsLayout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(pSimulation, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(pStatistics, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(pPatterns, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
        );

        pToolsLayout.linkSize(javax.swing.SwingConstants.VERTICAL, new java.awt.Component[] {pPatterns, pStatistics});

        panelComponents.setBorder(javax.swing.BorderFactory.createTitledBorder(javax.swing.BorderFactory.createTitledBorder(""), "Componentes"));
        panelComponents.setLayout(new java.awt.GridLayout(1, 0));

        panelStates.setBorder(javax.swing.BorderFactory.createTitledBorder(javax.swing.BorderFactory.createTitledBorder(""), "Estados"));

        statesLoad.setIcon(new javax.swing.ImageIcon(getClass().getResource("/luz/ve/ssacc/maker/gui/images/SSACC Open 20x20.png"))); // NOI18N
        statesLoad.setToolTipText("Seleccionar componente");
        statesLoad.addActionListener(new java.awt.event.ActionListener()
        {
            public void actionPerformed(java.awt.event.ActionEvent evt)
            {
                statesLoadActionPerformed(evt);
            }
        });

        statesAdd.setIcon(new javax.swing.ImageIcon(getClass().getResource("/luz/ve/ssacc/maker/gui/images/SSACC Button Create 20x20.png"))); // NOI18N
        statesAdd.setToolTipText("Crear nuevo componente");
        statesAdd.addActionListener(new java.awt.event.ActionListener()
        {
            public void actionPerformed(java.awt.event.ActionEvent evt)
            {
                statesAddActionPerformed(evt);
            }
        });

        statesEdit.setIcon(new javax.swing.ImageIcon(getClass().getResource("/luz/ve/ssacc/maker/gui/images/SSACC Edit 20x20.png"))); // NOI18N
        statesEdit.setToolTipText("Editar componente seleccionado");
        statesEdit.addActionListener(new java.awt.event.ActionListener()
        {
            public void actionPerformed(java.awt.event.ActionEvent evt)
            {
                statesEditActionPerformed(evt);
            }
        });

        lStatesName.setFont(new java.awt.Font("Ubuntu", 1, 15)); // NOI18N
        lStatesName.setText("estados");

        javax.swing.GroupLayout panelStatesLayout = new javax.swing.GroupLayout(panelStates);
        panelStates.setLayout(panelStatesLayout);
        panelStatesLayout.setHorizontalGroup(
            panelStatesLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(panelStatesLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(panelStatesLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(panelStatesLayout.createSequentialGroup()
                        .addComponent(statesLoad, javax.swing.GroupLayout.PREFERRED_SIZE, 36, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(statesAdd, javax.swing.GroupLayout.PREFERRED_SIZE, 33, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(statesEdit, javax.swing.GroupLayout.PREFERRED_SIZE, 44, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addComponent(lStatesName))
                .addContainerGap(31, Short.MAX_VALUE))
        );

        panelStatesLayout.linkSize(javax.swing.SwingConstants.HORIZONTAL, new java.awt.Component[] {statesAdd, statesEdit, statesLoad});

        panelStatesLayout.setVerticalGroup(
            panelStatesLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(panelStatesLayout.createSequentialGroup()
                .addGap(16, 16, 16)
                .addComponent(lStatesName)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(panelStatesLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(statesLoad)
                    .addComponent(statesAdd)
                    .addComponent(statesEdit))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        panelStatesLayout.linkSize(javax.swing.SwingConstants.VERTICAL, new java.awt.Component[] {statesAdd, statesEdit, statesLoad});

        panelComponents.add(panelStates);

        panelNeighbor.setBorder(javax.swing.BorderFactory.createTitledBorder(javax.swing.BorderFactory.createTitledBorder(""), "Vecindarios"));

        neighborhoodLoad.setIcon(new javax.swing.ImageIcon(getClass().getResource("/luz/ve/ssacc/maker/gui/images/SSACC Open 20x20.png"))); // NOI18N
        neighborhoodLoad.setToolTipText("Seleccionar componente");
        neighborhoodLoad.addActionListener(new java.awt.event.ActionListener()
        {
            public void actionPerformed(java.awt.event.ActionEvent evt)
            {
                neighborhoodLoadActionPerformed(evt);
            }
        });

        neighborhoodAdd.setIcon(new javax.swing.ImageIcon(getClass().getResource("/luz/ve/ssacc/maker/gui/images/SSACC Button Create 20x20.png"))); // NOI18N
        neighborhoodAdd.setToolTipText("Crear nuevo componente");
        neighborhoodAdd.addActionListener(new java.awt.event.ActionListener()
        {
            public void actionPerformed(java.awt.event.ActionEvent evt)
            {
                neighborhoodAddActionPerformed(evt);
            }
        });

        neighborhoodEdit.setIcon(new javax.swing.ImageIcon(getClass().getResource("/luz/ve/ssacc/maker/gui/images/SSACC Edit 20x20.png"))); // NOI18N
        neighborhoodEdit.setToolTipText("Editar componente seleccionado");
        neighborhoodEdit.addActionListener(new java.awt.event.ActionListener()
        {
            public void actionPerformed(java.awt.event.ActionEvent evt)
            {
                neighborhoodEditActionPerformed(evt);
            }
        });

        lNeighborhoodName.setFont(new java.awt.Font("Ubuntu", 1, 15)); // NOI18N
        lNeighborhoodName.setText("vecindario");

        javax.swing.GroupLayout panelNeighborLayout = new javax.swing.GroupLayout(panelNeighbor);
        panelNeighbor.setLayout(panelNeighborLayout);
        panelNeighborLayout.setHorizontalGroup(
            panelNeighborLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(panelNeighborLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(panelNeighborLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(panelNeighborLayout.createSequentialGroup()
                        .addComponent(neighborhoodLoad, javax.swing.GroupLayout.PREFERRED_SIZE, 36, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(neighborhoodAdd, javax.swing.GroupLayout.PREFERRED_SIZE, 33, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(neighborhoodEdit, javax.swing.GroupLayout.PREFERRED_SIZE, 44, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addComponent(lNeighborhoodName))
                .addContainerGap(31, Short.MAX_VALUE))
        );

        panelNeighborLayout.linkSize(javax.swing.SwingConstants.HORIZONTAL, new java.awt.Component[] {neighborhoodAdd, neighborhoodEdit, neighborhoodLoad});

        panelNeighborLayout.setVerticalGroup(
            panelNeighborLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(panelNeighborLayout.createSequentialGroup()
                .addGap(16, 16, 16)
                .addComponent(lNeighborhoodName)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(panelNeighborLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(neighborhoodLoad)
                    .addComponent(neighborhoodAdd)
                    .addComponent(neighborhoodEdit))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        panelNeighborLayout.linkSize(javax.swing.SwingConstants.VERTICAL, new java.awt.Component[] {neighborhoodAdd, neighborhoodEdit, neighborhoodLoad});

        panelComponents.add(panelNeighbor);

        panelRules.setBorder(javax.swing.BorderFactory.createTitledBorder(javax.swing.BorderFactory.createTitledBorder(""), "Reglas"));

        rulesLoad.setIcon(new javax.swing.ImageIcon(getClass().getResource("/luz/ve/ssacc/maker/gui/images/SSACC Open 20x20.png"))); // NOI18N
        rulesLoad.setToolTipText("Seleccionar componente");
        rulesLoad.addActionListener(new java.awt.event.ActionListener()
        {
            public void actionPerformed(java.awt.event.ActionEvent evt)
            {
                rulesLoadActionPerformed(evt);
            }
        });

        rulesAdd.setIcon(new javax.swing.ImageIcon(getClass().getResource("/luz/ve/ssacc/maker/gui/images/SSACC Button Create 20x20.png"))); // NOI18N
        rulesAdd.setToolTipText("Crear nuevo componente");
        rulesAdd.addActionListener(new java.awt.event.ActionListener()
        {
            public void actionPerformed(java.awt.event.ActionEvent evt)
            {
                rulesAddActionPerformed(evt);
            }
        });

        rulesEdit.setIcon(new javax.swing.ImageIcon(getClass().getResource("/luz/ve/ssacc/maker/gui/images/SSACC Edit 20x20.png"))); // NOI18N
        rulesEdit.setToolTipText("Editar componente seleccionado");
        rulesEdit.addActionListener(new java.awt.event.ActionListener()
        {
            public void actionPerformed(java.awt.event.ActionEvent evt)
            {
                rulesEditActionPerformed(evt);
            }
        });

        lRulesName.setFont(new java.awt.Font("Ubuntu", 1, 15)); // NOI18N
        lRulesName.setText("reglas");

        javax.swing.GroupLayout panelRulesLayout = new javax.swing.GroupLayout(panelRules);
        panelRules.setLayout(panelRulesLayout);
        panelRulesLayout.setHorizontalGroup(
            panelRulesLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(panelRulesLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(panelRulesLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(panelRulesLayout.createSequentialGroup()
                        .addComponent(rulesLoad, javax.swing.GroupLayout.PREFERRED_SIZE, 36, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(rulesAdd, javax.swing.GroupLayout.PREFERRED_SIZE, 33, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(rulesEdit, javax.swing.GroupLayout.PREFERRED_SIZE, 44, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addComponent(lRulesName))
                .addContainerGap(31, Short.MAX_VALUE))
        );

        panelRulesLayout.linkSize(javax.swing.SwingConstants.HORIZONTAL, new java.awt.Component[] {rulesAdd, rulesEdit, rulesLoad});

        panelRulesLayout.setVerticalGroup(
            panelRulesLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(panelRulesLayout.createSequentialGroup()
                .addGap(16, 16, 16)
                .addComponent(lRulesName)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(panelRulesLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(rulesLoad)
                    .addComponent(rulesAdd)
                    .addComponent(rulesEdit))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        panelRulesLayout.linkSize(javax.swing.SwingConstants.VERTICAL, new java.awt.Component[] {rulesAdd, rulesEdit, rulesLoad});

        panelComponents.add(panelRules);

        panelGrid.setBorder(javax.swing.BorderFactory.createTitledBorder(javax.swing.BorderFactory.createTitledBorder(""), "Retículas"));

        gridOpen.setIcon(new javax.swing.ImageIcon(getClass().getResource("/luz/ve/ssacc/maker/gui/images/SSACC Open 20x20.png"))); // NOI18N
        gridOpen.setToolTipText("Seleccionar componente");
        gridOpen.addActionListener(new java.awt.event.ActionListener()
        {
            public void actionPerformed(java.awt.event.ActionEvent evt)
            {
                gridOpenActionPerformed(evt);
            }
        });

        gridAdd.setIcon(new javax.swing.ImageIcon(getClass().getResource("/luz/ve/ssacc/maker/gui/images/SSACC Button Create 20x20.png"))); // NOI18N
        gridAdd.setToolTipText("Crear nuevo componente");
        gridAdd.addActionListener(new java.awt.event.ActionListener()
        {
            public void actionPerformed(java.awt.event.ActionEvent evt)
            {
                gridAddActionPerformed(evt);
            }
        });

        gridEdit.setIcon(new javax.swing.ImageIcon(getClass().getResource("/luz/ve/ssacc/maker/gui/images/SSACC Edit 20x20.png"))); // NOI18N
        gridEdit.setToolTipText("Editar componente seleccionado");
        gridEdit.addActionListener(new java.awt.event.ActionListener()
        {
            public void actionPerformed(java.awt.event.ActionEvent evt)
            {
                gridEditActionPerformed(evt);
            }
        });

        lGridName.setFont(new java.awt.Font("Ubuntu", 1, 15)); // NOI18N
        lGridName.setText("retícula");

        javax.swing.GroupLayout panelGridLayout = new javax.swing.GroupLayout(panelGrid);
        panelGrid.setLayout(panelGridLayout);
        panelGridLayout.setHorizontalGroup(
            panelGridLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(panelGridLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(panelGridLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(panelGridLayout.createSequentialGroup()
                        .addComponent(gridOpen, javax.swing.GroupLayout.PREFERRED_SIZE, 36, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(gridAdd, javax.swing.GroupLayout.PREFERRED_SIZE, 33, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(gridEdit, javax.swing.GroupLayout.PREFERRED_SIZE, 44, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addComponent(lGridName))
                .addContainerGap(31, Short.MAX_VALUE))
        );

        panelGridLayout.linkSize(javax.swing.SwingConstants.HORIZONTAL, new java.awt.Component[] {gridAdd, gridEdit, gridOpen});

        panelGridLayout.setVerticalGroup(
            panelGridLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(panelGridLayout.createSequentialGroup()
                .addGap(16, 16, 16)
                .addComponent(lGridName)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(panelGridLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(gridOpen)
                    .addComponent(gridAdd)
                    .addComponent(gridEdit))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        panelGridLayout.linkSize(javax.swing.SwingConstants.VERTICAL, new java.awt.Component[] {gridAdd, gridEdit, gridOpen});

        panelComponents.add(panelGrid);

        javax.swing.GroupLayout jPanelBackgroundLayout = new javax.swing.GroupLayout(jPanelBackground);
        jPanelBackground.setLayout(jPanelBackgroundLayout);
        jPanelBackgroundLayout.setHorizontalGroup(
            jPanelBackgroundLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanelBackgroundLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanelBackgroundLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(panelComponents, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addGroup(jPanelBackgroundLayout.createSequentialGroup()
                        .addComponent(jPanelDescription, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(pTools, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addComponent(panelAutomatonName, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
        );
        jPanelBackgroundLayout.setVerticalGroup(
            jPanelBackgroundLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanelBackgroundLayout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(panelAutomatonName, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanelBackgroundLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(pTools, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jPanelDescription, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(panelComponents, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
        );

        jMenuADAC.setMnemonic('A');
        jMenuADAC.setText("Autómatas");

        jMenuItemAbrirAdac.setMnemonic('A');
        jMenuItemAbrirAdac.setText("Abrir...");
        jMenuItemAbrirAdac.addActionListener(new java.awt.event.ActionListener()
        {
            public void actionPerformed(java.awt.event.ActionEvent evt)
            {
                jMenuItemAbrirAdacActionPerformed(evt);
            }
        });
        jMenuADAC.add(jMenuItemAbrirAdac);

        jMenuItemGuardarAdac.setMnemonic('G');
        jMenuItemGuardarAdac.setText("Guardar...");
        jMenuItemGuardarAdac.addActionListener(new java.awt.event.ActionListener()
        {
            public void actionPerformed(java.awt.event.ActionEvent evt)
            {
                jMenuItemGuardarAdacActionPerformed(evt);
            }
        });
        jMenuADAC.add(jMenuItemGuardarAdac);

        jMenuItemBorrarAdac.setMnemonic('B');
        jMenuItemBorrarAdac.setText("Reiniciar");
        jMenuItemBorrarAdac.addActionListener(new java.awt.event.ActionListener()
        {
            public void actionPerformed(java.awt.event.ActionEvent evt)
            {
                jMenuItemBorrarAdacActionPerformed(evt);
            }
        });
        jMenuADAC.add(jMenuItemBorrarAdac);

        jMenuSSACC.add(jMenuADAC);

        jMenuStates.setMnemonic('E');
        jMenuStates.setText("Estados");

        jMenuItemAbrirStates.setMnemonic('A');
        jMenuItemAbrirStates.setText("Abrir...");
        jMenuItemAbrirStates.addActionListener(new java.awt.event.ActionListener()
        {
            public void actionPerformed(java.awt.event.ActionEvent evt)
            {
                jMenuItemAbrirStatesActionPerformed(evt);
            }
        });
        jMenuStates.add(jMenuItemAbrirStates);

        jMenuItemEditarStates.setMnemonic('G');
        jMenuItemEditarStates.setText("Editar...");
        jMenuItemEditarStates.addActionListener(new java.awt.event.ActionListener()
        {
            public void actionPerformed(java.awt.event.ActionEvent evt)
            {
                jMenuItemEditarStatesActionPerformed(evt);
            }
        });
        jMenuStates.add(jMenuItemEditarStates);

        jMenuItemNuevoStates.setMnemonic('B');
        jMenuItemNuevoStates.setText("Nuevo...");
        jMenuItemNuevoStates.addActionListener(new java.awt.event.ActionListener()
        {
            public void actionPerformed(java.awt.event.ActionEvent evt)
            {
                jMenuItemNuevoStatesActionPerformed(evt);
            }
        });
        jMenuStates.add(jMenuItemNuevoStates);

        jMenuSSACC.add(jMenuStates);

        jMenuRules.setMnemonic('R');
        jMenuRules.setText("Reglas");

        jMenuItemAbrirRules.setMnemonic('A');
        jMenuItemAbrirRules.setText("Abrir...");
        jMenuItemAbrirRules.addActionListener(new java.awt.event.ActionListener()
        {
            public void actionPerformed(java.awt.event.ActionEvent evt)
            {
                jMenuItemAbrirRulesActionPerformed(evt);
            }
        });
        jMenuRules.add(jMenuItemAbrirRules);

        jMenuItemEditarRules.setMnemonic('G');
        jMenuItemEditarRules.setText("Editar......");
        jMenuItemEditarRules.addActionListener(new java.awt.event.ActionListener()
        {
            public void actionPerformed(java.awt.event.ActionEvent evt)
            {
                jMenuItemEditarRulesActionPerformed(evt);
            }
        });
        jMenuRules.add(jMenuItemEditarRules);

        jMenuItemNuevoRules.setMnemonic('B');
        jMenuItemNuevoRules.setText("Nuevo...");
        jMenuItemNuevoRules.addActionListener(new java.awt.event.ActionListener()
        {
            public void actionPerformed(java.awt.event.ActionEvent evt)
            {
                jMenuItemNuevoRulesActionPerformed(evt);
            }
        });
        jMenuRules.add(jMenuItemNuevoRules);

        jMenuSSACC.add(jMenuRules);

        jMenuNeighborhood.setMnemonic('V');
        jMenuNeighborhood.setText("Vecindario");

        jMenuItemAbrirNeighborhood.setMnemonic('A');
        jMenuItemAbrirNeighborhood.setText("Abrir...");
        jMenuItemAbrirNeighborhood.addActionListener(new java.awt.event.ActionListener()
        {
            public void actionPerformed(java.awt.event.ActionEvent evt)
            {
                jMenuItemAbrirNeighborhoodActionPerformed(evt);
            }
        });
        jMenuNeighborhood.add(jMenuItemAbrirNeighborhood);

        jMenuItemEditarNeighborhood.setMnemonic('G');
        jMenuItemEditarNeighborhood.setText("Editar......");
        jMenuItemEditarNeighborhood.addActionListener(new java.awt.event.ActionListener()
        {
            public void actionPerformed(java.awt.event.ActionEvent evt)
            {
                jMenuItemEditarNeighborhoodActionPerformed(evt);
            }
        });
        jMenuNeighborhood.add(jMenuItemEditarNeighborhood);

        jMenuItemNuevoNeighborhood.setMnemonic('B');
        jMenuItemNuevoNeighborhood.setText("Nuevo...");
        jMenuItemNuevoNeighborhood.addActionListener(new java.awt.event.ActionListener()
        {
            public void actionPerformed(java.awt.event.ActionEvent evt)
            {
                jMenuItemNuevoNeighborhoodActionPerformed(evt);
            }
        });
        jMenuNeighborhood.add(jMenuItemNuevoNeighborhood);

        jMenuSSACC.add(jMenuNeighborhood);

        jMenuGrid.setMnemonic('R');
        jMenuGrid.setText("Retícula");

        jMenuItemAbrirGrid.setMnemonic('A');
        jMenuItemAbrirGrid.setText("Abrir...");
        jMenuItemAbrirGrid.addActionListener(new java.awt.event.ActionListener()
        {
            public void actionPerformed(java.awt.event.ActionEvent evt)
            {
                jMenuItemAbrirGridActionPerformed(evt);
            }
        });
        jMenuGrid.add(jMenuItemAbrirGrid);

        jMenuItemEditarGrid.setMnemonic('G');
        jMenuItemEditarGrid.setText("Editar...");
        jMenuItemEditarGrid.addActionListener(new java.awt.event.ActionListener()
        {
            public void actionPerformed(java.awt.event.ActionEvent evt)
            {
                jMenuItemEditarGridActionPerformed(evt);
            }
        });
        jMenuGrid.add(jMenuItemEditarGrid);

        jMenuItemNuevoGrid.setMnemonic('B');
        jMenuItemNuevoGrid.setText("Nuevo...");
        jMenuItemNuevoGrid.addActionListener(new java.awt.event.ActionListener()
        {
            public void actionPerformed(java.awt.event.ActionEvent evt)
            {
                jMenuItemNuevoGridActionPerformed(evt);
            }
        });
        jMenuGrid.add(jMenuItemNuevoGrid);

        jMenuSSACC.add(jMenuGrid);

        jMenuPattern.setMnemonic('P');
        jMenuPattern.setText("Patrones");

        jMenuItemAbrirPattern.setMnemonic('A');
        jMenuItemAbrirPattern.setText("Abrir...");
        jMenuItemAbrirPattern.addActionListener(new java.awt.event.ActionListener()
        {
            public void actionPerformed(java.awt.event.ActionEvent evt)
            {
                jMenuItemAbrirPatternActionPerformed(evt);
            }
        });
        jMenuPattern.add(jMenuItemAbrirPattern);

        jMenuSSACC.add(jMenuPattern);

        jMenuHelp.setMnemonic('A');
        jMenuHelp.setText("Ayuda");
        jMenuSSACC.add(jMenuHelp);

        setJMenuBar(jMenuSSACC);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanelBackground, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanelBackground, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void statesLoadActionPerformed(java.awt.event.ActionEvent evt)//GEN-FIRST:event_statesLoadActionPerformed
    {//GEN-HEADEREND:event_statesLoadActionPerformed
        this.openWindowStates(SacUtility.LOAD_FILE);
    }//GEN-LAST:event_statesLoadActionPerformed

    private void statesEditActionPerformed(java.awt.event.ActionEvent evt)//GEN-FIRST:event_statesEditActionPerformed
    {//GEN-HEADEREND:event_statesEditActionPerformed
        this.openWindowStates(SacUtility.EDIT_CURRENT);
    }//GEN-LAST:event_statesEditActionPerformed

    private void neighborhoodLoadActionPerformed(java.awt.event.ActionEvent evt)//GEN-FIRST:event_neighborhoodLoadActionPerformed
    {//GEN-HEADEREND:event_neighborhoodLoadActionPerformed
        this.openWindowNeighborhood(SacUtility.LOAD_FILE);
    }//GEN-LAST:event_neighborhoodLoadActionPerformed

    private void neighborhoodEditActionPerformed(java.awt.event.ActionEvent evt)//GEN-FIRST:event_neighborhoodEditActionPerformed
    {//GEN-HEADEREND:event_neighborhoodEditActionPerformed
        this.openWindowNeighborhood(SacUtility.EDIT_CURRENT);
    }//GEN-LAST:event_neighborhoodEditActionPerformed

    private void gridOpenActionPerformed(java.awt.event.ActionEvent evt)//GEN-FIRST:event_gridOpenActionPerformed
    {//GEN-HEADEREND:event_gridOpenActionPerformed
        this.openWindowGrid(SacUtility.LOAD_FILE);
    }//GEN-LAST:event_gridOpenActionPerformed

    private void gridEditActionPerformed(java.awt.event.ActionEvent evt)//GEN-FIRST:event_gridEditActionPerformed
    {//GEN-HEADEREND:event_gridEditActionPerformed
        this.openWindowGrid(SacUtility.EDIT_CURRENT);
    }//GEN-LAST:event_gridEditActionPerformed

    private void rulesLoadActionPerformed(java.awt.event.ActionEvent evt)//GEN-FIRST:event_rulesLoadActionPerformed
    {//GEN-HEADEREND:event_rulesLoadActionPerformed
        this.openWindowRules(SacUtility.LOAD_FILE);
    }//GEN-LAST:event_rulesLoadActionPerformed

    private void rulesEditActionPerformed(java.awt.event.ActionEvent evt)//GEN-FIRST:event_rulesEditActionPerformed
    {//GEN-HEADEREND:event_rulesEditActionPerformed

        this.openWindowRules(SacUtility.EDIT_CURRENT);
    }//GEN-LAST:event_rulesEditActionPerformed

    private void jMenuItemAbrirAdacActionPerformed(java.awt.event.ActionEvent evt)//GEN-FIRST:event_jMenuItemAbrirAdacActionPerformed
    {//GEN-HEADEREND:event_jMenuItemAbrirAdacActionPerformed
        this.openAdacFile(SacUtility.LOAD_FILE, SacFileManager.SAC_ADAC);
    }//GEN-LAST:event_jMenuItemAbrirAdacActionPerformed

    private void jMenuItemAbrirStatesActionPerformed(java.awt.event.ActionEvent evt)//GEN-FIRST:event_jMenuItemAbrirStatesActionPerformed
    {//GEN-HEADEREND:event_jMenuItemAbrirStatesActionPerformed
        this.openWindowStates(SacUtility.LOAD_FILE);
    }//GEN-LAST:event_jMenuItemAbrirStatesActionPerformed

    private void jMenuItemAbrirRulesActionPerformed(java.awt.event.ActionEvent evt)//GEN-FIRST:event_jMenuItemAbrirRulesActionPerformed
    {//GEN-HEADEREND:event_jMenuItemAbrirRulesActionPerformed
        this.openWindowRules(SacUtility.LOAD_FILE);
    }//GEN-LAST:event_jMenuItemAbrirRulesActionPerformed

    private void jMenuItemAbrirNeighborhoodActionPerformed(java.awt.event.ActionEvent evt)//GEN-FIRST:event_jMenuItemAbrirNeighborhoodActionPerformed
    {//GEN-HEADEREND:event_jMenuItemAbrirNeighborhoodActionPerformed
       this.openWindowNeighborhood(SacUtility.LOAD_FILE);
    }//GEN-LAST:event_jMenuItemAbrirNeighborhoodActionPerformed

    private void jMenuItemAbrirGridActionPerformed(java.awt.event.ActionEvent evt)//GEN-FIRST:event_jMenuItemAbrirGridActionPerformed
    {//GEN-HEADEREND:event_jMenuItemAbrirGridActionPerformed
        this.openWindowGrid(SacUtility.LOAD_FILE);
    }//GEN-LAST:event_jMenuItemAbrirGridActionPerformed

    private void jMenuItemAbrirPatternActionPerformed(java.awt.event.ActionEvent evt)//GEN-FIRST:event_jMenuItemAbrirPatternActionPerformed
    {//GEN-HEADEREND:event_jMenuItemAbrirPatternActionPerformed
        this.openWindowPattern();
    }//GEN-LAST:event_jMenuItemAbrirPatternActionPerformed

    private void statesAddActionPerformed(java.awt.event.ActionEvent evt)//GEN-FIRST:event_statesAddActionPerformed
    {//GEN-HEADEREND:event_statesAddActionPerformed
        openWindowStates(SacUtility.ADD_NEW);
    }//GEN-LAST:event_statesAddActionPerformed

    private void neighborhoodAddActionPerformed(java.awt.event.ActionEvent evt)//GEN-FIRST:event_neighborhoodAddActionPerformed
    {//GEN-HEADEREND:event_neighborhoodAddActionPerformed
        openWindowNeighborhood(SacUtility.ADD_NEW);
    }//GEN-LAST:event_neighborhoodAddActionPerformed

    private void rulesAddActionPerformed(java.awt.event.ActionEvent evt)//GEN-FIRST:event_rulesAddActionPerformed
    {//GEN-HEADEREND:event_rulesAddActionPerformed
        openWindowRules(SacUtility.ADD_NEW);
    }//GEN-LAST:event_rulesAddActionPerformed

    private void gridAddActionPerformed(java.awt.event.ActionEvent evt)//GEN-FIRST:event_gridAddActionPerformed
    {//GEN-HEADEREND:event_gridAddActionPerformed
        openWindowGrid(SacUtility.ADD_NEW);
    }//GEN-LAST:event_gridAddActionPerformed

    private void automatonAddActionPerformed(java.awt.event.ActionEvent evt)//GEN-FIRST:event_automatonAddActionPerformed
    {//GEN-HEADEREND:event_automatonAddActionPerformed
        this.resetAutomaton();
    }//GEN-LAST:event_automatonAddActionPerformed

    private void automatonOpenActionPerformed(java.awt.event.ActionEvent evt)//GEN-FIRST:event_automatonOpenActionPerformed
    {//GEN-HEADEREND:event_automatonOpenActionPerformed
        this.openAdacFile(SacUtility.LOAD_FILE, SacFileManager.SAC_ADAC);
    }//GEN-LAST:event_automatonOpenActionPerformed

    private void simulationStartActionPerformed(java.awt.event.ActionEvent evt)//GEN-FIRST:event_simulationStartActionPerformed
    {//GEN-HEADEREND:event_simulationStartActionPerformed
        this.OpenWindowAnimation();
    }//GEN-LAST:event_simulationStartActionPerformed

    private void automatonSaveActionPerformed(java.awt.event.ActionEvent evt)//GEN-FIRST:event_automatonSaveActionPerformed
    {//GEN-HEADEREND:event_automatonSaveActionPerformed
        this.saveAdac();    
    }//GEN-LAST:event_automatonSaveActionPerformed

    private void bPatternOpenActionPerformed(java.awt.event.ActionEvent evt)//GEN-FIRST:event_bPatternOpenActionPerformed
    {//GEN-HEADEREND:event_bPatternOpenActionPerformed
        this.openWindowPattern();
    }//GEN-LAST:event_bPatternOpenActionPerformed

    private void formWindowActivated(java.awt.event.WindowEvent evt)//GEN-FIRST:event_formWindowActivated
    {//GEN-HEADEREND:event_formWindowActivated
        this.updateGUI();
    }//GEN-LAST:event_formWindowActivated

    private void jMenuItemGuardarAdacActionPerformed(java.awt.event.ActionEvent evt)//GEN-FIRST:event_jMenuItemGuardarAdacActionPerformed
    {//GEN-HEADEREND:event_jMenuItemGuardarAdacActionPerformed
        this.saveAdac();
    }//GEN-LAST:event_jMenuItemGuardarAdacActionPerformed

    private void jMenuItemBorrarAdacActionPerformed(java.awt.event.ActionEvent evt)//GEN-FIRST:event_jMenuItemBorrarAdacActionPerformed
    {//GEN-HEADEREND:event_jMenuItemBorrarAdacActionPerformed
        this.resetAutomaton();
    }//GEN-LAST:event_jMenuItemBorrarAdacActionPerformed

    private void jMenuItemNuevoStatesActionPerformed(java.awt.event.ActionEvent evt)//GEN-FIRST:event_jMenuItemNuevoStatesActionPerformed
    {//GEN-HEADEREND:event_jMenuItemNuevoStatesActionPerformed
       this.openWindowStates(SacUtility.ADD_NEW);
    }//GEN-LAST:event_jMenuItemNuevoStatesActionPerformed

    private void jMenuItemNuevoRulesActionPerformed(java.awt.event.ActionEvent evt)//GEN-FIRST:event_jMenuItemNuevoRulesActionPerformed
    {//GEN-HEADEREND:event_jMenuItemNuevoRulesActionPerformed
        this.openWindowRules(SacUtility.ADD_NEW);
    }//GEN-LAST:event_jMenuItemNuevoRulesActionPerformed

    private void jMenuItemEditarStatesActionPerformed(java.awt.event.ActionEvent evt)//GEN-FIRST:event_jMenuItemEditarStatesActionPerformed
    {//GEN-HEADEREND:event_jMenuItemEditarStatesActionPerformed
        this.openWindowStates(SacUtility.EDIT_CURRENT);
    }//GEN-LAST:event_jMenuItemEditarStatesActionPerformed

    private void jMenuItemEditarRulesActionPerformed(java.awt.event.ActionEvent evt)//GEN-FIRST:event_jMenuItemEditarRulesActionPerformed
    {//GEN-HEADEREND:event_jMenuItemEditarRulesActionPerformed
        this.openWindowRules(SacUtility.EDIT_CURRENT);
    }//GEN-LAST:event_jMenuItemEditarRulesActionPerformed

    private void jMenuItemNuevoGridActionPerformed(java.awt.event.ActionEvent evt)//GEN-FIRST:event_jMenuItemNuevoGridActionPerformed
    {//GEN-HEADEREND:event_jMenuItemNuevoGridActionPerformed
        this.openWindowGrid(SacUtility.ADD_NEW);
    }//GEN-LAST:event_jMenuItemNuevoGridActionPerformed

    private void jMenuItemEditarGridActionPerformed(java.awt.event.ActionEvent evt)//GEN-FIRST:event_jMenuItemEditarGridActionPerformed
    {//GEN-HEADEREND:event_jMenuItemEditarGridActionPerformed
        this.openWindowGrid(SacUtility.EDIT_CURRENT);
    }//GEN-LAST:event_jMenuItemEditarGridActionPerformed

    private void jMenuItemEditarNeighborhoodActionPerformed(java.awt.event.ActionEvent evt)//GEN-FIRST:event_jMenuItemEditarNeighborhoodActionPerformed
    {//GEN-HEADEREND:event_jMenuItemEditarNeighborhoodActionPerformed
        this.openWindowNeighborhood(SacUtility.EDIT_CURRENT);
    }//GEN-LAST:event_jMenuItemEditarNeighborhoodActionPerformed

    private void jMenuItemNuevoNeighborhoodActionPerformed(java.awt.event.ActionEvent evt)//GEN-FIRST:event_jMenuItemNuevoNeighborhoodActionPerformed
    {//GEN-HEADEREND:event_jMenuItemNuevoNeighborhoodActionPerformed
        this.openWindowNeighborhood(SacUtility.ADD_NEW);
    }//GEN-LAST:event_jMenuItemNuevoNeighborhoodActionPerformed

    private void bStatsOpenActionPerformed(java.awt.event.ActionEvent evt)//GEN-FIRST:event_bStatsOpenActionPerformed
    {//GEN-HEADEREND:event_bStatsOpenActionPerformed
        this.openWindowStatistics();
    }//GEN-LAST:event_bStatsOpenActionPerformed



    /**
     * @param args the command line arguments
     */
    public static void main(String args[])
    {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try
        {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels())
            {
                if ("Nimbus".equals(info.getName()))
                {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException | InstantiationException | IllegalAccessException | javax.swing.UnsupportedLookAndFeelException ex)
        {
            java.util.logging.Logger.getLogger(MainWindowMaker.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        /* Create and display the form */
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>

        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>

        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>

        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>

        /* Create and display the form */


        EventQueue.invokeLater(() ->
        {
            startUpMaker();
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton automatonAdd;
    private javax.swing.JLabel automatonIdLabel;
    private javax.swing.JLabel automatonNameLabel;
    private javax.swing.JButton automatonOpen;
    private javax.swing.JButton automatonSave;
    private javax.swing.JButton bPatternOpen;
    private javax.swing.JButton bStatsOpen;
    private javax.swing.JButton gridAdd;
    private javax.swing.JButton gridEdit;
    private javax.swing.JButton gridOpen;
    private javax.swing.JMenu jMenuADAC;
    private javax.swing.JMenu jMenuGrid;
    private javax.swing.JMenu jMenuHelp;
    private javax.swing.JMenuItem jMenuItemAbrirAdac;
    private javax.swing.JMenuItem jMenuItemAbrirGrid;
    private javax.swing.JMenuItem jMenuItemAbrirNeighborhood;
    private javax.swing.JMenuItem jMenuItemAbrirPattern;
    private javax.swing.JMenuItem jMenuItemAbrirRules;
    private javax.swing.JMenuItem jMenuItemAbrirStates;
    private javax.swing.JMenuItem jMenuItemBorrarAdac;
    private javax.swing.JMenuItem jMenuItemEditarGrid;
    private javax.swing.JMenuItem jMenuItemEditarNeighborhood;
    private javax.swing.JMenuItem jMenuItemEditarRules;
    private javax.swing.JMenuItem jMenuItemEditarStates;
    private javax.swing.JMenuItem jMenuItemGuardarAdac;
    private javax.swing.JMenuItem jMenuItemNuevoGrid;
    private javax.swing.JMenuItem jMenuItemNuevoNeighborhood;
    private javax.swing.JMenuItem jMenuItemNuevoRules;
    private javax.swing.JMenuItem jMenuItemNuevoStates;
    private javax.swing.JMenu jMenuNeighborhood;
    private javax.swing.JMenu jMenuPattern;
    private javax.swing.JMenu jMenuRules;
    private javax.swing.JMenuBar jMenuSSACC;
    private javax.swing.JMenu jMenuStates;
    private javax.swing.JPanel jPanelBackground;
    private javax.swing.JPanel jPanelDescription;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JLabel lGridName;
    private javax.swing.JLabel lNeighborhoodName;
    private javax.swing.JLabel lRulesName;
    private javax.swing.JLabel lStatesName;
    private javax.swing.JLabel labelAutomatonId;
    private javax.swing.JButton neighborhoodAdd;
    private javax.swing.JButton neighborhoodEdit;
    private javax.swing.JButton neighborhoodLoad;
    private javax.swing.JPanel pPatterns;
    private javax.swing.JPanel pSimulation;
    private javax.swing.JPanel pStatistics;
    private javax.swing.JPanel pTools;
    private javax.swing.JPanel panelAutomatonName;
    private javax.swing.JPanel panelComponents;
    private javax.swing.JPanel panelGrid;
    private javax.swing.JPanel panelNeighbor;
    private javax.swing.JPanel panelRules;
    private javax.swing.JPanel panelStates;
    private javax.swing.JButton rulesAdd;
    private javax.swing.JButton rulesEdit;
    private javax.swing.JButton rulesLoad;
    private javax.swing.JButton simulationStart;
    private javax.swing.JButton statesAdd;
    private javax.swing.JButton statesEdit;
    private javax.swing.JButton statesLoad;
    private javax.swing.JTextArea taAutomatonContent;
    private javax.swing.JTextArea taAutomatonDescription;
    private javax.swing.JTextField tfAutomatonName;
    // End of variables declaration//GEN-END:variables
}
