/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package luz.ve.ssacc.maker.gui.extras;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Toolkit;
import java.awt.Window;
import java.io.File;
import javax.swing.JButton;
import javax.swing.JColorChooser;
import javax.swing.JFrame;
import luz.ve.ssacc.engine.components.CustomPattern;
import luz.ve.ssacc.engine.parsers.CustomPatternParser;
import luz.ve.ssacc.utilities.SacFileManager;

/**
 *
 * @author Renzo Ciafardone Sciannelli <rciafardone@gmail.com>
 */
public class SacGuiManager
{
    
    public static final String VERSION = "2.0";

    /**
     * Establish the initial visual look and details of the gui
     * @param frameMain
     * @param frameStates
     * @param frameNeighborhood
     * @param frameRules
     * @param frameRulesCondition
     * @param frameRulesOperand
     * @param frameGrid
     * @param framePattern
     * @param frameStats
     */
    public static void configurateInitialGui(
        JFrame frameMain,
        JFrame frameStates,
        JFrame frameNeighborhood,
        JFrame frameRules,
        JFrame frameRulesCondition,
        JFrame frameRulesOperand,
        JFrame frameGrid,
        JFrame framePattern,
        JFrame frameStats)
    {

        SacGuiManager.setWindowsSizes(
            frameMain,
            frameStates,
            frameNeighborhood,
            frameRules,
            frameRulesCondition,
            frameRulesOperand,
            frameGrid,
            framePattern);

        SacGuiManager.centralizeWindows(
            frameMain,
            frameStates,
            frameNeighborhood,
            frameRules,
            frameGrid,
            framePattern,
            frameStats);
    }

    public static void setWindowsSizes(
        JFrame frameMain,
        JFrame frameStates,
        JFrame frameNeighborhood,
        JFrame frameRules,
        JFrame frameRulesCondition,
        JFrame frameRulesOperand,
        JFrame frameGrid,
        JFrame framePattern)
    {
        frameMain.setSize(new Dimension(517, 783));
        frameStates.setSize(new Dimension(464, 648));
        frameNeighborhood.setSize(new Dimension(380, 513));
        frameRules.setSize(new Dimension(372, 584));
        frameRulesCondition.setSize(new Dimension(372, 584));
        frameRulesOperand.setSize(new Dimension(372, 584));
        frameGrid.setSize(new Dimension(360, 487));
        framePattern.setSize(new Dimension(461, 450));
    }

    public static void centralizeWindows(
        JFrame frameMain,
        JFrame frameStates,
        JFrame frameNeighborhood,
        JFrame frameRules,
        JFrame frameGrid,
        JFrame framePattern,
        JFrame frameStats)
    {
        SacGuiManager.centralizeWindow(frameMain);
        SacGuiManager.centralizeWindow(frameStates);
        SacGuiManager.centralizeWindow(frameNeighborhood);
        SacGuiManager.centralizeWindow(frameRules);
        SacGuiManager.centralizeWindow(frameGrid);
        SacGuiManager.centralizeWindow(framePattern);
        SacGuiManager.centralizeWindow(frameStats);
    }

    
    public static void centralizeWindow(Window aWindow)
    {
        // Get the size of the screen
        Dimension dim = Toolkit.getDefaultToolkit().getScreenSize();

        // Determine the new location of the window
        int w = aWindow.getSize().width;
        int h = aWindow.getSize().height;
        int x = (dim.width - w) / 2;
        int y = (dim.height - h) / 2;

        // Move the window
        aWindow.setLocation(x, y);
    }

    /**
     * Cambia el estatus de visibilidad y disponibilida dde la ventana indicada 
     * @param aWindow
     * @param bEnabled Disponible
     * @param bVisible Visible
     */
     
    public static void switchWindowStatus(Window aWindow, boolean bEnabled, boolean bVisible)
    {
        aWindow.setEnabled(bEnabled);
        aWindow.setVisible(bVisible);
    }

    /**
     * Desactiva y vuelve invisible closingWindow y activa y vuelve visible
     * openingWIndow
     *
     * @param closingWindow
     * @param openingWindow
     */
    public static void closeWindow(Window closingWindow, Window openingWindow)
    {
        switchWindowStatus(closingWindow, false, false);
        switchWindowStatus(openingWindow, true, true);
        openingWindow.pack();
    }

    /**
     * Abre dialogo de color y le asigna el color escogido al botón
     * @param currentWindow
     * @param color 
     */
    public static void openColorChooser(Window currentWindow, JButton color)
    {
        //JColorChooser colorD = new JColorChooser();
        Color newColor = JColorChooser.showDialog(
            currentWindow,
            "Escoje el color del estado",
            color.getBackground());

        color.setBackground(newColor);
    }

    /**
     * Carga la lista de patrones para su uso en la edición.
     *
     * @return
     */
    public static CustomPattern[] loadPatterns()
    {
        int index;
        CustomPattern[] patterns;
        File[] listOfFiles;
        
        listOfFiles = new File(SacFileManager.SAC_PATHS[SacFileManager.SAC_PATTERN]).listFiles();
        patterns = new CustomPattern[listOfFiles.length];
        index=0;
        for (File fileName : listOfFiles)
        {
            patterns[index] = SacGuiManager.readPattern(fileName.getName());
            index++;
        }
        
        return patterns;
    }

    
    /**
     * Lee el archivo de patrón y devuelve el objeto correspondiente.
     *
     * @param fileName
     * @return
     */
    private static CustomPattern readPattern(String fileName)
    {
        StringBuilder fullFileName = new StringBuilder();
        fullFileName.append(SacFileManager.SAC_PATHS[SacFileManager.SAC_PATTERN]);
        fullFileName.append(SacFileManager.FILE_SEPARATOR);
        fullFileName.append(fileName);
        
        return CustomPatternParser.parse(SacFileManager.readSacFile(fullFileName.toString()));
    }
    
}
