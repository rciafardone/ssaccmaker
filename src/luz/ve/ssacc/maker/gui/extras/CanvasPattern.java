/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package luz.ve.ssacc.maker.gui.extras;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Rectangle;
import java.awt.RenderingHints;
import java.awt.Shape;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.Map;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Set;
import javax.swing.JComponent;
import luz.ve.ssacc.engine.components.Cell;
import luz.ve.ssacc.engine.components.State;
import luz.ve.ssacc.engine.components.CustomPattern;
import luz.ve.ssacc.maker.gui.MainWindowMaker;
import luz.ve.ssacc.utilities.SacUtility;
/**
 *
 * @author rciafardone
 */
public class CanvasPattern extends JComponent implements Canvas
{

    private CustomPattern currentPattern;
    private State selectedState;
    public boolean drawPixel = true;
    public boolean deleteOn = false;
    public CustomPattern selectedPattern;
    private final Map<Shape, Color> shapeColorMap;
    public boolean flipVertical = false;
    public boolean flipHorizontal = false;
    public int rotation = 0;

    private static final int GRID_HOR_CELL_NUM = 64; // número de celdas en la horizontal.
    private static final int GRID_VER_CELL_NUM = 32; // número de celdas en la vertical.

    private static final int GRID_ORIGIN_Y = 0; //coordenada y en pixels de la posicion del grid.
    private static final int GRID_ORIGIN_X = 20; //coordenada x en pixels de la posicion del grid.
    private static final int CELL_HEIGHT = 15; // alto de celda en pixeles.
    private static final int CELL_WIDTH = 15; // ancho de celda en pixeles.
    public static Color lineColor = Color.BLACK;

    private final int gridHeight; // alto del canvas en pixeles.
    private final int gridWidth; // ancho del cnvas en pixeles.
    private final int corGridHeight; // alto corregida con inicio.
    private final int corGridWidth; // ancho corregido con inicio.

    // private final int centerX;
    // private final int centerY;     
    
    
    public CanvasPattern()
    {
        this.selectedState = new State();
        this.currentPattern = new CustomPattern();
        this.shapeColorMap = new HashMap<>();
        
        //centerX = GRID_HOR_CELL_NUM / 2;
        //centerY = GRID_VER_CELL_NUM / 2;
        this.gridHeight = CanvasPattern.GRID_VER_CELL_NUM * CanvasPattern.CELL_HEIGHT;
        this.gridWidth = CanvasPattern.GRID_HOR_CELL_NUM * CanvasPattern.CELL_WIDTH;
        this.corGridHeight = this.gridHeight + CanvasPattern.GRID_ORIGIN_Y;
        this.corGridWidth = this.gridWidth + CanvasPattern.GRID_ORIGIN_X;

        initialConfig();
    }

    
    /**
     * Configuración inicial
     */
    private void initialConfig()
    {
        this.addMouseListener(new MouseAdapter()
        {
            @Override
            public void mouseReleased(MouseEvent e)
            {
                if (drawPixel)
                {
                    updateCell(e.getX(), e.getY(), e.getButton(), selectedState);
                }
                else
                {
                    updatePattern(e.getX(), e.getY(), e.getButton(), selectedPattern);
                }
                repaint();
            }
        });
    }
    
    
    /**
     * Asigna el valor del patrón y dibuja
     *
     * @param pattern
     */
    @Override
    public void setCurrentPattern(CustomPattern pattern)
    {
        this.currentPattern = pattern;
        this.drawPattern();
    }

    /**
     * Permite el acceso al currentPattern.
     *
     * @return
     */
    @Override
    public CustomPattern getCurrentPattern()
    {
        return currentPattern;
    }

    private Map<Shape, Color> getShapeColorMap()
    {
        return shapeColorMap;
    }

    /**
     *
     * @return
     */
    @Override
    public State getSelectedState()
    {
        return this.selectedState;
    }

    @Override
    public void setSelectedState(State selectedState)
    {
        this.selectedState = selectedState;
    }

    @Override
    public void paint(Graphics g)
    {
        Graphics2D g2 = (Graphics2D) g;
        g2.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_OFF);
        this.paintBackground(g2);
        this.getShapeColorMap().entrySet().forEach((e) ->
        {
            Shape shape = e.getKey();
            Color color = e.getValue();    
            g2.setPaint(color);
            g2.fill(shape);
        });
        CanvasUtility.paintGrid(g2, CanvasPattern.lineColor, this);
    }

    /**
     * Pinta la cuadrícula del patrón/vecindario y coloca los marcadores de
     * vacio.
     *
     * @param g2
     */
    private void paintBackground(Graphics2D g2)
    {
        //CanvasUtility.paintGrid(g2, CanvasPattern.lineColor, this);
        CanvasUtility.paintGrid(g2, CanvasPattern.lineColor, this);
        CanvasUtility.paintCrosses(g2, lineColor, this);
    }

    /**
     * Actualiza la celda del grid según el opción que se escoja.
     *
     * @param x
     * @param y
     * @param mouseButtonClicked
     */
    private void updateCell(int x, int y, int mouseButtonClicked, State state)
    {
        Cell cell;
        Shape shape;
        if (x > CanvasPattern.GRID_ORIGIN_X && x < this.corGridWidth && y > CanvasPattern.GRID_ORIGIN_Y && y < this.corGridHeight)
        {
            shape = createShapeFromClick(x, y);
            cell = getCellFromShape(shape, state);
            
            switch (mouseButtonClicked)
            {
                case MouseEvent.BUTTON1: //agregar celda al patrón
                {
                    if (CanvasUtility.isThisShapeNew(x, y, this.shapeColorMap))
                    {
                        this.getCurrentPattern().addCell(cell);
                    }
                    else
                    {
                        CanvasUtility.removeShapeFromMap(x, y, this.getShapeColorMap(), null);
                        this.getCurrentPattern().updateCell(cell); //cambia valor celda
                    }

                    this.getShapeColorMap().put(shape, MainWindowMaker.currentCA.getStates().getState(cell.getStateId()).getColor()); 
                    break;
                }

                case MouseEvent.BUTTON3: //Borrar Celda del patrón
                {
                    cell = getCellFromShape(shape, MainWindowMaker.currentCA.getGrids().getBackground());
                    CanvasUtility.removeShapeFromMap(x, y, this.getShapeColorMap(), null);

                    if (this.deleteOn)
                    {
                        this.getCurrentPattern().delCell(cell);
                    }
                    else
                    {
                        this.getCurrentPattern().updateCell(cell); //cambia valor celda
                        this.getShapeColorMap().put(shape, MainWindowMaker.currentCA.getStates().getState(cell.getStateId()).getColor());

                    }
                    break;
                }
            }
        }
    }

    
    /**
     * Actualiza el patrón según la opción escogida.
     *
     * @param x
     * @param y
     * @param mouseButtonClicked
     * @param pattern 
     */
    private void updatePattern(int x, int y, int mouseButtonClicked, CustomPattern pattern)
    {
        if (x >= CanvasPattern.GRID_ORIGIN_X && x <= this.corGridWidth && y >= CanvasPattern.GRID_ORIGIN_Y && y <= this.corGridHeight)
        {
            Cell originPoint = this.getCellFromShape(createShapeFromClick(x, y), selectedState);

            CustomPattern tmpPattern = CanvasUtility.applyPatternConversions(
                pattern, 
                this.rotation, 
                this.flipVertical, 
                this.flipHorizontal);

            switch (mouseButtonClicked)
            {
                case MouseEvent.BUTTON1: //Agrega patrón al grid
                {
                    this.addPattern(tmpPattern, originPoint);
                    break;
                }
                case MouseEvent.BUTTON3: //Borra con la forma del patrón
                {
                    this.removelPattern(tmpPattern, originPoint);
                    break;
                }
            }
            repaint();
        }
    }

    /**
     * Agrega el patrón al grid.
     * @param tmpPattern
     * @param originPoint 
     */
    private void addPattern(CustomPattern tmpPattern, Cell originPoint)
    {
        State tmpState;
        Shape tmpShape;
        Set<Short> errorSet = new HashSet<>();
        
        for (Cell cell : tmpPattern.normalize(originPoint))
        {
            if (MainWindowMaker.currentCA.getStates().conteinsId(cell.getStateId()))
            {
                tmpState = MainWindowMaker.currentCA.getStates().getState(cell.getStateId());
            }
            else
            {
                errorSet.add(cell.getStateId());
                tmpState = new State(cell.getStateId());
            }

            tmpShape = this.getShape(cell);
            int x2 = tmpShape.getBounds().x + tmpShape.getBounds().width / 2;
            int y2 = tmpShape.getBounds().y + tmpShape.getBounds().height / 2;

            if (x2 <= this.corGridWidth && y2 <= this.corGridHeight)
            {
                CanvasUtility.removeShapeFromMap(x2, y2, this.getShapeColorMap(), null);
                this.getShapeColorMap().put(tmpShape, tmpState.getColor());
                this.getCurrentPattern().updateCell(new Cell(cell.getRow(), cell.getColumn(), tmpState.getId())); //cambia valor celda
            }
        }
        if (errorSet.size() > 0)
        {
            CanvasUtility.errorMessagePattern(errorSet);
        }

    }

    /**
     * Remueve un patrón del grid luego de normalizarlo.
     * @param tmpPattern
     * @param originPoint 
     */
    private void removelPattern(CustomPattern tmpPattern, Cell originPoint)
    {
        State tmpState;
        Shape tmpShape;

        for (Cell cell : tmpPattern.normalize(originPoint))
        {
            tmpState = this.getSelectedState();

            tmpShape = this.getShape(cell);

            int x2 = tmpShape.getBounds().x + tmpShape.getBounds().width / 2;
            int y2 = tmpShape.getBounds().y + tmpShape.getBounds().height / 2;

            if (x2 <= this.gridWidth && y2 <= this.gridHeight)
            {
                CanvasUtility.removeShapeFromMap(x2, y2, this.getShapeColorMap(), null);
                if (this.deleteOn)
                {
                    this.getCurrentPattern().delCell(cell);
                }
                else
                {
                    this.getShapeColorMap().put(tmpShape, tmpState.getColor());
                    this.getCurrentPattern().updateCell(new Cell(cell.getRow(), cell.getColumn(), tmpState.getId())); //cambia valor celda
                }
            }
        }
    }


    /**
     * Crea el shape de la celda que corresponde a la posición del click
     *
     * @param mouseX coordenada centerX del mouse
     * @param mouseY coordenada centerY del mouse
     * @return
     */
    private Rectangle createShapeFromClick(int mouseX, int mouseY)
    {
        int xo;
        int yo;

        xo = (mouseX - GRID_ORIGIN_X) / CELL_WIDTH;
        xo = xo * CELL_WIDTH + GRID_ORIGIN_X;
        yo = (mouseY - GRID_ORIGIN_Y) / CELL_HEIGHT;
        yo = yo * CELL_HEIGHT + GRID_ORIGIN_Y;

        return new Rectangle(xo, yo, CELL_WIDTH, CELL_HEIGHT);
    }
    
    
    /**
     * Transforma las coordenadas de la matriz a coordenadas en pixeles.
     *
     * @param matrixX
     * @param matrixY
     * @return
     */
    private Shape getShape(Cell cell)
    {
        int gridX = cell.getColumn() * CELL_WIDTH + GRID_ORIGIN_X;
        int gridY = cell.getRow() * CELL_HEIGHT + GRID_ORIGIN_Y;

        return new Rectangle(gridX, gridY, CELL_WIDTH, CELL_HEIGHT);
    }
    
    

    /**
     * Convierte un shape con coordenadas en pixels a un Cell con coordenadas de
     * arreglo para agregar al patrón
     *
     * @param cellShape
     * @return
     */
    private Cell getCellFromShape(Shape cellShape, State state)
    {
        int shapeX = (int) cellShape.getBounds().getX();
        int shapeY = (int) cellShape.getBounds().getY();

        //int matrixX = ((shapeX - GRID_ORIGIN_X) / CELL_WIDTH) - centerX;
        //int matrixY = ((shapeY - GRID_ORIGIN_Y) / CELL_HEIGHT) - centerY;

        int matrixX = ((shapeX - GRID_ORIGIN_X) / CELL_WIDTH);
        int matrixY = ((shapeY - GRID_ORIGIN_Y) / CELL_HEIGHT);
        
        
        return new Cell(matrixY, matrixX, state.getId());
    }

    /**
     * Llena la totalidad el mapa con el estado seleccionado.
     */
    @Override
    public void fillPatternMatrix()
    {
        this.getCurrentPattern().getCells().clear();

        for (int x = 0; x < CanvasPattern.GRID_HOR_CELL_NUM; x++)
        {
            for (int y = 0; y < CanvasPattern.GRID_VER_CELL_NUM; y++)
            {
                this.getCurrentPattern().addCell(new Cell(y, x, this.getSelectedState().getId()));
            }
        }
        this.drawPattern();
    }

    /**
     * Empaqueta el patrón
     */
    @Override
    public void packPattern()
    {
        this.getShapeColorMap().clear();
        this.getCurrentPattern().packCells();
        this.drawPattern();
    }
    
    /**
     * Agrega las shapes al mapa a partir de la lista de celdas.
     */
    private void drawPattern()
    {
        Color tmpColor;
        boolean error = false;
        Set<Short> set = new HashSet<>();
        StringBuilder errorMessage = new StringBuilder("Los siguientes estados no estan definidos y serán reemplazados por un color temporal:");
        Color errorColor = Color.BLUE;
        
        for (Cell cell : this.getCurrentPattern().getCells())
        {
            if (MainWindowMaker.currentCA.getStates().conteinsId(cell.getStateId()))
            {
                tmpColor = MainWindowMaker.currentCA.getStates().getState(cell.getStateId()).getColor();
            }
            else
            {
                error = true;
                set.add(cell.getStateId());
                tmpColor = SacUtility.shiftColor(errorColor, cell.getStateId());
            }
            this.getShapeColorMap().put(this.getShape(cell), tmpColor);
        }

        if (error)
        {
            errorMessage.append(SacUtility.EOL).append(set.toString());
            errorMessage.append(SacUtility.EOL).append("Para mostrar correctamente el patrón cargue el conjunto de estados correspondiente.");
            
            //System.out.println(errorMessage + SacUtility.EOL + errorSet.toString());
            SacUtility.showWarning(errorMessage.toString());
        }
    }

    /**
     * Borra el hashmap y limpia la lista de celdas del patrón.
     */
    @Override
    public void clear()
    {
        this.getShapeColorMap().clear();
        this.getCurrentPattern().getCells().clear();
    }

    @Override
    public int getGridOriginX()
    {
        return CanvasPattern.GRID_ORIGIN_X;
    }

    @Override
    public int getGridOriginY()
    {
        return CanvasPattern.GRID_ORIGIN_Y;
    }

    @Override
    public int getGridHeight()
    {
        return this.corGridHeight;
    }

    @Override
    public int getGridWidth()
    {
        return this.corGridWidth;
    }

    @Override
    public int getCellHeight()
    {
        return CanvasPattern.CELL_HEIGHT;
    }

    @Override
    public int getCellWidth()
    {
        return CanvasPattern.CELL_WIDTH;
    }

    @Override
    public int getRotation()
    {
        return this.rotation;
    }

    @Override
    public void setRotation(int rotation)
    {
        this.rotation = rotation;
    }
    
    
    @Override
    public boolean getFlipVertical()
    {
        return this.flipVertical;
    }

    @Override
    public void setFlipVertical(boolean set)
    {
        this.flipVertical = set;
    }

    @Override
    public boolean getFlipHorizontal()
    {
        return this.flipHorizontal;
    }

    @Override
    public void setFlipHorizontal(boolean set)
    {
        this.flipHorizontal = set;
    }

}
