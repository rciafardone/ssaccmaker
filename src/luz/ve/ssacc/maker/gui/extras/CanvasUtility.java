/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package luz.ve.ssacc.maker.gui.extras;

import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.Point;
import java.awt.Shape;
import java.awt.geom.Line2D;
import java.util.Map;
import java.util.Random;
import java.util.Set;
import luz.ve.ssacc.engine.components.Automaton;
import luz.ve.ssacc.engine.components.Cell;
import luz.ve.ssacc.engine.components.CustomPattern;
import luz.ve.ssacc.engine.components.State;
import luz.ve.ssacc.utilities.SacUtility;

/**
 *
 * @author rciafardone
 */
public class CanvasUtility
{
    
    /**
     * Revisa la lista de celdas e indica si ya ha sido agregada
     *
     * @param x
     * @param y
     * @param map
     * @return
     */
    public static boolean isThisShapeNew(int x, int y, Map<Shape, Color> map)
    {
        return map.entrySet().stream().map((e) -> e.getKey()).noneMatch((key) -> (key.contains(x, y)));
    }
    
    
    /**
     * Elimina la forma de la celda (X,Y) del mapa, a menos que sea del color ignoreColor.
     * Si la figura no existe no pasa nada.
     *
     * @param x
     * @param y
     * @param map
     * @param ignoreColor
     */
    public static void removeShapeFromMap(int x, int y, Map<Shape, Color> map, Color ignoreColor)
    {
        Shape key;
        Color value;

        for (Map.Entry<Shape, Color> e : map.entrySet())
        {
            key = e.getKey();
            value = e.getValue();

            if (!value.equals(ignoreColor))
            {
                if (key.contains(x, y))
                {
                    map.remove(key);
                    break;
                }
            }
        }
    }

    /**
     * Devuelve un patron que es una imagen espejo vertical del patron original
     * @param pattern
     * @return 
     */
    public static CustomPattern mirrorVertical(CustomPattern pattern)
    {
        CustomPattern flippedPattern = new CustomPattern();
        
        Point lowerRightCorner = CustomPattern.getBoundingBoxLowerRightCorner(pattern.getCells());

        pattern.getCells().forEach((cell) ->
        {
            int newRow = lowerRightCorner.x - cell.getRow();
            int newCol = cell.getColumn();
            flippedPattern.addCell(new Cell(newRow, newCol, cell.getStateId()));
        });

        return flippedPattern;
    }

    /**
     * Devuelve un patron que es una imagen espejo horizontal del patron
     * original
     *
     * @param pattern
     * @return
     */
    public static CustomPattern mirrorHorizontal(CustomPattern pattern)
    {
        CustomPattern flippedPattern = new CustomPattern();

        Point lowerRightCorner = CustomPattern.getBoundingBoxLowerRightCorner(pattern.getCells());

        pattern.getCells().forEach((cell) ->
        {
            int newRow = cell.getRow();
            int newCol = lowerRightCorner.y - cell.getColumn();
            flippedPattern.addCell(new Cell(newRow, newCol, cell.getStateId()));
        });

        return flippedPattern;
    }

    /**
     * Rota el patrón original tantos grados como indica el parametro rotation
     *
     * @param pattern
     * @param rotation
     * @return
     */
    public static CustomPattern rotatePattern(CustomPattern pattern, Integer rotation)
    {
        CustomPattern rotatedPattern = new CustomPattern();

        rotatedPattern.setCells(pattern.getCells());

        if (rotation < 0)
        {
            for (int i = 0; i > rotation; i = i - 90)
            {
                rotatedPattern.setCells(CanvasUtility.rotateClockWise(rotatedPattern).getCells());
            }
        }
        else
        {
            for (int i = 0; i < rotation; i = i + 90)
            {
                rotatedPattern.setCells(CanvasUtility.rotateCounterClock(rotatedPattern).getCells());
            }
        }
        return rotatedPattern;
    }

    /**
     * Rota el patrón 90º en sentido contrareloj.
     * @param pattern
     * @return
     */
    public static CustomPattern rotateCounterClock(CustomPattern pattern)
    {
        CustomPattern rotatedPattern = new CustomPattern();
       
        Point lowerRightCorner = CustomPattern.getBoundingBoxLowerRightCorner(pattern.getCells());

        pattern.getCells().forEach((cell) ->
        {
            int newRow = lowerRightCorner.y - cell.getColumn();
            int newCol = cell.getRow();
            rotatedPattern.addCell(new Cell(newRow, newCol, cell.getStateId()));
        });

        return rotatedPattern;
    }

    /**
     * Rota el patrón 90º en el sentido del reloj.
     *
     * @param pattern
     * @return
     */
    public static CustomPattern rotateClockWise(CustomPattern pattern)
    {
        CustomPattern rotatedPattern = new CustomPattern();

        Point lowerRightCorner = CustomPattern.getBoundingBoxLowerRightCorner(pattern.getCells());

        pattern.getCells().forEach((cell) ->
        {
            int newCol = lowerRightCorner.x - cell.getRow();
            int newRow = cell.getColumn();
            rotatedPattern.addCell(new Cell(newRow, newCol, cell.getStateId()));
        });

        return rotatedPattern;
    }

    /**
     * Asegura que la rotación se mantenga entre +/-0 y +/-270.
     *
     * @param rotationStep
     * @param rotation
     * @return
     */
    public static Integer controlChangeRotation(Integer rotationStep, Integer rotation)
    {
        rotation = rotation + rotationStep;

        if (rotation == 360)
        {
            rotation = 0;
        }

        if (rotation == -360)
        {
            rotation = 0;
        }

        return rotation;
    }
   
    
    /**
     * Genera un patrón aleatorio de tamaño ROWSxCOLUMNS con densidad igual a density.
     * @param density Porcentage aproximado del grid que se llena aleatoriamente con el estado.
     * @param stateId Estado con que se llena el grid
     * @param rows número de filas del area a llenar
     * @param columns número de columnas del area a llenar
     * @return 
     */
    public static CustomPattern generateRandomPattern(Double density, Short stateId, int rows, int columns)
    {
        CustomPattern randomGrid = new CustomPattern();
        Random randomGenerator = new Random();
        int randomRow;
        int randomCol;
        Double max = rows * columns * density / 100;

        for (int count = 0; count < max; count++)
        {
            randomRow = randomGenerator.nextInt(rows);
            randomCol = randomGenerator.nextInt(columns);
            randomGrid.addCell(new Cell(randomRow, randomCol, stateId));
        }
        
        return randomGrid;
    }

    /**
     * Devuelve un color con contraste maximo al suministrado.
     *
     * @param color
     * @return
     */
    public static Color getContrastColor(Color color)
    {
        double y = (299 * color.getRed() + 587 * color.getGreen() + 114 * color.getBlue()) / 1000;
        return y >= 128 ? Color.black : Color.white;
    }

    /**
     * Aplica todas las conversiones indicadas
     * @param pattern
     * @param rotation
     * @param flipVertical
     * @param flipHorizontal
     * @return 
     */
    public static CustomPattern applyPatternConversions(
        CustomPattern pattern, 
        int rotation, 
        boolean flipVertical, 
        boolean flipHorizontal)
    {
        CustomPattern tmpPattern = pattern;

        if (rotation != 0)
        {
            tmpPattern = CanvasUtility.rotatePattern(tmpPattern, rotation);
        }

        if (flipVertical)
        {
            tmpPattern = CanvasUtility.mirrorVertical(tmpPattern);
        }

        if (flipHorizontal)
        {
            tmpPattern = CanvasUtility.mirrorHorizontal(tmpPattern);
        }

        return tmpPattern;
    }

    /**
     * Genera un patron aleatorio con el estado indicado utilizado la densidad
     * suministrada
     *
     * @param density Porcentage aproximado del grid que se llena aleatoriamente
     * con el estado.
     * @param state
     * @param ca
     * @return
     */
    public static CustomPattern generateRandomPattern(Double density, State state, Automaton ca)
    {
        CustomPattern randomGrid = new CustomPattern();
        Random randomGenerator = new Random();

        int rows = ca.getGrids().getRows();
        int cols = ca.getGrids().getColumns();
        int rRow;
        int rCol;
        Double max = rows * cols * density / 100;

        for (int count = 0; count < max; count++)
        {
            rRow = randomGenerator.nextInt(rows);
            rCol = randomGenerator.nextInt(cols);

            randomGrid.addCell(new Cell(rRow, rCol, state.getId()));
        }
        return randomGrid;
    }
    
    /**
     * Crea un grid del tamaño indicado.
     * @param g2
     * @param color
     * @param canvas
     */
    public static void paintGrid(Graphics2D g2, Color color, Canvas canvas)
    {
        Shape line;

        //Vertical lines 
        for (int x = canvas.getGridOriginX(); x <= canvas.getGridWidth(); x += canvas.getCellWidth())
        {
            g2.setPaint(color);
            line = new Line2D.Float(x, canvas.getGridOriginY(), x, canvas.getGridHeight());
            g2.draw(line);

        }

        //Horizontal lines
        for (int y = canvas.getGridOriginY(); y <= canvas.getGridHeight(); y += canvas.getCellHeight())
        {
            g2.setPaint(color);
            line = new Line2D.Float(canvas.getGridOriginX(), y, canvas.getGridWidth(), y);
            g2.draw(line);
        }
    }

    /**
     * Dibuja una cruz en el punto indicado con el ancho y largo de brazos
     * deseados
     *
     * @param g2
     * @param color
     * @param x coordenada en x del centro de la cruz.
     * @param y coordenada en y del centro de la cruz.
     * @param height largo de los brazos verticales.
     * @param width largo de los brazos horizontales.
     */
    public static void drawCross(Graphics2D g2, Color color, int x, int y, int height, int width)
    {
        Shape line;
        g2.setPaint(color);
        line = new Line2D.Float(x - width, y, x + width, y);
        g2.draw(line);
        line = new Line2D.Float(x, y - height, x, y + height);
        g2.draw(line);
    }

   /**
     * Llena el area indicada con cruces.
     *
     * @param g2
     * @param color
     * @param canvas
     */
    public static void paintCrosses(Graphics2D g2, Color color, Canvas canvas)
    {
        for (int x = canvas.getGridOriginX(); x <= canvas.getGridWidth(); x += canvas.getCellWidth())
        {
            for (int y = canvas.getGridOriginY(); y < canvas.getGridHeight(); y += canvas.getCellHeight())
            {
                CanvasUtility.drawCross(g2, color, x + 7, y + 7, 3, 3);
            }
        }
    }
    
    /**
     * Genera Mensaje de error para la adicion de patrones.
     * @param errorSet 
     */
    public static void errorMessagePattern(Set<Short> errorSet)
    {
        StringBuilder errorMessage = new StringBuilder("Los siguientes estados del patrón no estan definidos dentro del conjunto de estados en uso:");
        errorMessage.append(SacUtility.EOL).append(errorSet.toString());
        errorMessage.append(SacUtility.EOL).append("Para mostrar correctamente el patrón cargue el conjunto de estados correspondiente.");
        //System.out.println(errorMessage + SacUtility.EOL + errorSet.toString());
        SacUtility.showWarning(errorMessage.toString());
    }
}
