/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package luz.ve.ssacc.maker.gui.extras;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Rectangle;
import java.awt.RenderingHints;
import java.awt.Shape;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.Map;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Set;
import javax.swing.JComponent;
import luz.ve.ssacc.engine.components.Cell;
import luz.ve.ssacc.engine.components.State;
import luz.ve.ssacc.engine.components.CustomPattern;
import luz.ve.ssacc.engine.components.Grid;
import luz.ve.ssacc.maker.gui.MainWindowMaker;

/**
 *
 * @author rciafardone
 */
public class CanvasGrid extends JComponent implements Canvas
{

    private CustomPattern currentGridPattern;

    private final Map<Shape, Color> shapeColorMap;
    public Color borderColor;
    public State selectedState;
    public boolean drawPixel = true;
    public CustomPattern selectedPattern;
    private boolean flipVertical;
    private boolean flipHorizontal;
    private int rotation;
   
    private static final int GRID_ORIGIN_Y = 0; //coordenada y en pixels de la posicion del grid.
    private static final int GRID_ORIGIN_X = 0; //coordenada x en pixels de la posicion del grid.
    private static int CELL_HEIGHT; // alto de celda en pixeles.
    private static int CELL_WIDTH; // ancho de celda en pixeles.
    private static int GRID_VER_CELL_NUM; // número de celdas en la vertical.
    private static int GRID_HOR_CELL_NUM; // número de celdas en la horizontal.
    //public static final Color LINE_COLOR = Color.BLACK;

    private final int gridHeight; // alto del canvas en pixeles.
    private final int gridWidth; // ancho del cnvas en pixeles.
    private final int corGridHeight; // alto corregida con inicio.
    private final int corGridWidth; // ancho corregido con inicio.

    // private final int centerX;
    // private final int centerY;     
    public CanvasGrid()
    {
        this.setFlipVertical(false);
        this.setFlipHorizontal(false);
        this.setRotation(0);
        CELL_HEIGHT = MainWindowMaker.currentCA.getGrids().getCellSize();
        CELL_WIDTH = MainWindowMaker.currentCA.getGrids().getCellSize();
        GRID_VER_CELL_NUM = MainWindowMaker.currentCA.getGrids().getRows();
        GRID_HOR_CELL_NUM = MainWindowMaker.currentCA.getGrids().getColumns();

        gridHeight = GRID_VER_CELL_NUM * CELL_HEIGHT;
        gridWidth = GRID_HOR_CELL_NUM * CELL_WIDTH;
        corGridHeight = gridHeight + GRID_ORIGIN_Y;
        corGridWidth = gridWidth + GRID_ORIGIN_X;

        this.currentGridPattern = this.createPattern(MainWindowMaker.currentCA.getGrids().getGridOri());
        this.shapeColorMap = new HashMap<>();
        initialConfig();
        this.drawGridPattern();
    }

    /**
     * Configuración inicial
     */
    private void initialConfig()
    {
        this.addMouseListener(new MouseAdapter()
        {
            @Override
            public void mouseReleased(MouseEvent e)
            {
                if (drawPixel)
                {
                    updateCell(e.getX(), e.getY(), e.getButton(), selectedState);
                }
                else
                {
                    updatePattern(e.getX(), e.getY(), e.getButton(), selectedPattern);
                }
                repaint();
            }
        });
    }

    /**
     * Asigna el valor del patrón y dibuja
     *
     * @param pattern
     */
    public void setCurrentGridPattern(CustomPattern pattern)
    {
        this.currentGridPattern = pattern;
    }

    /**
     * Devuelve el patrón del grid actual.
     *
     * @return
     */
    public CustomPattern getCurrentGridPattern()
    {
        return currentGridPattern;
    }

    /**
     * Devuelve el mapa de las figuras en el grid.
     * @return 
     */
    private Map<Shape, Color> getShapeColorMap()
    {
        return shapeColorMap;
    }

    /**
     * Devuelve el estado actualmente seleccionado.
     * @return
     */
    @Override
    public State getSelectedState()
    {
        return selectedState;
    }

    /**
     * Establece cual es el estado actualmente seleccionado.
     * @param selectedState 
     */
    @Override
    public void setSelectedState(State selectedState)
    {
        this.selectedState = selectedState;
    }

    /**
     * Dibuja la retícula.
     * @param g 
     */
    @Override
    public void paint(Graphics g)
    {
        Graphics2D g2 = (Graphics2D) g;
        g2.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_OFF);
        if (MainWindowMaker.currentCA.getGrids().isBorderOn())
        {
            this.paintBackground(g2);
        }
        
        this.getShapeColorMap().entrySet().forEach((e) ->
        {
            Shape shape = e.getKey();
            Color color = e.getValue();
            g2.setPaint(color);
            g2.fill(shape);
        });
        if (MainWindowMaker.currentCA.getGrids().isBorderOn())
        {
            this.paintBackground(g2);
        }
        
    }

    /**
     * Pinta la cuadrícula del patrón/vecindario.
     *
     * @param g2
     */
    private void paintBackground(Graphics2D g2)
    {
        CanvasUtility.paintGrid(
            g2,
            MainWindowMaker.currentCA.getGrids().getBorderColor(),
            this);
    }
    

    /**
     * Actualiza la celda del grid según el opción que se escoja.
     *
     * @param x
     * @param y
     * @param mouseButtonClicked
     */
    private void updateCell(int x, int y, int mouseButtonClicked, State state)
    {
        if (x > GRID_ORIGIN_X && x < this.corGridWidth && y > GRID_ORIGIN_Y && y < this.corGridHeight)
        {
            switch (mouseButtonClicked)
            {
                case MouseEvent.BUTTON1: //agregar celda al patrón
                {
                    this.addCell(state, x, y);
                    break;
                }

                case MouseEvent.BUTTON3: //Borrar Celda del patrón
                {
                    this.removeCell(x, y);
                    break;
                }
            }
        }
    }

    /**
     * Agrega la celda al grid.
     * @param state
     * @param x
     * @param y 
     */
    private void addCell(State state, int x, int y)
    {
        Cell cell;
        Shape shape;
        shape = createShapeFromClick(x, y);
        cell = getCellFromShape(shape, state);
        if (CanvasUtility.isThisShapeNew(x, y, this.shapeColorMap))
        {
            this.getCurrentGridPattern().addCell(cell);
        }
        else
        {
            this.removeShapeFromMap(x, y);
            this.getCurrentGridPattern().updateCell(cell); //cambia valor celda
        }

        this.getShapeColorMap().put(shape, MainWindowMaker.currentCA.getStates().getState(cell.getStateId()).getColor());
    }

    /**
     * Elimina la celda del grid.
     * @param state
     * @param x
     * @param y 
     */
    private void removeCell(int x, int y)
    {
        Shape shape;
        Cell cell;
        Color color;
                
        shape = createShapeFromClick(x, y);
        cell = getCellFromShape(shape, MainWindowMaker.currentCA.getGrids().getBackground());
        color =  MainWindowMaker.currentCA.getStates().getState(cell.getStateId()).getColor();

        this.removeShapeFromMap(x, y);
        this.getCurrentGridPattern().updateCell(cell); //cambia valor celda
        this.getShapeColorMap().put(shape,color);
    }
    
    /**
     * Actualiza el grid con un patrón según la opción escogida.
     *
     * @param x
     * @param y
     * @param mouseButtonClicked
     * @param pattern
     */
    private void updatePattern(int x, int y, int mouseButtonClicked, CustomPattern pattern)
    {
        if (x >= CanvasGrid.GRID_ORIGIN_X && x <= this.corGridWidth && y >= CanvasGrid.GRID_ORIGIN_Y && y <= this.corGridHeight)
        {
            Cell originPoint = this.getCellFromShape(createShapeFromClick(x, y), selectedState);

            CustomPattern tmpPattern = CanvasUtility.applyPatternConversions(
                pattern,
                this.rotation,
                this.flipVertical,
                this.flipHorizontal);

            switch (mouseButtonClicked)
            {
                case MouseEvent.BUTTON1: //Agrega patrón al grid
                {
                    this.addPattern(tmpPattern, originPoint);
                    break;
                }
                case MouseEvent.BUTTON3: //Borra con la forma del patrón
                {
                    this.removePattern(tmpPattern, originPoint);
                    break;
                }
            }
            repaint();
        }
    }

    /**
     * Agrega el patrón al grid.
     * 
     * @param tmpPattern
     * @param originPoint 
     */
    private void addPattern(CustomPattern tmpPattern, Cell originPoint)
    {
        Shape tmpShape;
        State tmpState;    
        Set<Short> errorSet = new HashSet<>();

        for (Cell cell : tmpPattern.normalize(originPoint))
        {
            if (MainWindowMaker.currentCA.getStates().conteinsId(cell.getStateId()))
            {
                tmpState = MainWindowMaker.currentCA.getStates().getState(cell.getStateId());
            }
            else
            {
                errorSet.add(cell.getStateId());
                tmpState = new State(cell.getStateId());
            }

            tmpShape = this.getShape(cell);
            int x2 = tmpShape.getBounds().x + tmpShape.getBounds().width / 2;
            int y2 = tmpShape.getBounds().y + tmpShape.getBounds().height / 2;

            if (x2 <= this.corGridWidth && y2 <= this.corGridHeight)
            {
                this.removeShapeFromMap(x2, y2);
                this.getShapeColorMap().put(tmpShape, tmpState.getColor());
                this.getCurrentGridPattern().updateCell(new Cell(cell.getRow(), cell.getColumn(), tmpState.getId())); //cambia valor celda
            }
        }
        if (errorSet.size() > 0)
        {
            CanvasUtility.errorMessagePattern(errorSet);
        }
    }

    /**
     * Elimina el patrón del grid.
     * 
     * @param tmpPattern
     * @param originPoint 
     */
    private void removePattern(CustomPattern tmpPattern, Cell originPoint)
    {
        Shape tmpShape;
        State tmpState;

        for (Cell cell : tmpPattern.normalize(originPoint))
        {
            tmpState = MainWindowMaker.currentCA.getGrids().getBackground();
            
            tmpShape = this.getShape(cell);
            int x2 = tmpShape.getBounds().x + tmpShape.getBounds().width / 2;
            int y2 = tmpShape.getBounds().y + tmpShape.getBounds().height / 2;

            if (x2 <= this.corGridWidth && y2 <= this.corGridHeight)
            {
                this.removeShapeFromMap(x2, y2);
                this.getShapeColorMap().put(tmpShape, tmpState.getColor());
                this.getCurrentGridPattern().updateCell(new Cell(cell.getRow(), cell.getColumn(), tmpState.getId())); //cambia valor celda
            }
        }
    }


    /**
     * Elimina la celda (X,Y)
     *
     * @param x
     * @param y
     */
    private void removeShapeFromMap(int x, int y)
    {
        for (Map.Entry<Shape, Color> e : this.getShapeColorMap().entrySet())
        {
            Shape key = e.getKey();

            if (key.contains(x, y))
            {
                this.getShapeColorMap().remove(key);
                break;
            }
        }
    }

    /**
     * Crea el shape de la celda que corresponde a la posición del click
     *
     * @param mouseX coordenada centerX del mouse
     * @param mouseY coordenada centerY del mouse
     * @return
     */
    private Rectangle createShapeFromClick(int mouseX, int mouseY)
    {
        int xo;
        int yo;

        xo = (mouseX - GRID_ORIGIN_X) / CELL_WIDTH;
        xo = xo * CELL_WIDTH + GRID_ORIGIN_X;
        yo = (mouseY - GRID_ORIGIN_Y) / CELL_HEIGHT;
        yo = yo * CELL_HEIGHT + GRID_ORIGIN_Y;

        return new Rectangle(xo, yo, CELL_WIDTH, CELL_HEIGHT);
    }

    /**
     * Transforma las coordenadas de la matriz a coordenadas en pixeles.
     *
     * @param matrixX
     * @param matrixY
     * @return
     */
    private Shape getShape(Cell cell)
    {
        int gridX = cell.getColumn() * CELL_WIDTH + GRID_ORIGIN_X;
        int gridY = cell.getRow() * CELL_HEIGHT + GRID_ORIGIN_Y;

        return new Rectangle(gridX, gridY, CELL_WIDTH, CELL_HEIGHT);
    }

    /**
     * Convierte un shape con coordenadas en pixels a un Cell con coordenadas de
     * arreglo para agregar al patrón
     *
     * @param cellShape
     * @return
     */
    private Cell getCellFromShape(Shape cellShape, State state)
    {
        int shapeX = (int) cellShape.getBounds().getX();
        int shapeY = (int) cellShape.getBounds().getY();
        int matrixX = ((shapeX - GRID_ORIGIN_X) / CELL_WIDTH);
        int matrixY = ((shapeY - GRID_ORIGIN_Y) / CELL_HEIGHT);

        return new Cell(matrixY, matrixX, state.getId());
    }

    /**
     * Llena la totalidad el mapa con el estado seleccionado.
     *
     * @param selectedState
     */
    public void fillPatternMatrix(State selectedState)
    {
        this.getCurrentGridPattern().getCells().clear();

        for (int x = 0; x < CanvasGrid.GRID_HOR_CELL_NUM; x++)
        {
            for (int y = 0; y < CanvasGrid.GRID_VER_CELL_NUM; y++)
            {
                this.getCurrentGridPattern().addCell(new Cell(y, x, selectedState.getId()));
            }
        }
        this.drawGridPattern();
    }

  
    /**
     * Agrega las shapes al mapa a partir de la lista de celdas.
     */
    private void drawGridPattern()
    {
        this.getCurrentGridPattern().getCells().forEach((cell) ->
        {
            this.getShapeColorMap().put(this.getShape(cell), MainWindowMaker.currentCA.getStates().getState(cell.getStateId()).getColor());
        });
        repaint();
    }

    /**
     * Llena el grid con el estado indicado utilizando la densidad suministrada.
     * @param density 
     */
    public void fillWithRandomPattern(Double density)
    {
        CustomPattern randomPattern;
        randomPattern = CanvasUtility.generateRandomPattern(density, selectedState, MainWindowMaker.currentCA);
        this.addPattern(randomPattern, new Cell());
    }

    /**
     * Recibe un grid y retorna el patrón equivalente.
     *
     * @param grid
     * @return
     */
    private CustomPattern createPattern(Grid grid)
    {
        CustomPattern gridPattern = new CustomPattern();

        for (int row = 0; row < grid.getRows(); row++)
        {
            for (int col = 0; col < grid.getColumns(); col++)
            {
                gridPattern.addCell(new Cell(row, col, grid.getStateId(row, col)));
            }
        }
        return gridPattern;
    }

    @Override
    public void clear()
    {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void fillPatternMatrix()
    {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public CustomPattern getCurrentPattern()
    {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void packPattern()
    {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void setCurrentPattern(CustomPattern pattern)
    {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public int getGridOriginX()
    {
        return CanvasGrid.GRID_ORIGIN_X;
    }

    @Override
    public int getGridOriginY()
    {
        return CanvasGrid.GRID_ORIGIN_Y;
    }

    @Override
    public int getGridHeight()
    {
        return this.gridHeight;
    }

    @Override
    public int getGridWidth()
    {
        return this.gridWidth;
    }

    @Override
    public int getCellHeight()
    {
        return CanvasGrid.CELL_HEIGHT;
    }

    @Override
    public int getCellWidth()
    {
        return CanvasGrid.CELL_WIDTH;
    }

    @Override
    public int getRotation()
    {
        return this.rotation;
    }

    @Override
    public void setRotation(int rotation)
    {
        this.rotation = rotation;
    }

    @Override
    public boolean getFlipVertical()
    {
        return this.flipVertical;
    }

    @Override
    public void setFlipVertical(boolean set)
    {
        this.flipVertical = set;
    }

    @Override
    public boolean getFlipHorizontal()
    {
        return this.flipHorizontal;
    }

    @Override
    public void setFlipHorizontal(boolean set)
    {
        this.flipHorizontal = set;
    }

}
