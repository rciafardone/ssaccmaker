/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package luz.ve.ssacc.maker.gui.extras;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Rectangle;
import java.awt.RenderingHints;
import java.awt.Shape;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import javax.swing.JComponent;
import luz.ve.ssacc.engine.components.Cell;
import luz.ve.ssacc.engine.components.CustomPattern;
import luz.ve.ssacc.engine.components.Neighborhood;
import luz.ve.ssacc.engine.components.State;
import luz.ve.ssacc.maker.gui.MainWindowMaker;
import luz.ve.ssacc.utilities.SacUtility;
/**
 *
 * @author rciafardone
 */
public class CanvasNeighborhood extends JComponent implements Canvas{
 
    private static final int GRID_ORIGIN_Y = 0;
    private static final int GRID_ORIGIN_X = 20;
    private static final int CELL_HEIGHT = 24;
    private static final int CELL_WIDTH = 24;
    private static final int GRID_VER_CELL_NUM = 9;
    private static final int GRID_HOR_CELL_NUM = 9;
    private final int centerX;
    private final int centerY;
    private final int gridHeight;
    private final int gridWidth;
    private final int corGridHeight;
    private final int corGridWidth;
   
    private final Color LINE_COLOR = Color.BLACK;
    private final Color[] colors;
    
    private final Map<Shape, Color> shapeColorMap = new HashMap<>();
    
    

    public CanvasNeighborhood()
    {
        this.colors = new Color[]
        {
            Color.MAGENTA, //left click for normal cells
            Color.CYAN, 
            Color.BLACK, 
            Color.BLUE //right click for the radix cellShape
        };
        
        centerX = GRID_HOR_CELL_NUM / 2;
        centerY = GRID_VER_CELL_NUM / 2;
        gridHeight = GRID_VER_CELL_NUM * CELL_HEIGHT;
        gridWidth = GRID_HOR_CELL_NUM * CELL_WIDTH;
        corGridHeight = gridHeight + GRID_ORIGIN_Y;
        corGridWidth = gridWidth + GRID_ORIGIN_X;

        startingConfig();
    }

    private void startingConfig()
    {
        this.addMouseListener(new MouseAdapter()
        {
            @Override
            public void mouseReleased(MouseEvent e)
            {
                addCell(e.getX(), e.getY(), e.getButton());
                repaint();
            }
        });
    }

    @Override
    public void paint(Graphics g)
    {
        Graphics2D g2 = (Graphics2D) g;
        g2.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_OFF);
        shapeColorMap.entrySet().forEach((e) ->
        {
            Shape shape = e.getKey();
            Color color = e.getValue();
            g2.setPaint(color);
            g2.fill(shape);
        });

        this.paintBackground(g2);
    }

    /**
     * Pinta la cuadrícula del patrón/vecindario
     *
     * @param g2
     */
    private void paintBackground(Graphics2D g2)
    {
        g2.setPaint(this.LINE_COLOR);
        CanvasUtility.paintGrid(g2, LINE_COLOR, this);
    }

   
    /**
     * Agrega el tipo de celda correcto según el botón del mouse que se use
     * @param x
     * @param y
     * @param mouseButtonClicked 
     */
    private void addCell(int x, int y, int mouseButtonClicked)
    {
        if (x > GRID_ORIGIN_X && x < corGridWidth && y > GRID_ORIGIN_Y && y < corGridHeight)
        {
            switch (mouseButtonClicked)
            {
                case MouseEvent.BUTTON1: //Celda normal del patrón
                {
                    if (CanvasUtility.isThisShapeNew(x, y, this.shapeColorMap))
                    {
                        this.addShapeToMap(x, y, mouseButtonClicked);
                    }
                    else
                    {
                        CanvasUtility.removeShapeFromMap(x, y, this.shapeColorMap, colors[MouseEvent.BUTTON3]);
                    }
                    break;
                }

                case MouseEvent.BUTTON3: //Celda Raíz del patrón
                {
                    this.updateRadix(x, y, mouseButtonClicked);
                    break;
                }
            }
            this.updateNeighborhood();
        }
    }

    /**
     * Revisa el mapa de shapes y genera el patrón del vecindario.
     */
    private void updateNeighborhood()
    {
        List<Cell> cells = new ArrayList<>();

        this.shapeColorMap.entrySet().forEach((e) ->
        {
            Shape key = e.getKey();
            Color value = e.getValue();

            if (value.equals(colors[MouseEvent.BUTTON3])) //Actualiza la raíz
            {
                MainWindowMaker.currentCA.getNeighborhood().setRadix(this.getCellFromShape(key));
            }
            else
            {
                if (value.equals(colors[MouseEvent.BUTTON1]))
                {
                    cells.add(this.getCellFromShape(key));
                }
            }
        });

        MainWindowMaker.currentCA.getNeighborhood().setCellsBaseList(cells); //Actualiza el vecindario
    }


    /**
     * Actualiza la posición de la raíz
     *
     * @param x
     * @param y
     * @param mouseButtonClicked
     */
    private void updateRadix(int x, int y, int mouseButtonClicked)
    {
        this.transformsFormerRadix();
        CanvasUtility.removeShapeFromMap(x, y, this.shapeColorMap, colors[MouseEvent.BUTTON3]);
        this.addShapeToMap(x, y, mouseButtonClicked);
    }

    /**
     * Busca la celda de la raíz anterior y la marca como normal.
     */
    private void transformsFormerRadix()
    {
        for (Map.Entry<Shape, Color> e : shapeColorMap.entrySet())
        {
            Shape key = e.getKey();
            Color value = e.getValue();

            if (value.equals(colors[MouseEvent.BUTTON3]))
            {
                this.shapeColorMap.put(key, colors[MouseEvent.BUTTON1]);
                break;
            }
        }
    }
    
    /**
     * Agrega la shape (X,Y) al mapa
     *
     * @param x
     * @param y
     * @param mouseButtonClicked
     */
    private void addShapeToMap(int x, int y, int mouseButtonClicked)
    {
        Shape shape = this.createCellShape(x, y);
        this.shapeColorMap.put(shape, colors[mouseButtonClicked]);
    }
   

    /**
     * Crea el shape de la celda que corresponde a la posición del click
     *
     * @param x1 coordenada centerX del mouse
     * @param y1 coordenada centerY del mouse
     * @return
     */
    private Rectangle createCellShape(int x1, int y1)
    {
        int xo;
        int yo;

        xo = (x1 - GRID_ORIGIN_X) / CELL_WIDTH;
        xo = xo * CELL_WIDTH + GRID_ORIGIN_X;
        yo = (y1 - GRID_ORIGIN_Y) / CELL_HEIGHT;
        yo = yo * CELL_HEIGHT + GRID_ORIGIN_Y;

        return new Rectangle(xo, yo, CELL_WIDTH, CELL_HEIGHT);
    }

    /**
     * Llena la lista de celdas a dibujar a partir del localAutomaton.
     *
     */
    public void drawNeighborhood()
    {
        Short id = 0;
        Cell center = new Cell(centerX, centerY, id);
        Cell radix = MainWindowMaker.currentCA.getNeighborhood().getRadix();
        List<Cell> cList = Neighborhood.normalize(center, MainWindowMaker.currentCA.getNeighborhood().getCellsBaseList());
        
        if (radix != null && cList != null)
        {
            
            cList.stream().map((c) -> getMatrixCoordinates(c.getColumn(), c.getRow())).forEachOrdered((cellShape) ->
            {
                this.shapeColorMap.put(cellShape, colors[MouseEvent.BUTTON1]);
            });
            
            Shape radixShape = getMatrixCoordinates(radix.getColumn() + center.getColumn(), radix.getRow() + center.getRow());
            this.shapeColorMap.put(radixShape, colors[MouseEvent.BUTTON3]);
        }
        else
        {
            SacUtility.showWarning("Error inesperado. No se pudo dibujar el vecindario.");
        }
    }

    /**
     * Transforma las coordenadas de la lista del vecindario a coordenadas en
     * pixeles.
     *
     * @param matrixX
     * @param matrixY
     * @return
     */
    private Shape getMatrixCoordinates(int matrixX, int matrixY)
    {
        int gridX = matrixX * CELL_WIDTH + GRID_ORIGIN_X;
        int gridY = matrixY * CELL_HEIGHT + GRID_ORIGIN_Y;

        return new Rectangle(gridX, gridY, CELL_WIDTH, CELL_HEIGHT);
    }

    /**
     * Convierte un shape en Cell para agregar al vecindario
     *
     * @param cellShape
     * @return
     */
    private Cell getCellFromShape(Shape cellShape)
    {
        int shapeX = (int) cellShape.getBounds().getX();
        int shapeY = (int) cellShape.getBounds().getY();

        int neighX = ((shapeX - GRID_ORIGIN_X) / CELL_WIDTH) - centerX;
        int neighY = ((shapeY - GRID_ORIGIN_Y) / CELL_HEIGHT) - centerY;

        return new Cell(neighY, neighX, Neighborhood.DEFAULT_RADIX.getStateId());
    }

    /**
     * Limpia el hashmap.
     */
    @Override
    public void clear()
    {
        this.shapeColorMap.clear();
    }

    
    @Override
    public int getGridOriginX()
    {
        return CanvasNeighborhood.GRID_ORIGIN_X;
    }

    @Override
    public int getGridOriginY()
    {
        return CanvasNeighborhood.GRID_ORIGIN_Y;
    }

    @Override
    public int getGridHeight()
    {
        return this.corGridHeight;
    }

    @Override
    public int getGridWidth()
    {
        return this.corGridWidth;
    }

    @Override
    public int getCellHeight()
    {
        return CanvasNeighborhood.CELL_HEIGHT;
    }

    @Override
    public int getCellWidth()
    {
        return CanvasNeighborhood.CELL_WIDTH;
    }

    @Override
    public void fillPatternMatrix()
    {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public CustomPattern getCurrentPattern()
    {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public State getSelectedState()
    {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void packPattern()
    {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void setCurrentPattern(CustomPattern pattern)
    {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void setSelectedState(State selectedState)
    {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public int getRotation()
    {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void setRotation(int rotation)
    {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public boolean getFlipVertical()
    {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void setFlipVertical(boolean set)
    {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public boolean getFlipHorizontal()
    {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void setFlipHorizontal(boolean set)
    {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
    
    
}
