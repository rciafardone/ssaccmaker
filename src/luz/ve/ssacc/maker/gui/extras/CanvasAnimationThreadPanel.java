package luz.ve.ssacc.maker.gui.extras;

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Esta clase crea el panel donde se anima el autómata
 * @author Renzo Ciafardone Sciannelli
 */
import luz.ve.ssacc.maker.gui.WindowAnimation;
import java.awt.Color;
import java.awt.Graphics;
import java.awt.Toolkit;
import javax.swing.JPanel;
import java.awt.Dimension;
import java.awt.Graphics2D;
import java.awt.Shape;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.util.HashMap;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import luz.ve.ssacc.engine.components.Automaton;
import luz.ve.ssacc.engine.components.CustomPattern;
import luz.ve.ssacc.engine.components.State;
import luz.ve.ssacc.maker.gui.MainWindowMaker;
import luz.ve.ssacc.utilities.SacFileManager;
import luz.ve.ssacc.utilities.SacUtility;

public final class CanvasAnimationThreadPanel extends JPanel
    implements Runnable
{

    private Thread animator;
    private final Automaton cA;
    private WindowAnimation parentWindow;
    public Short[][] stepGrid;
    
    private Map<Shape, Color> shapeColorMap;

    private CustomPattern localPattern;
    private State selectedState;

    public volatile boolean play = true;
    public volatile boolean pause = true;
    public volatile boolean recordAnimation = false;
    public volatile boolean infinity = false;
    public volatile boolean reset = false;

    public CanvasAnimationThreadPanel(Automaton currentCA)
    {
        this.cA = currentCA;
        
        this.setShapeColorMap(new HashMap<>());
        this.setSelectedState(new State());
        this.setLocalPattern(new CustomPattern());
        
        initialConfig();
    }

    private void initialConfig()
    {
        int gWidth = (this.cA.getGrids().getColumns() * this.cA.getGrids().getCellSize()) + 1;
        int gHeight = (this.cA.getGrids().getRows() * this.cA.getGrids().getCellSize()) + 1;
        this.setBackground(this.cA.getGrids().getBackground().getColor());
        this.setPreferredSize(new Dimension(gWidth, gHeight));
        this.setLocation(0, 0);
        this.setDoubleBuffered(true);
    }

    /**
     * Activa y desactiva la pausa.
     * @param isSelected
     */
    public synchronized void pause(boolean isSelected) {
        try {
            this.pause = isSelected;
            if (!isSelected) {
                notify();
            }
        } catch (Exception ex) {
            SacUtility.showWarning(ex.getLocalizedMessage());
        }
    }

    public void setParentWindow(WindowAnimation animationFrame)
    {
        this.parentWindow = animationFrame;
    }

    /**
     * Establece el ColorMap inicial.
     *
     * @param shapeColorMap
     */
    private void setShapeColorMap(Map<Shape, Color> shapeColorMap)
    {
        this.shapeColorMap = shapeColorMap;
    }

    private void setSelectedState(State selectedState)
    {
        this.selectedState = selectedState;
    }

    private Map<Shape, Color> getShapeColorMap()
    {
        return shapeColorMap;
    }

    /**
     * Permite el acceso al localPattern.
     *
     * @return
     */
    public CustomPattern getLocalPattern()
    {
        return localPattern;
    }

    @Override
    public void addNotify()
    {
        super.addNotify();
        animator = new Thread(this);
        animator.start();
    }

    @Override
    public void paintComponent(Graphics canvas)
    {
        super.paintComponent(this.drawGrid(canvas));
    }

    /**
     * Actualiza el canvas con la información del grid.
     *
     * @param canvas
     * @return
     */
    private Graphics drawGrid(Graphics canvas)
    {
        if (this.stepGrid != null)
        {
            if (this.reset)
            {
                this.reset = false;
                CanvasAnimationThreadPanel.prepareGrid(canvas, cA, cA.getGrids().getGridOri().getArray()); //reset
            }
            CanvasAnimationThreadPanel.prepareGrid(canvas, cA, this.stepGrid); //Recibiendo el grid
        }
        Toolkit.getDefaultToolkit().sync();
        canvas.dispose();
        return canvas;
    }

    /**
     * Actualiza el canvas del grid en base al array de estados.
     *
     * @param canvas
     * @param currentAc
     * @param cellGrid
     */
    public static void prepareGrid(
        Graphics canvas,
        Automaton currentAc,
        Short[][] cellGrid)
    {
        Graphics2D canvas2D = (Graphics2D) canvas;

        for (int gridRow = 0; gridRow < currentAc.getGrids().getRows(); gridRow++)
        {
            for (int gridCol = 0; gridCol < currentAc.getGrids().getColumns(); gridCol++)
            {
                CanvasAnimationThreadPanel.drawCell(
                    canvas2D,
                    currentAc,
                    gridRow,
                    gridCol,
                    MainWindowMaker.currentCA.getStates().getState(cellGrid[gridRow][gridCol]).getColor());
            }
        }
    }

    /**
     * Función responsable por la evolución del autómata. Ademas crea los
     * archivos de estadistica (SACE) y animación (SACA).
     */
    @Override
    public void run()
    {

        int cStep; // contador de pasos.
        String runId = Automaton.generateNewId(); // Código único que identifica la corrida.
        String runPath = getRunPath(runId); // Ruta donde se van a guardar los archivos SACE y SACA.
        String runBaseFileName = runPath + MainWindowMaker.currentCA.getName() + "_" + runId; // Nombre básico de los archivos.
        StringBuilder dataStats = new StringBuilder(); // Para alamcenar las estadisticas de cada paso de toda la corrida.

        SacFileManager.createFolderSilently(runPath); // Crea el directorio para salvar los archivos de la corrida.

        this.stepGrid = cA.getGrids().getGridIni().getArray();
        
        repaint();
        
        cStep = 0; 
        while (this.play)
        {
            //TODO: debugging only System.out.println(Integer.toString(cStep));
            try
            {
                if (cStep >= cA.getGrids().getNumSteps() && this.infinity == false) // Limite de pasos
                {
                    break;
                }
                
                synchronized (this) // Para detener temporalmente la animación
                {
                    while (pause)
                    {
                        wait();
                    }
                }

                this.stepGrid = cA.step().getArray(); //"This is where the fun begins"

                dataStats.append(cA.getFormattedStatesStatsByStep(cStep));

                if (this.recordAnimation)
                {
                    this.writeSACAfile(runBaseFileName, cStep); // Salva archivo del paso.
                }

                cStep++;

                Thread.sleep(this.cA.getGrids().getAnimationDelay()); // Controla velocidad de animación
                repaint();
                
            } catch (InterruptedException e)
            {
                System.out.println("Interrupted: " + e.getMessage());
            } 
        }

        this.writeSACEfile(runBaseFileName, dataStats.toString()); // Salva archivo con las estadisticas
    }
    
    

    /**
     * Dibuja la celda del grid
     *
     * @param canvas Canvas donde se dibuja el grid
     * @param cA el AC que contiene todos los datos del grid
     * @param cellMatrixRow Fila de la celda a dibujar
     * @param cellMatrixCol Columna de la celda a dibujar
     * @param cellColor color de la celda
     */
    public static void drawCell(
        Graphics2D canvas,
        Automaton cA,
        int cellMatrixRow,
        int cellMatrixCol,
        Color cellColor)
    {
        int gridPixelRow = cA.getGrids().getCellSize() * cellMatrixRow; 
        int gridPixelCol = cA.getGrids().getCellSize() * cellMatrixCol; 

        canvas.setColor(cellColor);
        canvas.fillRect(gridPixelCol, gridPixelRow, cA.getGrids().getCellSize(), cA.getGrids().getCellSize());

        if (cA.getGrids().isBorderOn() && cA.getGrids().getColumns() > 1 && cA.getGrids().getRows() > 1)
        {
            canvas.setColor(cA.getGrids().getBorderColor());
            canvas.drawRect(gridPixelCol, gridPixelRow, cA.getGrids().getCellSize(), cA.getGrids().getCellSize());
        }
    }

    /**
     * Devuelve el directorio donde se almacena la corrida actual.
     *
     * @param runId
     * @return
     */
    private String getRunPath(String runId)
    {
        StringBuilder runPath = new StringBuilder();

        runPath.append(SacFileManager.SAC_PATHS[SacFileManager.SAC_ANIMATION]).append(SacFileManager.FILE_SEPARATOR); //Directorio de animaciones
        runPath.append(MainWindowMaker.currentCA.getName()).append(SacFileManager.FILE_SEPARATOR); // Directorio del automata
        runPath.append(MainWindowMaker.currentCA.getName()).append("_").append(runId).append(SacFileManager.FILE_SEPARATOR); //Directorio de la corrida
        
        return runPath.toString();
    }

    /**
     * Salva el archivo de estadísticas de las corridas.
     *
     * @param runBaseFileName
     * @param endingStats
     * @param dataStats
     */
    private void writeSACEfile(String runBaseFileName, String dataStats)
    {
        String endingStats = "." + SacFileManager.EXT_STATISTICS;
        String fullFileData = cA.getFormattedStatsHeader() + dataStats;
        
        try
        {
            Path statsFilePath = Paths.get(runBaseFileName + endingStats);
            Files.write(statsFilePath, fullFileData.getBytes(), StandardOpenOption.CREATE);
        } catch (IOException ex)
        {
            Logger.getLogger(CanvasAnimationThreadPanel.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Salva el archivo de la animación correspondiente al paso indicado
     * @param runBaseFileName
     * @param runId
     * @param cStep 
     */
    private void writeSACAfile(String runBaseFileName, int cStep)
    {
        try
        {
            String dataAnimation = cA.getGrids().getGridEnd().toFile(cStep, cA.getFormattedStepStatistics());
            String endingAnimation = "_" + cStep + "." + SacFileManager.EXT_ANIMATION;
            Path animationFile = Paths.get(runBaseFileName + endingAnimation);
            Files.write(animationFile, dataAnimation.getBytes(), StandardOpenOption.CREATE);
        } catch (IOException ex)
        {
            Logger.getLogger(CanvasAnimationThreadPanel.class.getName()).log(Level.SEVERE, null, ex);
        }
    }


    
    public void setLocalPattern(CustomPattern localPattern)
    {
        this.localPattern = localPattern;
    }

//******************************************************************************
//******************************************************************************
//******************************************************************************
}
