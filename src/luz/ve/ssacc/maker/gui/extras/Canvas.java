/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package luz.ve.ssacc.maker.gui.extras;

import java.awt.Graphics;
import luz.ve.ssacc.engine.components.CustomPattern;
import luz.ve.ssacc.engine.components.State;

/**
 *
 * @author Renzo Ciafardone Sciannelli <rciafardone@gmail.com>
 */
public interface Canvas
{

    /**
     * Borra el hashmap y limpia la lista de celdas del patrón.
     */
    void clear();

    /**
     * Llena la totalidad el mapa con el estado seleccionado.
     */
    void fillPatternMatrix();

    /**
     * Permite el acceso al currentPattern.
     *
     * @return
     */
    CustomPattern getCurrentPattern();

    /**
     *
     * @return
     */
    State getSelectedState();

    /**
     * Empaqueta el patrón
     */
    void packPattern();

    
    /**
     * Asigna el valor del patrón y dibuja
     *
     * @param pattern
     */
    void setCurrentPattern(CustomPattern pattern);

    /**
     * Asigna el valor al estado seleccionado.
     *
     * @param selectedState
     */
    void setSelectedState(State selectedState);

    /**
     * Devuelve el valor de la coordenada x del origen.
     *
     * @return
     */
    public int getGridOriginX();

    /**
     * Devuelve el valor de la coordenada y del origen.
     * @return 
     */
    public int getGridOriginY();

    /**
     * Devuelve la altura del grid en pixels.
     * @return 
     */
    public int getGridHeight();

    /**
     * Devuelve el ancho del grid en pixels.
     * @return 
     */
    public int getGridWidth();

    /**
     * devuelve el alto de la celda en pixels.
     * @return 
     */
    public int getCellHeight();

    /**
     * devuelve el ancho de la celda en pixels.
     * @return 
     */
    public int getCellWidth();
    
     /**
     * devuelve la rotacion de la figura.
     * @return 
     */
    public int getRotation();

    /**
     * Establece la rotacion de la figura.
     *
     * @param rotation
     */
    public void setRotation(int rotation);


     /**
     * devuelve si se activa el espejo vertical.
     * @return 
     */
    public boolean getFlipVertical();

    /**
     * Establece si se activa el espejo vertical.
     *
     * @param set
     */
    public void setFlipVertical(boolean set);
    
    /**
     * devuelve si se activa el espejo horizontal.
     * @return 
     */
    public boolean getFlipHorizontal();

    /**
     * Establece si se activa el espejo horizontal.
     *
     * @param set
     */
    public void setFlipHorizontal(boolean set);
}
