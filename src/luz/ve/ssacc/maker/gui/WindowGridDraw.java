/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package luz.ve.ssacc.maker.gui;

import javax.swing.DefaultComboBoxModel;
import luz.ve.ssacc.engine.components.CustomPattern;
import luz.ve.ssacc.engine.components.State;
import luz.ve.ssacc.maker.gui.extras.CanvasGrid;
import luz.ve.ssacc.maker.gui.extras.CanvasUtility;
import luz.ve.ssacc.maker.gui.extras.SacGuiManager;
import luz.ve.ssacc.utilities.SacUtility;

/**
 *
 * @author Renzo Ciafardone Sciannelli <rciafardone@gmail.com>
 */
public class WindowGridDraw extends javax.swing.JFrame
{
    CanvasGrid canvasGrid;
    WindowGrid parentWindow;
    CustomPattern[] patterns;
    boolean firstTimeflag = true;
    
    /**
     * Creates new form WindowGridDraw
     * @param wGrid
     */
    public WindowGridDraw(WindowGrid wGrid)
    {
        this.parentWindow = wGrid;
        this.canvasGrid = new CanvasGrid();
        this.initComponents();
        this.initialConfig();
    }

    /**
     * 
     */
    private void initialConfig()
    {
        this.load();
        this.jpGridCanvas.setSize(this.canvasGrid.getWidth(),this.canvasGrid.getHeight());
        this.jpGridCanvas.add(this.canvasGrid);
        this.jpGridCanvas.invalidate();
        this.jpGridCanvas.repaint();
        this.pack();
    }

    /**
     * 
     */
    public void load()
    {
        this.loadStatesGui();
        this.loadPatternsGui();
        this.canvasGrid.drawPixel = true;
    }
    

     /**
     * Carga los datos de los estados en el combobox y muestra los datos del
     * estado seleccionado.
     */
    private void loadStatesGui()
    {
        State bg = MainWindowMaker.currentCA.getGrids().getBackground();
        String bgText = "<html>Fondo: " + bg.getFormatedId() + "<br>" + bg.getText() + " " + bg.getValue().toString() + "</html>";

        this.cbStates.setModel(new DefaultComboBoxModel(MainWindowMaker.currentCA.getStates().getIdAsAStringArray()));
        this.pBackgroundStateColor.setBackground(bg.getColor());
        this.lBackground.setForeground(CanvasUtility.getContrastColor(bg.getColor()));
        this.lBackground.setText(bgText);
        this.paintCell();
        this.rbStates.setSelected(true);
    }

    private void loadPatternsGui()
    {
        this.patterns = SacGuiManager.loadPatterns();
        this.loadPatternsComboBox();
    }
    
    private void loadPatternsComboBox()
    {
        this.cbPatterns.removeAllItems();
        for (CustomPattern p : this.patterns)
        {
            this.cbPatterns.addItem(p.getName());
        }
    }

   
    
    /**
     * Carga los datos del estado seleccionado.
     */
    private void paintCell()
    {
        Short selectedId;
        State sState;
        String sText;
                
        this.rbStates.setSelected(true);
        this.canvasGrid.drawPixel = this.rbStates.isSelected();
        selectedId = Short.parseShort(this.cbStates.getSelectedItem().toString());
        this.canvasGrid.setSelectedState(MainWindowMaker.currentCA.getStates().getState(selectedId));
        
        sState = this.canvasGrid.getSelectedState();
        sText = "<html>Selección:"+ "<br>" + sState.getText() + " " + sState.getValue().toString() + "</html>";
        
        this.pSelectedStateColor.setBackground(sState.getColor());
        this.lStateText.setText(sText);
        
        
        this.lStateText.setForeground(CanvasUtility.getContrastColor(this.pSelectedStateColor.getBackground()));
        
    }

    /**
     * Activa el uso de patrones.
     */
    private void paintPattern()
    {
        if (cbPatterns.getItemCount() > 0)
        {
            if (this.firstTimeflag)
            {
                this.firstTimeflag = false;
            }
            else
            {
                this.rbPatterns.setSelected(true);
                this.canvasGrid.drawPixel = false;
            }
            this.canvasGrid.selectedPattern = this.patterns[this.cbPatterns.getSelectedIndex()];
        }
        else
        {
            this.paintCell();
        }
    }

    /**
     * Limpia la imagen del vecindario.
     */
    private void clearCanvasGrid()
    {
        this.canvasGrid.fillPatternMatrix(MainWindowMaker.currentCA.getGrids().getBackground());
        this.getContentPane().validate();
        this.getContentPane().repaint();
    }

    /**
     * Agrega estados al azar
     */
    private void addRandomPattern(Double density)
    {
        this.canvasGrid.fillWithRandomPattern(density);
        this.getContentPane().validate();
        this.getContentPane().repaint();
    }

    
    /**
     * Llena el patrón con el estado actual
     */
    private void fillCanvasGrid()
    {
        this.canvasGrid.fillPatternMatrix(this.canvasGrid.getSelectedState());
        this.getContentPane().validate();
        this.getContentPane().repaint();
    }
    
    
    private void close()
    {   
        this.canvasGrid = null;
        SacGuiManager.closeWindow(this, parentWindow);
    }
    
    
    /**
     * Carga los datos del patrón diseñado en el Grid del Autómata
     */
    private void acceptGrid()
    {
        MainWindowMaker.currentCA.getGrids().getGridOri().add(this.canvasGrid.getCurrentGridPattern());
        SacUtility.gridCopy(MainWindowMaker.currentCA.getGrids().getGridOri(), MainWindowMaker.currentCA.getGrids().getGridIni());
        this.close();
    }

    private void updateRotation(Integer stepRotation)
    {
        canvasGrid.setRotation(CanvasUtility.controlChangeRotation(stepRotation, canvasGrid.getRotation()));
        this.lRotation.setText(Integer.toString(canvasGrid.getRotation()));
    }
    
    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents()
    {

        rbGroupPaintTool = new javax.swing.ButtonGroup();
        pBackground = new javax.swing.JPanel();
        jpGridCanvas = new javax.swing.JPanel();
        pDrawTools = new javax.swing.JPanel();
        pStates = new javax.swing.JPanel();
        cbStates = new javax.swing.JComboBox<>();
        pStateColors = new javax.swing.JPanel();
        pSelectedStateColor = new javax.swing.JPanel();
        lStateText = new javax.swing.JLabel();
        pBackgroundStateColor = new javax.swing.JPanel();
        lBackground = new javax.swing.JLabel();
        rbStates = new javax.swing.JRadioButton();
        bFillImage = new javax.swing.JButton();
        pRandom = new javax.swing.JPanel();
        bRandom = new javax.swing.JButton();
        spinnerRandom = new javax.swing.JSpinner();
        bClearImage = new javax.swing.JButton();
        pControls = new javax.swing.JPanel();
        bAccept = new javax.swing.JButton();
        bCancel = new javax.swing.JButton();
        pPatterns = new javax.swing.JPanel();
        cbPatterns = new javax.swing.JComboBox<>();
        rbPatterns = new javax.swing.JRadioButton();
        pFlipVertical = new javax.swing.JPanel();
        lFlipVertical = new javax.swing.JLabel();
        cbFlipVertical = new javax.swing.JCheckBox();
        pFlipHorizontal = new javax.swing.JPanel();
        lFlipHorizontal = new javax.swing.JLabel();
        cbFlipHorizontal = new javax.swing.JCheckBox();
        jPanel3 = new javax.swing.JPanel();
        bRotateCW = new javax.swing.JButton();
        lRotation = new javax.swing.JLabel();
        bRotateCC = new javax.swing.JButton();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        setTitle("SSACC - DR");
        addWindowListener(new java.awt.event.WindowAdapter()
        {
            public void windowClosing(java.awt.event.WindowEvent evt)
            {
                formWindowClosing(evt);
            }
        });

        jpGridCanvas.setBorder(javax.swing.BorderFactory.createEtchedBorder());
        jpGridCanvas.setLayout(new javax.swing.BoxLayout(jpGridCanvas, javax.swing.BoxLayout.LINE_AXIS));

        pDrawTools.setBorder(javax.swing.BorderFactory.createEtchedBorder());

        pStates.setBorder(javax.swing.BorderFactory.createTitledBorder("Estados"));

        cbStates.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "Item 1", "Item 2", "Item 3", "Item 4" }));
        cbStates.addActionListener(new java.awt.event.ActionListener()
        {
            public void actionPerformed(java.awt.event.ActionEvent evt)
            {
                cbStatesActionPerformed(evt);
            }
        });

        pStateColors.setBorder(new javax.swing.border.LineBorder(new java.awt.Color(0, 0, 0), 1, true));
        pStateColors.setForeground(new java.awt.Color(206, 69, 69));
        pStateColors.setLayout(new java.awt.GridLayout(1, 0));

        pSelectedStateColor.setBackground(new java.awt.Color(239, 151, 64));
        pSelectedStateColor.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));
        pSelectedStateColor.setForeground(new java.awt.Color(206, 69, 69));
        pSelectedStateColor.setLayout(new java.awt.GridLayout(1, 0));

        lStateText.setFont(new java.awt.Font("Ubuntu", 1, 15)); // NOI18N
        lStateText.setForeground(new java.awt.Color(224, 19, 19));
        lStateText.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        lStateText.setText("text");
        pSelectedStateColor.add(lStateText);

        pStateColors.add(pSelectedStateColor);

        pBackgroundStateColor.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));
        pBackgroundStateColor.setLayout(new java.awt.GridLayout(1, 0));

        lBackground.setFont(new java.awt.Font("Ubuntu", 1, 15)); // NOI18N
        lBackground.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        lBackground.setText("FONDO");
        pBackgroundStateColor.add(lBackground);

        pStateColors.add(pBackgroundStateColor);

        rbGroupPaintTool.add(rbStates);
        rbStates.setSelected(true);
        rbStates.addActionListener(new java.awt.event.ActionListener()
        {
            public void actionPerformed(java.awt.event.ActionEvent evt)
            {
                rbStatesActionPerformed(evt);
            }
        });

        bFillImage.setIcon(new javax.swing.ImageIcon(getClass().getResource("/luz/ve/ssacc/maker/gui/images/SSACC - fill image icon.png"))); // NOI18N
        bFillImage.setToolTipText("Llenar retícula");
        bFillImage.addActionListener(new java.awt.event.ActionListener()
        {
            public void actionPerformed(java.awt.event.ActionEvent evt)
            {
                bFillImageActionPerformed(evt);
            }
        });

        pRandom.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));

        bRandom.setIcon(new javax.swing.ImageIcon(getClass().getResource("/luz/ve/ssacc/maker/gui/images/SSACC - random icon 20x20.png"))); // NOI18N
        bRandom.setToolTipText("Llenado aleatorio");
        bRandom.addActionListener(new java.awt.event.ActionListener()
        {
            public void actionPerformed(java.awt.event.ActionEvent evt)
            {
                bRandomActionPerformed(evt);
            }
        });

        spinnerRandom.setModel(new javax.swing.SpinnerNumberModel(0.0d, 0.0d, 100.0d, 0.1d));

        javax.swing.GroupLayout pRandomLayout = new javax.swing.GroupLayout(pRandom);
        pRandom.setLayout(pRandomLayout);
        pRandomLayout.setHorizontalGroup(
            pRandomLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, pRandomLayout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(spinnerRandom, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(bRandom)
                .addGap(18, 18, 18))
        );
        pRandomLayout.setVerticalGroup(
            pRandomLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, pRandomLayout.createSequentialGroup()
                .addContainerGap(13, Short.MAX_VALUE)
                .addGroup(pRandomLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(bRandom, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(spinnerRandom))
                .addContainerGap())
        );

        bClearImage.setIcon(new javax.swing.ImageIcon(getClass().getResource("/luz/ve/ssacc/maker/gui/images/SSACC - clear image icon.png"))); // NOI18N
        bClearImage.setToolTipText("Borrar todo");
        bClearImage.addActionListener(new java.awt.event.ActionListener()
        {
            public void actionPerformed(java.awt.event.ActionEvent evt)
            {
                bClearImageActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout pStatesLayout = new javax.swing.GroupLayout(pStates);
        pStates.setLayout(pStatesLayout);
        pStatesLayout.setHorizontalGroup(
            pStatesLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(pStatesLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(pStatesLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addGroup(pStatesLayout.createSequentialGroup()
                        .addComponent(pStateColors, javax.swing.GroupLayout.PREFERRED_SIZE, 245, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(pRandom, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(pStatesLayout.createSequentialGroup()
                        .addComponent(rbStates)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(cbStates, javax.swing.GroupLayout.PREFERRED_SIZE, 135, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(bFillImage)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(bClearImage)))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        pStatesLayout.setVerticalGroup(
            pStatesLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, pStatesLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(pStatesLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(rbStates)
                    .addComponent(cbStates, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(bFillImage)
                    .addComponent(bClearImage))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(pStatesLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(pStateColors, javax.swing.GroupLayout.PREFERRED_SIZE, 55, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGroup(pStatesLayout.createSequentialGroup()
                        .addGap(0, 0, Short.MAX_VALUE)
                        .addComponent(pRandom, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addContainerGap())
        );

        pStatesLayout.linkSize(javax.swing.SwingConstants.VERTICAL, new java.awt.Component[] {pRandom, pStateColors});

        pControls.setBorder(javax.swing.BorderFactory.createTitledBorder("Controles"));

        bAccept.setIcon(new javax.swing.ImageIcon(getClass().getResource("/luz/ve/ssacc/maker/gui/images/SSACC Ok 20x20.png"))); // NOI18N
        bAccept.addActionListener(new java.awt.event.ActionListener()
        {
            public void actionPerformed(java.awt.event.ActionEvent evt)
            {
                bAcceptActionPerformed(evt);
            }
        });

        bCancel.setIcon(new javax.swing.ImageIcon(getClass().getResource("/luz/ve/ssacc/maker/gui/images/SSACC - cancel 20x20.png"))); // NOI18N
        bCancel.addActionListener(new java.awt.event.ActionListener()
        {
            public void actionPerformed(java.awt.event.ActionEvent evt)
            {
                bCancelActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout pControlsLayout = new javax.swing.GroupLayout(pControls);
        pControls.setLayout(pControlsLayout);
        pControlsLayout.setHorizontalGroup(
            pControlsLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, pControlsLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(bCancel, javax.swing.GroupLayout.PREFERRED_SIZE, 76, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(bAccept, javax.swing.GroupLayout.PREFERRED_SIZE, 76, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        pControlsLayout.setVerticalGroup(
            pControlsLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(pControlsLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(pControlsLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(bCancel)
                    .addComponent(bAccept))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        pPatterns.setBorder(javax.swing.BorderFactory.createTitledBorder("Patrones"));

        cbPatterns.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "Item 1", "Item 2", "Item 3", "Item 4" }));
        cbPatterns.addActionListener(new java.awt.event.ActionListener()
        {
            public void actionPerformed(java.awt.event.ActionEvent evt)
            {
                cbPatternsActionPerformed(evt);
            }
        });

        rbGroupPaintTool.add(rbPatterns);
        rbPatterns.addActionListener(new java.awt.event.ActionListener()
        {
            public void actionPerformed(java.awt.event.ActionEvent evt)
            {
                rbPatternsActionPerformed(evt);
            }
        });

        pFlipVertical.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));

        lFlipVertical.setIcon(new javax.swing.ImageIcon(getClass().getResource("/luz/ve/ssacc/maker/gui/images/SSACC - flip vertical 20x20.png"))); // NOI18N

        cbFlipVertical.addActionListener(new java.awt.event.ActionListener()
        {
            public void actionPerformed(java.awt.event.ActionEvent evt)
            {
                cbFlipVerticalActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout pFlipVerticalLayout = new javax.swing.GroupLayout(pFlipVertical);
        pFlipVertical.setLayout(pFlipVerticalLayout);
        pFlipVerticalLayout.setHorizontalGroup(
            pFlipVerticalLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(pFlipVerticalLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(lFlipVertical)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 13, Short.MAX_VALUE)
                .addComponent(cbFlipVertical)
                .addContainerGap())
        );

        pFlipVerticalLayout.linkSize(javax.swing.SwingConstants.HORIZONTAL, new java.awt.Component[] {cbFlipVertical, lFlipVertical});

        pFlipVerticalLayout.setVerticalGroup(
            pFlipVerticalLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, pFlipVerticalLayout.createSequentialGroup()
                .addContainerGap(20, Short.MAX_VALUE)
                .addGroup(pFlipVerticalLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(lFlipVertical)
                    .addComponent(cbFlipVertical))
                .addContainerGap())
        );

        pFlipVerticalLayout.linkSize(javax.swing.SwingConstants.VERTICAL, new java.awt.Component[] {cbFlipVertical, lFlipVertical});

        pFlipHorizontal.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));

        lFlipHorizontal.setIcon(new javax.swing.ImageIcon(getClass().getResource("/luz/ve/ssacc/maker/gui/images/SSACC - flip horizontal 20x20.png"))); // NOI18N
        lFlipHorizontal.setToolTipText("");

        cbFlipHorizontal.addActionListener(new java.awt.event.ActionListener()
        {
            public void actionPerformed(java.awt.event.ActionEvent evt)
            {
                cbFlipHorizontalActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout pFlipHorizontalLayout = new javax.swing.GroupLayout(pFlipHorizontal);
        pFlipHorizontal.setLayout(pFlipHorizontalLayout);
        pFlipHorizontalLayout.setHorizontalGroup(
            pFlipHorizontalLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(pFlipHorizontalLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(lFlipHorizontal)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 14, Short.MAX_VALUE)
                .addComponent(cbFlipHorizontal)
                .addContainerGap())
        );
        pFlipHorizontalLayout.setVerticalGroup(
            pFlipHorizontalLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, pFlipHorizontalLayout.createSequentialGroup()
                .addContainerGap(21, Short.MAX_VALUE)
                .addGroup(pFlipHorizontalLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(lFlipHorizontal)
                    .addComponent(cbFlipHorizontal))
                .addContainerGap())
        );

        jPanel3.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));

        bRotateCW.setIcon(new javax.swing.ImageIcon(getClass().getResource("/luz/ve/ssacc/maker/gui/images/SSACC - rotate cw.jpg"))); // NOI18N
        bRotateCW.addActionListener(new java.awt.event.ActionListener()
        {
            public void actionPerformed(java.awt.event.ActionEvent evt)
            {
                bRotateCWActionPerformed(evt);
            }
        });

        lRotation.setFont(new java.awt.Font("Ubuntu", 1, 14)); // NOI18N
        lRotation.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        lRotation.setText("000");
        lRotation.setHorizontalTextPosition(javax.swing.SwingConstants.RIGHT);

        bRotateCC.setIcon(new javax.swing.ImageIcon(getClass().getResource("/luz/ve/ssacc/maker/gui/images/SSACC - rotate cc.jpg"))); // NOI18N
        bRotateCC.addActionListener(new java.awt.event.ActionListener()
        {
            public void actionPerformed(java.awt.event.ActionEvent evt)
            {
                bRotateCCActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel3Layout = new javax.swing.GroupLayout(jPanel3);
        jPanel3.setLayout(jPanel3Layout);
        jPanel3Layout.setHorizontalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel3Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(bRotateCW)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(lRotation, javax.swing.GroupLayout.PREFERRED_SIZE, 32, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(bRotateCC)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        jPanel3Layout.setVerticalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel3Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(bRotateCW, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(bRotateCC, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(lRotation, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addContainerGap(12, Short.MAX_VALUE))
        );

        javax.swing.GroupLayout pPatternsLayout = new javax.swing.GroupLayout(pPatterns);
        pPatterns.setLayout(pPatternsLayout);
        pPatternsLayout.setHorizontalGroup(
            pPatternsLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(pPatternsLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(pPatternsLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(pPatternsLayout.createSequentialGroup()
                        .addComponent(rbPatterns)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(cbPatterns, javax.swing.GroupLayout.PREFERRED_SIZE, 294, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(0, 0, Short.MAX_VALUE))
                    .addGroup(pPatternsLayout.createSequentialGroup()
                        .addComponent(pFlipHorizontal, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(pFlipVertical, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(jPanel3, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        pPatternsLayout.linkSize(javax.swing.SwingConstants.HORIZONTAL, new java.awt.Component[] {pFlipHorizontal, pFlipVertical});

        pPatternsLayout.setVerticalGroup(
            pPatternsLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, pPatternsLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(pPatternsLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(cbPatterns, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(rbPatterns))
                .addGap(18, 18, 18)
                .addGroup(pPatternsLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(pFlipHorizontal, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(pFlipVertical, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jPanel3, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        pPatternsLayout.linkSize(javax.swing.SwingConstants.VERTICAL, new java.awt.Component[] {jPanel3, pFlipHorizontal, pFlipVertical});

        javax.swing.GroupLayout pDrawToolsLayout = new javax.swing.GroupLayout(pDrawTools);
        pDrawTools.setLayout(pDrawToolsLayout);
        pDrawToolsLayout.setHorizontalGroup(
            pDrawToolsLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(pDrawToolsLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(pStates, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGap(18, 18, 18)
                .addComponent(pPatterns, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addComponent(pControls, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
        );
        pDrawToolsLayout.setVerticalGroup(
            pDrawToolsLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, pDrawToolsLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(pDrawToolsLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(pControls, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addGroup(pDrawToolsLayout.createSequentialGroup()
                        .addGap(0, 0, Short.MAX_VALUE)
                        .addComponent(pStates, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addComponent(pPatterns, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addGap(7, 7, 7))
        );

        javax.swing.GroupLayout pBackgroundLayout = new javax.swing.GroupLayout(pBackground);
        pBackground.setLayout(pBackgroundLayout);
        pBackgroundLayout.setHorizontalGroup(
            pBackgroundLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(pBackgroundLayout.createSequentialGroup()
                .addComponent(pDrawTools, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(53, Short.MAX_VALUE))
            .addComponent(jpGridCanvas, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        pBackgroundLayout.setVerticalGroup(
            pBackgroundLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, pBackgroundLayout.createSequentialGroup()
                .addComponent(jpGridCanvas, javax.swing.GroupLayout.DEFAULT_SIZE, 433, Short.MAX_VALUE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(pDrawTools, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(pBackground, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(pBackground, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void formWindowClosing(java.awt.event.WindowEvent evt)//GEN-FIRST:event_formWindowClosing
    {//GEN-HEADEREND:event_formWindowClosing
        this.close();
    }//GEN-LAST:event_formWindowClosing

    private void cbStatesActionPerformed(java.awt.event.ActionEvent evt)//GEN-FIRST:event_cbStatesActionPerformed
    {//GEN-HEADEREND:event_cbStatesActionPerformed
        this.paintCell();
    }//GEN-LAST:event_cbStatesActionPerformed

    private void cbPatternsActionPerformed(java.awt.event.ActionEvent evt)//GEN-FIRST:event_cbPatternsActionPerformed
    {//GEN-HEADEREND:event_cbPatternsActionPerformed
        this.paintPattern();
    }//GEN-LAST:event_cbPatternsActionPerformed

    private void bFillImageActionPerformed(java.awt.event.ActionEvent evt)//GEN-FIRST:event_bFillImageActionPerformed
    {//GEN-HEADEREND:event_bFillImageActionPerformed
        this.fillCanvasGrid();
    }//GEN-LAST:event_bFillImageActionPerformed

    private void bClearImageActionPerformed(java.awt.event.ActionEvent evt)//GEN-FIRST:event_bClearImageActionPerformed
    {//GEN-HEADEREND:event_bClearImageActionPerformed
        this.clearCanvasGrid();
    }//GEN-LAST:event_bClearImageActionPerformed

    private void bAcceptActionPerformed(java.awt.event.ActionEvent evt)//GEN-FIRST:event_bAcceptActionPerformed
    {//GEN-HEADEREND:event_bAcceptActionPerformed
        this.acceptGrid();
    }//GEN-LAST:event_bAcceptActionPerformed

    private void bCancelActionPerformed(java.awt.event.ActionEvent evt)//GEN-FIRST:event_bCancelActionPerformed
    {//GEN-HEADEREND:event_bCancelActionPerformed
        this.close();
    }//GEN-LAST:event_bCancelActionPerformed

    private void rbStatesActionPerformed(java.awt.event.ActionEvent evt)//GEN-FIRST:event_rbStatesActionPerformed
    {//GEN-HEADEREND:event_rbStatesActionPerformed
        this.paintCell();
    }//GEN-LAST:event_rbStatesActionPerformed

    private void rbPatternsActionPerformed(java.awt.event.ActionEvent evt)//GEN-FIRST:event_rbPatternsActionPerformed
    {//GEN-HEADEREND:event_rbPatternsActionPerformed
        this.paintPattern();
    }//GEN-LAST:event_rbPatternsActionPerformed

    private void bRandomActionPerformed(java.awt.event.ActionEvent evt)//GEN-FIRST:event_bRandomActionPerformed
    {//GEN-HEADEREND:event_bRandomActionPerformed
        this.addRandomPattern((Double)spinnerRandom.getValue());
    }//GEN-LAST:event_bRandomActionPerformed

    private void cbFlipHorizontalActionPerformed(java.awt.event.ActionEvent evt)//GEN-FIRST:event_cbFlipHorizontalActionPerformed
    {//GEN-HEADEREND:event_cbFlipHorizontalActionPerformed
        canvasGrid.setFlipHorizontal(this.cbFlipHorizontal.isSelected());
    }//GEN-LAST:event_cbFlipHorizontalActionPerformed

    private void cbFlipVerticalActionPerformed(java.awt.event.ActionEvent evt)//GEN-FIRST:event_cbFlipVerticalActionPerformed
    {//GEN-HEADEREND:event_cbFlipVerticalActionPerformed
        canvasGrid.setFlipVertical(this.cbFlipVertical.isSelected());
    }//GEN-LAST:event_cbFlipVerticalActionPerformed

    private void bRotateCCActionPerformed(java.awt.event.ActionEvent evt)//GEN-FIRST:event_bRotateCCActionPerformed
    {//GEN-HEADEREND:event_bRotateCCActionPerformed
        this.updateRotation(90);
    }//GEN-LAST:event_bRotateCCActionPerformed

    private void bRotateCWActionPerformed(java.awt.event.ActionEvent evt)//GEN-FIRST:event_bRotateCWActionPerformed
    {//GEN-HEADEREND:event_bRotateCWActionPerformed
        this.updateRotation(-90);
    }//GEN-LAST:event_bRotateCWActionPerformed


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton bAccept;
    private javax.swing.JButton bCancel;
    private javax.swing.JButton bClearImage;
    private javax.swing.JButton bFillImage;
    private javax.swing.JButton bRandom;
    private javax.swing.JButton bRotateCC;
    private javax.swing.JButton bRotateCW;
    private javax.swing.JCheckBox cbFlipHorizontal;
    private javax.swing.JCheckBox cbFlipVertical;
    private javax.swing.JComboBox<String> cbPatterns;
    private javax.swing.JComboBox<String> cbStates;
    private javax.swing.JPanel jPanel3;
    private javax.swing.JPanel jpGridCanvas;
    private javax.swing.JLabel lBackground;
    private javax.swing.JLabel lFlipHorizontal;
    private javax.swing.JLabel lFlipVertical;
    private javax.swing.JLabel lRotation;
    private javax.swing.JLabel lStateText;
    private javax.swing.JPanel pBackground;
    private javax.swing.JPanel pBackgroundStateColor;
    private javax.swing.JPanel pControls;
    private javax.swing.JPanel pDrawTools;
    private javax.swing.JPanel pFlipHorizontal;
    private javax.swing.JPanel pFlipVertical;
    private javax.swing.JPanel pPatterns;
    private javax.swing.JPanel pRandom;
    private javax.swing.JPanel pSelectedStateColor;
    private javax.swing.JPanel pStateColors;
    private javax.swing.JPanel pStates;
    private javax.swing.ButtonGroup rbGroupPaintTool;
    private javax.swing.JRadioButton rbPatterns;
    private javax.swing.JRadioButton rbStates;
    private javax.swing.JSpinner spinnerRandom;
    // End of variables declaration//GEN-END:variables
}
