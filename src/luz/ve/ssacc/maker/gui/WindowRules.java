/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package luz.ve.ssacc.maker.gui;

import java.util.ArrayList;
import java.util.List;
import javax.swing.DefaultComboBoxModel;
import javax.swing.DefaultListModel;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JRadioButton;
import javax.swing.ListSelectionModel;
import javax.swing.WindowConstants;
import luz.ve.ssacc.engine.components.Automaton;
import luz.ve.ssacc.engine.components.Condition;
import luz.ve.ssacc.engine.components.Operand;
import luz.ve.ssacc.engine.components.Operator;
import luz.ve.ssacc.engine.components.Rule;
import luz.ve.ssacc.engine.components.Rules;
import luz.ve.ssacc.engine.components.State;
import luz.ve.ssacc.engine.functions.Function;
import luz.ve.ssacc.engine.parsers.FunctionParser;
import luz.ve.ssacc.engine.parsers.OperatorParser;
import luz.ve.ssacc.engine.parsers.RulesParser;
import luz.ve.ssacc.maker.gui.extras.SacGuiManager;
import luz.ve.ssacc.utilities.SacFileManager;
import luz.ve.ssacc.utilities.SacUtility;

/**
 *
 * @author Renzo Ciafardone Sciannelli <rciafardone@gmail.com>
 */
public class WindowRules extends javax.swing.JFrame
{

    MainWindowMaker parentWindow;
    
    Rule selectedRule;
    Condition selectedCondition;
    
    DefaultListModel rulesListModel;
    DefaultListModel conditionsListModel;
    
    JRadioButton[] rbListOperandA;
    JRadioButton[] rbListOperandB;

    /**
     * Creates new form testRule
     *
     * @param mainWindow
     */
    public WindowRules(MainWindowMaker mainWindow)
    {
        this.conditionsListModel = new DefaultListModel();
        this.rulesListModel = new DefaultListModel();
        parentWindow = mainWindow;
        initComponents();
        this.configGUI();
    }

    /**
     * Configura detalles de la ventana
     */
    private void configGUI()
    {
        this.setDefaultCloseOperation(WindowConstants.HIDE_ON_CLOSE);

        this.fillRulesListModel();
        this.fillConditionsListModel(MainWindowMaker.currentCA.getRules().getList().get(0));

        this.rulesList.setModel(this.rulesListModel);
        this.conditionsList.setModel(this.conditionsListModel);

        this.rulesList.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
        this.conditionsList.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);

        this.updateGui();
    }

    public void updateGui()
    {
        this.loadComboBoxes();
        this.initRadioButtonList();
    }

    /**
     * Carga todas las reglas en el modelo de reglas.
     */
    private DefaultListModel fillRulesListModel()
    {
        int selectedRuleIndex = this.rulesList.getSelectedIndex();
        this.rulesListModel.clear();
        for (Rule rule : MainWindowMaker.currentCA.getRules().getList())
        {
            rulesListModel.addElement(rule);
        }
    this.rulesList.setSelectedIndex(selectedRuleIndex);
    return this.rulesListModel;
    }

    /**
     * Carga todas las condiciones de la regla indicada
     *
     * @param ruleIndex
     */
    private DefaultListModel fillConditionsListModel(Rule rule)
    {
        this.conditionsListModel.clear();
        for (Condition cond : rule.getConditions())
        {
            conditionsListModel.addElement(cond);
        }
    
        return this.conditionsListModel;
    }


   

    private void initRadioButtonList()
    {
        this.rbListOperandA = new JRadioButton[]
        {
            rbFieldIdA, rbFieldTextA, rbFieldValueA, rbFieldColorA
        };
        this.rbListOperandB = new JRadioButton[]
        {
            rbFieldIdB, rbFieldTextB, rbFieldValueB, rbFieldColorB
        };
    }
       
    
    /**
     * Llena los campos del dialogo de las reglas con todos los datos
     * correspondientes
     *
     * @param selectedFile
     * @param editOption
     * @return
     */
    public Automaton load(int editOption, String selectedFile)
    {
        if (MainWindowMaker.currentCA.getStates().getList().size() > 0)
        {
            switch (editOption)
            {
                case SacUtility.LOAD_FILE:
                {
                    selectedFile = SacFileManager.getSacFileName(this, SacFileManager.SAC_RULES);
                    if (selectedFile == null)
                    {
                        return null;
                    }

                    MainWindowMaker.currentCA.setRules(RulesParser.parse(SacFileManager.readSacFile(selectedFile)));

                    break;
                }
                case SacUtility.ADD_NEW:
                {
                    MainWindowMaker.currentCA.setRules(new Rules());
                    break;
                }
                case SacUtility.EDIT_CURRENT:
                {
                    if (MainWindowMaker.currentCA.getRules() == null)
                    {
                        MainWindowMaker.currentCA.setRules(new Rules());
                    }
                    break;
                }
            }
            this.loadRulesGUI();
            return MainWindowMaker.currentCA;
        }
        else
        {
            SacUtility.showWarning("No existe un conjunto de estados definido en el autómata para crear reglas.");
            return null;
        }
    }

    private void updateRuleStatesData(int stateCombo)
    {
        if (stateCombo == 0)
        {
            this.selectedRule.setStateIniId(Short.parseShort(this.cbStateIni.getSelectedItem().toString()));
        }
        if (stateCombo == 1)
        {
            this.selectedRule.setStateEndId(Short.parseShort(this.cbStateEnd.getSelectedItem().toString()));
        }
        this.rulesList.setModel(this.fillRulesListModel());
    }


    /**
     * Carga los datos de las reglas en la GUI
     *
     * @param currentCA Autómata Celular en uso.
     */
    private void loadRulesGUI()
    {
        if (MainWindowMaker.currentCA.getRules() != null)
        {
            this.loadComboBoxes();
            
            this.tfRulesName.setText(MainWindowMaker.currentCA.getRules().getName());
            this.taRulesDescription.setText(MainWindowMaker.currentCA.getRules().getDescription());

            this.fillRulesListModel();
            this.fillConditionsListModel(MainWindowMaker.currentCA.getRules().getList().get(0));

            this.rulesList.setModel(this.rulesListModel);
            this.rulesList.setSelectedIndex(0);
            this.selectedRule = (Rule) this.rulesListModel.get(0);
            
            this.conditionsList.setModel(this.conditionsListModel);
            this.conditionsList.setSelectedIndex(0);
            this.selectedCondition = this.selectedRule.getConditions().get(0);

            this.lRulesNum.setText(Integer.toString(MainWindowMaker.currentCA.getRules().getList().size()));
            this.updateRuleGui((Rule)this.rulesListModel.get(this.rulesList.getSelectedIndex()));
        }
        else
        {
            SacUtility.showWarning("No existe un conjunto de reglas definido en el autómata");
        }
    }
    
    
    
    /**
     * Carga los datos de una regla en la GUI.
     * @param rule 
     */
    private void updateRuleGui(Rule rule)
    {
        this.lRuleId.setText(Integer.toString(rule.getId()));
        this.cbStateIni.setSelectedItem(Integer.toString(rule.getStateIniId()));
        this.cbStateEnd.setSelectedItem(Integer.toString(rule.getStateEndId()));
        this.lConditionsNum.setText(Integer.toString(rule.getConditions().size()));
        this.loadConditionsGui(rule);
    }
    
        
    /**
     * Carga los datos de las condiciones de la regla seleccionada en la GUI.
     * @param rule 
     */
    private void loadConditionsGui(Rule rule)
    {
        this.fillConditionsListModel(rule);
        this.conditionsList.setModel(this.conditionsListModel);
        this.conditionsList.setSelectedIndex(0);
        this.loadOperandsGui((Condition)this.conditionsListModel.get(0));
    }

    /**
     * Se cargan en los operands los datos de la condición seleccionada.
     */
    private void loadConditionGui()
    {
        int selectedConditionIndex = this.conditionsList.getSelectedIndex();
        int ruleId = Integer.parseInt(this.lRuleId.getText());
        selectedCondition = MainWindowMaker.currentCA.getRules().getRule(ruleId).getConditions().get(selectedConditionIndex);
        this.loadOperandsGui(selectedCondition);
        this.cbOperators.setSelectedItem(selectedCondition.getOperator().getSymbol());
    }

    /**
     * Llena los operands con la data de condición indicada
     * @param condition 
     */
    private void loadOperandsGui(Condition condition)
    {
        this.cbFunctionA.setSelectedItem(condition.getOperandA().getFunction().getName());
        this.cbVariableA.setSelectedItem(condition.getOperandA().getVariable());
        this.lIdA.setText(condition.getOperandA().getVariable().getId().toString());
        this.lTextA.setText(condition.getOperandA().getVariable().getText());
        this.lValueA.setText(condition.getOperandA().getVariable().getValue().toString());
        this.bColorA.setBackground(condition.getOperandA().getVariable().getColor());
        this.rbListOperandA[condition.getOperandA().getField()].setSelected(true);

        this.cbFunctionB.setSelectedItem(condition.getOperandB().getFunction().getName());
        this.cbVariableB.setSelectedItem(condition.getOperandB().getVariable());
        this.lIdB.setText(condition.getOperandB().getVariable().getId().toString());
        this.lTextB.setText(condition.getOperandB().getVariable().getText());
        this.lValueB.setText(condition.getOperandB().getVariable().getValue().toString());
        this.bColorB.setBackground(condition.getOperandB().getVariable().getColor());
        this.rbListOperandB[condition.getOperandB().getField()].setSelected(true);
        
        
   }
    
    
    
    /**
     * Agrega una regla nueva por default con el primer ID disponible.
     */
    private void addRule()
    {
        Rule rule;
        Short id = MainWindowMaker.currentCA.getRules().getNewRuleId();
        Short stateIni = MainWindowMaker.currentCA.getStates().getList().get(0).getId();
        Short stateEnd = MainWindowMaker.currentCA.getStates().getList().get(0).getId();
        List<Condition> conditions = new ArrayList<>();
        conditions.add(new Condition());
        rule = new Rule(id, stateIni, stateEnd, conditions);
        MainWindowMaker.currentCA.getRules().add(rule);
        MainWindowMaker.currentCA.getRules().sort();
        this.loadRulesGUI();
    }


    /**
     *
     * Elimina la regla seleccionada de la lista.
     */
    private void deleteRule()
    {
        int selectedIndex;

        if (MainWindowMaker.currentCA.getRules().getList().size() > 1)
        {

            if (this.rulesList.isSelectionEmpty())
            {
                this.rulesList.setSelectedIndex(this.rulesList.getLastVisibleIndex());
            }

            selectedIndex = this.rulesList.getSelectedIndex();
            this.selectedRule = (Rule)this.rulesListModel.get(selectedIndex);
            
            this.rulesListModel.remove(selectedIndex); 
            MainWindowMaker.currentCA.getRules().delete(selectedRule);
            
            this.lRulesNum.setText(Integer.toString(MainWindowMaker.currentCA.getRules().getList().size()));

            this.rulesList.setSelectedIndex(this.rulesList.getLastVisibleIndex());
            if ((selectedIndex - 1) >= 0)
            {
                this.rulesList.setSelectedIndex(selectedIndex - 1);
            }
            
            selectedIndex = this.rulesList.getSelectedIndex();
            this.selectedRule = MainWindowMaker.currentCA.getRules().getList().get(selectedIndex); //keep track of selected rule
            this.updateRuleGui(this.selectedRule);
            
        }
        else
        {
            SacUtility.showWarning("No se puede borrar la regla." + SacUtility.EOL + "El conjunto de reglas debe tener al menos un miembro.");
        }
    }

    
         
    
    private void save()
    {
        this.updateRulesHeaderData();
        SacFileManager.saveFile(MainWindowMaker.currentCA.getRules().getName(),
            MainWindowMaker.currentCA.getRules().toString(),
            SacFileManager.SAC_RULES,
            this);
    }

    
    /**
     * Crea una nueva instancia de Rules y la carga en el GUI.
     */
    private void reset()
    {
        MainWindowMaker.currentCA.setRules(new Rules());
        this.loadRulesGUI();
    }
    
      
    /**
     * Carga la información que es siempre la misma para todas las reglas:
     * Estados/Funciones/Operandores
     *
     * @param currentCA
     */
    private void loadComboBoxes()
    {
        if (MainWindowMaker.currentCA != null)
        {
            this.cbStateIni.setModel(new DefaultComboBoxModel(MainWindowMaker.currentCA.getStates().getIdAsAStringArray()));
            this.cbStateEnd.setModel(new DefaultComboBoxModel(MainWindowMaker.currentCA.getStates().getIdAsAStringArray()));

            this.cbFunctionA.setModel(new DefaultComboBoxModel(Function.FUNCTION_LIST));
            this.cbFunctionB.setModel(new DefaultComboBoxModel(Function.FUNCTION_LIST));

            this.cbVariableA.setModel(new DefaultComboBoxModel(MainWindowMaker.currentCA.getStates().getIdAsAStringArray()));
            this.cbVariableB.setModel(new DefaultComboBoxModel(MainWindowMaker.currentCA.getStates().getIdAsAStringArray()));

            this.cbOperators.setModel(new DefaultComboBoxModel(Operator.OPERATORS_LIST));
        }
    }

    private void updateSelectedRuleGui()
    {
        int tmpIndex = this.rulesList.getSelectedIndex();
        this.selectedRule = (Rule) this.rulesListModel.get(tmpIndex);
        this.conditionsList.setModel(this.fillConditionsListModel(this.selectedRule));
        this.conditionsList.setSelectedIndex(0);
        this.selectedCondition = this.selectedRule.getConditions().get(0);
        
        
        this.updateRuleGui(this.selectedRule);
        this.updateConditionGui(this.selectedCondition);
        this.rulesList.setSelectedIndex(tmpIndex);
    }

    /**
     * Actualiza los datos de las reglas tomandolos del GUI.
     */
    private void updateRulesHeaderData()
    {
        MainWindowMaker.currentCA.getRules().setName(this.tfRulesName.getText());
        MainWindowMaker.currentCA.getRules().setDescription(this.taRulesDescription.getText());
    }
    
    
    private void updateRulesHeaderGui(Rules rules)
    {
        this.tfRulesName.setText(rules.getName());
        this.taRulesDescription.setText(rules.getDescription());
    }
        
    
    
    private void updateConditionGui(Condition condition)
    {
        this.updateOperandAGui(condition);
        this.updateOperandBGui(condition);
        this.cbOperators.setSelectedItem(condition.getOperator().getSymbol());
    }

        
    private void updateOperandAGui(Condition condition)
    {
        this.cbFunctionA.setSelectedItem(condition.getOperandA().getFunction());
        this.cbVariableA.setSelectedItem(condition.getOperandA().getVariable());
        this.lIdA.setText(condition.getOperandA().getVariable().getId().toString());
        this.lTextA.setText(condition.getOperandA().getVariable().getText());
        this.lValueA.setText(condition.getOperandA().getVariable().getValue().toString());
        this.bColorA.setBackground(condition.getOperandA().getVariable().getColor());
        this.rbListOperandA[condition.getOperandA().getField()].setSelected(true);
    }

    private void updateOperandBGui(Condition condition)
    {
        this.cbFunctionB.setSelectedItem(condition.getOperandB().getFunction());
        this.cbVariableB.setSelectedItem(condition.getOperandB().getVariable());
        this.lIdB.setText(condition.getOperandB().getVariable().getId().toString());
        this.lTextB.setText(condition.getOperandB().getVariable().getText());
        this.lValueB.setText(condition.getOperandB().getVariable().getValue().toString());
        this.bColorB.setBackground(condition.getOperandB().getVariable().getColor());
        this.rbListOperandB[condition.getOperandB().getField()].setSelected(true);
    }


    
    /**
     * Llena los campos del GUI con los datos del estado cuando se cambia.
     *
     * @param nState
     */
    private void loadStateGUI(State nState,JLabel id, JLabel text, JLabel value, JButton color)
    {
        id.setText(nState.getFormatedId());
        value.setText(nState.getValue().toString());
        text.setText(nState.getText());
        color.setBackground(nState.getColor());
    }

    private void addCondition()
    {
        this.selectedRule.addCondition(new Condition());
        this.updateRuleGui(this.selectedRule);
    }

    
    private void deleteCondition()
    {
        int selectedIndex;

        if (this.selectedRule.getConditions().size() > 1)
        {

            if (this.conditionsList.isSelectionEmpty())
            {
                this.conditionsList.setSelectedIndex(this.conditionsList.getLastVisibleIndex());
            }

            selectedIndex = this.conditionsList.getSelectedIndex();
            this.selectedCondition = (Condition)this.conditionsListModel.get(selectedIndex);
            
            this.conditionsListModel.remove(selectedIndex); 
            this.selectedRule.getConditions().remove(this.selectedCondition);
            
            
            this.conditionsList.setSelectedIndex(this.rulesList.getLastVisibleIndex());
            
            if ((selectedIndex - 1) >= 0)
            {
                this.conditionsList.setSelectedIndex(selectedIndex - 1);
            }
            
            selectedIndex = this.conditionsList.getSelectedIndex();
            this.selectedCondition = this.selectedRule.getConditions().get(selectedIndex);
            this.updateRuleGui(this.selectedRule);
            
        }
        else
        {
            SacUtility.showWarning("No se puede borrar la condición." + SacUtility.EOL + "La regla debe tener al menos una condición.");
        }
    }
    
    
    
    private void changeFunction(JComboBox cb, int operandId)
    {
       
        String item = cb.getModel().getSelectedItem().toString();
        this.selectedCondition.getOperand(operandId).setFunction(FunctionParser.parse(item));
        this.conditionsList.setModel(this.fillConditionsListModel(this.selectedRule));
    }

    
    private void changeVariableState(JComboBox cb, 
                                     JLabel id, 
                                     JLabel text, 
                                     JLabel value, 
                                     JButton color, 
                                     int operandId)
    {
        Short stateId = Short.parseShort((String)cb.getSelectedItem());
        State currentState = MainWindowMaker.currentCA.getStates().getState(stateId); 
        this.loadStateGUI(currentState, id, text, value, color);
        this.selectedCondition.getOperand(operandId).setVariable(currentState);
        this.conditionsList.setModel(this.fillConditionsListModel(this.selectedRule));
    }

    private void changeOperator()
    {
        Operator operator=OperatorParser.parse((String) this.cbOperators.getSelectedItem());
        this.selectedCondition.setOperator(operator);
        this.conditionsList.setModel(this.fillConditionsListModel(this.selectedRule));
    }

    private void changeField(int field, int operandId)
    {
        this.selectedCondition.getOperand(operandId).setField(field);
        this.conditionsList.setModel(this.fillConditionsListModel(this.selectedRule));
    }
    

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents()
    {

        buttonGroupA = new javax.swing.ButtonGroup();
        buttonGroupB = new javax.swing.ButtonGroup();
        tabbedPaneRules = new javax.swing.JTabbedPane();
        pBackground = new javax.swing.JPanel();
        pName = new javax.swing.JPanel();
        bSave = new javax.swing.JButton();
        bReset = new javax.swing.JButton();
        tfRulesName = new javax.swing.JTextField();
        bRulesLoad = new javax.swing.JButton();
        pRules = new javax.swing.JPanel();
        jScrollPane1 = new javax.swing.JScrollPane();
        rulesList = new javax.swing.JList<>();
        bRuleAdd = new javax.swing.JButton();
        bRuleDel = new javax.swing.JButton();
        lRulesNumTitle = new javax.swing.JLabel();
        lRulesNum = new javax.swing.JLabel();
        pDescription = new javax.swing.JPanel();
        jScrollPane2 = new javax.swing.JScrollPane();
        taRulesDescription = new javax.swing.JTextArea();
        pRuleBackground = new javax.swing.JPanel();
        pRuleControls = new javax.swing.JPanel();
        lArrow = new javax.swing.JLabel();
        pStateIni = new javax.swing.JPanel();
        cbStateIni = new javax.swing.JComboBox<>();
        lInicio = new javax.swing.JLabel();
        pStateEnd = new javax.swing.JPanel();
        cbStateEnd = new javax.swing.JComboBox<>();
        lFinal = new javax.swing.JLabel();
        pConditions = new javax.swing.JPanel();
        jScrollPane3 = new javax.swing.JScrollPane();
        conditionsList = new javax.swing.JList<>();
        bConditionAdd = new javax.swing.JButton();
        bConditionDel = new javax.swing.JButton();
        pRuleTitle = new javax.swing.JPanel();
        lRuleTitle = new javax.swing.JLabel();
        lRuleId = new javax.swing.JLabel();
        jPanel1 = new javax.swing.JPanel();
        lConditionsNumTitle = new javax.swing.JLabel();
        lConditionsNum = new javax.swing.JLabel();
        jPanel2 = new javax.swing.JPanel();
        pOperandA = new javax.swing.JPanel();
        pFieldValueA = new javax.swing.JPanel();
        rbFieldTextA = new javax.swing.JRadioButton();
        lTextTitleA = new javax.swing.JLabel();
        lTextA = new javax.swing.JLabel();
        pFieldIdA = new javax.swing.JPanel();
        rbFieldIdA = new javax.swing.JRadioButton();
        lIdTitleA = new javax.swing.JLabel();
        lIdA = new javax.swing.JLabel();
        pFieldColorA = new javax.swing.JPanel();
        rbFieldColorA = new javax.swing.JRadioButton();
        bColorA = new javax.swing.JButton();
        pFieldTextA = new javax.swing.JPanel();
        rbFieldValueA = new javax.swing.JRadioButton();
        lValueTitleA = new javax.swing.JLabel();
        lValueA = new javax.swing.JLabel();
        pFunctionA = new javax.swing.JPanel();
        lFunctionA = new javax.swing.JLabel();
        cbFunctionA = new javax.swing.JComboBox<>();
        cbVariableA = new javax.swing.JComboBox<>();
        lVariableA = new javax.swing.JLabel();
        pOperandB = new javax.swing.JPanel();
        pFieldValueB = new javax.swing.JPanel();
        rbFieldTextB = new javax.swing.JRadioButton();
        lTextTitleB = new javax.swing.JLabel();
        lTextB = new javax.swing.JLabel();
        pFieldIdB = new javax.swing.JPanel();
        rbFieldIdB = new javax.swing.JRadioButton();
        lIdTitleB = new javax.swing.JLabel();
        lIdB = new javax.swing.JLabel();
        pFieldColorB = new javax.swing.JPanel();
        rbFieldColorB = new javax.swing.JRadioButton();
        bColorB = new javax.swing.JButton();
        pFieldTextB = new javax.swing.JPanel();
        rbFieldValueB = new javax.swing.JRadioButton();
        lValueTitleB = new javax.swing.JLabel();
        lValueB = new javax.swing.JLabel();
        pFunctionCon2 = new javax.swing.JPanel();
        lFunctionB = new javax.swing.JLabel();
        cbFunctionB = new javax.swing.JComboBox<>();
        cbVariableB = new javax.swing.JComboBox<>();
        lVariableB = new javax.swing.JLabel();
        jPanel3 = new javax.swing.JPanel();
        cbOperators = new javax.swing.JComboBox<>();

        setTitle("SSACC - Reglas");
        setResizable(false);
        addWindowListener(new java.awt.event.WindowAdapter()
        {
            public void windowClosing(java.awt.event.WindowEvent evt)
            {
                formWindowClosing(evt);
            }
        });

        tabbedPaneRules.addFocusListener(new java.awt.event.FocusAdapter()
        {
            public void focusGained(java.awt.event.FocusEvent evt)
            {
                tabbedPaneRulesFocusGained(evt);
            }
        });

        pName.setBorder(javax.swing.BorderFactory.createTitledBorder("Nombre"));

        bSave.setIcon(new javax.swing.ImageIcon(getClass().getResource("/luz/ve/ssacc/maker/gui/images/SSACC Ok 20x20.png"))); // NOI18N
        bSave.setToolTipText("Salvar archivo de reglas");
        bSave.addActionListener(new java.awt.event.ActionListener()
        {
            public void actionPerformed(java.awt.event.ActionEvent evt)
            {
                bSaveActionPerformed(evt);
            }
        });

        bReset.setIcon(new javax.swing.ImageIcon(getClass().getResource("/luz/ve/ssacc/maker/gui/images/SSACC - redo 20x20.png"))); // NOI18N
        bReset.setToolTipText("Reiniciar");
        bReset.addActionListener(new java.awt.event.ActionListener()
        {
            public void actionPerformed(java.awt.event.ActionEvent evt)
            {
                bResetActionPerformed(evt);
            }
        });

        bRulesLoad.setIcon(new javax.swing.ImageIcon(getClass().getResource("/luz/ve/ssacc/maker/gui/images/SSACC Open 20x20.png"))); // NOI18N
        bRulesLoad.setToolTipText("Cargar archivo de reglas");
        bRulesLoad.addActionListener(new java.awt.event.ActionListener()
        {
            public void actionPerformed(java.awt.event.ActionEvent evt)
            {
                bRulesLoadActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout pNameLayout = new javax.swing.GroupLayout(pName);
        pName.setLayout(pNameLayout);
        pNameLayout.setHorizontalGroup(
            pNameLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, pNameLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(tfRulesName, javax.swing.GroupLayout.PREFERRED_SIZE, 529, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 101, Short.MAX_VALUE)
                .addComponent(bSave)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(bReset)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(bRulesLoad, javax.swing.GroupLayout.PREFERRED_SIZE, 50, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
        );

        pNameLayout.linkSize(javax.swing.SwingConstants.HORIZONTAL, new java.awt.Component[] {bReset, bRulesLoad, bSave});

        pNameLayout.setVerticalGroup(
            pNameLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(pNameLayout.createSequentialGroup()
                .addGap(17, 17, 17)
                .addGroup(pNameLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(pNameLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                        .addComponent(bSave)
                        .addComponent(bReset)
                        .addComponent(bRulesLoad))
                    .addComponent(tfRulesName, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        pNameLayout.linkSize(javax.swing.SwingConstants.VERTICAL, new java.awt.Component[] {bReset, bRulesLoad, bSave});

        pRules.setBorder(javax.swing.BorderFactory.createTitledBorder("Reglas"));

        rulesList.setSelectionMode(javax.swing.ListSelectionModel.SINGLE_SELECTION);
        rulesList.addMouseListener(new java.awt.event.MouseAdapter()
        {
            public void mouseClicked(java.awt.event.MouseEvent evt)
            {
                rulesListMouseClicked(evt);
            }
        });
        jScrollPane1.setViewportView(rulesList);

        bRuleAdd.setIcon(new javax.swing.ImageIcon(getClass().getResource("/luz/ve/ssacc/maker/gui/images/SSACC Button Create 20x20.png"))); // NOI18N
        bRuleAdd.setToolTipText("Agregar regla");
        bRuleAdd.addActionListener(new java.awt.event.ActionListener()
        {
            public void actionPerformed(java.awt.event.ActionEvent evt)
            {
                bRuleAddActionPerformed(evt);
            }
        });

        bRuleDel.setIcon(new javax.swing.ImageIcon(getClass().getResource("/luz/ve/ssacc/maker/gui/images/SSACC Delete 20x20.png"))); // NOI18N
        bRuleDel.setToolTipText("Eliminar regla");
        bRuleDel.addActionListener(new java.awt.event.ActionListener()
        {
            public void actionPerformed(java.awt.event.ActionEvent evt)
            {
                bRuleDelActionPerformed(evt);
            }
        });

        lRulesNumTitle.setFont(new java.awt.Font("Ubuntu", 1, 18)); // NOI18N
        lRulesNumTitle.setText("Total Reglas:");

        lRulesNum.setFont(new java.awt.Font("Ubuntu", 1, 18)); // NOI18N
        lRulesNum.setText("00");

        javax.swing.GroupLayout pRulesLayout = new javax.swing.GroupLayout(pRules);
        pRules.setLayout(pRulesLayout);
        pRulesLayout.setHorizontalGroup(
            pRulesLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(pRulesLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(pRulesLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jScrollPane1)
                    .addGroup(pRulesLayout.createSequentialGroup()
                        .addComponent(bRuleAdd)
                        .addGap(18, 18, 18)
                        .addComponent(bRuleDel)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(lRulesNumTitle)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(lRulesNum)))
                .addContainerGap())
        );
        pRulesLayout.setVerticalGroup(
            pRulesLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(pRulesLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 188, Short.MAX_VALUE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(pRulesLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, pRulesLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(lRulesNumTitle)
                        .addComponent(lRulesNum))
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, pRulesLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addComponent(bRuleDel)
                        .addComponent(bRuleAdd)))
                .addContainerGap())
        );

        pDescription.setBorder(javax.swing.BorderFactory.createTitledBorder("Descripción"));

        taRulesDescription.setColumns(20);
        taRulesDescription.setRows(5);
        jScrollPane2.setViewportView(taRulesDescription);

        javax.swing.GroupLayout pDescriptionLayout = new javax.swing.GroupLayout(pDescription);
        pDescription.setLayout(pDescriptionLayout);
        pDescriptionLayout.setHorizontalGroup(
            pDescriptionLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(pDescriptionLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jScrollPane2)
                .addContainerGap())
        );
        pDescriptionLayout.setVerticalGroup(
            pDescriptionLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(pDescriptionLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jScrollPane2, javax.swing.GroupLayout.DEFAULT_SIZE, 133, Short.MAX_VALUE)
                .addContainerGap())
        );

        javax.swing.GroupLayout pBackgroundLayout = new javax.swing.GroupLayout(pBackground);
        pBackground.setLayout(pBackgroundLayout);
        pBackgroundLayout.setHorizontalGroup(
            pBackgroundLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(pBackgroundLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(pBackgroundLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(pRules, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addGroup(pBackgroundLayout.createSequentialGroup()
                        .addGroup(pBackgroundLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                            .addComponent(pDescription, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(pName, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                        .addGap(0, 88, Short.MAX_VALUE)))
                .addContainerGap())
        );
        pBackgroundLayout.setVerticalGroup(
            pBackgroundLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(pBackgroundLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(pName, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(pDescription, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(pRules, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(75, Short.MAX_VALUE))
        );

        tabbedPaneRules.addTab("Reglas", pBackground);

        pRuleBackground.setBorder(javax.swing.BorderFactory.createEtchedBorder());

        pRuleControls.setBorder(javax.swing.BorderFactory.createEtchedBorder());

        lArrow.setFont(new java.awt.Font("Ubuntu", 1, 36)); // NOI18N
        lArrow.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        lArrow.setText(">>>");

        pStateIni.setBorder(javax.swing.BorderFactory.createEtchedBorder());

        cbStateIni.setFont(new java.awt.Font("Ubuntu", 1, 16)); // NOI18N
        cbStateIni.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "Item 1", "Item 2", "Item 3", "Item 4" }));
        cbStateIni.addActionListener(new java.awt.event.ActionListener()
        {
            public void actionPerformed(java.awt.event.ActionEvent evt)
            {
                cbStateIniActionPerformed(evt);
            }
        });

        lInicio.setFont(new java.awt.Font("Ubuntu", 1, 16)); // NOI18N
        lInicio.setText("Inicio");

        javax.swing.GroupLayout pStateIniLayout = new javax.swing.GroupLayout(pStateIni);
        pStateIni.setLayout(pStateIniLayout);
        pStateIniLayout.setHorizontalGroup(
            pStateIniLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(pStateIniLayout.createSequentialGroup()
                .addGap(12, 12, 12)
                .addGroup(pStateIniLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(pStateIniLayout.createSequentialGroup()
                        .addComponent(lInicio, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addGap(12, 12, 12))
                    .addGroup(pStateIniLayout.createSequentialGroup()
                        .addComponent(cbStateIni, 0, 108, Short.MAX_VALUE)
                        .addContainerGap())))
        );
        pStateIniLayout.setVerticalGroup(
            pStateIniLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, pStateIniLayout.createSequentialGroup()
                .addGap(18, 18, 18)
                .addComponent(lInicio)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(cbStateIni, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(12, 12, 12))
        );

        pStateEnd.setBorder(javax.swing.BorderFactory.createEtchedBorder());

        cbStateEnd.setFont(new java.awt.Font("Ubuntu", 1, 16)); // NOI18N
        cbStateEnd.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "Item 1", "Item 2", "Item 3", "Item 4" }));
        cbStateEnd.addActionListener(new java.awt.event.ActionListener()
        {
            public void actionPerformed(java.awt.event.ActionEvent evt)
            {
                cbStateEndActionPerformed(evt);
            }
        });

        lFinal.setFont(new java.awt.Font("Ubuntu", 1, 16)); // NOI18N
        lFinal.setText("Final");

        javax.swing.GroupLayout pStateEndLayout = new javax.swing.GroupLayout(pStateEnd);
        pStateEnd.setLayout(pStateEndLayout);
        pStateEndLayout.setHorizontalGroup(
            pStateEndLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(pStateEndLayout.createSequentialGroup()
                .addGap(12, 12, 12)
                .addGroup(pStateEndLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(lFinal, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(cbStateEnd, 0, 108, Short.MAX_VALUE))
                .addContainerGap())
        );
        pStateEndLayout.setVerticalGroup(
            pStateEndLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, pStateEndLayout.createSequentialGroup()
                .addContainerGap(18, Short.MAX_VALUE)
                .addComponent(lFinal)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(cbStateEnd, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(12, 12, 12))
        );

        javax.swing.GroupLayout pRuleControlsLayout = new javax.swing.GroupLayout(pRuleControls);
        pRuleControls.setLayout(pRuleControlsLayout);
        pRuleControlsLayout.setHorizontalGroup(
            pRuleControlsLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(pRuleControlsLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(pStateIni, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addComponent(lArrow)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 18, Short.MAX_VALUE)
                .addComponent(pStateEnd, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
        );

        pRuleControlsLayout.linkSize(javax.swing.SwingConstants.HORIZONTAL, new java.awt.Component[] {pStateEnd, pStateIni});

        pRuleControlsLayout.setVerticalGroup(
            pRuleControlsLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, pRuleControlsLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(pRuleControlsLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(lArrow, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addGroup(javax.swing.GroupLayout.Alignment.LEADING, pRuleControlsLayout.createSequentialGroup()
                        .addGroup(pRuleControlsLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                            .addComponent(pStateIni, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(pStateEnd, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                        .addGap(0, 3, Short.MAX_VALUE)))
                .addContainerGap())
        );

        pRuleControlsLayout.linkSize(javax.swing.SwingConstants.VERTICAL, new java.awt.Component[] {pStateEnd, pStateIni});

        pConditions.setBorder(javax.swing.BorderFactory.createTitledBorder("Condiciones"));

        conditionsList.addMouseListener(new java.awt.event.MouseAdapter()
        {
            public void mouseReleased(java.awt.event.MouseEvent evt)
            {
                conditionsListMouseReleased(evt);
            }
        });
        jScrollPane3.setViewportView(conditionsList);

        bConditionAdd.setIcon(new javax.swing.ImageIcon(getClass().getResource("/luz/ve/ssacc/maker/gui/images/SSACC Button Create 20x20.png"))); // NOI18N
        bConditionAdd.addActionListener(new java.awt.event.ActionListener()
        {
            public void actionPerformed(java.awt.event.ActionEvent evt)
            {
                bConditionAddActionPerformed(evt);
            }
        });

        bConditionDel.setIcon(new javax.swing.ImageIcon(getClass().getResource("/luz/ve/ssacc/maker/gui/images/SSACC Delete 20x20.png"))); // NOI18N
        bConditionDel.addActionListener(new java.awt.event.ActionListener()
        {
            public void actionPerformed(java.awt.event.ActionEvent evt)
            {
                bConditionDelActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout pConditionsLayout = new javax.swing.GroupLayout(pConditions);
        pConditions.setLayout(pConditionsLayout);
        pConditionsLayout.setHorizontalGroup(
            pConditionsLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(pConditionsLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(pConditionsLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(bConditionAdd)
                    .addComponent(bConditionDel))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jScrollPane3, javax.swing.GroupLayout.PREFERRED_SIZE, 770, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        pConditionsLayout.setVerticalGroup(
            pConditionsLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(pConditionsLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(pConditionsLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jScrollPane3, javax.swing.GroupLayout.PREFERRED_SIZE, 0, Short.MAX_VALUE)
                    .addGroup(pConditionsLayout.createSequentialGroup()
                        .addComponent(bConditionAdd)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 61, Short.MAX_VALUE)
                        .addComponent(bConditionDel)))
                .addContainerGap())
        );

        pRuleTitle.setBorder(javax.swing.BorderFactory.createEtchedBorder());

        lRuleTitle.setFont(new java.awt.Font("Ubuntu", 1, 24)); // NOI18N
        lRuleTitle.setText("Regla #:");

        lRuleId.setFont(new java.awt.Font("Ubuntu", 1, 24)); // NOI18N
        lRuleId.setText("0000");

        javax.swing.GroupLayout pRuleTitleLayout = new javax.swing.GroupLayout(pRuleTitle);
        pRuleTitle.setLayout(pRuleTitleLayout);
        pRuleTitleLayout.setHorizontalGroup(
            pRuleTitleLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(pRuleTitleLayout.createSequentialGroup()
                .addGap(37, 37, 37)
                .addComponent(lRuleTitle)
                .addGap(18, 18, 18)
                .addComponent(lRuleId)
                .addContainerGap(18, Short.MAX_VALUE))
        );
        pRuleTitleLayout.setVerticalGroup(
            pRuleTitleLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, pRuleTitleLayout.createSequentialGroup()
                .addGap(41, 41, 41)
                .addGroup(pRuleTitleLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(lRuleId)
                    .addComponent(lRuleTitle))
                .addGap(39, 39, 39))
        );

        jPanel1.setBorder(javax.swing.BorderFactory.createEtchedBorder());

        lConditionsNumTitle.setFont(new java.awt.Font("Ubuntu", 1, 18)); // NOI18N
        lConditionsNumTitle.setText("Total Condiciones:");

        lConditionsNum.setFont(new java.awt.Font("Ubuntu", 1, 24)); // NOI18N
        lConditionsNum.setText("00");

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGap(92, 92, 92)
                        .addComponent(lConditionsNum))
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGap(26, 26, 26)
                        .addComponent(lConditionsNumTitle)))
                .addContainerGap())
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addGap(35, 35, 35)
                .addComponent(lConditionsNumTitle)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(lConditionsNum)
                .addContainerGap())
        );

        jPanel2.setBorder(javax.swing.BorderFactory.createEtchedBorder());

        pOperandA.setBorder(javax.swing.BorderFactory.createTitledBorder("Operando A"));
        pOperandA.setDoubleBuffered(false);

        pFieldValueA.setBorder(javax.swing.BorderFactory.createEtchedBorder());

        buttonGroupA.add(rbFieldTextA);
        rbFieldTextA.addMouseListener(new java.awt.event.MouseAdapter()
        {
            public void mouseClicked(java.awt.event.MouseEvent evt)
            {
                rbFieldTextAMouseClicked(evt);
            }
        });

        lTextTitleA.setText("Texto:");

        lTextA.setText("jLabel4");

        javax.swing.GroupLayout pFieldValueALayout = new javax.swing.GroupLayout(pFieldValueA);
        pFieldValueA.setLayout(pFieldValueALayout);
        pFieldValueALayout.setHorizontalGroup(
            pFieldValueALayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, pFieldValueALayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(lTextTitleA)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(lTextA)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(rbFieldTextA)
                .addContainerGap())
        );
        pFieldValueALayout.setVerticalGroup(
            pFieldValueALayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(pFieldValueALayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(pFieldValueALayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(rbFieldTextA)
                    .addGroup(pFieldValueALayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(lTextTitleA)
                        .addComponent(lTextA)))
                .addContainerGap(17, Short.MAX_VALUE))
        );

        pFieldIdA.setBorder(javax.swing.BorderFactory.createEtchedBorder());

        buttonGroupA.add(rbFieldIdA);
        rbFieldIdA.setSelected(true);
        rbFieldIdA.addMouseListener(new java.awt.event.MouseAdapter()
        {
            public void mouseClicked(java.awt.event.MouseEvent evt)
            {
                rbFieldIdAMouseClicked(evt);
            }
        });

        lIdTitleA.setText("ID:");

        lIdA.setText("jLabel1");

        javax.swing.GroupLayout pFieldIdALayout = new javax.swing.GroupLayout(pFieldIdA);
        pFieldIdA.setLayout(pFieldIdALayout);
        pFieldIdALayout.setHorizontalGroup(
            pFieldIdALayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, pFieldIdALayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(lIdTitleA)
                .addGap(18, 18, 18)
                .addComponent(lIdA)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(rbFieldIdA)
                .addContainerGap())
        );
        pFieldIdALayout.setVerticalGroup(
            pFieldIdALayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(pFieldIdALayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(pFieldIdALayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addGroup(pFieldIdALayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(lIdTitleA)
                        .addComponent(lIdA))
                    .addComponent(rbFieldIdA))
                .addContainerGap(17, Short.MAX_VALUE))
        );

        pFieldColorA.setBorder(javax.swing.BorderFactory.createEtchedBorder());

        buttonGroupA.add(rbFieldColorA);
        rbFieldColorA.addMouseListener(new java.awt.event.MouseAdapter()
        {
            public void mouseClicked(java.awt.event.MouseEvent evt)
            {
                rbFieldColorAMouseClicked(evt);
            }
        });

        bColorA.setText("Color");

        javax.swing.GroupLayout pFieldColorALayout = new javax.swing.GroupLayout(pFieldColorA);
        pFieldColorA.setLayout(pFieldColorALayout);
        pFieldColorALayout.setHorizontalGroup(
            pFieldColorALayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, pFieldColorALayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(bColorA, javax.swing.GroupLayout.PREFERRED_SIZE, 134, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 71, Short.MAX_VALUE)
                .addComponent(rbFieldColorA)
                .addContainerGap())
        );
        pFieldColorALayout.setVerticalGroup(
            pFieldColorALayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(pFieldColorALayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(pFieldColorALayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(rbFieldColorA)
                    .addComponent(bColorA, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(17, Short.MAX_VALUE))
        );

        pFieldTextA.setBorder(javax.swing.BorderFactory.createEtchedBorder());

        buttonGroupA.add(rbFieldValueA);
        rbFieldValueA.addMouseListener(new java.awt.event.MouseAdapter()
        {
            public void mouseClicked(java.awt.event.MouseEvent evt)
            {
                rbFieldValueAMouseClicked(evt);
            }
        });

        lValueTitleA.setText("Valor:");

        lValueA.setText("jLabel3");

        javax.swing.GroupLayout pFieldTextALayout = new javax.swing.GroupLayout(pFieldTextA);
        pFieldTextA.setLayout(pFieldTextALayout);
        pFieldTextALayout.setHorizontalGroup(
            pFieldTextALayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, pFieldTextALayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(lValueTitleA)
                .addGap(18, 18, 18)
                .addComponent(lValueA)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(rbFieldValueA)
                .addContainerGap())
        );
        pFieldTextALayout.setVerticalGroup(
            pFieldTextALayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(pFieldTextALayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(pFieldTextALayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addGroup(pFieldTextALayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(lValueTitleA)
                        .addComponent(lValueA))
                    .addComponent(rbFieldValueA))
                .addContainerGap(17, Short.MAX_VALUE))
        );

        pFunctionA.setBorder(javax.swing.BorderFactory.createEtchedBorder());

        lFunctionA.setText("Función");

        cbFunctionA.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "Item 1", "Item 2", "Item 3", "Item 4" }));
        cbFunctionA.addActionListener(new java.awt.event.ActionListener()
        {
            public void actionPerformed(java.awt.event.ActionEvent evt)
            {
                cbFunctionAActionPerformed(evt);
            }
        });

        cbVariableA.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "Item 1", "Item 2", "Item 3", "Item 4" }));
        cbVariableA.addActionListener(new java.awt.event.ActionListener()
        {
            public void actionPerformed(java.awt.event.ActionEvent evt)
            {
                cbVariableAActionPerformed(evt);
            }
        });

        lVariableA.setText("Variable");

        javax.swing.GroupLayout pFunctionALayout = new javax.swing.GroupLayout(pFunctionA);
        pFunctionA.setLayout(pFunctionALayout);
        pFunctionALayout.setHorizontalGroup(
            pFunctionALayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(pFunctionALayout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGroup(pFunctionALayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, pFunctionALayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addComponent(cbFunctionA, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(lFunctionA))
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, pFunctionALayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addComponent(lVariableA)
                        .addComponent(cbVariableA, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addContainerGap())
        );

        pFunctionALayout.linkSize(javax.swing.SwingConstants.HORIZONTAL, new java.awt.Component[] {cbFunctionA, cbVariableA, lFunctionA, lVariableA});

        pFunctionALayout.setVerticalGroup(
            pFunctionALayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(pFunctionALayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(lFunctionA, javax.swing.GroupLayout.PREFERRED_SIZE, 27, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(cbFunctionA, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addComponent(lVariableA, javax.swing.GroupLayout.PREFERRED_SIZE, 27, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(cbVariableA, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        pFunctionALayout.linkSize(javax.swing.SwingConstants.VERTICAL, new java.awt.Component[] {cbFunctionA, cbVariableA, lFunctionA, lVariableA});

        javax.swing.GroupLayout pOperandALayout = new javax.swing.GroupLayout(pOperandA);
        pOperandA.setLayout(pOperandALayout);
        pOperandALayout.setHorizontalGroup(
            pOperandALayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(pOperandALayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(pFunctionA, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(pOperandALayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                    .addComponent(pFieldIdA, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(pFieldValueA, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(pFieldTextA, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(pFieldColorA, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        pOperandALayout.setVerticalGroup(
            pOperandALayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(pOperandALayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(pOperandALayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(pFunctionA, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, pOperandALayout.createSequentialGroup()
                        .addComponent(pFieldIdA, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(pFieldValueA, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(pFieldTextA, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(pFieldColorA, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        pOperandB.setBorder(javax.swing.BorderFactory.createTitledBorder("Operando B"));
        pOperandB.setDoubleBuffered(false);

        pFieldValueB.setBorder(javax.swing.BorderFactory.createEtchedBorder());

        buttonGroupB.add(rbFieldTextB);
        rbFieldTextB.addMouseListener(new java.awt.event.MouseAdapter()
        {
            public void mouseClicked(java.awt.event.MouseEvent evt)
            {
                rbFieldTextBMouseClicked(evt);
            }
        });

        lTextTitleB.setText("Texto:");

        lTextB.setText("jLabel4");

        javax.swing.GroupLayout pFieldValueBLayout = new javax.swing.GroupLayout(pFieldValueB);
        pFieldValueB.setLayout(pFieldValueBLayout);
        pFieldValueBLayout.setHorizontalGroup(
            pFieldValueBLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, pFieldValueBLayout.createSequentialGroup()
                .addGap(7, 7, 7)
                .addComponent(lTextTitleB)
                .addGap(18, 18, 18)
                .addComponent(lTextB)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(rbFieldTextB)
                .addContainerGap())
        );
        pFieldValueBLayout.setVerticalGroup(
            pFieldValueBLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(pFieldValueBLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(pFieldValueBLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(rbFieldTextB)
                    .addGroup(pFieldValueBLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(lTextTitleB)
                        .addComponent(lTextB)))
                .addContainerGap(17, Short.MAX_VALUE))
        );

        pFieldIdB.setBorder(javax.swing.BorderFactory.createEtchedBorder());

        buttonGroupB.add(rbFieldIdB);
        rbFieldIdB.setSelected(true);
        rbFieldIdB.addMouseListener(new java.awt.event.MouseAdapter()
        {
            public void mouseClicked(java.awt.event.MouseEvent evt)
            {
                rbFieldIdBMouseClicked(evt);
            }
        });

        lIdTitleB.setText("ID:");

        lIdB.setText("jLabel1");

        javax.swing.GroupLayout pFieldIdBLayout = new javax.swing.GroupLayout(pFieldIdB);
        pFieldIdB.setLayout(pFieldIdBLayout);
        pFieldIdBLayout.setHorizontalGroup(
            pFieldIdBLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, pFieldIdBLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(lIdTitleB)
                .addGap(18, 18, 18)
                .addComponent(lIdB)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(rbFieldIdB)
                .addContainerGap())
        );
        pFieldIdBLayout.setVerticalGroup(
            pFieldIdBLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(pFieldIdBLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(pFieldIdBLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addGroup(pFieldIdBLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(lIdTitleB)
                        .addComponent(lIdB))
                    .addComponent(rbFieldIdB))
                .addContainerGap(17, Short.MAX_VALUE))
        );

        pFieldColorB.setBorder(javax.swing.BorderFactory.createEtchedBorder());

        buttonGroupB.add(rbFieldColorB);
        rbFieldColorB.addMouseListener(new java.awt.event.MouseAdapter()
        {
            public void mouseClicked(java.awt.event.MouseEvent evt)
            {
                rbFieldColorBMouseClicked(evt);
            }
        });

        bColorB.setText("Color");

        javax.swing.GroupLayout pFieldColorBLayout = new javax.swing.GroupLayout(pFieldColorB);
        pFieldColorB.setLayout(pFieldColorBLayout);
        pFieldColorBLayout.setHorizontalGroup(
            pFieldColorBLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, pFieldColorBLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(bColorB, javax.swing.GroupLayout.PREFERRED_SIZE, 134, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 71, Short.MAX_VALUE)
                .addComponent(rbFieldColorB)
                .addContainerGap())
        );
        pFieldColorBLayout.setVerticalGroup(
            pFieldColorBLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(pFieldColorBLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(pFieldColorBLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(rbFieldColorB)
                    .addComponent(bColorB, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(17, Short.MAX_VALUE))
        );

        pFieldTextB.setBorder(javax.swing.BorderFactory.createEtchedBorder());

        buttonGroupB.add(rbFieldValueB);
        rbFieldValueB.addMouseListener(new java.awt.event.MouseAdapter()
        {
            public void mouseClicked(java.awt.event.MouseEvent evt)
            {
                rbFieldValueBMouseClicked(evt);
            }
        });

        lValueTitleB.setText("Valor:");

        lValueB.setText("jLabel3");

        javax.swing.GroupLayout pFieldTextBLayout = new javax.swing.GroupLayout(pFieldTextB);
        pFieldTextB.setLayout(pFieldTextBLayout);
        pFieldTextBLayout.setHorizontalGroup(
            pFieldTextBLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, pFieldTextBLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(lValueTitleB)
                .addGap(18, 18, 18)
                .addComponent(lValueB)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(rbFieldValueB)
                .addContainerGap())
        );
        pFieldTextBLayout.setVerticalGroup(
            pFieldTextBLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(pFieldTextBLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(pFieldTextBLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addGroup(pFieldTextBLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(lValueTitleB)
                        .addComponent(lValueB))
                    .addComponent(rbFieldValueB))
                .addContainerGap(17, Short.MAX_VALUE))
        );

        pFunctionCon2.setBorder(javax.swing.BorderFactory.createEtchedBorder());

        lFunctionB.setText("Función");

        cbFunctionB.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "Item 1", "Item 2", "Item 3", "Item 4" }));
        cbFunctionB.addActionListener(new java.awt.event.ActionListener()
        {
            public void actionPerformed(java.awt.event.ActionEvent evt)
            {
                cbFunctionBActionPerformed(evt);
            }
        });

        cbVariableB.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "Item 1", "Item 2", "Item 3", "Item 4" }));
        cbVariableB.addActionListener(new java.awt.event.ActionListener()
        {
            public void actionPerformed(java.awt.event.ActionEvent evt)
            {
                cbVariableBActionPerformed(evt);
            }
        });

        lVariableB.setText("Variable");

        javax.swing.GroupLayout pFunctionCon2Layout = new javax.swing.GroupLayout(pFunctionCon2);
        pFunctionCon2.setLayout(pFunctionCon2Layout);
        pFunctionCon2Layout.setHorizontalGroup(
            pFunctionCon2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(pFunctionCon2Layout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGroup(pFunctionCon2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(lVariableB)
                    .addComponent(cbVariableB, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap())
            .addGroup(pFunctionCon2Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(pFunctionCon2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(cbFunctionB, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(lFunctionB))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        pFunctionCon2Layout.linkSize(javax.swing.SwingConstants.HORIZONTAL, new java.awt.Component[] {cbFunctionB, cbVariableB, lFunctionB, lVariableB});

        pFunctionCon2Layout.setVerticalGroup(
            pFunctionCon2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(pFunctionCon2Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(lFunctionB, javax.swing.GroupLayout.PREFERRED_SIZE, 27, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(cbFunctionB, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addComponent(lVariableB, javax.swing.GroupLayout.PREFERRED_SIZE, 27, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(cbVariableB, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        pFunctionCon2Layout.linkSize(javax.swing.SwingConstants.VERTICAL, new java.awt.Component[] {cbFunctionB, cbVariableB, lFunctionB, lVariableB});

        javax.swing.GroupLayout pOperandBLayout = new javax.swing.GroupLayout(pOperandB);
        pOperandB.setLayout(pOperandBLayout);
        pOperandBLayout.setHorizontalGroup(
            pOperandBLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, pOperandBLayout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(pFunctionCon2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(pOperandBLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                    .addComponent(pFieldValueB, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(pFieldIdB, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(pFieldTextB, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(pFieldColorB, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap())
        );
        pOperandBLayout.setVerticalGroup(
            pOperandBLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(pOperandBLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(pOperandBLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                    .addComponent(pFunctionCon2, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addGroup(pOperandBLayout.createSequentialGroup()
                        .addComponent(pFieldIdB, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(pFieldValueB, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(pFieldTextB, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(pFieldColorB, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        jPanel3.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        cbOperators.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "==" }));
        cbOperators.addActionListener(new java.awt.event.ActionListener()
        {
            public void actionPerformed(java.awt.event.ActionEvent evt)
            {
                cbOperatorsActionPerformed(evt);
            }
        });
        jPanel3.add(cbOperators, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 130, -1, -1));

        javax.swing.GroupLayout jPanel2Layout = new javax.swing.GroupLayout(jPanel2);
        jPanel2.setLayout(jPanel2Layout);
        jPanel2Layout.setHorizontalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel2Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(pOperandA, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jPanel3, javax.swing.GroupLayout.PREFERRED_SIZE, 59, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(pOperandB, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        jPanel2Layout.linkSize(javax.swing.SwingConstants.HORIZONTAL, new java.awt.Component[] {pOperandA, pOperandB});

        jPanel2Layout.setVerticalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(pOperandB, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(pOperandA, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jPanel3, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        jPanel2Layout.linkSize(javax.swing.SwingConstants.VERTICAL, new java.awt.Component[] {pOperandA, pOperandB});

        javax.swing.GroupLayout pRuleBackgroundLayout = new javax.swing.GroupLayout(pRuleBackground);
        pRuleBackground.setLayout(pRuleBackgroundLayout);
        pRuleBackgroundLayout.setHorizontalGroup(
            pRuleBackgroundLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(pRuleBackgroundLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(pRuleBackgroundLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jPanel2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGroup(pRuleBackgroundLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                        .addComponent(pConditions, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addGroup(javax.swing.GroupLayout.Alignment.LEADING, pRuleBackgroundLayout.createSequentialGroup()
                            .addComponent(pRuleTitle, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                            .addComponent(pRuleControls, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                            .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        pRuleBackgroundLayout.linkSize(javax.swing.SwingConstants.HORIZONTAL, new java.awt.Component[] {jPanel1, pRuleTitle});

        pRuleBackgroundLayout.setVerticalGroup(
            pRuleBackgroundLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(pRuleBackgroundLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(pRuleBackgroundLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(pRuleControls, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(pRuleTitle, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(pConditions, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jPanel2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(29, 29, 29))
        );

        pRuleBackgroundLayout.linkSize(javax.swing.SwingConstants.VERTICAL, new java.awt.Component[] {jPanel1, pRuleControls, pRuleTitle});

        tabbedPaneRules.addTab("Condiciones", pRuleBackground);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(tabbedPaneRules, javax.swing.GroupLayout.PREFERRED_SIZE, 948, javax.swing.GroupLayout.PREFERRED_SIZE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(tabbedPaneRules, javax.swing.GroupLayout.PREFERRED_SIZE, 679, javax.swing.GroupLayout.PREFERRED_SIZE)
        );

        tabbedPaneRules.getAccessibleContext().setAccessibleName("rulesTab");
        tabbedPaneRules.getAccessibleContext().setAccessibleDescription("");

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void bRuleDelActionPerformed(java.awt.event.ActionEvent evt)//GEN-FIRST:event_bRuleDelActionPerformed
    {//GEN-HEADEREND:event_bRuleDelActionPerformed
        this.deleteRule();
    }//GEN-LAST:event_bRuleDelActionPerformed

    private void bRuleAddActionPerformed(java.awt.event.ActionEvent evt)//GEN-FIRST:event_bRuleAddActionPerformed
    {//GEN-HEADEREND:event_bRuleAddActionPerformed
        this.addRule();
    }//GEN-LAST:event_bRuleAddActionPerformed

    private void bRulesLoadActionPerformed(java.awt.event.ActionEvent evt)//GEN-FIRST:event_bRulesLoadActionPerformed
    {//GEN-HEADEREND:event_bRulesLoadActionPerformed
        this.load(SacUtility.LOAD_FILE, "");
    }//GEN-LAST:event_bRulesLoadActionPerformed

    private void bResetActionPerformed(java.awt.event.ActionEvent evt)//GEN-FIRST:event_bResetActionPerformed
    {//GEN-HEADEREND:event_bResetActionPerformed
        this.reset();
    }//GEN-LAST:event_bResetActionPerformed

    private void bSaveActionPerformed(java.awt.event.ActionEvent evt)//GEN-FIRST:event_bSaveActionPerformed
    {//GEN-HEADEREND:event_bSaveActionPerformed
        this.save();
    }//GEN-LAST:event_bSaveActionPerformed

    private void formWindowClosing(java.awt.event.WindowEvent evt)//GEN-FIRST:event_formWindowClosing
    {//GEN-HEADEREND:event_formWindowClosing
         SacGuiManager.closeWindow(this, parentWindow);
    }//GEN-LAST:event_formWindowClosing

    private void conditionsListMouseReleased(java.awt.event.MouseEvent evt)//GEN-FIRST:event_conditionsListMouseReleased
    {//GEN-HEADEREND:event_conditionsListMouseReleased
        this.loadConditionGui();       
    }//GEN-LAST:event_conditionsListMouseReleased

    private void cbVariableAActionPerformed(java.awt.event.ActionEvent evt)//GEN-FIRST:event_cbVariableAActionPerformed
    {//GEN-HEADEREND:event_cbVariableAActionPerformed
        this.changeVariableState(
            this.cbVariableA,
            this.lIdA,
            this.lTextA,
            this.lValueA,
            this.bColorA,
            Operand.OPERAND_A_ID);   
    }//GEN-LAST:event_cbVariableAActionPerformed

    private void cbVariableBActionPerformed(java.awt.event.ActionEvent evt)//GEN-FIRST:event_cbVariableBActionPerformed
    {//GEN-HEADEREND:event_cbVariableBActionPerformed
        this.changeVariableState(
            this.cbVariableB,
            this.lIdB,
            this.lTextB,
            this.lValueB,
            this.bColorB,
            Operand.OPERAND_B_ID);      
    }//GEN-LAST:event_cbVariableBActionPerformed

    private void cbFunctionAActionPerformed(java.awt.event.ActionEvent evt)//GEN-FIRST:event_cbFunctionAActionPerformed
    {//GEN-HEADEREND:event_cbFunctionAActionPerformed
        this.changeFunction(this.cbFunctionA, Operand.OPERAND_A_ID);
    }//GEN-LAST:event_cbFunctionAActionPerformed

    private void tabbedPaneRulesFocusGained(java.awt.event.FocusEvent evt)//GEN-FIRST:event_tabbedPaneRulesFocusGained
    {//GEN-HEADEREND:event_tabbedPaneRulesFocusGained
     
    }//GEN-LAST:event_tabbedPaneRulesFocusGained

    private void cbFunctionBActionPerformed(java.awt.event.ActionEvent evt)//GEN-FIRST:event_cbFunctionBActionPerformed
    {//GEN-HEADEREND:event_cbFunctionBActionPerformed
        this.changeFunction(this.cbFunctionB, Operand.OPERAND_B_ID);
    }//GEN-LAST:event_cbFunctionBActionPerformed

    private void bConditionAddActionPerformed(java.awt.event.ActionEvent evt)//GEN-FIRST:event_bConditionAddActionPerformed
    {//GEN-HEADEREND:event_bConditionAddActionPerformed
        this.addCondition();
    }//GEN-LAST:event_bConditionAddActionPerformed

    private void bConditionDelActionPerformed(java.awt.event.ActionEvent evt)//GEN-FIRST:event_bConditionDelActionPerformed
    {//GEN-HEADEREND:event_bConditionDelActionPerformed
        this.deleteCondition();
    }//GEN-LAST:event_bConditionDelActionPerformed

    private void cbOperatorsActionPerformed(java.awt.event.ActionEvent evt)//GEN-FIRST:event_cbOperatorsActionPerformed
    {//GEN-HEADEREND:event_cbOperatorsActionPerformed
         this.changeOperator();
    }//GEN-LAST:event_cbOperatorsActionPerformed

    private void rbFieldIdAMouseClicked(java.awt.event.MouseEvent evt)//GEN-FIRST:event_rbFieldIdAMouseClicked
    {//GEN-HEADEREND:event_rbFieldIdAMouseClicked
       this.changeField(State.FIELD_NUM_ID, Operand.OPERAND_A_ID);
    }//GEN-LAST:event_rbFieldIdAMouseClicked

    private void rbFieldTextAMouseClicked(java.awt.event.MouseEvent evt)//GEN-FIRST:event_rbFieldTextAMouseClicked
    {//GEN-HEADEREND:event_rbFieldTextAMouseClicked
        this.changeField(State.FIELD_NUM_TEXT, Operand.OPERAND_A_ID);
    }//GEN-LAST:event_rbFieldTextAMouseClicked

    private void rbFieldValueAMouseClicked(java.awt.event.MouseEvent evt)//GEN-FIRST:event_rbFieldValueAMouseClicked
    {//GEN-HEADEREND:event_rbFieldValueAMouseClicked
        this.changeField(State.FIELD_NUM_VALUE, Operand.OPERAND_A_ID);
    }//GEN-LAST:event_rbFieldValueAMouseClicked

    private void rbFieldColorAMouseClicked(java.awt.event.MouseEvent evt)//GEN-FIRST:event_rbFieldColorAMouseClicked
    {//GEN-HEADEREND:event_rbFieldColorAMouseClicked
        this.changeField(State.FIELD_NUM_COLOR, Operand.OPERAND_A_ID);
    }//GEN-LAST:event_rbFieldColorAMouseClicked

    private void rbFieldIdBMouseClicked(java.awt.event.MouseEvent evt)//GEN-FIRST:event_rbFieldIdBMouseClicked
    {//GEN-HEADEREND:event_rbFieldIdBMouseClicked
        this.changeField(State.FIELD_NUM_ID, Operand.OPERAND_B_ID);
    }//GEN-LAST:event_rbFieldIdBMouseClicked

    private void rbFieldTextBMouseClicked(java.awt.event.MouseEvent evt)//GEN-FIRST:event_rbFieldTextBMouseClicked
    {//GEN-HEADEREND:event_rbFieldTextBMouseClicked
        this.changeField(State.FIELD_NUM_TEXT, Operand.OPERAND_B_ID);
    }//GEN-LAST:event_rbFieldTextBMouseClicked

    private void rbFieldValueBMouseClicked(java.awt.event.MouseEvent evt)//GEN-FIRST:event_rbFieldValueBMouseClicked
    {//GEN-HEADEREND:event_rbFieldValueBMouseClicked
           this.changeField(State.FIELD_NUM_VALUE, Operand.OPERAND_B_ID);
    }//GEN-LAST:event_rbFieldValueBMouseClicked

    private void rbFieldColorBMouseClicked(java.awt.event.MouseEvent evt)//GEN-FIRST:event_rbFieldColorBMouseClicked
    {//GEN-HEADEREND:event_rbFieldColorBMouseClicked
           this.changeField(State.FIELD_NUM_COLOR, Operand.OPERAND_B_ID);
    }//GEN-LAST:event_rbFieldColorBMouseClicked

    private void cbStateEndActionPerformed(java.awt.event.ActionEvent evt)//GEN-FIRST:event_cbStateEndActionPerformed
    {//GEN-HEADEREND:event_cbStateEndActionPerformed
        this.updateRuleStatesData(1);
    }//GEN-LAST:event_cbStateEndActionPerformed

    private void cbStateIniActionPerformed(java.awt.event.ActionEvent evt)//GEN-FIRST:event_cbStateIniActionPerformed
    {//GEN-HEADEREND:event_cbStateIniActionPerformed
         this.updateRuleStatesData(0);
    }//GEN-LAST:event_cbStateIniActionPerformed

    private void rulesListMouseClicked(java.awt.event.MouseEvent evt)//GEN-FIRST:event_rulesListMouseClicked
    {//GEN-HEADEREND:event_rulesListMouseClicked
        this.updateSelectedRuleGui();
    }//GEN-LAST:event_rulesListMouseClicked

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton bColorA;
    private javax.swing.JButton bColorB;
    private javax.swing.JButton bConditionAdd;
    private javax.swing.JButton bConditionDel;
    private javax.swing.JButton bReset;
    private javax.swing.JButton bRuleAdd;
    private javax.swing.JButton bRuleDel;
    private javax.swing.JButton bRulesLoad;
    private javax.swing.JButton bSave;
    private javax.swing.ButtonGroup buttonGroupA;
    private javax.swing.ButtonGroup buttonGroupB;
    private javax.swing.JComboBox<String> cbFunctionA;
    private javax.swing.JComboBox<String> cbFunctionB;
    private javax.swing.JComboBox<String> cbOperators;
    private javax.swing.JComboBox<String> cbStateEnd;
    private javax.swing.JComboBox<String> cbStateIni;
    private javax.swing.JComboBox<String> cbVariableA;
    private javax.swing.JComboBox<String> cbVariableB;
    private javax.swing.JList<String> conditionsList;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JPanel jPanel3;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JScrollPane jScrollPane3;
    private javax.swing.JLabel lArrow;
    private javax.swing.JLabel lConditionsNum;
    private javax.swing.JLabel lConditionsNumTitle;
    private javax.swing.JLabel lFinal;
    private javax.swing.JLabel lFunctionA;
    private javax.swing.JLabel lFunctionB;
    private javax.swing.JLabel lIdA;
    private javax.swing.JLabel lIdB;
    private javax.swing.JLabel lIdTitleA;
    private javax.swing.JLabel lIdTitleB;
    private javax.swing.JLabel lInicio;
    private javax.swing.JLabel lRuleId;
    private javax.swing.JLabel lRuleTitle;
    private javax.swing.JLabel lRulesNum;
    private javax.swing.JLabel lRulesNumTitle;
    private javax.swing.JLabel lTextA;
    private javax.swing.JLabel lTextB;
    private javax.swing.JLabel lTextTitleA;
    private javax.swing.JLabel lTextTitleB;
    private javax.swing.JLabel lValueA;
    private javax.swing.JLabel lValueB;
    private javax.swing.JLabel lValueTitleA;
    private javax.swing.JLabel lValueTitleB;
    private javax.swing.JLabel lVariableA;
    private javax.swing.JLabel lVariableB;
    private javax.swing.JPanel pBackground;
    private javax.swing.JPanel pConditions;
    private javax.swing.JPanel pDescription;
    private javax.swing.JPanel pFieldColorA;
    private javax.swing.JPanel pFieldColorB;
    private javax.swing.JPanel pFieldIdA;
    private javax.swing.JPanel pFieldIdB;
    private javax.swing.JPanel pFieldTextA;
    private javax.swing.JPanel pFieldTextB;
    private javax.swing.JPanel pFieldValueA;
    private javax.swing.JPanel pFieldValueB;
    private javax.swing.JPanel pFunctionA;
    private javax.swing.JPanel pFunctionCon2;
    private javax.swing.JPanel pName;
    private javax.swing.JPanel pOperandA;
    private javax.swing.JPanel pOperandB;
    private javax.swing.JPanel pRuleBackground;
    private javax.swing.JPanel pRuleControls;
    private javax.swing.JPanel pRuleTitle;
    private javax.swing.JPanel pRules;
    private javax.swing.JPanel pStateEnd;
    private javax.swing.JPanel pStateIni;
    private javax.swing.JRadioButton rbFieldColorA;
    private javax.swing.JRadioButton rbFieldColorB;
    private javax.swing.JRadioButton rbFieldIdA;
    private javax.swing.JRadioButton rbFieldIdB;
    private javax.swing.JRadioButton rbFieldTextA;
    private javax.swing.JRadioButton rbFieldTextB;
    private javax.swing.JRadioButton rbFieldValueA;
    private javax.swing.JRadioButton rbFieldValueB;
    private javax.swing.JList<String> rulesList;
    private javax.swing.JTextArea taRulesDescription;
    private javax.swing.JTabbedPane tabbedPaneRules;
    private javax.swing.JTextField tfRulesName;
    // End of variables declaration//GEN-END:variables
}
