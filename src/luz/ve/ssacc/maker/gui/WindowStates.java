/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package luz.ve.ssacc.maker.gui;

import luz.ve.ssacc.maker.gui.extras.SacGuiManager;
import javax.swing.*;
import luz.ve.ssacc.engine.components.Automaton;
import luz.ve.ssacc.engine.components.State;
import luz.ve.ssacc.engine.components.States;
import luz.ve.ssacc.utilities.*;
import luz.ve.ssacc.engine.parsers.StatesParser;
import java.awt.Color;

/**
 *
 * @author Renzo Ciafardone Sciannelli <rciafardone@gmail.com>
 */
public class WindowStates extends javax.swing.JFrame
{
    MainWindowMaker parentWindow;
  
    
    public WindowStates(MainWindowMaker mainFrame)
    {
        initComponents();
        parentWindow = mainFrame;
    }
    
    /*
     * Prepara el GUI para un nuevo estado.
     */
    private void addNewState()
    {
        this.clearStateGUI();
        this.labelStatesCountNum.setText(Integer.toString(MainWindowMaker.currentCA.getStates().getList().size()));
        this.clearStateGUI();
        this.spinnerId.setValue(MainWindowMaker.currentCA.getStates().getNextHighestId());
    }

    /**
     *
     * Baja los datos del GUI en un estado y luego lo agrega al proyecto.
     */
    private void addState()
    {
        String id;

        id = this.spinnerId.getValue().toString();
        
        Short sid = Short.valueOf(id);
        String text = this.tfText.getText();
        Double value = (Double) spinnerValue.getValue();
        Color color = this.bStateColor.getBackground();

        if (MainWindowMaker.currentCA.getStates().getState(sid) == null)
        {
            State newState = new State(sid, text, value, color);
            MainWindowMaker.currentCA.getStates().add(newState);
        }
        else
        {
            MainWindowMaker.currentCA.getStates().getState(sid).setText(text);
            MainWindowMaker.currentCA.getStates().getState(sid).setValue(value);
            MainWindowMaker.currentCA.getStates().getState(sid).setColor(color);
        }

        this.cbStates.setModel(new DefaultComboBoxModel(MainWindowMaker.currentCA.getStates().getIdAsAStringArray()));
        this.labelStatesCountNum.setText(Integer.toString(MainWindowMaker.currentCA.getStates().getList().size()));

    }

    /**
     * 
     * Elimina el estado seleccionado en el comboBox del ADAC.
     */
    private void delState()
    {
        if (MainWindowMaker.currentCA.getStates().getList().size() > 0)
        {
            MainWindowMaker.currentCA.getStates().getList().remove(MainWindowMaker.currentCA.getStates().getList().get(this.cbStates.getSelectedIndex()));
            this.cbStates.setModel(new DefaultComboBoxModel(MainWindowMaker.currentCA.getStates().getIdAsAStringArray()));
            this.labelStatesCountNum.setText(Integer.toString(MainWindowMaker.currentCA.getStates().getList().size()));

            if (MainWindowMaker.currentCA.getStates().getList().size() > 0)
            {
                this.loadStateGUI(MainWindowMaker.currentCA.getStates().getList().get(0));
            } else
            {
                this.clearStateGUI();
            }
        }
    }

    /**
     *  Elimina todos los estados
     */
    private void delAllStates()
    {
        int n = MainWindowMaker.currentCA.getStates().getList().size();
        for(int i = 1; i<=n; i++)
        {
            MainWindowMaker.currentCA.getStates().getList().remove(MainWindowMaker.currentCA.getStates().getList().get(this.cbStates.getSelectedIndex()));
            this.cbStates.setModel(new DefaultComboBoxModel(MainWindowMaker.currentCA.getStates().getIdAsAStringArray()));
            this.labelStatesCountNum.setText(Integer.toString(MainWindowMaker.currentCA.getStates().getList().size()));
        }
    }

    /**
     * Carga los datos del compoente States y llena los campos del diálogo de
     * Estados
     *
     * @param editOption
     * @param selectedFile
     * @return
     */
    public Automaton load(int editOption, String selectedFile)
    {
        switch (editOption)
        {
            case SacUtility.LOAD_FILE: //cargar de archivo de estados
            {
                selectedFile = SacFileManager.getSacFileName(this, SacFileManager.SAC_STATES);

                if (selectedFile == null)
                {
                    return null; //No file selected then do nothing...
                }

                MainWindowMaker.currentCA.setStates(StatesParser.parse(SacFileManager.readSacFile(selectedFile)));

                break;
            }
            case SacUtility.ADD_NEW: //agregar un nuevo conjunto de estados
            {
                MainWindowMaker.currentCA.setStates(new States());
            }

            case SacUtility.EDIT_CURRENT:
            {
                if (MainWindowMaker.currentCA.getStates() == null)
                {
                    MainWindowMaker.currentCA.setStates(new States());
                }
                break;
            }
        }

        this.loadStatesGUI(MainWindowMaker.currentCA.getStates());
        return MainWindowMaker.currentCA;
    }

    /**
     * Llena los campo del dialogo de Estados segun la opción
     */
    private void clearStateGUI()
    {
        this.spinnerId.setValue(0);
        this.tfText.setText("");
        this.spinnerValue.setValue(0.0);
        this.bStateColor.setBackground(State.DEFAULT_COLOR);
    }
   
    /**
     * Llena los campos del GUI con los datos del conjunto de estados.
     *
     * @param states
     */
    private void loadStatesGUI(States states)
    {
        Short selectedId;

        this.tfStatesName.setText(states.getName());
        this.taStatesDescription.setText(states.getDescription());
        this.labelStatesCountNum.setText(Integer.toString(states.getList().size()));
        this.cbStates.setModel(new DefaultComboBoxModel(states.getIdAsAStringArray()));
        selectedId = Short.parseShort(this.cbStates.getSelectedItem().toString());
        this.loadStateGUI(MainWindowMaker.currentCA.getStates().getState(selectedId));
    }

    /**
     * Llena los campos del GUI con los datos del estado.
     *
     * @param nState
     */
    private void loadStateGUI(State nState)
    {
        this.spinnerId.setValue(nState.getId());
        this.tfText.setText(nState.getText());
        this.spinnerValue.setValue(nState.getValue());
        this.bStateColor.setBackground(nState.getColor());
    }

  

    /**
     * Salva la configuración de estados en un archivo.
     */
    private void save()
    {
        if (!this.tfStatesName.getText().isEmpty())
        {
            if (!MainWindowMaker.currentCA.getStates().getList().isEmpty())
            {
                MainWindowMaker.currentCA.getStates().setName(this.tfStatesName.getText());
                MainWindowMaker.currentCA.getStates().setDescription(this.taStatesDescription.getText());

                SacFileManager.saveFile(MainWindowMaker.currentCA.getStates().getName(),
                                        MainWindowMaker.currentCA.getStates().toString(),
                                        SacFileManager.SAC_STATES,
                                        this);
            }
            else
            {
                SacUtility.showWarning("Debe existir al menos un estado para que el archivo pueda ser creado.");
            }

        }
        else
        {
            SacUtility.showWarning("El atributo de nombre no puede estar vacio");
        }
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents()
    {

        stateColorChooser = new javax.swing.JColorChooser();
        panelStateBackground = new javax.swing.JPanel();
        panelName = new javax.swing.JPanel();
        tfStatesName = new javax.swing.JTextField();
        bSave = new javax.swing.JButton();
        bRestart = new javax.swing.JButton();
        bLoad = new javax.swing.JButton();
        jScrollPane1 = new javax.swing.JScrollPane();
        taStatesDescription = new javax.swing.JTextArea();
        panelState = new javax.swing.JPanel();
        jPanelStatesName = new javax.swing.JPanel();
        cbStates = new javax.swing.JComboBox<>();
        bDelete = new javax.swing.JButton();
        bCreate = new javax.swing.JButton();
        labelStatesCount = new javax.swing.JLabel();
        labelStatesCountNum = new javax.swing.JLabel();
        jPanel1 = new javax.swing.JPanel();
        jLabel1 = new javax.swing.JLabel();
        labelText = new javax.swing.JLabel();
        tfText = new javax.swing.JTextField();
        labelValue = new javax.swing.JLabel();
        bStateColor = new javax.swing.JButton();
        spinnerValue = new javax.swing.JSpinner();
        bAdd = new javax.swing.JButton();
        spinnerId = new javax.swing.JSpinner();
        menuBarStates = new javax.swing.JMenuBar();
        miFile = new javax.swing.JMenu();
        miLoad = new javax.swing.JMenuItem();
        miSave = new javax.swing.JMenuItem();
        miRestart = new javax.swing.JMenuItem();
        miExit = new javax.swing.JMenuItem();

        setTitle("SSACC - Estados");
        setResizable(false);
        addWindowListener(new java.awt.event.WindowAdapter()
        {
            public void windowClosing(java.awt.event.WindowEvent evt)
            {
                formWindowClosing(evt);
            }
        });

        panelStateBackground.setBorder(javax.swing.BorderFactory.createEtchedBorder());

        panelName.setBorder(javax.swing.BorderFactory.createEtchedBorder());

        bSave.setIcon(new javax.swing.ImageIcon(getClass().getResource("/luz/ve/ssacc/maker/gui/images/SSACC Ok 20x20.png"))); // NOI18N
        bSave.setToolTipText("Aceptar y agregar estados al automata celular.");
        bSave.addActionListener(new java.awt.event.ActionListener()
        {
            public void actionPerformed(java.awt.event.ActionEvent evt)
            {
                bSaveActionPerformed(evt);
            }
        });

        bRestart.setIcon(new javax.swing.ImageIcon(getClass().getResource("/luz/ve/ssacc/maker/gui/images/SSACC - redo 20x20.png"))); // NOI18N
        bRestart.setToolTipText("Reiniciar ");
        bRestart.addActionListener(new java.awt.event.ActionListener()
        {
            public void actionPerformed(java.awt.event.ActionEvent evt)
            {
                bRestartActionPerformed(evt);
            }
        });

        bLoad.setIcon(new javax.swing.ImageIcon(getClass().getResource("/luz/ve/ssacc/maker/gui/images/SSACC Open 20x20.png"))); // NOI18N
        bLoad.setToolTipText("Reiniciar ");
        bLoad.addActionListener(new java.awt.event.ActionListener()
        {
            public void actionPerformed(java.awt.event.ActionEvent evt)
            {
                bLoadActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout panelNameLayout = new javax.swing.GroupLayout(panelName);
        panelName.setLayout(panelNameLayout);
        panelNameLayout.setHorizontalGroup(
            panelNameLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(panelNameLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(tfStatesName, javax.swing.GroupLayout.PREFERRED_SIZE, 247, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(bSave)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(bRestart)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(bLoad)
                .addGap(17, 17, 17))
        );
        panelNameLayout.setVerticalGroup(
            panelNameLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, panelNameLayout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGroup(panelNameLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addGroup(panelNameLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addComponent(bSave)
                        .addComponent(bRestart)
                        .addComponent(bLoad))
                    .addComponent(tfStatesName, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap())
        );

        taStatesDescription.setColumns(20);
        taStatesDescription.setRows(5);
        taStatesDescription.setWrapStyleWord(true);
        jScrollPane1.setViewportView(taStatesDescription);

        panelState.setBorder(javax.swing.BorderFactory.createTitledBorder("Estado"));

        jPanelStatesName.setBorder(javax.swing.BorderFactory.createEtchedBorder());

        cbStates.addActionListener(new java.awt.event.ActionListener()
        {
            public void actionPerformed(java.awt.event.ActionEvent evt)
            {
                cbStatesActionPerformed(evt);
            }
        });

        bDelete.setIcon(new javax.swing.ImageIcon(getClass().getResource("/luz/ve/ssacc/maker/gui/images/SSACC Delete 20x20.png"))); // NOI18N
        bDelete.setToolTipText("Borrar estado seleccionado");
        bDelete.addActionListener(new java.awt.event.ActionListener()
        {
            public void actionPerformed(java.awt.event.ActionEvent evt)
            {
                bDeleteActionPerformed(evt);
            }
        });

        bCreate.setIcon(new javax.swing.ImageIcon(getClass().getResource("/luz/ve/ssacc/maker/gui/images/SSACC Button Create 20x20.png"))); // NOI18N
        bCreate.setToolTipText("Agregar un nuevo estado.");
        bCreate.addActionListener(new java.awt.event.ActionListener()
        {
            public void actionPerformed(java.awt.event.ActionEvent evt)
            {
                bCreateActionPerformed(evt);
            }
        });

        labelStatesCount.setText("# Estados:");

        labelStatesCountNum.setText("000");

        javax.swing.GroupLayout jPanelStatesNameLayout = new javax.swing.GroupLayout(jPanelStatesName);
        jPanelStatesName.setLayout(jPanelStatesNameLayout);
        jPanelStatesNameLayout.setHorizontalGroup(
            jPanelStatesNameLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanelStatesNameLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanelStatesNameLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanelStatesNameLayout.createSequentialGroup()
                        .addComponent(cbStates, javax.swing.GroupLayout.PREFERRED_SIZE, 92, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(18, 18, 18)
                        .addComponent(bCreate, javax.swing.GroupLayout.PREFERRED_SIZE, 44, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(bDelete, javax.swing.GroupLayout.PREFERRED_SIZE, 46, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(jPanelStatesNameLayout.createSequentialGroup()
                        .addComponent(labelStatesCount)
                        .addGap(18, 18, 18)
                        .addComponent(labelStatesCountNum)))
                .addContainerGap(16, Short.MAX_VALUE))
        );
        jPanelStatesNameLayout.setVerticalGroup(
            jPanelStatesNameLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanelStatesNameLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanelStatesNameLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(bDelete, javax.swing.GroupLayout.PREFERRED_SIZE, 28, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(cbStates, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(bCreate, javax.swing.GroupLayout.PREFERRED_SIZE, 28, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 23, Short.MAX_VALUE)
                .addGroup(jPanelStatesNameLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(labelStatesCount)
                    .addComponent(labelStatesCountNum))
                .addContainerGap())
        );

        jPanelStatesNameLayout.linkSize(javax.swing.SwingConstants.VERTICAL, new java.awt.Component[] {bDelete, cbStates});

        jPanel1.setBorder(javax.swing.BorderFactory.createEtchedBorder());

        jLabel1.setText("ID:");

        labelText.setText("Texto:");

        labelValue.setText("Valor:");

        bStateColor.setBackground(new java.awt.Color(255, 0, 31));
        bStateColor.setFont(new java.awt.Font("Ubuntu", 1, 15)); // NOI18N
        bStateColor.setText("Color");
        bStateColor.addActionListener(new java.awt.event.ActionListener()
        {
            public void actionPerformed(java.awt.event.ActionEvent evt)
            {
                bStateColorActionPerformed(evt);
            }
        });

        spinnerValue.setModel(new javax.swing.SpinnerNumberModel(0.0d, null, null, 0.5d));

        bAdd.setIcon(new javax.swing.ImageIcon(getClass().getResource("/luz/ve/ssacc/maker/gui/images/SSACC Add Left 20x20.png"))); // NOI18N
        bAdd.setToolTipText("Agregar estado");
        bAdd.addActionListener(new java.awt.event.ActionListener()
        {
            public void actionPerformed(java.awt.event.ActionEvent evt)
            {
                bAddActionPerformed(evt);
            }
        });

        spinnerId.setModel(new javax.swing.SpinnerNumberModel(0, 0, 9999, 1));

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addContainerGap()
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                            .addGroup(javax.swing.GroupLayout.Alignment.LEADING, jPanel1Layout.createSequentialGroup()
                                .addComponent(bAdd, javax.swing.GroupLayout.PREFERRED_SIZE, 70, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(34, 34, 34)
                                .addComponent(jLabel1)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(spinnerId, javax.swing.GroupLayout.DEFAULT_SIZE, 108, Short.MAX_VALUE))
                            .addGroup(javax.swing.GroupLayout.Alignment.LEADING, jPanel1Layout.createSequentialGroup()
                                .addComponent(labelText)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(tfText))))
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGap(18, 18, 18)
                        .addComponent(labelValue)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(spinnerValue, javax.swing.GroupLayout.PREFERRED_SIZE, 99, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(18, 18, 18)
                        .addComponent(bStateColor, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)))
                .addContainerGap())
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(bAdd, javax.swing.GroupLayout.PREFERRED_SIZE, 28, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(jLabel1)
                        .addComponent(spinnerId, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(labelText)
                    .addComponent(tfText, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(bStateColor)
                    .addComponent(labelValue)
                    .addComponent(spinnerValue, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        jPanel1Layout.linkSize(javax.swing.SwingConstants.VERTICAL, new java.awt.Component[] {bStateColor, jLabel1, labelText, labelValue, spinnerValue, tfText});

        javax.swing.GroupLayout panelStateLayout = new javax.swing.GroupLayout(panelState);
        panelState.setLayout(panelStateLayout);
        panelStateLayout.setHorizontalGroup(
            panelStateLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(panelStateLayout.createSequentialGroup()
                .addComponent(jPanelStatesName, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 17, Short.MAX_VALUE)
                .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(58, 58, 58))
        );
        panelStateLayout.setVerticalGroup(
            panelStateLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(panelStateLayout.createSequentialGroup()
                .addGroup(panelStateLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jPanelStatesName, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(22, Short.MAX_VALUE))
        );

        javax.swing.GroupLayout panelStateBackgroundLayout = new javax.swing.GroupLayout(panelStateBackground);
        panelStateBackground.setLayout(panelStateBackgroundLayout);
        panelStateBackgroundLayout.setHorizontalGroup(
            panelStateBackgroundLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(panelStateBackgroundLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(panelStateBackgroundLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(panelName, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(panelState, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jScrollPane1))
                .addGap(0, 17, Short.MAX_VALUE))
        );
        panelStateBackgroundLayout.setVerticalGroup(
            panelStateBackgroundLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(panelStateBackgroundLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(panelName, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(panelState, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        miFile.setMnemonic('A');
        miFile.setText("Archivo");

        miLoad.setMnemonic('C');
        miLoad.setText("Cargar");
        miLoad.addActionListener(new java.awt.event.ActionListener()
        {
            public void actionPerformed(java.awt.event.ActionEvent evt)
            {
                miLoadActionPerformed(evt);
            }
        });
        miFile.add(miLoad);

        miSave.setMnemonic('G');
        miSave.setText("Guardar");
        miSave.setToolTipText("");
        miSave.addActionListener(new java.awt.event.ActionListener()
        {
            public void actionPerformed(java.awt.event.ActionEvent evt)
            {
                miSaveActionPerformed(evt);
            }
        });
        miFile.add(miSave);

        miRestart.setMnemonic('R');
        miRestart.setText("Reiniciar");
        miRestart.addActionListener(new java.awt.event.ActionListener()
        {
            public void actionPerformed(java.awt.event.ActionEvent evt)
            {
                miRestartActionPerformed(evt);
            }
        });
        miFile.add(miRestart);

        miExit.setMnemonic('S');
        miExit.setText("Salir");
        miExit.addActionListener(new java.awt.event.ActionListener()
        {
            public void actionPerformed(java.awt.event.ActionEvent evt)
            {
                miExitActionPerformed(evt);
            }
        });
        miFile.add(miExit);

        menuBarStates.add(miFile);

        setJMenuBar(menuBarStates);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(panelStateBackground, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(panelStateBackground, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void miLoadActionPerformed(java.awt.event.ActionEvent evt)//GEN-FIRST:event_miLoadActionPerformed
    {//GEN-HEADEREND:event_miLoadActionPerformed
           this.load(SacUtility.LOAD_FILE, "");
    }//GEN-LAST:event_miLoadActionPerformed

    private void bStateColorActionPerformed(java.awt.event.ActionEvent evt)//GEN-FIRST:event_bStateColorActionPerformed
    {//GEN-HEADEREND:event_bStateColorActionPerformed
        SacGuiManager.openColorChooser(this, this.bStateColor);
    }//GEN-LAST:event_bStateColorActionPerformed

    private void cbStatesActionPerformed(java.awt.event.ActionEvent evt)//GEN-FIRST:event_cbStatesActionPerformed
    {//GEN-HEADEREND:event_cbStatesActionPerformed
       Short id = Short.parseShort(this.cbStates.getSelectedItem().toString());
       this.loadStateGUI(MainWindowMaker.currentCA.getStates().getState(id));      
    }//GEN-LAST:event_cbStatesActionPerformed

    private void bAddActionPerformed(java.awt.event.ActionEvent evt)//GEN-FIRST:event_bAddActionPerformed
    {//GEN-HEADEREND:event_bAddActionPerformed
        this.addState();
    }//GEN-LAST:event_bAddActionPerformed

    private void bCreateActionPerformed(java.awt.event.ActionEvent evt)//GEN-FIRST:event_bCreateActionPerformed
    {//GEN-HEADEREND:event_bCreateActionPerformed
        this.addNewState();
    }//GEN-LAST:event_bCreateActionPerformed

    private void bSaveActionPerformed(java.awt.event.ActionEvent evt)//GEN-FIRST:event_bSaveActionPerformed
    {//GEN-HEADEREND:event_bSaveActionPerformed
        this.save();
    }//GEN-LAST:event_bSaveActionPerformed

    private void bDeleteActionPerformed(java.awt.event.ActionEvent evt)//GEN-FIRST:event_bDeleteActionPerformed
    {//GEN-HEADEREND:event_bDeleteActionPerformed
       delState();
    }//GEN-LAST:event_bDeleteActionPerformed

    private void miSaveActionPerformed(java.awt.event.ActionEvent evt)//GEN-FIRST:event_miSaveActionPerformed
    {//GEN-HEADEREND:event_miSaveActionPerformed
        save();
    }//GEN-LAST:event_miSaveActionPerformed

    private void miRestartActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_miRestartActionPerformed
        delAllStates();
        clearStateGUI();
    }//GEN-LAST:event_miRestartActionPerformed

    private void bRestartActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_bRestartActionPerformed
        delAllStates();
        clearStateGUI();
    }//GEN-LAST:event_bRestartActionPerformed

    private void bLoadActionPerformed(java.awt.event.ActionEvent evt)//GEN-FIRST:event_bLoadActionPerformed
    {//GEN-HEADEREND:event_bLoadActionPerformed
        this.load(SacUtility.LOAD_FILE, "");
    }//GEN-LAST:event_bLoadActionPerformed

    private void miExitActionPerformed(java.awt.event.ActionEvent evt)//GEN-FIRST:event_miExitActionPerformed
    {//GEN-HEADEREND:event_miExitActionPerformed
        SacGuiManager.closeWindow(this, parentWindow);
    }//GEN-LAST:event_miExitActionPerformed

    private void formWindowClosing(java.awt.event.WindowEvent evt) {//GEN-FIRST:event_formWindowClosing
        SacGuiManager.closeWindow(this, parentWindow);
    }//GEN-LAST:event_formWindowClosing

    


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton bAdd;
    private javax.swing.JButton bCreate;
    private javax.swing.JButton bDelete;
    private javax.swing.JButton bLoad;
    private javax.swing.JButton bRestart;
    private javax.swing.JButton bSave;
    private javax.swing.JButton bStateColor;
    private javax.swing.JComboBox<String> cbStates;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanelStatesName;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JLabel labelStatesCount;
    private javax.swing.JLabel labelStatesCountNum;
    private javax.swing.JLabel labelText;
    private javax.swing.JLabel labelValue;
    private javax.swing.JMenuBar menuBarStates;
    private javax.swing.JMenuItem miExit;
    private javax.swing.JMenu miFile;
    private javax.swing.JMenuItem miLoad;
    private javax.swing.JMenuItem miRestart;
    private javax.swing.JMenuItem miSave;
    private javax.swing.JPanel panelName;
    private javax.swing.JPanel panelState;
    private javax.swing.JPanel panelStateBackground;
    private javax.swing.JSpinner spinnerId;
    private javax.swing.JSpinner spinnerValue;
    private javax.swing.JColorChooser stateColorChooser;
    private javax.swing.JTextArea taStatesDescription;
    private javax.swing.JTextField tfStatesName;
    private javax.swing.JTextField tfText;
    // End of variables declaration//GEN-END:variables
}
