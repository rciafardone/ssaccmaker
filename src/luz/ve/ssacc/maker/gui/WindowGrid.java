/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package luz.ve.ssacc.maker.gui;

import java.awt.Color;
import javax.swing.DefaultComboBoxModel;
import luz.ve.ssacc.maker.gui.extras.SacGuiManager;
import luz.ve.ssacc.engine.components.Automaton;
import luz.ve.ssacc.engine.components.Grids;
import luz.ve.ssacc.engine.components.State;
import luz.ve.ssacc.engine.parsers.GridsParser;
import luz.ve.ssacc.utilities.SacFileManager;
import luz.ve.ssacc.utilities.*;


/**
 *
 * @author Renzo Ciafardone Sciannelli <rciafardone@gmail.com>
 */
public class WindowGrid extends javax.swing.JFrame
{
   
    MainWindowMaker parentWindow;
    
    /**
     * Creates new form EditorGrid
     * @param mainFrame
     */
    public WindowGrid(MainWindowMaker mainFrame)
    {
        initComponents();
        initialConfig();
        parentWindow = mainFrame;
    }

    private void initialConfig()
    {
        this.pack();
    }

    /**
     * Salva el contenido de la instancia del objeto GRIDS en un archivo.
     */
    private void save()
    {
        
        this.updateGridsData();
        SacFileManager.saveFile(MainWindowMaker.currentCA.getGrids().getName(), 
                                MainWindowMaker.currentCA.getGrids().toString(), 
                                SacFileManager.SAC_GRID, 
                                this);
    }

    /**
     * Devuelve todos los datos a sus valores por defecto y limpia la interfaz
     */
    private void reset()
    {
        MainWindowMaker.currentCA.setGrids(new Grids());
        this.loadGridsGui();
    }

    /**
     * Llena los campo del dialogo de Estados segun la opción.G.
     * @param editOption
     * @param selectedFile
     * @return 
     */
    public Automaton load(int editOption, String selectedFile)
    {
        switch (editOption)
        {
            case SacUtility.LOAD_FILE: //cargar de archivo de reticulas
            {
                selectedFile = SacFileManager.getSacFileName(this, SacFileManager.SAC_GRID);
                if (selectedFile == null)
                {
                    return null;
                }

                MainWindowMaker.currentCA.setGrids(GridsParser.parse(SacFileManager.readSacFile(selectedFile)));
                break;
            }
            case SacUtility.ADD_NEW: //agrega un nuevo conjunto de reticulas
            {
                MainWindowMaker.currentCA.setGrids(new Grids());
                break;
            }
            case SacUtility.EDIT_CURRENT: //editar el conjunto de reticulas actualmente cargado
            {
                if (MainWindowMaker.currentCA.getGrids() == null)
                {
                    MainWindowMaker.currentCA.setGrids(new Grids());
                }
                break;
            }

        }

        this.loadGridsGui();
        return MainWindowMaker.currentCA;
    }

    /**
     * Toma los datos de la ventana y actualiza los del Grid
     */
    private void updateGridsData()
    {
        Short stateId = Short.parseShort(this.cbBackgroundState.getSelectedItem().toString());
        try
        {
            this.spinnerHeight.commitEdit();
            this.spinnerWidth.commitEdit();
            this.spinnerCellSize.commitEdit();
            this.spinnerStepNumber.commitEdit();
            this.spinnerDelay.commitEdit();
        } catch (java.text.ParseException e)
        {
            SacUtility.showWarning(e.getMessage());
        }

        String name = this.tfGridName.getText();
        String desc = this.taGridDescription.getText();
        int rows = (Integer) this.spinnerHeight.getValue();
        int cols = (Integer) this.spinnerWidth.getValue();
        int cellSize = (Integer) this.spinnerCellSize.getValue();
        boolean borderFlag = this.checkBoxDrawBorder.isSelected();
        Color borderColor = bChooseBorderColor.getBackground();
        int frontier = this.comboFrontierType.getSelectedIndex();
        State background = MainWindowMaker.currentCA.getStates().getState(stateId);
        int steps = (Integer) this.spinnerStepNumber.getValue();
        int delay = (Integer) this.spinnerDelay.getValue();

        
        MainWindowMaker.currentCA.getGrids().setName(name);
        MainWindowMaker.currentCA.getGrids().setDescription(desc);
        MainWindowMaker.currentCA.getGrids().setRows(rows);
        MainWindowMaker.currentCA.getGrids().setColumns(cols);
        MainWindowMaker.currentCA.getGrids().setCellSize(cellSize);
        MainWindowMaker.currentCA.getGrids().setBorderFlag(borderFlag);
        MainWindowMaker.currentCA.getGrids().setBorderColor(borderColor);
        MainWindowMaker.currentCA.getGrids().setFrontierType(frontier);
        MainWindowMaker.currentCA.getGrids().setBackground(background);
        MainWindowMaker.currentCA.getGrids().setNumSteps(steps);
        MainWindowMaker.currentCA.getGrids().setAnimationDelay(delay);
        MainWindowMaker.currentCA.getGrids().resize(rows, cols);
        
    }

    /**
     * Llena los campos del GUI con los datos del conjunto de reticulas.G
     *
     * @param grids
     */
    private void loadGridsGui()
    {
        this.tfGridName.setText(MainWindowMaker.currentCA.getGrids().getName());
        this.taGridDescription.setText(MainWindowMaker.currentCA.getGrids().getDescription());
        this.spinnerCellSize.setValue(MainWindowMaker.currentCA.getGrids().getCellSize());
        this.spinnerHeight.setValue(MainWindowMaker.currentCA.getGrids().getRows());
        this.spinnerWidth.setValue(MainWindowMaker.currentCA.getGrids().getColumns());
        this.checkBoxDrawBorder.setSelected(MainWindowMaker.currentCA.getGrids().isBorderOn());
        this.bChooseBorderColor.setBackground(MainWindowMaker.currentCA.getGrids().getBorderColor());
        this.comboFrontierType.setSelectedIndex(MainWindowMaker.currentCA.getGrids().getFrontierType());
        this.cbBackgroundState.setModel(new DefaultComboBoxModel(MainWindowMaker.currentCA.getStates().getIdAsAStringArray()));
        this.cbBackgroundState.setSelectedItem(MainWindowMaker.currentCA.getGrids().getBackground().getId());
        this.spinnerStepNumber.setValue(MainWindowMaker.currentCA.getGrids().getNumSteps());
        this.spinnerDelay.setValue(MainWindowMaker.currentCA.getGrids().getAnimationDelay());
    }

    /**
     * Abre la ventana para editar el grid inicial del automata
     */
    private void openWindowGridDraw()
    {
        this.updateGridsData();
        parentWindow.editorGridDraw = new WindowGridDraw(this);
        SacGuiManager.switchWindowStatus(this, false, true);
        SacGuiManager.switchWindowStatus(parentWindow.editorGridDraw, true, true);
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents()
    {

        panelGridBackground = new javax.swing.JPanel();
        panelGridName = new javax.swing.JPanel();
        tfGridName = new javax.swing.JTextField();
        panelGridDescription = new javax.swing.JPanel();
        jScrollPane1 = new javax.swing.JScrollPane();
        taGridDescription = new javax.swing.JTextArea();
        panelGridConfig = new javax.swing.JPanel();
        labelDelay = new javax.swing.JLabel();
        labelCellSize = new javax.swing.JLabel();
        comboFrontierType = new javax.swing.JComboBox<>();
        labelBackgroundState = new javax.swing.JLabel();
        labelFrontierType = new javax.swing.JLabel();
        cbBackgroundState = new javax.swing.JComboBox<>();
        labelNumSteps = new javax.swing.JLabel();
        labelGridHeight = new javax.swing.JLabel();
        labelGridWidth = new javax.swing.JLabel();
        spinnerHeight = new javax.swing.JSpinner();
        spinnerWidth = new javax.swing.JSpinner();
        spinnerDelay = new javax.swing.JSpinner();
        spinnerCellSize = new javax.swing.JSpinner();
        checkBoxDrawBorder = new javax.swing.JCheckBox();
        bChooseBorderColor = new javax.swing.JButton();
        spinnerStepNumber = new javax.swing.JSpinner();
        bSave = new javax.swing.JButton();
        bOpenGridEdit = new javax.swing.JButton();
        bReset = new javax.swing.JButton();
        bRulesLoad = new javax.swing.JButton();
        menuBarStates = new javax.swing.JMenuBar();
        miFile = new javax.swing.JMenu();
        miLoad = new javax.swing.JMenuItem();
        miSave = new javax.swing.JMenuItem();
        miRestart = new javax.swing.JMenuItem();
        miExit = new javax.swing.JMenuItem();

        setTitle("SSACC - Retícula");
        addWindowListener(new java.awt.event.WindowAdapter()
        {
            public void windowClosing(java.awt.event.WindowEvent evt)
            {
                formWindowClosing(evt);
            }
        });

        panelGridBackground.setBorder(javax.swing.BorderFactory.createEtchedBorder());

        panelGridName.setBorder(javax.swing.BorderFactory.createTitledBorder("Nombre"));

        javax.swing.GroupLayout panelGridNameLayout = new javax.swing.GroupLayout(panelGridName);
        panelGridName.setLayout(panelGridNameLayout);
        panelGridNameLayout.setHorizontalGroup(
            panelGridNameLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(panelGridNameLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(tfGridName)
                .addContainerGap())
        );
        panelGridNameLayout.setVerticalGroup(
            panelGridNameLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(panelGridNameLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(tfGridName, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        panelGridDescription.setBorder(javax.swing.BorderFactory.createTitledBorder("Descripción"));

        taGridDescription.setColumns(20);
        taGridDescription.setRows(5);
        jScrollPane1.setViewportView(taGridDescription);

        javax.swing.GroupLayout panelGridDescriptionLayout = new javax.swing.GroupLayout(panelGridDescription);
        panelGridDescription.setLayout(panelGridDescriptionLayout);
        panelGridDescriptionLayout.setHorizontalGroup(
            panelGridDescriptionLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(panelGridDescriptionLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jScrollPane1)
                .addContainerGap())
        );
        panelGridDescriptionLayout.setVerticalGroup(
            panelGridDescriptionLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(panelGridDescriptionLayout.createSequentialGroup()
                .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 117, Short.MAX_VALUE)
                .addContainerGap())
        );

        panelGridConfig.setBorder(javax.swing.BorderFactory.createTitledBorder("Configuración"));

        labelDelay.setText("Retardo Animación:");

        labelCellSize.setText("Tamaño Celda:");

        comboFrontierType.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "Abierta", "Cerrada" }));
        comboFrontierType.addActionListener(new java.awt.event.ActionListener()
        {
            public void actionPerformed(java.awt.event.ActionEvent evt)
            {
                comboFrontierTypeActionPerformed(evt);
            }
        });

        labelBackgroundState.setText("Estado de Fondo:");

        labelFrontierType.setText("Tipo de Frontera:");

        cbBackgroundState.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "fondo" }));
        cbBackgroundState.addActionListener(new java.awt.event.ActionListener()
        {
            public void actionPerformed(java.awt.event.ActionEvent evt)
            {
                cbBackgroundStateActionPerformed(evt);
            }
        });

        labelNumSteps.setText("Número de Pasos:");

        labelGridHeight.setText("Alto:");

        labelGridWidth.setText("Ancho:");

        spinnerHeight.setModel(new javax.swing.SpinnerNumberModel(3, 1, 500, 1));

        spinnerWidth.setModel(new javax.swing.SpinnerNumberModel(3, 1, 500, 1));

        spinnerDelay.setModel(new javax.swing.SpinnerNumberModel(100, 0, 5000, 10));
        spinnerDelay.addPropertyChangeListener(new java.beans.PropertyChangeListener()
        {
            public void propertyChange(java.beans.PropertyChangeEvent evt)
            {
                spinnerDelayPropertyChange(evt);
            }
        });

        spinnerCellSize.setModel(new javax.swing.SpinnerNumberModel(5, 1, 256, 1));
        spinnerCellSize.addPropertyChangeListener(new java.beans.PropertyChangeListener()
        {
            public void propertyChange(java.beans.PropertyChangeEvent evt)
            {
                spinnerCellSizePropertyChange(evt);
            }
        });

        checkBoxDrawBorder.setSelected(true);
        checkBoxDrawBorder.setText("Borde");
        checkBoxDrawBorder.addPropertyChangeListener(new java.beans.PropertyChangeListener()
        {
            public void propertyChange(java.beans.PropertyChangeEvent evt)
            {
                checkBoxDrawBorderPropertyChange(evt);
            }
        });

        bChooseBorderColor.setText("Color Borde");
        bChooseBorderColor.addActionListener(new java.awt.event.ActionListener()
        {
            public void actionPerformed(java.awt.event.ActionEvent evt)
            {
                bChooseBorderColorActionPerformed(evt);
            }
        });

        spinnerStepNumber.setModel(new javax.swing.SpinnerNumberModel(100, 1, null, 1));
        spinnerStepNumber.addPropertyChangeListener(new java.beans.PropertyChangeListener()
        {
            public void propertyChange(java.beans.PropertyChangeEvent evt)
            {
                spinnerStepNumberPropertyChange(evt);
            }
        });

        javax.swing.GroupLayout panelGridConfigLayout = new javax.swing.GroupLayout(panelGridConfig);
        panelGridConfig.setLayout(panelGridConfigLayout);
        panelGridConfigLayout.setHorizontalGroup(
            panelGridConfigLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(panelGridConfigLayout.createSequentialGroup()
                .addGroup(panelGridConfigLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(panelGridConfigLayout.createSequentialGroup()
                        .addContainerGap()
                        .addComponent(labelCellSize)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(spinnerCellSize, javax.swing.GroupLayout.PREFERRED_SIZE, 59, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(18, 18, 18)
                        .addComponent(checkBoxDrawBorder)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(bChooseBorderColor))
                    .addGroup(panelGridConfigLayout.createSequentialGroup()
                        .addGap(61, 61, 61)
                        .addComponent(labelGridHeight)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(spinnerHeight, javax.swing.GroupLayout.PREFERRED_SIZE, 64, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(18, 18, 18)
                        .addComponent(labelGridWidth)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(spinnerWidth, javax.swing.GroupLayout.PREFERRED_SIZE, 59, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(0, 0, Short.MAX_VALUE)))
                .addContainerGap())
            .addGroup(panelGridConfigLayout.createSequentialGroup()
                .addGap(47, 47, 47)
                .addGroup(panelGridConfigLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(labelFrontierType, javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(labelBackgroundState, javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(labelNumSteps, javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(labelDelay, javax.swing.GroupLayout.Alignment.TRAILING))
                .addGap(18, 18, 18)
                .addGroup(panelGridConfigLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(panelGridConfigLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                        .addComponent(comboFrontierType, 0, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(cbBackgroundState, 0, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(spinnerDelay, javax.swing.GroupLayout.PREFERRED_SIZE, 150, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addComponent(spinnerStepNumber, javax.swing.GroupLayout.PREFERRED_SIZE, 150, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        panelGridConfigLayout.linkSize(javax.swing.SwingConstants.HORIZONTAL, new java.awt.Component[] {spinnerHeight, spinnerWidth});

        panelGridConfigLayout.setVerticalGroup(
            panelGridConfigLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(panelGridConfigLayout.createSequentialGroup()
                .addGroup(panelGridConfigLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(labelGridHeight)
                    .addComponent(labelGridWidth)
                    .addComponent(spinnerHeight, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(spinnerWidth, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(panelGridConfigLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(labelCellSize)
                    .addComponent(spinnerCellSize, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(bChooseBorderColor)
                    .addComponent(checkBoxDrawBorder))
                .addGap(18, 18, 18)
                .addGroup(panelGridConfigLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(labelFrontierType)
                    .addComponent(comboFrontierType, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(panelGridConfigLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(labelBackgroundState)
                    .addComponent(cbBackgroundState, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(12, 12, 12)
                .addGroup(panelGridConfigLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(labelNumSteps)
                    .addComponent(spinnerStepNumber, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(panelGridConfigLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(labelDelay)
                    .addComponent(spinnerDelay, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        panelGridConfigLayout.linkSize(javax.swing.SwingConstants.VERTICAL, new java.awt.Component[] {comboFrontierType, labelBackgroundState, labelCellSize, labelDelay});

        panelGridConfigLayout.linkSize(javax.swing.SwingConstants.VERTICAL, new java.awt.Component[] {spinnerHeight, spinnerWidth});

        bSave.setIcon(new javax.swing.ImageIcon(getClass().getResource("/luz/ve/ssacc/maker/gui/images/SSACC Ok 20x20.png"))); // NOI18N
        bSave.addActionListener(new java.awt.event.ActionListener()
        {
            public void actionPerformed(java.awt.event.ActionEvent evt)
            {
                bSaveActionPerformed(evt);
            }
        });

        bOpenGridEdit.setIcon(new javax.swing.ImageIcon(getClass().getResource("/luz/ve/ssacc/maker/gui/images/SSACC grid draw 30x30.png"))); // NOI18N
        bOpenGridEdit.addActionListener(new java.awt.event.ActionListener()
        {
            public void actionPerformed(java.awt.event.ActionEvent evt)
            {
                bOpenGridEditActionPerformed(evt);
            }
        });

        bReset.setIcon(new javax.swing.ImageIcon(getClass().getResource("/luz/ve/ssacc/maker/gui/images/SSACC - redo 20x20.png"))); // NOI18N
        bReset.setToolTipText("Reiniciar");
        bReset.addActionListener(new java.awt.event.ActionListener()
        {
            public void actionPerformed(java.awt.event.ActionEvent evt)
            {
                bResetActionPerformed(evt);
            }
        });

        bRulesLoad.setIcon(new javax.swing.ImageIcon(getClass().getResource("/luz/ve/ssacc/maker/gui/images/SSACC Open 20x20.png"))); // NOI18N
        bRulesLoad.setToolTipText("Cargar archivo de reglas");
        bRulesLoad.addActionListener(new java.awt.event.ActionListener()
        {
            public void actionPerformed(java.awt.event.ActionEvent evt)
            {
                bRulesLoadActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout panelGridBackgroundLayout = new javax.swing.GroupLayout(panelGridBackground);
        panelGridBackground.setLayout(panelGridBackgroundLayout);
        panelGridBackgroundLayout.setHorizontalGroup(
            panelGridBackgroundLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(panelGridBackgroundLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(panelGridBackgroundLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(panelGridName, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(panelGridDescription, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(panelGridConfig, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addGroup(panelGridBackgroundLayout.createSequentialGroup()
                        .addComponent(bSave)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(bReset)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(bRulesLoad, javax.swing.GroupLayout.PREFERRED_SIZE, 50, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(bOpenGridEdit, javax.swing.GroupLayout.PREFERRED_SIZE, 66, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addContainerGap())
        );

        panelGridBackgroundLayout.linkSize(javax.swing.SwingConstants.HORIZONTAL, new java.awt.Component[] {bReset, bRulesLoad, bSave});

        panelGridBackgroundLayout.setVerticalGroup(
            panelGridBackgroundLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(panelGridBackgroundLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(panelGridName, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(panelGridDescription, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(panelGridConfig, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(panelGridBackgroundLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(panelGridBackgroundLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addComponent(bOpenGridEdit, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, 32, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(bSave))
                    .addGroup(panelGridBackgroundLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                        .addComponent(bReset)
                        .addComponent(bRulesLoad)))
                .addContainerGap(14, Short.MAX_VALUE))
        );

        panelGridBackgroundLayout.linkSize(javax.swing.SwingConstants.VERTICAL, new java.awt.Component[] {bReset, bRulesLoad, bSave});

        miFile.setMnemonic('A');
        miFile.setText("Archivo");

        miLoad.setMnemonic('C');
        miLoad.setText("Cargar");
        miLoad.addActionListener(new java.awt.event.ActionListener()
        {
            public void actionPerformed(java.awt.event.ActionEvent evt)
            {
                miLoadActionPerformed(evt);
            }
        });
        miFile.add(miLoad);

        miSave.setMnemonic('G');
        miSave.setText("Guardar");
        miSave.addActionListener(new java.awt.event.ActionListener()
        {
            public void actionPerformed(java.awt.event.ActionEvent evt)
            {
                miSaveActionPerformed(evt);
            }
        });
        miFile.add(miSave);

        miRestart.setMnemonic('R');
        miRestart.setText("Reiniciar");
        miRestart.addActionListener(new java.awt.event.ActionListener()
        {
            public void actionPerformed(java.awt.event.ActionEvent evt)
            {
                miRestartActionPerformed(evt);
            }
        });
        miFile.add(miRestart);

        miExit.setMnemonic('S');
        miExit.setText("Salir");
        miExit.addActionListener(new java.awt.event.ActionListener()
        {
            public void actionPerformed(java.awt.event.ActionEvent evt)
            {
                miExitActionPerformed(evt);
            }
        });
        miFile.add(miExit);

        menuBarStates.add(miFile);

        setJMenuBar(menuBarStates);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(panelGridBackground, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(panelGridBackground, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void bChooseBorderColorActionPerformed(java.awt.event.ActionEvent evt)//GEN-FIRST:event_bChooseBorderColorActionPerformed
    {//GEN-HEADEREND:event_bChooseBorderColorActionPerformed
        SacGuiManager.openColorChooser(this, bChooseBorderColor);
    }//GEN-LAST:event_bChooseBorderColorActionPerformed

    private void miLoadActionPerformed(java.awt.event.ActionEvent evt)//GEN-FIRST:event_miLoadActionPerformed
    {//GEN-HEADEREND:event_miLoadActionPerformed

    }//GEN-LAST:event_miLoadActionPerformed

    private void bSaveActionPerformed(java.awt.event.ActionEvent evt)//GEN-FIRST:event_bSaveActionPerformed
    {//GEN-HEADEREND:event_bSaveActionPerformed
        this.save();
    }//GEN-LAST:event_bSaveActionPerformed

    private void miSaveActionPerformed(java.awt.event.ActionEvent evt)//GEN-FIRST:event_miSaveActionPerformed
    {//GEN-HEADEREND:event_miSaveActionPerformed
        this.save();
    }//GEN-LAST:event_miSaveActionPerformed

    private void comboFrontierTypeActionPerformed(java.awt.event.ActionEvent evt)//GEN-FIRST:event_comboFrontierTypeActionPerformed
    {//GEN-HEADEREND:event_comboFrontierTypeActionPerformed
        MainWindowMaker.currentCA.getGrids().setFrontierType(this.comboFrontierType.getSelectedIndex());
    }//GEN-LAST:event_comboFrontierTypeActionPerformed

    private void bOpenGridEditActionPerformed(java.awt.event.ActionEvent evt)//GEN-FIRST:event_bOpenGridEditActionPerformed
    {//GEN-HEADEREND:event_bOpenGridEditActionPerformed
        this.openWindowGridDraw();
    }//GEN-LAST:event_bOpenGridEditActionPerformed

    private void miRestartActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_miRestartActionPerformed
        this.reset();
    }//GEN-LAST:event_miRestartActionPerformed

    private void miExitActionPerformed(java.awt.event.ActionEvent evt)//GEN-FIRST:event_miExitActionPerformed
    {//GEN-HEADEREND:event_miExitActionPerformed
        SacGuiManager.closeWindow(this, parentWindow);
    }//GEN-LAST:event_miExitActionPerformed

    private void formWindowClosing(java.awt.event.WindowEvent evt)//GEN-FIRST:event_formWindowClosing
    {//GEN-HEADEREND:event_formWindowClosing
        this.updateGridsData();
        SacGuiManager.closeWindow(this, parentWindow);
    }//GEN-LAST:event_formWindowClosing

    private void spinnerCellSizePropertyChange(java.beans.PropertyChangeEvent evt)//GEN-FIRST:event_spinnerCellSizePropertyChange
    {//GEN-HEADEREND:event_spinnerCellSizePropertyChange
       
    }//GEN-LAST:event_spinnerCellSizePropertyChange

    private void checkBoxDrawBorderPropertyChange(java.beans.PropertyChangeEvent evt)//GEN-FIRST:event_checkBoxDrawBorderPropertyChange
    {//GEN-HEADEREND:event_checkBoxDrawBorderPropertyChange
        
    }//GEN-LAST:event_checkBoxDrawBorderPropertyChange

    private void cbBackgroundStateActionPerformed(java.awt.event.ActionEvent evt)//GEN-FIRST:event_cbBackgroundStateActionPerformed
    {//GEN-HEADEREND:event_cbBackgroundStateActionPerformed
        
    }//GEN-LAST:event_cbBackgroundStateActionPerformed

    private void spinnerStepNumberPropertyChange(java.beans.PropertyChangeEvent evt)//GEN-FIRST:event_spinnerStepNumberPropertyChange
    {//GEN-HEADEREND:event_spinnerStepNumberPropertyChange
      
    }//GEN-LAST:event_spinnerStepNumberPropertyChange

    private void spinnerDelayPropertyChange(java.beans.PropertyChangeEvent evt)//GEN-FIRST:event_spinnerDelayPropertyChange
    {//GEN-HEADEREND:event_spinnerDelayPropertyChange
        
    }//GEN-LAST:event_spinnerDelayPropertyChange

    private void bResetActionPerformed(java.awt.event.ActionEvent evt)//GEN-FIRST:event_bResetActionPerformed
    {//GEN-HEADEREND:event_bResetActionPerformed
        this.reset();
    }//GEN-LAST:event_bResetActionPerformed

    private void bRulesLoadActionPerformed(java.awt.event.ActionEvent evt)//GEN-FIRST:event_bRulesLoadActionPerformed
    {//GEN-HEADEREND:event_bRulesLoadActionPerformed
        this.load(SacUtility.LOAD_FILE, "");
    }//GEN-LAST:event_bRulesLoadActionPerformed

  

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton bChooseBorderColor;
    private javax.swing.JButton bOpenGridEdit;
    private javax.swing.JButton bReset;
    private javax.swing.JButton bRulesLoad;
    private javax.swing.JButton bSave;
    private javax.swing.JComboBox<String> cbBackgroundState;
    private javax.swing.JCheckBox checkBoxDrawBorder;
    private javax.swing.JComboBox<String> comboFrontierType;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JLabel labelBackgroundState;
    private javax.swing.JLabel labelCellSize;
    private javax.swing.JLabel labelDelay;
    private javax.swing.JLabel labelFrontierType;
    private javax.swing.JLabel labelGridHeight;
    private javax.swing.JLabel labelGridWidth;
    private javax.swing.JLabel labelNumSteps;
    private javax.swing.JMenuBar menuBarStates;
    private javax.swing.JMenuItem miExit;
    private javax.swing.JMenu miFile;
    private javax.swing.JMenuItem miLoad;
    private javax.swing.JMenuItem miRestart;
    private javax.swing.JMenuItem miSave;
    private javax.swing.JPanel panelGridBackground;
    private javax.swing.JPanel panelGridConfig;
    private javax.swing.JPanel panelGridDescription;
    private javax.swing.JPanel panelGridName;
    private javax.swing.JSpinner spinnerCellSize;
    private javax.swing.JSpinner spinnerDelay;
    private javax.swing.JSpinner spinnerHeight;
    private javax.swing.JSpinner spinnerStepNumber;
    private javax.swing.JSpinner spinnerWidth;
    private javax.swing.JTextArea taGridDescription;
    private javax.swing.JTextField tfGridName;
    // End of variables declaration//GEN-END:variables
}
