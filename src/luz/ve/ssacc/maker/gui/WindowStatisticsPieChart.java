/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package luz.ve.ssacc.maker.gui;

/**
 *
 * @author Renzo Ciafardone Sciannelli <rciafardone@gmail.com>
 */
import java.awt.BorderLayout;
import javax.swing.JFrame;
import javax.swing.JPanel;
import luz.ve.ssacc.engine.components.State;
import luz.ve.ssacc.engine.components.Statistics;
import org.jfree.chart.ChartFactory;
import org.jfree.chart.ChartPanel;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.plot.PiePlot;
import org.jfree.data.general.DefaultPieDataset;
import org.jfree.data.general.PieDataset;

public class WindowStatisticsPieChart extends JFrame
{

    public WindowStatisticsPieChart(String windowTitle,
                                    String chartTitle,
                                    Statistics stats,
                                    Integer step)
    {
        super(windowTitle);
        initialConfig(chartTitle, stats, step);
    }
   
   private void initialConfig(String chartTitle, Statistics stats, Integer step)
    {
        this.add(createPiePanel(chartTitle, stats, step), BorderLayout.SOUTH);
        this.setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
    }
   
   public static JPanel createPiePanel( String chartTitle, Statistics stats, Integer step)
   {
      JFreeChart chart = createChart(createDataset(stats, step), chartTitle + step.toString(), stats );  
      return new ChartPanel( chart ); 
   }
 
   private static JFreeChart createChart( PieDataset dataset, String chartTitle, Statistics stats )
   {
      JFreeChart chart = ChartFactory.createPieChart(      
         chartTitle,  // chart title 
         dataset,        // data    
         true,           // include legend   
         true, 
         false);

      
        PiePlot plot = (PiePlot) chart.getPlot();

        int stateCount = 0;
        for (State state : stats.getStates())
        {
            plot.setSectionPaint(stateCount, state.getColor());
            stateCount++;
        }
      
      return chart;
   }
    
    private static PieDataset createDataset(Statistics stats, Integer step)
    {
        DefaultPieDataset dataset = new DefaultPieDataset();

        int stateNumber = stats.getStates().size();

        for (Integer dataIndex = 0; dataIndex < stateNumber; dataIndex++)
        {
            Integer stateData = stats.getData().get(step)[dataIndex];
            String stateText = stats.getStates().get(dataIndex).getText();

            dataset.setValue(stateText, stateData);
        }

        return dataset;
    }

}
