/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package luz.ve.ssacc.maker.gui;

import javax.swing.DefaultComboBoxModel;
import luz.ve.ssacc.maker.gui.extras.SacGuiManager;
import luz.ve.ssacc.engine.components.CustomPattern;
import luz.ve.ssacc.engine.parsers.CustomPatternParser;
import luz.ve.ssacc.maker.gui.extras.CanvasUtility;
import luz.ve.ssacc.utilities.SacFileManager;
import luz.ve.ssacc.utilities.SacUtility;
/**
 *
 * @author Renzo Ciafardone Sciannelli <rciafardone@gmail.com>
 */
public class WindowPattern extends javax.swing.JFrame
{

    // Automaton localAutomaton;
    MainWindowMaker parentWindow;
    CustomPattern[] patterns;
    boolean firstTimeflag = true;

    /**
     *
     * @param mainFrame
     */
    public WindowPattern(MainWindowMaker mainFrame)
    {
        initComponents();
        parentWindow = mainFrame;
        this.rbStates.setSelected(true);
    }
  
    /**
     * Devuelve el patrón local
     * @return 
     */
    public CustomPattern getLocalPattern()
    {
        return this.canvasPattern.getCurrentPattern();
    }

    /**
     * Establece el patrón local.
     * @param localPattern 
     */
    public void setLocalPattern(CustomPattern localPattern)
    {
        this.canvasPattern.setCurrentPattern(localPattern);
    }
    
    /**
     *  Llena los campos de la ventada patrón según la opción.
     * @param editOption
     * @param selectedFile
     * @return 
     */
    public CustomPattern load(int editOption, String selectedFile)
    {
        this.firstTimeflag = true;
        switch (editOption)
        {
            case SacUtility.LOAD_FILE:
            {
                this.loadPatternFile(selectedFile);
                break;
            }

            case SacUtility.ADD_NEW: 
            {
                this.addNewPattern();
                break;
            }
        }
        return this.getLocalPattern();
    }

    /**
     * Carga en la GUI los datos del patrón desde un archivo.
     * @param selectedFile 
     */
    private void loadPatternFile(String selectedFile)
    {
        selectedFile = SacFileManager.getSacFileName(this, SacFileManager.SAC_PATTERN);
        if (selectedFile != null)
        {
            this.canvasPattern.clear();
            this.setLocalPattern(CustomPatternParser.parse(SacFileManager.readSacFile(selectedFile)));
            this.loadGui();
        }
    }

    
    /**
     * Carga en la GUI los datos para un patrón nuevo.
     */
    private void addNewPattern()
    {
        this.canvasPattern.clear();
        this.setLocalPattern(new CustomPattern());
        this.loadGui();
    }

    /**
     * Carga todos los datos del patrón en el GUI.
     */
    private void loadGui()
    {
        this.loadHeader();
        this.loadStatesGui();
        this.loadPatternsGui();
        this.getContentPane().validate();
        this.getContentPane().repaint();
    }

    /**
     * Carga la lista de patrones preexistentes.
     */
    private void loadPatternsGui()
    {
        this.patterns = SacGuiManager.loadPatterns();
        this.loadPatternsComboBox();
    }

    /**
     * Carga en el combobox de patrones la lista de patrones.
     */
    private void loadPatternsComboBox()
    {
        this.cbPatterns.removeAllItems();
        for (CustomPattern p : this.patterns)
        {
            this.cbPatterns.addItem(p.getName());
        }
    }

    /**
     * Coloca todos los campos en sus valores por defecto.
     */
    private void reset()
    {
        this.tfName.setText(CustomPattern.DEFAULT_NAME);
        this.taDescription.setText(CustomPattern.DEFAULT_DESCRIPTION);
        this.loadStatesGui();
        this.clearPatternImage();
    }

    /**
     * Carga los datos del encabezado en el GUI.
     */
    private void loadHeader()
    {
        this.tfName.setText(this.getLocalPattern().getName());
        this.taDescription.setText(this.getLocalPattern().getDescription());
    }

    /**
     * Carga los datos de los estados en el combobox y muestra los datos del
     * estado seleccionado.
     */
    private void loadStatesGui()
    {
        this.cbStates.setModel(new DefaultComboBoxModel(MainWindowMaker.currentCA.getStates().getIdAsAStringArray()));
        this.loadStateGui();
    }

    /**
     * Carga los datos del estado seleccionado.
     */
    private void loadStateGui()
    {
        Short selectedId; 
        
        selectedId = Short.parseShort(this.cbStates.getSelectedItem().toString());
        this.canvasPattern.setSelectedState(MainWindowMaker.currentCA.getStates().getState(selectedId));
        this.pStateColor.setBackground(canvasPattern.getSelectedState().getColor());
        this.lStateText.setText(canvasPattern.getSelectedState().getText()+ canvasPattern.getSelectedState().getValue().toString());
    }
   
    /**
     * Asegura que el objeto patrón tenga cargados los elementos actuales.
     */
    private void updatePattern()
    {
        this.getLocalPattern().setName(this.tfName.getText());
        this.getLocalPattern().setDescription(this.taDescription.getText());
    }

    /**
     * Salva el archivo del patrón.
     */
    private void save()
    {
        this.updatePattern();
        SacFileManager.saveFile(this.getLocalPattern().getName(),
                                this.getLocalPattern().toString(),
                                SacFileManager.SAC_PATTERN,
                                this);
        this.firstTimeflag = true;
        this.loadPatternsGui();
    }

    /**
     * Limpia la imagen del patrón.
     */
    private void clearPatternImage()
    {
        this.canvasPattern.clear();
        this.getContentPane().validate();
        this.getContentPane().repaint();
    }
    
    /**
     * Llena el patrón con el estado actual.
     */
    private void fillPatternImage()
    {
        this.canvasPattern.fillPatternMatrix();
        this.getContentPane().validate();
        this.getContentPane().repaint();
    }

    /**
     * Recalcula las coordenadas de la imagen para alinearla lo mas cerca
     * posible de las coordenadas (0,0).
     *
     */
    private void packPatternImage()
    {
        if (!this.canvasPattern.getCurrentPattern().getCells().isEmpty())
        {
            this.canvasPattern.packPattern();
            this.getContentPane().validate();
            this.getContentPane().repaint();
        }
    }

    /**
     * Carga los datos del estado seleccionado.
     */
    private void paintCell()
    {
        Short selectedId;
        this.rbStates.setSelected(true);
        this.canvasPattern.drawPixel = true;
        selectedId = Short.parseShort(this.cbStates.getSelectedItem().toString());
        this.canvasPattern.setSelectedState(MainWindowMaker.currentCA.getStates().getState(selectedId));
        this.pStateColor.setBackground(this.canvasPattern.getSelectedState().getColor());
        this.lStateText.setText(this.canvasPattern.getSelectedState().getText());
    }

    /**
     * Activa el uso de patrones.
     */
    private void paintPattern()
    {
        if (cbPatterns.getItemCount() > 0)
        {
            if (this.firstTimeflag)
            {
                this.firstTimeflag = false;
            }
            else
            {
                this.rbPatterns.setSelected(true);
                this.canvasPattern.drawPixel = false;
            }

            this.canvasPattern.selectedPattern = this.patterns[this.cbPatterns.getSelectedIndex()];
        }
        else
        {
            this.paintCell();
        }
    }

    private void updateRotation(Integer stepRotation)
    {
        canvasPattern.rotation = CanvasUtility.controlChangeRotation(stepRotation, canvasPattern.rotation);
        this.lRotation.setText(Integer.toString(canvasPattern.rotation));
    }
    
    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents()
    {

        buttonGroupPaint = new javax.swing.ButtonGroup();
        panelPatternBackground = new javax.swing.JPanel();
        pPattern = new javax.swing.JPanel();
        canvasPattern = new luz.ve.ssacc.maker.gui.extras.CanvasPattern();
        panelName = new javax.swing.JPanel();
        tfName = new javax.swing.JTextField();
        bSave = new javax.swing.JButton();
        bReset = new javax.swing.JButton();
        bLoad = new javax.swing.JButton();
        pTools = new javax.swing.JPanel();
        cbStates = new javax.swing.JComboBox<>();
        rbStates = new javax.swing.JRadioButton();
        pStateColor = new javax.swing.JPanel();
        lStateText = new javax.swing.JLabel();
        tbDelete = new javax.swing.JToggleButton();
        bFillImage = new javax.swing.JButton();
        bClearImage = new javax.swing.JButton();
        rbPatterns = new javax.swing.JRadioButton();
        cbPatterns = new javax.swing.JComboBox<>();
        bPack = new javax.swing.JButton();
        pFlipHorizontal = new javax.swing.JPanel();
        lFlipHorizontal = new javax.swing.JLabel();
        cbFlipHorizontal = new javax.swing.JCheckBox();
        pFlipVertical = new javax.swing.JPanel();
        lFlipVertical = new javax.swing.JLabel();
        cbFlipVertical = new javax.swing.JCheckBox();
        jPanel3 = new javax.swing.JPanel();
        bRotateCW = new javax.swing.JButton();
        lRotation = new javax.swing.JLabel();
        bRotateCC = new javax.swing.JButton();
        panelDescription = new javax.swing.JPanel();
        jScrollPane1 = new javax.swing.JScrollPane();
        taDescription = new javax.swing.JTextArea();
        menuBarPattern = new javax.swing.JMenuBar();
        miFile = new javax.swing.JMenu();
        miLoad = new javax.swing.JMenuItem();
        miSave = new javax.swing.JMenuItem();
        miRestart = new javax.swing.JMenuItem();
        miExit = new javax.swing.JMenuItem();

        setTitle("SSACC - Patrón");
        setResizable(false);
        addWindowListener(new java.awt.event.WindowAdapter()
        {
            public void windowClosing(java.awt.event.WindowEvent evt)
            {
                formWindowClosing(evt);
            }
        });

        panelPatternBackground.setBorder(javax.swing.BorderFactory.createEtchedBorder());

        pPattern.setBorder(javax.swing.BorderFactory.createTitledBorder(javax.swing.BorderFactory.createTitledBorder(javax.swing.BorderFactory.createEtchedBorder(), "Patrón")));

        javax.swing.GroupLayout pPatternLayout = new javax.swing.GroupLayout(pPattern);
        pPattern.setLayout(pPatternLayout);
        pPatternLayout.setHorizontalGroup(
            pPatternLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(pPatternLayout.createSequentialGroup()
                .addComponent(canvasPattern, javax.swing.GroupLayout.DEFAULT_SIZE, 985, Short.MAX_VALUE)
                .addContainerGap())
        );
        pPatternLayout.setVerticalGroup(
            pPatternLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(pPatternLayout.createSequentialGroup()
                .addComponent(canvasPattern, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addContainerGap())
        );

        panelName.setBorder(javax.swing.BorderFactory.createTitledBorder("Nombre"));

        bSave.setIcon(new javax.swing.ImageIcon(getClass().getResource("/luz/ve/ssacc/maker/gui/images/SSACC Ok 20x20.png"))); // NOI18N
        bSave.setToolTipText("Guardar");
        bSave.addActionListener(new java.awt.event.ActionListener()
        {
            public void actionPerformed(java.awt.event.ActionEvent evt)
            {
                bSaveActionPerformed(evt);
            }
        });

        bReset.setIcon(new javax.swing.ImageIcon(getClass().getResource("/luz/ve/ssacc/maker/gui/images/SSACC - redo 20x20.png"))); // NOI18N
        bReset.setToolTipText("Reiniciar");
        bReset.addActionListener(new java.awt.event.ActionListener()
        {
            public void actionPerformed(java.awt.event.ActionEvent evt)
            {
                bResetActionPerformed(evt);
            }
        });

        bLoad.setIcon(new javax.swing.ImageIcon(getClass().getResource("/luz/ve/ssacc/maker/gui/images/SSACC Open 20x20.png"))); // NOI18N
        bLoad.setToolTipText("Cargar");
        bLoad.addActionListener(new java.awt.event.ActionListener()
        {
            public void actionPerformed(java.awt.event.ActionEvent evt)
            {
                bLoadActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout panelNameLayout = new javax.swing.GroupLayout(panelName);
        panelName.setLayout(panelNameLayout);
        panelNameLayout.setHorizontalGroup(
            panelNameLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(panelNameLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(tfName, javax.swing.GroupLayout.PREFERRED_SIZE, 360, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(bSave, javax.swing.GroupLayout.PREFERRED_SIZE, 61, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addComponent(bReset, javax.swing.GroupLayout.PREFERRED_SIZE, 38, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addComponent(bLoad)
                .addContainerGap())
        );

        panelNameLayout.linkSize(javax.swing.SwingConstants.HORIZONTAL, new java.awt.Component[] {bLoad, bReset, bSave});

        panelNameLayout.setVerticalGroup(
            panelNameLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(panelNameLayout.createSequentialGroup()
                .addGroup(panelNameLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(panelNameLayout.createSequentialGroup()
                        .addGap(1, 1, 1)
                        .addComponent(tfName, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addComponent(bSave)
                    .addComponent(bReset)
                    .addComponent(bLoad))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        panelNameLayout.linkSize(javax.swing.SwingConstants.VERTICAL, new java.awt.Component[] {bLoad, bReset, bSave});

        pTools.setBorder(javax.swing.BorderFactory.createEtchedBorder());

        cbStates.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "Item 1", "Item 2", "Item 3", "Item 4" }));
        cbStates.addActionListener(new java.awt.event.ActionListener()
        {
            public void actionPerformed(java.awt.event.ActionEvent evt)
            {
                cbStatesActionPerformed(evt);
            }
        });

        buttonGroupPaint.add(rbStates);
        rbStates.setSelected(true);
        rbStates.addActionListener(new java.awt.event.ActionListener()
        {
            public void actionPerformed(java.awt.event.ActionEvent evt)
            {
                rbStatesActionPerformed(evt);
            }
        });

        pStateColor.setBorder(javax.swing.BorderFactory.createEtchedBorder());

        lStateText.setText("text");

        javax.swing.GroupLayout pStateColorLayout = new javax.swing.GroupLayout(pStateColor);
        pStateColor.setLayout(pStateColorLayout);
        pStateColorLayout.setHorizontalGroup(
            pStateColorLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, pStateColorLayout.createSequentialGroup()
                .addContainerGap(24, Short.MAX_VALUE)
                .addComponent(lStateText, javax.swing.GroupLayout.PREFERRED_SIZE, 48, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(42, 42, 42))
        );
        pStateColorLayout.setVerticalGroup(
            pStateColorLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(pStateColorLayout.createSequentialGroup()
                .addComponent(lStateText)
                .addGap(0, 6, Short.MAX_VALUE))
        );

        tbDelete.setIcon(new javax.swing.ImageIcon(getClass().getResource("/luz/ve/ssacc/maker/gui/images/SSACC - eraser icon 20x20.png"))); // NOI18N
        tbDelete.addActionListener(new java.awt.event.ActionListener()
        {
            public void actionPerformed(java.awt.event.ActionEvent evt)
            {
                tbDeleteActionPerformed(evt);
            }
        });

        bFillImage.setIcon(new javax.swing.ImageIcon(getClass().getResource("/luz/ve/ssacc/maker/gui/images/SSACC - fill image icon.png"))); // NOI18N
        bFillImage.setToolTipText("Llenar patrón");
        bFillImage.addActionListener(new java.awt.event.ActionListener()
        {
            public void actionPerformed(java.awt.event.ActionEvent evt)
            {
                bFillImageActionPerformed(evt);
            }
        });

        bClearImage.setIcon(new javax.swing.ImageIcon(getClass().getResource("/luz/ve/ssacc/maker/gui/images/SSACC - clear image icon.png"))); // NOI18N
        bClearImage.setToolTipText("Limpiar patrón");
        bClearImage.addActionListener(new java.awt.event.ActionListener()
        {
            public void actionPerformed(java.awt.event.ActionEvent evt)
            {
                bClearImageActionPerformed(evt);
            }
        });

        buttonGroupPaint.add(rbPatterns);
        rbPatterns.addActionListener(new java.awt.event.ActionListener()
        {
            public void actionPerformed(java.awt.event.ActionEvent evt)
            {
                rbPatternsActionPerformed(evt);
            }
        });

        cbPatterns.addActionListener(new java.awt.event.ActionListener()
        {
            public void actionPerformed(java.awt.event.ActionEvent evt)
            {
                cbPatternsActionPerformed(evt);
            }
        });

        bPack.setIcon(new javax.swing.ImageIcon(getClass().getResource("/luz/ve/ssacc/maker/gui/images/SSACC - pack icon 20x20.png"))); // NOI18N
        bPack.setToolTipText("Empaquetar");
        bPack.addActionListener(new java.awt.event.ActionListener()
        {
            public void actionPerformed(java.awt.event.ActionEvent evt)
            {
                bPackActionPerformed(evt);
            }
        });

        pFlipHorizontal.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));

        lFlipHorizontal.setIcon(new javax.swing.ImageIcon(getClass().getResource("/luz/ve/ssacc/maker/gui/images/SSACC - flip horizontal 20x20.png"))); // NOI18N
        lFlipHorizontal.setToolTipText("");

        cbFlipHorizontal.addActionListener(new java.awt.event.ActionListener()
        {
            public void actionPerformed(java.awt.event.ActionEvent evt)
            {
                cbFlipHorizontalActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout pFlipHorizontalLayout = new javax.swing.GroupLayout(pFlipHorizontal);
        pFlipHorizontal.setLayout(pFlipHorizontalLayout);
        pFlipHorizontalLayout.setHorizontalGroup(
            pFlipHorizontalLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(pFlipHorizontalLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(lFlipHorizontal)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 16, Short.MAX_VALUE)
                .addComponent(cbFlipHorizontal)
                .addContainerGap())
        );
        pFlipHorizontalLayout.setVerticalGroup(
            pFlipHorizontalLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, pFlipHorizontalLayout.createSequentialGroup()
                .addContainerGap(20, Short.MAX_VALUE)
                .addGroup(pFlipHorizontalLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(lFlipHorizontal)
                    .addComponent(cbFlipHorizontal))
                .addContainerGap())
        );

        pFlipVertical.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));

        lFlipVertical.setIcon(new javax.swing.ImageIcon(getClass().getResource("/luz/ve/ssacc/maker/gui/images/SSACC - flip vertical 20x20.png"))); // NOI18N

        cbFlipVertical.addActionListener(new java.awt.event.ActionListener()
        {
            public void actionPerformed(java.awt.event.ActionEvent evt)
            {
                cbFlipVerticalActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout pFlipVerticalLayout = new javax.swing.GroupLayout(pFlipVertical);
        pFlipVertical.setLayout(pFlipVerticalLayout);
        pFlipVerticalLayout.setHorizontalGroup(
            pFlipVerticalLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(pFlipVerticalLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(lFlipVertical)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 16, Short.MAX_VALUE)
                .addComponent(cbFlipVertical)
                .addContainerGap())
        );
        pFlipVerticalLayout.setVerticalGroup(
            pFlipVerticalLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, pFlipVerticalLayout.createSequentialGroup()
                .addContainerGap(19, Short.MAX_VALUE)
                .addGroup(pFlipVerticalLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(lFlipVertical)
                    .addComponent(cbFlipVertical))
                .addContainerGap())
        );

        jPanel3.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));

        bRotateCW.setIcon(new javax.swing.ImageIcon(getClass().getResource("/luz/ve/ssacc/maker/gui/images/SSACC - rotate cw.jpg"))); // NOI18N
        bRotateCW.addActionListener(new java.awt.event.ActionListener()
        {
            public void actionPerformed(java.awt.event.ActionEvent evt)
            {
                bRotateCWActionPerformed(evt);
            }
        });

        lRotation.setFont(new java.awt.Font("Ubuntu", 1, 14)); // NOI18N
        lRotation.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        lRotation.setText("000");
        lRotation.setHorizontalTextPosition(javax.swing.SwingConstants.RIGHT);

        bRotateCC.setIcon(new javax.swing.ImageIcon(getClass().getResource("/luz/ve/ssacc/maker/gui/images/SSACC - rotate cc.jpg"))); // NOI18N
        bRotateCC.addActionListener(new java.awt.event.ActionListener()
        {
            public void actionPerformed(java.awt.event.ActionEvent evt)
            {
                bRotateCCActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel3Layout = new javax.swing.GroupLayout(jPanel3);
        jPanel3.setLayout(jPanel3Layout);
        jPanel3Layout.setHorizontalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel3Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(bRotateCW)
                    .addComponent(lRotation, javax.swing.GroupLayout.PREFERRED_SIZE, 32, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(bRotateCC))
                .addContainerGap(38, Short.MAX_VALUE))
        );
        jPanel3Layout.setVerticalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel3Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(bRotateCW)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(lRotation, javax.swing.GroupLayout.PREFERRED_SIZE, 29, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(bRotateCC)
                .addContainerGap(17, Short.MAX_VALUE))
        );

        javax.swing.GroupLayout pToolsLayout = new javax.swing.GroupLayout(pTools);
        pTools.setLayout(pToolsLayout);
        pToolsLayout.setHorizontalGroup(
            pToolsLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(pToolsLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(pToolsLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(pToolsLayout.createSequentialGroup()
                        .addGroup(pToolsLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(pFlipHorizontal, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(pFlipVertical, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(jPanel3, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(pStateColor, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(0, 0, Short.MAX_VALUE))
                    .addGroup(pToolsLayout.createSequentialGroup()
                        .addGroup(pToolsLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addGroup(pToolsLayout.createSequentialGroup()
                                .addComponent(rbStates)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addComponent(cbStates, 0, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                            .addComponent(bPack, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(bClearImage, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(bFillImage, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(tbDelete, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addGroup(pToolsLayout.createSequentialGroup()
                                .addComponent(rbPatterns)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(cbPatterns, javax.swing.GroupLayout.PREFERRED_SIZE, 90, javax.swing.GroupLayout.PREFERRED_SIZE)))
                        .addContainerGap(34, Short.MAX_VALUE))))
        );

        pToolsLayout.linkSize(javax.swing.SwingConstants.HORIZONTAL, new java.awt.Component[] {jPanel3, pFlipHorizontal, pFlipVertical});

        pToolsLayout.setVerticalGroup(
            pToolsLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(pToolsLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(pToolsLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(cbStates, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(rbStates))
                .addGap(18, 18, 18)
                .addComponent(pStateColor, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(8, 8, 8)
                .addComponent(tbDelete)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(bFillImage)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(bClearImage)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(bPack)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(pToolsLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(rbPatterns)
                    .addComponent(cbPatterns, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(11, 11, 11)
                .addComponent(pFlipHorizontal, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(pFlipVertical, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jPanel3, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addContainerGap())
        );

        pToolsLayout.linkSize(javax.swing.SwingConstants.VERTICAL, new java.awt.Component[] {cbPatterns, cbStates, pStateColor});

        pToolsLayout.linkSize(javax.swing.SwingConstants.VERTICAL, new java.awt.Component[] {bClearImage, bFillImage, bPack, tbDelete});

        panelDescription.setBorder(javax.swing.BorderFactory.createTitledBorder("Descripción"));

        taDescription.setColumns(20);
        taDescription.setLineWrap(true);
        taDescription.setRows(5);
        jScrollPane1.setViewportView(taDescription);

        javax.swing.GroupLayout panelDescriptionLayout = new javax.swing.GroupLayout(panelDescription);
        panelDescription.setLayout(panelDescriptionLayout);
        panelDescriptionLayout.setHorizontalGroup(
            panelDescriptionLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(panelDescriptionLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 388, Short.MAX_VALUE)
                .addContainerGap())
        );
        panelDescriptionLayout.setVerticalGroup(
            panelDescriptionLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(panelDescriptionLayout.createSequentialGroup()
                .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 36, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(0, 0, Short.MAX_VALUE))
        );

        javax.swing.GroupLayout panelPatternBackgroundLayout = new javax.swing.GroupLayout(panelPatternBackground);
        panelPatternBackground.setLayout(panelPatternBackgroundLayout);
        panelPatternBackgroundLayout.setHorizontalGroup(
            panelPatternBackgroundLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(panelPatternBackgroundLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(panelPatternBackgroundLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(panelPatternBackgroundLayout.createSequentialGroup()
                        .addComponent(panelName, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(panelDescription, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(panelPatternBackgroundLayout.createSequentialGroup()
                        .addComponent(pPattern, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(pTools, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))))
        );
        panelPatternBackgroundLayout.setVerticalGroup(
            panelPatternBackgroundLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(panelPatternBackgroundLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(panelPatternBackgroundLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(panelName, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(panelDescription, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(panelPatternBackgroundLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(pPattern, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(pTools, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)))
        );

        miFile.setMnemonic('A');
        miFile.setText("Archivo");

        miLoad.setMnemonic('C');
        miLoad.setText("Cargar");
        miLoad.addActionListener(new java.awt.event.ActionListener()
        {
            public void actionPerformed(java.awt.event.ActionEvent evt)
            {
                miLoadActionPerformed(evt);
            }
        });
        miFile.add(miLoad);

        miSave.setMnemonic('G');
        miSave.setText("Guardar");
        miFile.add(miSave);

        miRestart.setMnemonic('R');
        miRestart.setText("Reiniciar");
        miFile.add(miRestart);

        miExit.setMnemonic('S');
        miExit.setText("Salir");
        miFile.add(miExit);

        menuBarPattern.add(miFile);

        setJMenuBar(menuBarPattern);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(panelPatternBackground, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(panelPatternBackground, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void miLoadActionPerformed(java.awt.event.ActionEvent evt)//GEN-FIRST:event_miLoadActionPerformed
    {//GEN-HEADEREND:event_miLoadActionPerformed
        
    }//GEN-LAST:event_miLoadActionPerformed

    private void formWindowClosing(java.awt.event.WindowEvent evt)//GEN-FIRST:event_formWindowClosing
    {//GEN-HEADEREND:event_formWindowClosing
        SacGuiManager.closeWindow(this, parentWindow);
    }//GEN-LAST:event_formWindowClosing

    private void bSaveActionPerformed(java.awt.event.ActionEvent evt)//GEN-FIRST:event_bSaveActionPerformed
    {//GEN-HEADEREND:event_bSaveActionPerformed
        this.save();
    }//GEN-LAST:event_bSaveActionPerformed

    private void bResetActionPerformed(java.awt.event.ActionEvent evt)//GEN-FIRST:event_bResetActionPerformed
    {//GEN-HEADEREND:event_bResetActionPerformed
        this.reset();
    }//GEN-LAST:event_bResetActionPerformed

    private void cbStatesActionPerformed(java.awt.event.ActionEvent evt)//GEN-FIRST:event_cbStatesActionPerformed
    {//GEN-HEADEREND:event_cbStatesActionPerformed
        this.loadStateGui();  
        this.paintCell();
    }//GEN-LAST:event_cbStatesActionPerformed

    private void bLoadActionPerformed(java.awt.event.ActionEvent evt)//GEN-FIRST:event_bLoadActionPerformed
    {//GEN-HEADEREND:event_bLoadActionPerformed
        this.load(SacUtility.LOAD_FILE, "");
    }//GEN-LAST:event_bLoadActionPerformed

    private void bClearImageActionPerformed(java.awt.event.ActionEvent evt)//GEN-FIRST:event_bClearImageActionPerformed
    {//GEN-HEADEREND:event_bClearImageActionPerformed
        this.clearPatternImage();
    }//GEN-LAST:event_bClearImageActionPerformed

    private void bFillImageActionPerformed(java.awt.event.ActionEvent evt)//GEN-FIRST:event_bFillImageActionPerformed
    {//GEN-HEADEREND:event_bFillImageActionPerformed
        this.fillPatternImage();
    }//GEN-LAST:event_bFillImageActionPerformed

    private void bPackActionPerformed(java.awt.event.ActionEvent evt)//GEN-FIRST:event_bPackActionPerformed
    {//GEN-HEADEREND:event_bPackActionPerformed
        this.packPatternImage();
    }//GEN-LAST:event_bPackActionPerformed

    private void cbPatternsActionPerformed(java.awt.event.ActionEvent evt)//GEN-FIRST:event_cbPatternsActionPerformed
    {//GEN-HEADEREND:event_cbPatternsActionPerformed
        this.paintPattern();
    }//GEN-LAST:event_cbPatternsActionPerformed

    private void rbPatternsActionPerformed(java.awt.event.ActionEvent evt)//GEN-FIRST:event_rbPatternsActionPerformed
    {//GEN-HEADEREND:event_rbPatternsActionPerformed
        this.paintPattern();
    }//GEN-LAST:event_rbPatternsActionPerformed

    private void rbStatesActionPerformed(java.awt.event.ActionEvent evt)//GEN-FIRST:event_rbStatesActionPerformed
    {//GEN-HEADEREND:event_rbStatesActionPerformed
        this.paintCell();
    }//GEN-LAST:event_rbStatesActionPerformed

    private void tbDeleteActionPerformed(java.awt.event.ActionEvent evt)//GEN-FIRST:event_tbDeleteActionPerformed
    {//GEN-HEADEREND:event_tbDeleteActionPerformed
        this.canvasPattern.deleteOn = this.tbDelete.isSelected();
    }//GEN-LAST:event_tbDeleteActionPerformed

    private void cbFlipHorizontalActionPerformed(java.awt.event.ActionEvent evt)//GEN-FIRST:event_cbFlipHorizontalActionPerformed
    {//GEN-HEADEREND:event_cbFlipHorizontalActionPerformed
        canvasPattern.flipHorizontal = this.cbFlipHorizontal.isSelected();
    }//GEN-LAST:event_cbFlipHorizontalActionPerformed

    private void cbFlipVerticalActionPerformed(java.awt.event.ActionEvent evt)//GEN-FIRST:event_cbFlipVerticalActionPerformed
    {//GEN-HEADEREND:event_cbFlipVerticalActionPerformed
        canvasPattern.flipVertical = this.cbFlipVertical.isSelected();
    }//GEN-LAST:event_cbFlipVerticalActionPerformed

    private void bRotateCWActionPerformed(java.awt.event.ActionEvent evt)//GEN-FIRST:event_bRotateCWActionPerformed
    {//GEN-HEADEREND:event_bRotateCWActionPerformed
        this.updateRotation(-90);
    }//GEN-LAST:event_bRotateCWActionPerformed

    private void bRotateCCActionPerformed(java.awt.event.ActionEvent evt)//GEN-FIRST:event_bRotateCCActionPerformed
    {//GEN-HEADEREND:event_bRotateCCActionPerformed
        this.updateRotation(90);  
    }//GEN-LAST:event_bRotateCCActionPerformed

   
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton bClearImage;
    private javax.swing.JButton bFillImage;
    private javax.swing.JButton bLoad;
    private javax.swing.JButton bPack;
    private javax.swing.JButton bReset;
    private javax.swing.JButton bRotateCC;
    private javax.swing.JButton bRotateCW;
    private javax.swing.JButton bSave;
    private javax.swing.ButtonGroup buttonGroupPaint;
    private luz.ve.ssacc.maker.gui.extras.CanvasPattern canvasPattern;
    private javax.swing.JCheckBox cbFlipHorizontal;
    private javax.swing.JCheckBox cbFlipVertical;
    private javax.swing.JComboBox<String> cbPatterns;
    private javax.swing.JComboBox<String> cbStates;
    private javax.swing.JPanel jPanel3;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JLabel lFlipHorizontal;
    private javax.swing.JLabel lFlipVertical;
    private javax.swing.JLabel lRotation;
    private javax.swing.JLabel lStateText;
    private javax.swing.JMenuBar menuBarPattern;
    private javax.swing.JMenuItem miExit;
    private javax.swing.JMenu miFile;
    private javax.swing.JMenuItem miLoad;
    private javax.swing.JMenuItem miRestart;
    private javax.swing.JMenuItem miSave;
    private javax.swing.JPanel pFlipHorizontal;
    private javax.swing.JPanel pFlipVertical;
    private javax.swing.JPanel pPattern;
    private javax.swing.JPanel pStateColor;
    private javax.swing.JPanel pTools;
    private javax.swing.JPanel panelDescription;
    private javax.swing.JPanel panelName;
    private javax.swing.JPanel panelPatternBackground;
    private javax.swing.JRadioButton rbPatterns;
    private javax.swing.JRadioButton rbStates;
    private javax.swing.JTextArea taDescription;
    private javax.swing.JToggleButton tbDelete;
    private javax.swing.JTextField tfName;
    // End of variables declaration//GEN-END:variables

   
}
