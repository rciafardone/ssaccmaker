/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template fileName, choose Tools | Templates
 * and open the template in the editor.
 */
package luz.ve.ssacc.maker.gui;

import java.nio.file.Paths;
import javax.swing.SpinnerNumberModel;
import luz.ve.ssacc.engine.components.Statistics;
import luz.ve.ssacc.maker.gui.extras.SacGuiManager;
import luz.ve.ssacc.utilities.SacFileManager;
import luz.ve.ssacc.utilities.SacUtility;
import org.jfree.ui.RefineryUtilities;

/**
 *
 * @author Renzo Ciafardone Sciannelli <rciafardone@gmail.com>
 */
public class WindowStatistics extends javax.swing.JFrame
{
    MainWindowMaker parentWindow;
    Statistics currentStats;
    String windowTitleLinear = "SSACC - Gráfica Lineal - ";
    String chartTitleLinear = "Número de estados x paso";
    String windowTitlePie = "SSACC - Gráfica Lineal - ";
    String chartTitlePie = "Número de estados en paso #";
    
    
    /**
     * Creates new form SimulationStatisticsFrame
     * @param mwm
     */
    public WindowStatistics(MainWindowMaker mwm)
    {
        initComponents();
        initialConfig(mwm);
    }

    private void initialConfig(MainWindowMaker mwm)
    {
        this.parentWindow = mwm;
    }


    public void loadName()
    {
        this.lAutomatonName.setText(MainWindowMaker.currentCA.getName());
    }

    private void loadData(String selectedFile)
    {
        String statsData = SacFileManager.readSacFile(selectedFile);

        if (!statsData.isEmpty())
        {
            this.currentStats = new Statistics(statsData, MainWindowMaker.currentCA);
        }
        else
        {
            SacUtility.showWarning("El archivo escogido esta vacio.");
        }
    }

    
    private void loadGui(String selectedFile)
    {
        this.bOpenChartLinear.setEnabled(true);
        this.bOpenChartPie.setEnabled(true);
        this.sFirstStep.setEnabled(true);
        this.sLastStep.setEnabled(true);
        this.sStep.setEnabled(true);

        SpinnerNumberModel nModel0 = new SpinnerNumberModel(0.0, 0.0, this.currentStats.getData().size()-1, 1.0);
        SpinnerNumberModel nModel1 = new SpinnerNumberModel(0.0, 0.0, this.currentStats.getData().size()-1, 1.0);
        SpinnerNumberModel nModel2 = new SpinnerNumberModel(0.0, 0.0, this.currentStats.getData().size()-1, 1.0);
                
        this.sFirstStep.setModel(nModel0);
        this.sLastStep.setModel(nModel1);
        this.sStep.setModel(nModel2);
        
        this.sLastStep.setValue(this.currentStats.getData().size()-1);

        this.lFileName.setText(Paths.get(selectedFile).getFileName().toString());
    }

    /**
     *
     */
    public void load()
    {
        String selectedFile;
        selectedFile = SacFileManager.getSacFileName(this, SacFileManager.SAC_STATISTICS);

        if (selectedFile != null)
        {
            this.loadData(selectedFile);
            this.loadGui(selectedFile);
            this.repaint();
        }
        else
        {
            this.deactivateButtons();
        }
    }

    /**
     *
     */
    private void close()
    {
        SacGuiManager.closeWindow(this, parentWindow);
    }

    private void openLineChart()
    {
        
        //   Integer firstStep = (Integer) this.sFirstStep.getValue(); // TODO: Misterio, no se que pasa aqui con el JSpinner
        //   Integer lastStep = (Integer) this.sLastStep.getValue(); 
        Integer firstStep = new Double(this.sFirstStep.getValue().toString()).intValue(); 
        Integer lastStep = new Double(this.sLastStep.getValue().toString()).intValue();

        WindowStatisticsLineChart chart;
        chart = new WindowStatisticsLineChart(
            this.windowTitleLinear + this.lFileName.getText(),
            this.chartTitleLinear,
            this.currentStats,
            firstStep,
            lastStep
        );
        chart.pack();
        RefineryUtilities.centerFrameOnScreen(chart);
        chart.setVisible(true);
    }

    private void openPieChart()
    {
        //   Integer firstStep = (Integer) this.sFirstStep.getValue(); // TODO: Misterio, no se que pasa aqui con el JSpinner
        //   Integer lastStep = (Integer) this.sLastStep.getValue(); 
        Integer step = new Double(this.sStep.getValue().toString()).intValue(); 
        
        WindowStatisticsPieChart chart;
        /*
        chart = new WindowStatisticsPieChart(
            this.windowTitleLinear + this.lFileName.getText(),
            this.chartTitleLinear,
            this.currentStats,
            step
        );
        */
        chart = new WindowStatisticsPieChart(
            this.windowTitlePie + this.lFileName.getText(),
            this.chartTitlePie,
            this.currentStats,
            step
        );
        chart.pack();
        RefineryUtilities.centerFrameOnScreen(chart);
        chart.setVisible(true);
    }


    
    private void deactivateButtons()
    {
        this.lFileName.setText("escoger archivo");
        this.bOpenChartLinear.setEnabled(false);
        this.bOpenChartPie.setEnabled(false);
        this.sFirstStep.setEnabled(false);
        this.sLastStep.setEnabled(false);
        this.sStep.setEnabled(false);
    }
    
    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents()
    {

        pBackground = new javax.swing.JPanel();
        pAutomatonName = new javax.swing.JPanel();
        lAutomatonNameTitle = new javax.swing.JLabel();
        lAutomatonName = new javax.swing.JLabel();
        pAnimationFileName = new javax.swing.JPanel();
        bLoadFile = new javax.swing.JButton();
        lFileNameTitle = new javax.swing.JLabel();
        lFileName = new javax.swing.JLabel();
        pChartLinear = new javax.swing.JPanel();
        bOpenChartLinear = new javax.swing.JButton();
        lBegins = new javax.swing.JLabel();
        sFirstStep = new javax.swing.JSpinner();
        lEnds = new javax.swing.JLabel();
        sLastStep = new javax.swing.JSpinner();
        pChartPie = new javax.swing.JPanel();
        bOpenChartPie = new javax.swing.JButton();
        sStep = new javax.swing.JSpinner();

        setTitle("SSACC - Estadísticas");
        setResizable(false);
        addWindowListener(new java.awt.event.WindowAdapter()
        {
            public void windowClosing(java.awt.event.WindowEvent evt)
            {
                formWindowClosing(evt);
            }
        });

        pAutomatonName.setBorder(javax.swing.BorderFactory.createEtchedBorder());

        lAutomatonNameTitle.setFont(new java.awt.Font("Ubuntu", 0, 18)); // NOI18N
        lAutomatonNameTitle.setText("Automata Activo:");

        lAutomatonName.setFont(new java.awt.Font("Ubuntu", 1, 18)); // NOI18N
        lAutomatonName.setText("automaton name");

        javax.swing.GroupLayout pAutomatonNameLayout = new javax.swing.GroupLayout(pAutomatonName);
        pAutomatonName.setLayout(pAutomatonNameLayout);
        pAutomatonNameLayout.setHorizontalGroup(
            pAutomatonNameLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(pAutomatonNameLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(lAutomatonNameTitle)
                .addGap(18, 18, 18)
                .addComponent(lAutomatonName)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        pAutomatonNameLayout.setVerticalGroup(
            pAutomatonNameLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(pAutomatonNameLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(pAutomatonNameLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(lAutomatonNameTitle)
                    .addComponent(lAutomatonName))
                .addContainerGap(23, Short.MAX_VALUE))
        );

        pAnimationFileName.setBorder(javax.swing.BorderFactory.createEtchedBorder());

        bLoadFile.setIcon(new javax.swing.ImageIcon(getClass().getResource("/luz/ve/ssacc/maker/gui/images/SSACC Open 20x20.png"))); // NOI18N
        bLoadFile.setToolTipText("Seleccionar componente");
        bLoadFile.addActionListener(new java.awt.event.ActionListener()
        {
            public void actionPerformed(java.awt.event.ActionEvent evt)
            {
                bLoadFileActionPerformed(evt);
            }
        });

        lFileNameTitle.setFont(new java.awt.Font("Ubuntu", 0, 18)); // NOI18N
        lFileNameTitle.setText("Archivo datos cargados:");

        lFileName.setFont(new java.awt.Font("Ubuntu", 1, 18)); // NOI18N
        lFileName.setText("escoger archivo");

        javax.swing.GroupLayout pAnimationFileNameLayout = new javax.swing.GroupLayout(pAnimationFileName);
        pAnimationFileName.setLayout(pAnimationFileNameLayout);
        pAnimationFileNameLayout.setHorizontalGroup(
            pAnimationFileNameLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, pAnimationFileNameLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(lFileNameTitle)
                .addGap(18, 18, 18)
                .addComponent(lFileName)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 322, Short.MAX_VALUE)
                .addComponent(bLoadFile, javax.swing.GroupLayout.PREFERRED_SIZE, 36, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
        );
        pAnimationFileNameLayout.setVerticalGroup(
            pAnimationFileNameLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(pAnimationFileNameLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(pAnimationFileNameLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(pAnimationFileNameLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(lFileNameTitle)
                        .addComponent(lFileName))
                    .addComponent(bLoadFile))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        pAnimationFileNameLayout.linkSize(javax.swing.SwingConstants.VERTICAL, new java.awt.Component[] {bLoadFile, lFileName, lFileNameTitle});

        pChartLinear.setBorder(javax.swing.BorderFactory.createEtchedBorder());

        bOpenChartLinear.setIcon(new javax.swing.ImageIcon(getClass().getResource("/luz/ve/ssacc/maker/gui/images/SSACC - line chart 20x20.png"))); // NOI18N
        bOpenChartLinear.setText("Intevalo");
        bOpenChartLinear.setEnabled(false);
        bOpenChartLinear.addActionListener(new java.awt.event.ActionListener()
        {
            public void actionPerformed(java.awt.event.ActionEvent evt)
            {
                bOpenChartLinearActionPerformed(evt);
            }
        });

        lBegins.setText("Inicio:");

        sFirstStep.setEnabled(false);

        lEnds.setText("Final:");

        sLastStep.setEnabled(false);

        javax.swing.GroupLayout pChartLinearLayout = new javax.swing.GroupLayout(pChartLinear);
        pChartLinear.setLayout(pChartLinearLayout);
        pChartLinearLayout.setHorizontalGroup(
            pChartLinearLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(pChartLinearLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(bOpenChartLinear)
                .addGap(18, 18, 18)
                .addComponent(lBegins)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(sFirstStep, javax.swing.GroupLayout.PREFERRED_SIZE, 64, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addComponent(lEnds)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(sLastStep, javax.swing.GroupLayout.PREFERRED_SIZE, 73, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        pChartLinearLayout.linkSize(javax.swing.SwingConstants.HORIZONTAL, new java.awt.Component[] {sFirstStep, sLastStep});

        pChartLinearLayout.setVerticalGroup(
            pChartLinearLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(pChartLinearLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(pChartLinearLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(bOpenChartLinear)
                    .addComponent(lBegins)
                    .addComponent(lEnds)
                    .addComponent(sFirstStep, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(sLastStep, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        pChartLinearLayout.linkSize(javax.swing.SwingConstants.VERTICAL, new java.awt.Component[] {sFirstStep, sLastStep});

        pChartPie.setBorder(javax.swing.BorderFactory.createEtchedBorder());

        bOpenChartPie.setIcon(new javax.swing.ImageIcon(getClass().getResource("/luz/ve/ssacc/maker/gui/images/SSACC - pie chart 20x20.png"))); // NOI18N
        bOpenChartPie.setText("Paso");
        bOpenChartPie.setEnabled(false);
        bOpenChartPie.addActionListener(new java.awt.event.ActionListener()
        {
            public void actionPerformed(java.awt.event.ActionEvent evt)
            {
                bOpenChartPieActionPerformed(evt);
            }
        });

        sStep.setEnabled(false);

        javax.swing.GroupLayout pChartPieLayout = new javax.swing.GroupLayout(pChartPie);
        pChartPie.setLayout(pChartPieLayout);
        pChartPieLayout.setHorizontalGroup(
            pChartPieLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(pChartPieLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(bOpenChartPie, javax.swing.GroupLayout.PREFERRED_SIZE, 91, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addComponent(sStep, javax.swing.GroupLayout.PREFERRED_SIZE, 109, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(0, 0, Short.MAX_VALUE))
        );
        pChartPieLayout.setVerticalGroup(
            pChartPieLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(pChartPieLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(pChartPieLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(bOpenChartPie)
                    .addComponent(sStep, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        javax.swing.GroupLayout pBackgroundLayout = new javax.swing.GroupLayout(pBackground);
        pBackground.setLayout(pBackgroundLayout);
        pBackgroundLayout.setHorizontalGroup(
            pBackgroundLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(pBackgroundLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(pBackgroundLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(pChartPie, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(pChartLinear, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(pAnimationFileName, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(pAutomatonName, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addContainerGap())
        );
        pBackgroundLayout.setVerticalGroup(
            pBackgroundLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(pBackgroundLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(pAutomatonName, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(pAnimationFileName, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(pChartLinear, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(pChartPie, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
        );

        pBackgroundLayout.linkSize(javax.swing.SwingConstants.VERTICAL, new java.awt.Component[] {pAnimationFileName, pAutomatonName, pChartLinear, pChartPie});

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(pBackground, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(pBackground, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void bLoadFileActionPerformed(java.awt.event.ActionEvent evt)//GEN-FIRST:event_bLoadFileActionPerformed
    {//GEN-HEADEREND:event_bLoadFileActionPerformed
        this.load();
    }//GEN-LAST:event_bLoadFileActionPerformed

    private void bOpenChartLinearActionPerformed(java.awt.event.ActionEvent evt)//GEN-FIRST:event_bOpenChartLinearActionPerformed
    {//GEN-HEADEREND:event_bOpenChartLinearActionPerformed
        this.openLineChart();
    }//GEN-LAST:event_bOpenChartLinearActionPerformed

    private void formWindowClosing(java.awt.event.WindowEvent evt)//GEN-FIRST:event_formWindowClosing
    {//GEN-HEADEREND:event_formWindowClosing
        this.close();
    }//GEN-LAST:event_formWindowClosing

    private void bOpenChartPieActionPerformed(java.awt.event.ActionEvent evt)//GEN-FIRST:event_bOpenChartPieActionPerformed
    {//GEN-HEADEREND:event_bOpenChartPieActionPerformed
         this.openPieChart();
    }//GEN-LAST:event_bOpenChartPieActionPerformed


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton bLoadFile;
    private javax.swing.JButton bOpenChartLinear;
    private javax.swing.JButton bOpenChartPie;
    private javax.swing.JLabel lAutomatonName;
    private javax.swing.JLabel lAutomatonNameTitle;
    private javax.swing.JLabel lBegins;
    private javax.swing.JLabel lEnds;
    private javax.swing.JLabel lFileName;
    private javax.swing.JLabel lFileNameTitle;
    private javax.swing.JPanel pAnimationFileName;
    private javax.swing.JPanel pAutomatonName;
    private javax.swing.JPanel pBackground;
    private javax.swing.JPanel pChartLinear;
    private javax.swing.JPanel pChartPie;
    private javax.swing.JSpinner sFirstStep;
    private javax.swing.JSpinner sLastStep;
    private javax.swing.JSpinner sStep;
    // End of variables declaration//GEN-END:variables
}
