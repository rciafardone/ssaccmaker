/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package luz.ve.ssacc.maker.gui;

import javax.swing.JFrame;
import luz.ve.ssacc.engine.components.State;
import luz.ve.ssacc.engine.components.Statistics;
import org.jfree.chart.ChartPanel;
import org.jfree.chart.ChartFactory;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.plot.CategoryPlot;
import org.jfree.chart.plot.PlotOrientation;
import org.jfree.data.category.DefaultCategoryDataset;

public class WindowStatisticsLineChart extends JFrame
{

    public WindowStatisticsLineChart(String windowTitle,
                                     String chartTitle,
                                     Statistics stats,
                                     Integer firstStep,
                                     Integer lastStep)
    {
        super(windowTitle);
        
        this.initialConfig();

        populateChart(chartTitle, stats, firstStep, lastStep);
    }

    private void initialConfig()
    {

        this.setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);

    }

    private void populateChart(String chartTitle,
                               Statistics stats,
                               Integer firstStep,
                               Integer lastStep)
    {
        DefaultCategoryDataset dataset = createDataset(stats, firstStep, lastStep);

        JFreeChart lineChart = ChartFactory.createLineChart(
            chartTitle, "Paso", "Cantidad", dataset,
            PlotOrientation.VERTICAL, true, true, false);

        CategoryPlot plot = (CategoryPlot) lineChart.getPlot();

        int stateCount = 0;
        for (State state : stats.getStates())
        {
            plot.getRenderer().setSeriesPaint(stateCount, state.getColor());
            stateCount++;
        }

        ChartPanel chartPanel = new ChartPanel(lineChart);
        chartPanel.setPreferredSize(new java.awt.Dimension(1200, 768));
        setContentPane(chartPanel);
    }

    /**
     * Carga los datos de las estadisticas en el dataset para mostrar.
     * @param stats
     * @param firstStep
     * @param lastStep
     * @return 
     */
    private DefaultCategoryDataset createDataset(Statistics stats, Integer firstStep, Integer lastStep)
    {
        DefaultCategoryDataset dataset = new DefaultCategoryDataset();
        int stateNumber = stats.getStates().size();

        for (Integer stepIndex = firstStep; stepIndex <= lastStep; stepIndex++)
        {
            for (Integer dataIndex = 0; dataIndex < stateNumber; dataIndex++)
            {
                Integer stateData = stats.getData().get(stepIndex)[dataIndex];
                String stateText = stats.getStates().get(dataIndex).getText();

                dataset.addValue(stateData, stateText, stepIndex.toString());
            }
        }
        return dataset;
    }
}
