/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package luz.ve.ssacc.test;

import luz.ve.ssacc.engine.components.Automaton;
import luz.ve.ssacc.engine.components.Cell;
import luz.ve.ssacc.engine.components.Grid;
import luz.ve.ssacc.engine.components.State;

/**
 *
 * @author renzo
 */
public class TestPatterns
{

    /**
     * Funcion simple que rota el patrón 90º contra reloj tantas veces como lo indique
     * la variable rotate (0=0, 1=90, 2=180; 3=270; 4=360)
     * A simple function that rotates the pattern 90º counter-clockwise as many times as
     * indicated by the rotate variable
     * @param coordinatesList
     * @param rotate
     * @return
     */
    public static Cell[] rotateX90(Cell[] coordinatesList, int rotate)
    {
        rotate = rotate % 4;  // to reduce loop to minimun if user wants to make it a random rotation
        
        if (rotate == 0) //Do nothing condition
        {
            return coordinatesList;
        }
             
        Cell[] tempList = new Cell[coordinatesList.length]; 
        
        System.arraycopy(coordinatesList, 0, tempList, 0, coordinatesList.length);
        
        for (int s = 0; s < rotate; s++)
        {
            {
                for (int i = 0; i < coordinatesList.length; i++)
                {
                    Cell tempCell = new Cell(tempList[i].getColumn() * (-1), tempList[i].getRow(),tempList[i].getStateId());
                    coordinatesList[i] = tempCell;
                }
            }
            System.arraycopy(coordinatesList, 0, tempList, 0, coordinatesList.length);
        }

        return tempList;
    }

    /**
     * Limpia un area del grid del tamaño indicado en el Grid de Inicio
     *
     * @param ca
     * @param origen
     * @param height
     * @param width
     */
    public static void clearArea(Automaton ca, Cell origen, int height, int width)
    {
        int newX;
        int newY;

        for (int x = 0; x < width; x++)
        {
            for (int y = 0; y < height; y++)
            {
                newY = y + origen.getRow();
                newX = x + origen.getColumn();

                if (newX < ca.getGrids().getColumns() && newY < ca.getGrids().getRows())//Me aseguro de no salirme de los limites del grid
                {
                    ca.getGrids().getGridIni().getArray()[newY][newX] = ca.getGrids().getBackground().getId();
                }
            }
        }
    }

    public static void drawPattern(Grid grid, Cell origen, Cell[] coordinatesList, State patternState)
    {
        int newX;
        int newY;
                
        for (Cell coordinate : coordinatesList) // Grafica el patrón en el grid
        {
            newX = coordinate.getColumn() + origen.getColumn();
            newY = coordinate.getRow() + origen.getRow();

            if ( newX>=0 && newX < grid.getColumns() && newY>=0 && newY < grid.getRows() ) //Me aseguro de no salirme de los limites del grid
            {
                grid.getArray()[newY][newX] = patternState.getId();
            }
            
        }
    }
    
    
    
    
    /**
     * Hardcoded glider del juego de la vida
     * Configuración sin rotación es Sur-Este
     *
     * @param ca //Contiene toda la información del automata celular
     * @param origen //Donde en el grid se ubicará el patron
     * @param patternState //Estado con el que se graficara el patrón
     * @param rotate//Variable que determina cuanto se rotará el patrón
     * @param clearSpace //boolean que indica si el automata al ser escrito en
     * el grid limpiará su espacio completo. Se recomienda que sea verdadero
     * @return grid
    */
    public static Grid glider(Automaton ca, 
                              Cell origen, 
                              State patternState, 
                              int rotate, 
                              boolean clearSpace)
    {
        int patternHeight = 3; // alto de la bounding box
        int patternWidth = 3; // ancho de la bounding box
        int numActiveStates = 5; // cuantos estados activos tiene el patrón

        Cell[] coordinatesList = new Cell[numActiveStates]; //Establece el patrón

        coordinatesList[0] = new Cell(0, 1, origen.getStateId());
        coordinatesList[1] = new Cell(1, 2, origen.getStateId());
        coordinatesList[2] = new Cell(2, 0, origen.getStateId());
        coordinatesList[3] = new Cell(2, 1, origen.getStateId());
        coordinatesList[4] = new Cell(2, 2, origen.getStateId());

        coordinatesList = rotateX90(coordinatesList, rotate);

        if (clearSpace)// limpia area del patrón
        {
            clearArea(ca, origen, patternHeight, patternWidth);
        }
        
        drawPattern(ca.getGrids().getGridIni(), origen, coordinatesList, patternState);

        return ca.getGrids().getGridIni();
    }

    public static Grid eater1(
        Automaton ca,
        Cell origen,
        State patternState,
        int rotate,
        boolean clearSpace)
    {
        int patternHeight = 4; // alto de la bounding box
        int patternWidth = 4; // ancho de la bounding box
        int numActiveStates = 7; // cuantos estados activos tiene el patrón

        Cell[] coordinatesList = new Cell[numActiveStates]; //Establece el patrón

        coordinatesList[0] = new Cell(0, 0, patternState.getId());
        coordinatesList[1] = new Cell(0, 1, patternState.getId());
        coordinatesList[2] = new Cell(1, 0, patternState.getId());
        coordinatesList[3] = new Cell(1, 2, patternState.getId());
        coordinatesList[4] = new Cell(2, 2, patternState.getId());
        coordinatesList[5] = new Cell(3, 2, patternState.getId());
        coordinatesList[6] = new Cell(3, 3, patternState.getId());

        coordinatesList = rotateX90(coordinatesList, rotate);

        if (clearSpace)// limpia area del patrón
        {
            clearArea(ca, origen, patternHeight, patternWidth);
        }

        drawPattern(ca.getGrids().getGridIni(), origen, coordinatesList, patternState);
        return ca.getGrids().getGridIni();
    }


//*****************************************************************************    
//*****************************************************************************
//*****************************************************************************    
}
