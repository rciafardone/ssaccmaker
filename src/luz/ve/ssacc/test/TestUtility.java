/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package luz.ve.ssacc.test;

import luz.ve.ssacc.engine.components.Grids;
import luz.ve.ssacc.engine.components.Cell;
import luz.ve.ssacc.engine.components.Automaton;
import luz.ve.ssacc.engine.components.Grid;
import luz.ve.ssacc.engine.components.State;
import luz.ve.ssacc.engine.components.Neighborhood;
import luz.ve.ssacc.engine.components.States;
import java.awt.Color;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.util.Random;
import luz.ve.ssacc.maker.gui.extras.CanvasAnimationThreadPanel;



/**
 * Clase donde se crean funciones para pruebas
 * @author Renzo Ciafardone Sciannelli
 */
public class TestUtility
{

    private static final int MAX = 255; 
    private static final int MIN = 0; 
    private static final int PERIOD = 50;
    private static int STABILIZER = 0;
    private static int QU = 0; 
   
public static String GOLComponentsHardcodedAsString()
    {
        String eol = System.getProperty("line.separator");
        StringBuilder sGOL = new StringBuilder();

        sGOL.append("@CAN=(Game of Life - ANIMATOR);");
        sGOL.append(eol);
        sGOL.append("@CAI=(0011);");
        sGOL.append(eol);
        sGOL.append("@CAD=(A cellular automaton of Conway Game of life hardcoded for test);");
        sGOL.append(eol);
        sGOL.append("@StatesBlockIni=(Game of Life - ANIMATOR);");
        sGOL.append(eol);
        sGOL.append("@StatesDescription=(Estos son los estados del juego de la vida: Vivo y Muerto.);");
        sGOL.append(eol);
        sGOL.append("@SC(0000)=(txt(dead))(val(0.0))(col(100:100:120));");
        sGOL.append(eol);
        sGOL.append("@SC(0001)=(txt(alive))(val(0.0))(col(255:255:255));");
        sGOL.append(eol);
        sGOL.append("@StatesBlockEnd;");
        sGOL.append(eol);
        sGOL.append("@RulesBlockIni=(Game of Life - ANIMATOR);");
        sGOL.append(eol);
        sGOL.append("@RulesDescription=(This is the game of life \"TEST\" hardcoded version - for test only, doh);");
        sGOL.append(eol);
        sGOL.append("@Rule(001)=(0001>>0000);");
        sGOL.append(eol);
        sGOL.append("@?OPN(FUN(Count):SC(0001)=(txt(alive))(val(0.0))(col(255:255:255)):FIELD(0))OPX(<)OPN(FUN(Constant):SC(0002)=(txt(DEFAULT))(val(2.0))(col(255:255:255)):FIELD(2));");
        sGOL.append(eol);
        sGOL.append("@Rule(001);");
        sGOL.append(eol);
        sGOL.append("@Rule(002)=(0001>>0001);");
        sGOL.append(eol);
        sGOL.append("@?OPN(FUN(Count):SC(0001)=(txt(alive))(val(0.0))(col(255:255:255)):FIELD(0))OPX(==)OPN(FUN(Constant):SC(0002)=(txt(DEFAULT))(val(2.0))(col(255:255:255)):FIELD(2));");
        sGOL.append(eol);
        sGOL.append("@Rule(002);");
        sGOL.append(eol);
        sGOL.append("@Rule(003)=(0001>>0001);");
        sGOL.append(eol);
        sGOL.append("@?OPN(FUN(Count):SC(0001)=(txt(alive))(val(0.0))(col(255:255:255)):FIELD(0))OPX(==)OPN(FUN(Constant):SC(0003)=(txt(DEFAULT))(val(3.0))(col(255:255:255)):FIELD(2));");
        sGOL.append(eol);
        sGOL.append("@Rule(003);");
        sGOL.append(eol);
        sGOL.append("@Rule(004)=(0001>>0000);");
        sGOL.append(eol);
        sGOL.append("@?OPN(FUN(Count):SC(0001)=(txt(alive))(val(0.0))(col(255:255:255)):FIELD(0))OPX(>)OPN(FUN(Constant):SC(0003)=(txt(DEFAULT))(val(3.0))(col(255:255:255)):FIELD(2));");
        sGOL.append(eol);
        sGOL.append("@Rule(004);");
        sGOL.append(eol);
        sGOL.append("@Rule(005)=(0000>>0001);");
        sGOL.append(eol);
        sGOL.append("@?OPN(FUN(Count):SC(0001)=(txt(alive))(val(0.0))(col(255:255:255)):FIELD(0))OPX(==)OPN(FUN(Constant):SC(0003)=(txt(DEFAULT))(val(3.0))(col(255:255:255)):FIELD(2));");
        sGOL.append(eol);
        sGOL.append("@Rule(005);");
        sGOL.append(eol);
        sGOL.append("@RulesBlockEnd;");
        sGOL.append(eol);
        sGOL.append("@NeighborhoodBlockIni=(Moore);");
        sGOL.append(eol);
        sGOL.append("@NeighborhoodDescription=(Este es el vecindario de moore);");
        sGOL.append(eol);
        sGOL.append("@NeighborhoodPatternRadix=[(0:0)(SC(0000)=(txt(DEFAULT))(val(0.0))(col(255:000:000)))];");
        sGOL.append(eol);
        sGOL.append("@NeighborhoodIncludesRadix=(false);");
        sGOL.append(eol);
        sGOL.append("@NeighborhoodRadius=(1);");
        sGOL.append(eol);
        sGOL.append("@NeighborhoodBasePattern=" + "(-1:-1)(-1:0)(-1:1)" + "(0:-1)(0:0)(0:1)" + "(1:-1)(1:0)(1:1);");
        sGOL.append(eol);
        sGOL.append("@NeighborhoodBlockEnd;");
        sGOL.append(eol);
        sGOL.append("@GridConfigurationIni=(Some Grid - ANIMATOR);");
        sGOL.append(eol);
        sGOL.append("@GridDescription=(Some Grid Description - ANIMATOR);");
        sGOL.append(eol);
        sGOL.append("@GridBackgroundState=(SC(0000)=(txt(dead))(val(0.0))(col(100:100:120)));");
        sGOL.append(eol);
        sGOL.append("@GridFrontierKind=(1);");
        sGOL.append(eol);
        sGOL.append("@GridNumberSteps=(50000);");
        sGOL.append(eol);
        sGOL.append("@GridAnimationDelay=(25);");
        sGOL.append(eol);
        sGOL.append("@GridArrayWidth=(100);");
        sGOL.append(eol);
        sGOL.append("@GridArrayHeight=(100);");
        sGOL.append(eol);
        sGOL.append("@GridCellSize=(6);");
        sGOL.append(eol);
        sGOL.append("@GridDrawBorder=(true);");
        sGOL.append(eol);
        sGOL.append("@GridBorderColor=(120:120:120);");
        sGOL.append(eol);
        sGOL.append("@GridConfigurationEnd;");
        sGOL.append("@CAE;");

        return sGOL.toString();
    }

    public static String TestGridHardcodedAsString()
    {
        String eol = System.getProperty("line.separator");
        StringBuilder sGOL = new StringBuilder();

        sGOL.append("@GridInitialState(60:30)=");
        sGOL.append(eol);
        sGOL.append("[(0:0)(SC(0001)=(txt(Vivo))(val(1.0))(col(000:153:204)))]");
        sGOL.append(eol);
        sGOL.append("[(0:1)(SC(0000)=(txt(Muerto))(val(1.0))(col(000:153:204)))]");
        sGOL.append(eol);
        sGOL.append("[(1:0)(SC(0000)=(txt(Muerto))(val(1.0))(col(000:153:204)))]");
        sGOL.append(eol);
        sGOL.append("[(1:1)(SC(0001)=(txt(Vivo))(val(1.0))(col(000:153:204)))]");
        sGOL.append(eol);
        sGOL.append(";");
        sGOL.append(eol);
        sGOL.append("@GridInitialState;");
        
        return sGOL.toString();
    }



    /**
     *
     * @param startPoint
     * @param grid
     * @param fillerState
     * @param areaHeight
     * @param areaWidth
     * @return
     */
    public static Grid fillArea(Cell startPoint, Grid grid, State fillerState, int areaHeight, int areaWidth)
    {
        int yIni = startPoint.getRow();
        int xIni = startPoint.getColumn();
        int yMax = yIni + areaHeight;
        int xMax = xIni + areaWidth;

        for (int y = yIni; y < yMax; y++)
        {
            for (int x = xIni; x < xMax; x++)
            {
                grid.getArray()[y][x] = fillerState.getId();
            }
        }
        return grid;
    }

    /**
     * Reemplaza el contenido del grid llenandolo de manera aleatoria.
     * @param grid
     * @param statesList
     * @return 
     */
    public static Grid populateGridAtRandom(Grid grid, States statesList)
    {
        grid = new Grid(grid.getRows(), grid.getColumns(), pickRandomState(statesList));
        return grid;
    }

    public static Grid addSomeStatesAtRandom(Grid grid, States statesList, int quantity)
    {
        Random randomGenerator = new Random();
       
        int randomState;
        int randomRow;
        int randomCol;
        
        for(int i=0; i<quantity;i++)
        {
            randomState = randomGenerator.nextInt(statesList.getList().size());
            randomRow = randomGenerator.nextInt(grid.getRows());
            randomCol = randomGenerator.nextInt(grid.getColumns());
            
            grid.getArray()[randomRow][randomCol] = statesList.getState((short)randomState).getId();
        }
        
        return grid;
    }
    
    
    
    /**
     * Selecciona un estado al azar de la lista de estados proporcionada
     *
     * @param statesList
     * @return
     */
    public static State pickRandomState(States statesList)
    {
        Random randomGenerator = new Random();
        int a = randomGenerator.nextInt(statesList.getList().size());
        return statesList.getState((short)a);
    }

  
  /**
   * Crea un color al azar dentro de los rangos establecidos
   * @param redMin
   * @param redMax
   * @param greenMin
   * @param greenMax
   * @param blueMin
   * @param blueMax
   * @return 
   */
    public static Color getRandomColor(int redMin, int redMax, int greenMin, int greenMax, int blueMin, int blueMax)
    {
        Random randomGenerator = new Random();
        
        int randomNumberR;
        int randomNumberG;
        int randomNumberB;

        randomNumberR = randomGenerator.nextInt(redMax - redMin) + redMin;
        randomNumberG = randomGenerator.nextInt(greenMax - greenMin) + greenMin;
        randomNumberB = randomGenerator.nextInt(blueMax - blueMin) + blueMin;

        Color color = new Color(randomNumberR, randomNumberG, randomNumberB);

        return color;
    }

    

   
    public static String printCellNeighborhoodStates(
                                                     Cell cell,
                                                     Automaton automata)
    {
        String eol = System.getProperty("line.separator");
        StringBuilder temp = new StringBuilder();
        temp.append("These are the States in Cell (").append(cell.getRow()).append(":").append(cell.getColumn()).append(") Neighborhood:").append(eol);
        automata.getNeighborhood().getCellsList(cell).stream().forEach((currentCell) ->
        {
            temp.append("( ").append(automata.getGrids().getStateId(currentCell)).append(" )");
        });
        return temp.toString();
    }

    /**
     * Crea un conjunto de estados apartir de una lista suministrada de manera
     * aleatoria
     *
     * @param currentSet
     * @param setSize
     * @param statesList
     */
    public static void createRandomSetOfStates(States currentSet, int setSize,
            States statesList)
    {
        for (int i = 0; i < setSize; i++)
        {
            currentSet.add(TestUtility.pickRandomState(statesList));
        }
    }

    public static Cell pickRandomCell(Grids grids)
    {
        Random randomGenerator = new Random();
        
        int rRow;
        int rCol;
        Short stateId;
        
        rRow = randomGenerator.nextInt(grids.getGridIni().getRows());
        rCol = randomGenerator.nextInt(grids.getGridIni().getColumns());
        stateId = grids.getGridIni().getStateId(rRow, rCol);
        
        return new Cell(rRow, rCol, stateId);
    }

    /**
     * Vecindario de moore con radio 1, raiz excluida
     * @return
     */
    public static Neighborhood hardCodedMooreNeighborhood()
    {
        Neighborhood moore = new Neighborhood("Moore", Neighborhood.DEFAULT_RADIX, false);
        moore.addCell(-1, -1, (short)0);
        moore.addCell(-1, 0, (short)0);
        moore.addCell(-1, +1, (short)0);
        moore.addCell(0, -1, (short)0);
        moore.addCell(0, 0, (short)0);
        moore.addCell(0, +1, (short)0);
        moore.addCell(+1, -1, (short)0);
        moore.addCell(+1, 0, (short)0);
        moore.addCell(+1, +1, (short)0);
        return moore;
    }

 
    /**
     * Genera una matriz aleatoria de colores para dibujar en el area
     * @param canvas
     * @param currentAc
*/
    public static void prepareRandomGrid(Graphics canvas, Automaton currentAc)
    {
        Graphics2D grid2DCanvas = (Graphics2D) canvas;
        Random randomGen = new Random(System.currentTimeMillis());
        if (STABILIZER == PERIOD)
        {
            Random randomQ = new Random(System.currentTimeMillis());
            QU = randomQ.nextInt(7);
            STABILIZER = 0;
        }
        else
        {
            STABILIZER++;
        }
        Color cellColor;
        int gWidth = currentAc.getGrids().getColumns(); //gridData.getGridColumns();
        int gHeight = currentAc.getGrids().getRows(); //gridData.getGridRows();
        for (int gX = 0; gX < gWidth; gX++)
        {
            for (int gY = 0; gY < gHeight; gY++)
            {
                cellColor = pickRandomColor(randomGen, TestUtility.QU, TestUtility.MIN, TestUtility.MAX);
                CanvasAnimationThreadPanel.drawCell(grid2DCanvas, currentAc, gX, gY, cellColor);
            }
        }
    }

    /**
     * Funcion para pruebas de color de la animacion, FOR TEST ONLY
     *
     * @param randomGen
     * @param q
     * @param min
     * @param max
     * @return
     */
    public static Color pickRandomColor(Random randomGen, int q, int min,
                                        int max)
    {
        int nMax = max - min + 1;
        int red = 0;
        int green = 0;
        int blue = 0;
        if (nMax < 0)
        {
            nMax = 0;
        }
        if (nMax > 256)
        {
            nMax = 256;
        }
        switch (q)
        {
            case 0:
                {
                    red = randomGen.nextInt(nMax) + min;
                    green = randomGen.nextInt(nMax) + min;
                    blue = randomGen.nextInt(nMax) + min;
                    break;
                }
            case 1:
                {
                    red = 0;
                    green = randomGen.nextInt(nMax) + min;
                    blue = randomGen.nextInt(nMax) + min;
                    break;
                }
            case 2:
                {
                    red = randomGen.nextInt(nMax) + min;
                    green = 0;
                    blue = randomGen.nextInt(nMax) + min;
                    break;
                }
            case 3:
                {
                    red = randomGen.nextInt(nMax) + min;
                    green = randomGen.nextInt(nMax) + min;
                    blue = 0;
                    break;
                }
            case 4:
                {
                    red = 0;
                    green = 0;
                    blue = randomGen.nextInt(nMax) + min;
                    break;
                }
            case 5:
                {
                    red = 0;
                    green = randomGen.nextInt(nMax) + min;
                    blue = 0;
                    break;
                }
            case 6:
                {
                    red = randomGen.nextInt(nMax) + min;
                    green = 0;
                    blue = 0;
                    break;
                }
        }
        return new Color(red, green, blue);
    }
    

    
}
